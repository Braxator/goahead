﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoAhead.FPGA;

namespace GoAhead.Objects
{
    class PRLinkManager : IEnumerable<PRLink>
    {
        private PRLinkManager()
        {
        }

        public void Add(PRLink prLink)
        {
            this.m_prLinks.Add(prLink);
        }        

        public static PRLinkManager Instance = new PRLinkManager();
              
        public IEnumerator<PRLink> GetEnumerator()
        {
            return this.m_prLinks.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private List<PRLink> m_prLinks = new List<PRLink>();
    }
    public class PRLink
    {
        public PRLink(Tile t, string netName)
        {
            this.Tile = t;
            this.Ports = new List<Port>();
            this.NetName = netName;
        }

        public void Add(Port port)
        {
            this.Ports.Add(port);
        }

        public readonly Tile Tile;
        public readonly List<Port> Ports;
        public readonly string NetName;
    }
    
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using GoAhead.Commands;

namespace GoAhead.Objects
{
    public class AliasManager
    {
        private AliasManager()
        {
        }

        public static AliasManager Instance = new AliasManager();

        public void AddAlias(String aliasName, String commands)
        {
            bool nameConflict = CommandStringParser.GetAllCommandTypes().Where(t => t.Name.Equals(aliasName)).Any();
            if (nameConflict)
            {
                throw new ArgumentException(aliasName + " is an invalid alias name as it overwrites an existing command name");
            }

            this.m_aliases[aliasName] = commands;
        }

        public String GetCommand(String aliasName)
        {
            String aliasNameWithoutSemicolon = aliasName.EndsWith(";") ? aliasName.Substring(0, aliasName.Length - 1) : aliasName;
            return this.m_aliases[aliasNameWithoutSemicolon];
        }

        public bool HasAlias(String aliasName)
        {
            String aliasNameWithoutSemicolon = aliasName.EndsWith(";") ? aliasName.Substring(0, aliasName.Length - 1) : aliasName;
            return this.m_aliases.ContainsKey(aliasNameWithoutSemicolon);
        }

        private Dictionary<String, String> m_aliases = new Dictionary<String, String>();
    }
}
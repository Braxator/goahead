﻿using System;
using GoAhead.Commands;

namespace GoAhead.Objects
{
    public class CommandStackManager
    {
        private CommandStackManager()
        {
        }

        public static CommandStackManager Instance = new CommandStackManager();

        public String Up()
        {
            return Instance.MoveInCommandStack(-1);
        }

        public String Down()
        {
            return Instance.MoveInCommandStack(1);
        }

        /// <summary>
        /// update index to point to last command
        /// </summary>
        public void Execute()
        {
            this.m_currentInputIndex = CommandExecuter.Instance.CommandCount - 1;
        }

        private String MoveInCommandStack(int incr)
        {
            if (this.m_currentInputIndex < CommandExecuter.Instance.CommandCount)
            {
                String storedInput = CommandExecuter.Instance.GetCommandString(this.m_currentInputIndex);

                this.m_currentInputIndex += incr;

                // wrap around
                if (this.m_currentInputIndex < 0)
                {
                    this.m_currentInputIndex = CommandExecuter.Instance.CommandCount - 1;
                }
                if (this.m_currentInputIndex >= CommandExecuter.Instance.CommandCount)
                {
                    this.m_currentInputIndex = 0;
                }

                return storedInput;
            }
            else
            {
                return "";
            }
        }

        private int m_currentInputIndex = 0;
    }
}
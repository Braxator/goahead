using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using GoAhead.Commands.Debug;

namespace GoAhead.Objects
{
    public class NetlistContainerManager
    {
        private NetlistContainerManager()
        {
            GoAhead.Commands.Debug.PrintGoAheadInternals.ObjectsToPrint.Add(this);
        }

        public void Add(NetlistContainer netlistContainer)
        {
            if (this.Contains(netlistContainer.Name))
            {
                throw new ArgumentException(this + ": A netlist container with the name " + netlistContainer.Name + " has already been added");
            }
            this.m_netlistContainer.Add(netlistContainer);
        }

        public String GetName()
        {
            int index = 0;
            while (this.Contains("temp_" + index))
            {
                index++;
            }
            return "temp_" + index;
        }

        public bool Contains(String netlistContainerName)
        {
            NetlistContainer probe = this.m_netlistContainer.FirstOrDefault(macro => macro.Name.Equals(netlistContainerName));

            return probe != null;
        }

        public NetlistContainer Get(String netlistContainerName)
        {
            NetlistContainer netlistContainer = this.m_netlistContainer.FirstOrDefault(m => m.Name.Equals(netlistContainerName));
            if (netlistContainer == null)
            {
                throw new ArgumentException("A netlist container with the name " + netlistContainerName + " has not been added");
            }

            return netlistContainer;
        }

        public IEnumerable<NetlistContainer> NetlistContainer
        {
            get { return this.m_netlistContainer; }
        }

        public void Reset()
        {
            this.m_netlistContainer.Clear();
        }

        public BindingList<NetlistContainer> NetlistContainerBindingList
        {
            get { return this.m_netlistContainer; }
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();

            if (this.m_netlistContainer.Count == 0)
            {
                result.AppendLine("No netlist container added");
            }
            else
            {
                result.AppendLine("Added netlist container are");
                foreach (NetlistContainer m in this.NetlistContainer)
                {
                    PrintStatistics printCmd = new PrintStatistics();
                    printCmd.NetlistContainerName = m.Name;
                    result.AppendLine(m.Name + " (use " + printCmd.ToString() + " for more information)");
                }
            }
            return result.ToString();
        }

        /// <summary>
        /// The class MacroManager is a Singelton
        /// </summary>
        public static NetlistContainerManager Instance = new NetlistContainerManager();

        public const String DefaultNetlistContainerName = "default_netlist_container";

        private BindingList<NetlistContainer> m_netlistContainer = new BindingList<NetlistContainer>();
    }
}
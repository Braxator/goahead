﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using GoAhead.Commands.Identifier;
using GoAhead.FPGA;
using GoAhead.Interfaces;

namespace GoAhead.Objects
{
    public class BRAMDSPSetting
    {
        public BRAMDSPSetting(int width, int height, bool leftRightHandling, String buttomLeft, String buttomRight)
        {
            this.Width = width;
            this.Heigth = height;
            this.LeftRightHandling = leftRightHandling;
            this.ButtomLeft = buttomLeft;
            this.ButtomRight = buttomRight;
        }

        public readonly int Width;
        public readonly int Heigth;
        public readonly bool LeftRightHandling;
        public readonly String ButtomLeft;
        public readonly String ButtomRight;
    }

    public class BRAMDSPSettingsManager
    {
        public void SetBRAMParameters(String family, int width, int height, bool leftRightHandling, String buttomLeft, String buttomRight)
        {
            this.m_bram[family] = new BRAMDSPSetting(width, height, leftRightHandling, buttomLeft, buttomRight);
        }

        public void SetDSPParameters(String family, int width, int height, bool leftRightHandling, String buttomLeft, String buttomRight)
        {
            this.m_dsp[family] = new BRAMDSPSetting(width, height, leftRightHandling, buttomLeft, buttomRight);
        }

        /// <summary>
        /// IdentifierManager is a singelton
        /// </summary>
        public static BRAMDSPSettingsManager Instance = new BRAMDSPSettingsManager();

        public BRAMDSPSetting GetBRAMSetting()
        {
            String key = FPGA.FPGA.Instance.Family.ToString();
            return this.m_bram[key];
        }

        public BRAMDSPSetting GetDSPSetting()
        {
            String key = FPGA.FPGA.Instance.Family.ToString();
            return this.m_dsp[key];
        }

        public void GetBRAMWidthAndHeight(out int width, out int height)
        {
            String key = FPGA.FPGA.Instance.Family.ToString();
            if (!this.m_bram.ContainsKey(key))
            {
                width = 0;
                height = 0;
            }
            else
            {
                width = this.m_bram[key].Width;
                height = this.m_bram[key].Heigth;
            }
        }

        private Dictionary<String, BRAMDSPSetting> m_bram = new Dictionary<String, BRAMDSPSetting>();
        private Dictionary<String, BRAMDSPSetting> m_dsp = new Dictionary<String, BRAMDSPSetting>();
    }

    public class LineParameter
    {
        public LineParameter(LineManager.Orienation orientation, int offset, String tileIdentifierRegexp)
        {
            this.m_orientation = orientation;
            this.m_offset = offset;
            this.m_tileIdentifierRegexp = new Regex(tileIdentifierRegexp, RegexOptions.Compiled);
        }

        public LineManager.Orienation Orientation
        {
            get { return m_orientation; }
        }

        public int Offset
        {
            get { return m_offset; }
        }

        public Regex TileIdentifierRegexp
        {
            get { return m_tileIdentifierRegexp; }
        }

        private readonly LineManager.Orienation m_orientation;
        private readonly int m_offset;
        private readonly Regex m_tileIdentifierRegexp;
    }

    public class LineManager
    {
        public enum Orienation { Undefined, Vertical, Horizontal }

        private LineManager()
        {
        }

        public static LineManager Instance = new LineManager();

        public void AddSetting(String family, Orienation orientation, int offset, String tileIdentifierRegexp)
        {
            if (!this.m_parameter.ContainsKey(family))
            {
                this.m_parameter.Add(family, new List<LineParameter>());
            }

            LineParameter lp = new LineParameter(orientation, offset, tileIdentifierRegexp);

            this.m_parameter[family].Add(lp);
        }

        public IEnumerable<LineParameter> GetLineParameter()
        {
            String key = FPGA.FPGA.Instance.Family.ToString();
            if (this.m_parameter.ContainsKey(key))
            {
                foreach (LineParameter lp in this.m_parameter[key])
                {
                    yield return lp;
                }
            }
        }

        private Dictionary<String, List<LineParameter>> m_parameter = new Dictionary<String, List<LineParameter>>();
    }

    public class IdentifierManager : IResetable
    {
        public enum RegexTypes { Unknown = 0, BRAMRegex = 1, DSPRegex = 2, CLBRegex = 3, InterconnectRegex = 4, SliceRegex = 5, ProhibitExcludeFilter = 6, VLineAnchor = 7, HLineAnchor = 8 }

        private IdentifierManager()
        {
            Commands.Reset.ObjectsToReset.Add(this);
        }

        public void Reset()
        {
            this.m_regularExpressionBuffer.Clear();
        }

        /// <summary>
        /// IdentifierManager is a singelton
        /// </summary>
        public static IdentifierManager Instance = new IdentifierManager();

        public void SetRegexp(RegexTypes type, String family, String regexp)
        {
            if (!this.m_regex.ContainsKey(type))
            {
                this.m_regex.Add(type, new Dictionary<String, String>());
            }
            this.m_regex[type][family] = regexp;
        }

        public String GetRegex(RegexTypes type)
        {
            if (FPGA.FPGA.Instance.Family.Equals(FPGA.FPGATypes.FPGAFamily.Undefined))
            {
                throw new ArgumentException("No FPGA loaded");
            }

            if (!this.m_regex.ContainsKey(type))
            {
                throw new ArgumentException("No Regexp of type " + type + " found. Use SetRegexp command. Are you missing init.goa?");
            }

            String familiy = FPGA.FPGA.Instance.Family.ToString();
            if (!this.m_regex[type].ContainsKey(familiy))
            {
                throw new ArgumentException("No Regexp of type " + type + " found for FPGA family " + familiy + ". Use SetRegexp. Are you missing init.goa?");
            }
            return this.m_regex[type][familiy];
        }

        public bool HasRegexp(RegexTypes type)
        {
            if (!this.m_regex.ContainsKey(type))
            {
                return false;
            }

            String familiy = FPGA.FPGA.Instance.Family.ToString();
            if (!this.m_regex[type].ContainsKey(familiy))
            {
                return false;
            }
            return true;
        }

        public String GetRegex(RegexTypes type1, RegexTypes type2)
        {
            String regexp1 = this.GetRegex(type1);
            String regexp2 = this.GetRegex(type2);

            return "(" + regexp1 + ")|(" + regexp2 + ")";
        }

        public bool IsMatch(String input, RegexTypes regexpType)
        {
            if (!this.m_regularExpressionBuffer.ContainsKey(regexpType))
            {
                String pattern = this.GetRegex(regexpType);
                Regex regex = new Regex(pattern, RegexOptions.Compiled);
                this.m_regularExpressionBuffer.Add(regexpType, regex);
            }

            return this.m_regularExpressionBuffer[regexpType].IsMatch(input);
        }

        public RegexTypes GetRegexpType(Tile t)
        {
            foreach (KeyValuePair<RegexTypes, Dictionary<String, String>> tupel in this.m_regex)
            {
                if (this.IsMatch(t.Location, tupel.Key))
                {
                    return tupel.Key;
                }
            }
            return RegexTypes.Unknown;
            //throw new ArgumentException("Could not find a resource type for " + t.Location);
        }

        private Dictionary<RegexTypes, Dictionary<String, String>> m_regex = new Dictionary<RegexTypes, Dictionary<String, String>>();
        private Dictionary<RegexTypes, Regex> m_regularExpressionBuffer = new Dictionary<RegexTypes, Regex>();
    }

    public class IdentifierPrefixManager
    {
        public static IdentifierPrefixManager Instance = new IdentifierPrefixManager();

        public List<String> Prefices
        {
            get { return m_prefices; }
            set { m_prefices = value; }
        }

        private List<String> m_prefices = new List<String>();
    }

    public class ColumnTypeNameManager
    {
        public static ColumnTypeNameManager Instance = new ColumnTypeNameManager();

        public string GetColumnTypeNameByResource(string resource, out SetColumnTypeNames addCmd)
        {
            if (resource.Contains("SLICEL"))
            {
            }

            addCmd = null;
            if (string.IsNullOrEmpty(resource))
            {
                return "EMPTY";
            }
            foreach (KeyValuePair<string, string> t in this.m_nameByResources)
            {
                string candidate = t.Value;
                while (resource.Length >= candidate.Length)
                {
                    if (resource.Equals(candidate))
                    {
                        return t.Key;
                    }
                    candidate += "," + t.Value;
                }
            }
            // nothing found -> define new type
            int i = 0;
            while (this.m_nameByResources.ContainsKey("unknown" + i))
            {
                i++;
            }
            // return how to set this unknown type name
            addCmd = new SetColumnTypeNames();
            addCmd.ColumnTypeName = "unknown" + i;
            addCmd.Resources = resource;


            this.AddTypeNameByResource("unknown" + i, resource);
            return "unknown" + i;
        }

        public void AddTypeNameByResource(string typeName, string resource)
        {
            if (this.m_nameByResources.ContainsKey(typeName))
            {
                throw new ArgumentException("Type name " + typeName + " already exists");
            }
            this.m_nameByResources.Add(typeName, resource);
        }

        private Dictionary<string, string> m_nameByResources = new Dictionary<string, string>();

    }

    public class SliceCompare
    {
        private SliceCompare()
        {
            //Commands.Reset.ObjectsToReset.Add(this);
        }

        public void Add(String familiy, String requiredSliceType, String possibleTargetSliceType)
        {
            if (!this.m_rules.ContainsKey(familiy))
            {
                this.m_rules.Add(familiy, new Dictionary<String, List<String>>());
            }

            if (!this.m_rules[familiy].ContainsKey(requiredSliceType))
            {
                this.m_rules[familiy].Add(requiredSliceType, new List<String>());
            }

            this.m_rules[familiy][requiredSliceType].Add(possibleTargetSliceType);
        }

        public bool Matches(String requiredSliceType, String possibleTargetSliceType)
        {
            if (FPGA.FPGA.Instance.Family.Equals(FPGA.FPGATypes.FPGAFamily.Undefined))
            {
                throw new ArgumentException("No FPGA loaded");
            }

            String familiy = FPGA.FPGA.Instance.Family.ToString();
            if (!this.m_rules.ContainsKey(familiy))
            {
                return false;
            }
            if (!this.m_rules[familiy].ContainsKey(requiredSliceType))
            {
                return false;
            }
            if (!this.m_rules[familiy][requiredSliceType].Contains(possibleTargetSliceType))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// IdentifierManager is a singelton
        /// </summary>
        public static SliceCompare Instance = new SliceCompare();

        private Dictionary<String, Dictionary<String, List<String>>> m_rules = new Dictionary<String, Dictionary<String, List<String>>>();
    }
}
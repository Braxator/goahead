﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using GoAhead.Code;
using GoAhead.Code.TCL;
using GoAhead.Code.XDL;
using GoAhead.Commands.VHDL;
using GoAhead.FPGA;

namespace GoAhead.Objects
{
    /// <summary>
    /// Singleton
    /// </summary>
    [Serializable]
    public class Library : Interfaces.IResetable
    {
        private Library()
        {
            GoAhead.Commands.Debug.PrintGoAheadInternals.ObjectsToPrint.Add(this);
        }

        public void Reset()
        {
            this.m_macros.Clear();
        }

        public static Library Instance = new Library();

        public LibraryElement GetElement(String elementName)
        {
            if (!this.Contains(elementName))
            {
                throw new ArgumentException("No libray element named " + elementName + " is part of the library");
            }

            return this.m_macros.First(macro => macro.Name.Equals(elementName));
        }

        public LibraryElement GetElement(String elementName, Tile tile, int sliceNumber)
        {
            if (!this.Contains(elementName, tile, sliceNumber))
            {
                throw new ArgumentException("There is no library element called " + elementName + " for the tile " + tile + " on slice number " + sliceNumber + " in the library.");
            }

            return this.m_macros.First(macro => macro.Name.Equals(elementName) && 
                                       tile.Location.StartsWith(macro.TileType) &&
                                       macro.SliceNumber == sliceNumber);
        }

        public bool Contains(String elementName)
        {
            return this.m_macros.FirstOrDefault(macro => macro.Name.Equals(elementName)) != null;
        }

        public bool Contains(String elementName, Tile tile, int sliceNumber)
        {
            return this.m_macros.FirstOrDefault(macro => macro.Name.Equals(elementName) && 
                                                tile.Location.StartsWith(macro.TileType) &&
                                                macro.SliceNumber == sliceNumber) != null;
        }

        public void Add(LibraryElement element)
        {
            // !! MY CHANGE !!
            // I commented the if-statement, because I want to add library elements with the same name (A6LUT for example), however they can be located in different slice number etc.

            // if the library already contains the library element, throw exception
            //if (this.Contains(element.Name))
            //{
            //    throw new ArgumentException("A macro named " + element.Name + " is already part of the library");
            //}

            // add the library element to the list
            this.m_macros.Add(element);
        }

        public void Remove(String elementName)
        {
            if (!this.Contains(elementName))
            {
                throw new ArgumentException("No macro named " + elementName + " is part of the library");
            }

            this.m_macros.Remove(this.m_macros.First(macro => macro.Name.Equals(elementName)));
        }

        public void Clear()
        {
            this.m_macros.Clear();
        }

        public IEnumerable<LibraryElement> GetAllElements()
        {
            foreach (LibraryElement el in this.m_macros)
            {
                yield return el;
            }
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            if (this.m_macros.Count == 0)
            {
                result.AppendLine("No library elements added");
            }
            else
            {
                result.AppendLine("Added library elements are");
                foreach (LibraryElement libElement in this.GetAllElements())
                {
                    result.AppendLine(libElement.ToString());
                }
            }
            return result.ToString();
        }

        public BindingList<LibraryElement> LibraryElements
        {
            get { return m_macros; }
        }

        private BindingList<LibraryElement> m_macros = new BindingList<LibraryElement>();
    }

    [Serializable]
    public class LibraryElement
    {
        public void ClearPortsToBlock()
        {
            this.m_portsToBlock.Clear();
        }

        public void Add(LibraryElement other)
        {
            this.m_others.Add(other);
        }

        public void AddPortToBlock(Tile where, Port port)
        {
            if (!this.m_portsToBlock.ContainsKey(where))
            {
                this.m_portsToBlock.Add(where, new List<String>());
            }

            // do not add duplicates
            if (!this.m_portsToBlock[where].Contains(port.Name))
            {
                this.m_portsToBlock[where].Add(port.Name);
            }
        }

        public bool IsRelocationPossible(Tile anchor, out StringBuilder errorList)
        {
            errorList = new StringBuilder();

            foreach (Instance inst in this.Containter.Instances)
            {
                String targetLocation = "";
                bool success = this.GetTargetLocation(inst.Location, anchor, out targetLocation);

                if (!success)
                {
                    errorList.Append("GoAhead failed to relocate " + inst.Location + " using anchor " + anchor.Location);
                    // debug only
                    success = this.GetTargetLocation(inst.Location, anchor, out targetLocation);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Return tupel of an XDLInstance and the tile where the instance will be placed based on the given placementAnchor
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Tuple<Instance, Tile>> GetInstanceTiles(Tile anchor, LibraryElement libElement) // anchor example; {X103Y138:CLBLL_L_X40Y17},  libElement example; A6LUT
        {
            if(FPGA.FPGA.Instance.BackendType == FPGATypes.BackendType.ISE || (FPGA.FPGA.Instance.BackendType == FPGATypes.BackendType.Vivado && !libElement.VivadoConnectionPrimitve))
            {
                foreach (Instance inst in this.Containter.Instances)
                {
                    String targetLocation = "";
                    bool success = this.GetTargetLocation(inst.Location, anchor, out targetLocation);

                    if (success)
                    {
                        Tile targetTile = FPGA.FPGA.Instance.GetTile(targetLocation);
                        yield return new Tuple<Instance, Tile>(inst, targetTile);
                    }
                    else
                    {
                        throw new ArgumentException("Could not relocate " + inst.Location + " from anchor " + anchor);
                    }
                }
            } 
            else if(FPGA.FPGA.Instance.BackendType == FPGATypes.BackendType.Vivado && libElement.VivadoConnectionPrimitve)
            {
                TCLInstance inst = new TCLInstance();
                inst.SliceNumber = this.SliceNumber; // example; 1
                inst.SliceName = anchor.Slices[this.SliceNumber].SliceName; // example; "SLICE_X61Y17" 
                inst.Location = anchor.Location; // example; "CLBLL_L_X40Y17"
                inst.LocationX = anchor.LocationX; // example; 40
                inst.LocationY = anchor.LocationY; // example; 17
                inst.SliceType = anchor.Slices[this.SliceNumber].SliceType; // example; "SLICEL"
                inst.Name = anchor.Location; // example; "CLBLL_L_X40Y17"
                inst.TileKey = anchor.TileKey; // example; {X103Y138}
                inst.OmitPlaceCommand = true;
                inst.BELType = libElement.PrimitiveName;
                Tuple<Instance, Tile> result = new Tuple<Instance, Tile>(inst, anchor);
                yield return result;
            }
            else
            {
                throw new ArgumentException("Unsupported branch in GetInstanceTiles");
            }             
        }

        public Shape ResourceShape
        {
            get { return this.m_resourceShape; }
            set { this.m_resourceShape = value; }
        }

        /// <summary>
        /// Relocate the tile given by instanceTileLocation from anchor
        /// instanceTileLocation does not have to be part of the FPGA
        /// </summary>
        /// <param name="instanceTileLocation"></param>
        /// <param name="anchor"></param>
        /// <returns></returns>
        ///
        public bool GetTargetLocation(String instanceTileLocation, Tile anchor, out String targetLocation)
        {
            targetLocation = "";

            int x, y;
            FPGATypes.GetXYFromIdentifier(instanceTileLocation, out x, out y);

            int locationIncrX = x - this.ResourceShape.Anchor.AnchorLocationX;
            int locationIncrY = y - this.ResourceShape.Anchor.AnchorLocationY;

            int targetLocationX = anchor.LocationX + locationIncrX;
            int targetLocationY = anchor.LocationY + locationIncrY;


            // three chars should match the type CLB/BRAM/DSP
            if (String.IsNullOrEmpty(instanceTileLocation) || instanceTileLocation.Length < 3)
            {
            }
            string prefix = instanceTileLocation.Substring(0, 3);
            Tile targetTile = FPGA.FPGA.Instance.GetAllTiles().FirstOrDefault(t => t.Location.StartsWith(prefix) && t.LocationX == targetLocationX && t.LocationY == targetLocationY);


            //int lastUnderScore = instanceTileLocation.LastIndexOf("_");
            //targetLocation += instanceTileLocation.Substring(0, lastUnderScore);
            //targetLocation += "_X" + targetLocationX + "Y" + targetLocationY;
            // naming fun
            //String resolvedTargetLocation = null;
            //bool success = FPGA.FPGATypes.ResolveLMIdentifier(targetLocation, str => !FPGA.FPGA.Instance.Contains(str), out resolvedTargetLocation);

            if (targetTile != null)
            {
                targetLocation = targetTile.Location;
                return true;
            }
            else
            {
                // relocated DSP INT_X3Y5 -> INT_BRAM_X3Y5 or INT_BRAM_BRK_X3Y5
                // move to init.goa
                Tile differentlyNamedTile = FPGA.FPGA.Instance.GetAllTiles().Where(t =>
                    ((t.Location.StartsWith("BRAM_") && FPGA.FPGA.Instance.Family.Equals(FPGATypes.FPGAFamily.Virtex6)) ||
                     (t.Location.StartsWith("INT_BRAM_") && FPGA.FPGA.Instance.Family.Equals(FPGATypes.FPGAFamily.Spartan6))) &&
                    t.LocationX == targetLocationX &&
                    t.LocationY == targetLocationY).FirstOrDefault();
                if (differentlyNamedTile != null)
                {
                    targetLocation = differentlyNamedTile.Location;
                    return true;
                }
                else
                {
                    targetLocation = "";
                    return false;
                }
                //throw new ArgumentException("Error during relocation: Can not relocate " + instanceTileLocation + " using anchor " + anchor.Location + ". Target " + targetLocation + " not found");
            }
        }
        /*
        /// <summary>
        /// Return tiles along with the ports to block on their original positions
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Tuple<Tile, List<String>>> GetPortsToBlock()
        {
            foreach (Tile macroTile in this.m_portsToBlock.Keys)
            {
                yield return new Tuple<Tile, List<String>>(macroTile, this.m_portsToBlock[macroTile]);
            }
        }
        */
        /// <summary>
        /// Return tiles along with the ports to block in the relocated positions
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Tuple<Tile, List<FPGA.Port>>> GetPortsToBlock(Tile anchor) // anchor example; {X103Y138:CLBLL_L_X40Y17}
        {
            if (FPGA.FPGA.Instance.BackendType == FPGATypes.BackendType.ISE)
            {
                foreach (Tile tileAtOriginalPosition in this.m_portsToBlock.Keys)
                {
                    String targetLocation = "";
                    bool success = this.GetTargetLocation(tileAtOriginalPosition.Location, anchor, out targetLocation);

                    if (success)
                    {
                        Tile targetTile = FPGA.FPGA.Instance.GetTile(targetLocation);

                        // adapt port names after relocation
                        List<Port> portsToBlock = new List<Port>();
                        foreach (String p in this.m_portsToBlock[tileAtOriginalPosition])
                        {
                            String resolvedPortName = p;
                            bool relocationSuccess = FPGA.FPGATypes.ResolveLMIdentifier(resolvedPortName, str => !targetTile.SwitchMatrix.Contains(str), out resolvedPortName);
                            if (!relocationSuccess)
                            {
                                throw new ArgumentException("Error during relocation: Can not relocate " + resolvedPortName + " to its new position from anchor " + anchor.Location);
                            }
                            portsToBlock.Add(new Port(resolvedPortName));
                        }
                        yield return new Tuple<Tile, List<Port>>(targetTile, portsToBlock);
                    }
                    else
                    {
                        success = this.GetTargetLocation(tileAtOriginalPosition.Location, anchor, out targetLocation);
                        throw new ArgumentException("Error during relocation: Can not relocate " + tileAtOriginalPosition.Location + " to its new position from anchor " + anchor.Location);
                    }
                }
            }
            else if (FPGA.FPGA.Instance.BackendType == FPGATypes.BackendType.Vivado)
            {
                // a connection macro can only be placed on CLB tiles
                if (!IdentifierManager.Instance.IsMatch(anchor.Location, IdentifierManager.RegexTypes.CLBRegex))
                {
                    throw new ArgumentException("Error during relocation: Can only place on CLBs");
                }

                // get the corresponding interconnect tile of the CLB
                Tile interconnect = FPGA.FPGATypes.GetInterconnectTile(anchor);

                // get the CLB Tile 
                Tile clbKey = this.m_portsToBlock.Keys.FirstOrDefault(t => IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.CLBRegex));

                // get the interconnect tile
                Tile intKey = this.m_portsToBlock.Keys.FirstOrDefault(t => IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.InterconnectRegex));

                // List<T>.ForEach(Action <T>) Method 
                // performs the specified action on each element of the List<T>

                if (clbKey != null)
                {
                    // create tuple with the anchor tile and all the blocked ports
                    Tuple<Tile, List<Port>> clbResult = new Tuple<Tile, List<Port>>(anchor, new List<Port>());

                    // add all the ports that must be blocked to the tuple
                    this.m_portsToBlock[clbKey].ForEach(s => clbResult.Item2.Add(new Port(s)));

                    yield return clbResult;
                }
                if (intKey != null)
                {
                    Tuple<Tile, List<Port>> intResult = new Tuple<Tile, List<Port>>(interconnect, new List<Port>());

                    this.m_portsToBlock[intKey].ForEach(s => intResult.Item2.Add(new Port(s)));

                    yield return intResult;
                }
            }
        }

        public override string ToString()
        {
            StringBuilder buffer = new StringBuilder();
            buffer.AppendLine("Name: " + this.Name);

            try
            {
                buffer.AppendLine(this.ResourceShape != null ? "ResourceShape: " + this.ResourceShape.ToString() : "No resource shape provided");
            }
            catch (NullReferenceException)
            {
                buffer.AppendLine("Warning: This binary libray element seems to be out of date. Regenerate it by reading in the XDL version with the command AddXDLLibraryElement");
            }

            PrintComponentDeclaration printCmd = new PrintComponentDeclaration();
            printCmd.LibraryElement = this.Name;
            printCmd.Do();
            buffer.AppendLine(printCmd.OutputManager.GetVHDLOuput());

            return buffer.ToString();
        }

        /// <summary>
        /// The unique nameof the library element
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// The name of the instantiated primitive
        /// </summary>
        public String PrimitiveName { get; set; }

        /// <summary>
        /// The Command with which this library element was added
        /// </summary>
        public String LoadCommand { get; set; }

        /// <summary>
        /// The VHDL generic map inserted at instantiation
        /// </summary>
        public String VHDLGenericMap { get; set; }

        /// <summary>
        /// The slice element (Vivado only)
        /// </summary>
        public String BEL { get; set; }

        /// <summary>
        /// The slice number (Vivado only)
        /// </summary>
        public int SliceNumber { get; set; }

        /// <summary>
        /// Whether or not this library element is a vivado connection primitive (Vivado only)
        /// </summary>
        public bool VivadoConnectionPrimitve { get; set; }

       
        public String TileType { get; set; }

        public String InputDirection { get; set; }

        public String OutputDirection { get; set; }

        /// <summary>
        /// The parsed in XDL Module/Design
        /// </summary>
        public NetlistContainer Containter { get; set; }

        public IEnumerable<LibraryElement> SubElements
        {
            get { return this.m_others; }
        }

        public String TunnelDirection { get; set; }

        private Shape m_resourceShape = new Shape();

        private Dictionary<Tile, List<String>> m_portsToBlock = new Dictionary<Tile, List<String>>();
        private List<LibraryElement> m_others = new List<LibraryElement>();
    }

    [Serializable]
    public class Shape
    {
        public void Add(String identifier)
        {
            this.m_containedTileIdentifier.Add(identifier);
        }

        public IEnumerable<String> GetContainedTileIdentifier()
        {
            return this.m_containedTileIdentifier;
        }

        /// <summary>
        /// The number of tiles in the module shape
        /// </summary>
        public int NumberOfTiles
        {
            get { return this.m_containedTileIdentifier.Count; }
        }

        public Anchor Anchor
        {
            get { return m_anchor; }
            set { m_anchor = value; }
        }

        public override string ToString()
        {
            StringBuilder buffer = new StringBuilder();

            buffer.AppendLine("ResourceShape");
            foreach (String s in this.GetContainedTileIdentifier())
            {
                buffer.AppendLine(s);
            }
            buffer.AppendLine("Anchor");
            buffer.AppendLine(this.m_anchor.ToString());

            return buffer.ToString();
        }

        private Anchor m_anchor = new Anchor();

        private List<String> m_containedTileIdentifier = new List<String>();
    }

    [Serializable]
    public class Anchor
    {
        public String AnchorSliceName { get; set; }
        public int AnchorSliceNumber { get; set; }
        public int AnchorLocationX { get; set; }
        public int AnchorLocationY { get; set; }
        public String AnchorTileLocation { get; set; }

        public override string ToString()
        {
            StringBuilder buffer = new StringBuilder();
            buffer.AppendLine("AnchorSliceName: " + this.AnchorSliceName);
            buffer.AppendLine("AnchorSliceNumber: " + this.AnchorSliceNumber);
            buffer.AppendLine("AnchorTileLocation: " + this.AnchorTileLocation);
            buffer.AppendLine("AnchorLocationX: " + this.AnchorLocationX);
            buffer.AppendLine("AnchorLocationY: " + this.AnchorLocationY);
            return buffer.ToString();
        }
    }
}
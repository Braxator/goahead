﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace GoAhead.Objects
{
    public class ExpressionParser
    {
        public bool Evaluate(String expression, out int result)
        {
            // string compare
            if (Regex.IsMatch(expression, @".*\D+.*=.*\D+.*"))
            {
                String[] atoms = expression.Split('=');
                result = atoms[0].Equals(atoms[1]) ? 1 : 0;
                return true;
            }

            Stack<String> operatorStack = new Stack<String>();
            // the epxression in reverse polish notation
            Queue<String> outQueue = new Queue<String>();

            int strPos = 0;
            char c = ' ';
            char lastC = ' ';
            bool negation = false;
            while (strPos < expression.Length)
            {
                String token = "";
                while (strPos < expression.Length)
                {
                    lastC = c;
                    c = expression[strPos];
                    if (c == ' ')
                    {
                        strPos++; // consume whitespace
                        break;
                    }
                    /*
                else if ((lastC == '(' && c == '-') || (strPos == 0 && c == '-') || (this.IsOperator(new String(lastC, 1)) && c == '-'))
                {
                    negation = true;
                    strPos++;
                }*/
                    else if (c == '(' || c == ')' || this.IsOperator(new String(c, 1)))
                    {
                        if (String.IsNullOrEmpty(token))
                        {
                            token += c;
                            strPos++;
                        }
                        break;
                    }
                    else
                    {
                        token += c;
                        strPos++;
                    }
                }

                if (this.IsNumber(token))
                {
                    outQueue.Enqueue((negation ? "-" : "") + token);
                    negation = false;
                }
                else if (this.IsOperator(token))
                {
                    /*    If the token is an operator, o1, then:
                            while there is an operator token, o2, at the top of the stack, and
                                    either o1 is left-associative and its precedence is less than or equal to that of o2,
                                    or o1 has precedence less than that of o2,
                                pop o2 off the stack, onto the output queue;
                            push o1 onto the stack.
                    */
                    while (operatorStack.Count > 0)
                    {
                        String lastOperator = operatorStack.Peek();

                        bool isOperator = this.IsOperator(lastOperator);
                        bool isLeftAssociativeAndCHasLEQPrecedence = this.IsLeftAssociative(token) && this.OperatorPrecedence(token) <= this.OperatorPrecedence(lastOperator);
                        bool smallerPrecedence = this.OperatorPrecedence(token) < this.OperatorPrecedence(lastOperator);
                        if (isOperator && (isLeftAssociativeAndCHasLEQPrecedence || smallerPrecedence))
                        {
                            outQueue.Enqueue(operatorStack.Pop());
                        }
                        else
                        {
                            break;
                        }
                    }
                    operatorStack.Push(token);
                }
                else if (token.Equals("("))
                {
                    operatorStack.Push(token);
                }
                else if (token.Equals(")"))
                {
                    /*
                        If the token is a right parenthesis:
                            Until the token at the top of the stack is a left parenthesis, pop operators off the stack onto the output queue.
                            Pop the left parenthesis from the stack, but not onto the output queue.
                            If the token at the top of the stack is a function token, pop it onto the output queue.
                            If the stack runs out without finding a left parenthesis, then there are mismatched parentheses.
                    */

                    while (operatorStack.Peek() != "(" && operatorStack.Count > 0)
                    {
                        outQueue.Enqueue(operatorStack.Pop());
                    }
                    if (operatorStack.Count == 0)
                    {
                        throw new ArgumentException("Missing ( in " + expression);
                    }
                    operatorStack.Pop();
                }
            }

            // 3 4 2 * 1 5 − 2 3 ^ ^ / +
            while (operatorStack.Count > 0)
            {
                outQueue.Enqueue(operatorStack.Pop());
            }

            if (outQueue.Count == 0)
            {
                result = -1;
                return false;
            }

            try
            {
                result = this.CalculateRPN(outQueue);
            }
            catch
            {
                result = -1;
                return false;
            }
            return true;
        }

        private int CalculateRPN(Queue<String> outQueue)
        {
            Stack<int> stack = new Stack<int>();

            while (outQueue.Count > 0)
            {
                String token = outQueue.Dequeue();

                if (this.IsOperator(token))
                {
                    if (stack.Count < 2)
                    {
                        throw new ArgumentException("To few operands in " + outQueue + " for operand " + token);
                    }
                    int op2 = stack.Pop();
                    int op1 = stack.Pop();
                    int intermediateResult = this.ApplyOperand(token, op1, op2);
                    stack.Push(intermediateResult);
                }
                else if (this.IsNumber(token))
                {
                    stack.Push(Int32.Parse(token));
                }
                else
                {
                    throw new ArgumentException("Neither number nor operand found " + token);
                }
            }

            return stack.Peek();
        }

        private int OperatorPrecedence(String c)
        {
            switch (c)
            {
                case "!":
                    return 4;
                case "*":
                case "/":
                case "%":
                case "&":
                    return 3;
                case "+":
                case "-":
                case "|":
                    return 2;
                case "=":
                    return 1;
            }
            return 0;
        }

        private bool IsLeftAssociative(String c)
        {
            switch (c)
            {
                // left to right
                case "*":
                case "/":
                case "%":
                case "+":
                case "-":
                case "&":
                case "|":
                    return true;
                // right to left
                case "=":
                case "!":
                    return false;
            }
            return false;
        }

        private bool IsOperator(String c)
        {
            return (c.Equals("+") || c.Equals("-") || c.Equals("/") || c.Equals("*") || c.Equals("!") || c.Equals("%") || c.Equals("=") || c.Equals("<") || c.Equals(">") || c.Equals("&") || c.Equals("|"));
        }

        private bool IsNumber(String c)
        {
            int i;
            return Int32.TryParse(c, out i);
        }

        private int ApplyOperand(String operand, int left, int right)
        {
            switch (operand)
            {
                // left to right
                case "*": return left * right;
                case "+": return left + right;
                case "-": return left - right;
                case "/": return left / right;
                case "<": return (left < right ? 1 : 0);
                case ">": return (left > right ? 1 : 0);
                case "=": return (left == right ? 1 : 0);
                case "&": return (left == 1 && right == 1 ? 1 : 0);
                case "|": return (left == 1 || right == 1 ? 1 : 0);
                default: throw new ArgumentException(operand + " is not a valid operand");
            };
        }
    }
}
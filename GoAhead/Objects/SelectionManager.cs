﻿using System;
using GoAhead.FPGA;

namespace GoAhead.Objects
{
    public class SelectionManager
    {
        private SelectionManager()
        {
        }

        public static SelectionManager Instance = new SelectionManager();

        public Tile Anchor
        {
            get { return m_anchor; }
            set { m_anchor = value; }
        }

        public String XAnchorName
        {
            get { return m_xAnchorName; }
            set { m_xAnchorName = value; }
        }

        public String YAnchorName
        {
            get { return m_yAnchorName; }
            set { m_yAnchorName = value; }
        }

        private Tile m_anchor = null;
        private String m_xAnchorName = "";
        private String m_yAnchorName = "";
    }
}
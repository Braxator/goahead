﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using GoAhead.FPGA;

namespace GoAhead.Objects
{
    public class BlockerOrderElement
    {
        public string DriverRegexp = "";
        public string SinkRegexp = "";
        public string VivadoPipConnector = "";
        public bool ConnectAll = true;
        public bool EndPip = true;

        public override string ToString()
        {
            return "DriverRegexp=" + this.DriverRegexp + " SinkRegexp=" + this.SinkRegexp + " ConnectAll=" + this.ConnectAll;
        }
    }

    public class BlockerPath
    {
        public string DriverRegexp = "";
        public string HopRegexp = "";
        public string SinkRegexp = "";
    }

    public class BlockerSettings
    {
        private class ReplacementInfo
        {
            public string PrimitveRegexp = "";
            public string SliceNumberPattern = "";
            public string Template = "";
        }

        private BlockerSettings()
        {
        }

        /// <summary>
        /// BlockerSettings is a singelton
        /// </summary>
        public static BlockerSettings Instance = new BlockerSettings();

        public void AddBlockerPath(String family, String fromRegexp, String hopRegexp, String sinkRegexp)
        {
            BlockerPath p = new BlockerPath();
            p.DriverRegexp = fromRegexp;
            p.HopRegexp = hopRegexp;
            p.SinkRegexp = sinkRegexp;

            if (!this.m_blockerPaths.ContainsKey(family))
            {
                this.m_blockerPaths.Add(family, new List<BlockerPath>());
            }

            this.m_blockerPaths[family].Add(p);
        }

        public IEnumerable<BlockerPath> GetAllBlockerPaths()
        {
            String family = FPGA.FPGA.Instance.Family.ToString();

            foreach (KeyValuePair<String, List<BlockerPath>> tupel in this.m_blockerPaths.Where(tupel => Regex.IsMatch(family, tupel.Key)))
            {
                foreach (BlockerPath bp in tupel.Value)
                {
                    yield return bp;
                }
            }
        }

        public void AddPortFilter(String family, String regexp)
        {
            if (!this.m_portFilter.ContainsKey(family))
            {
                this.m_portFilter.Add(family, new List<String>());
            }

            this.m_portFilter[family].Add(regexp);
        }

        public bool SkipPort(Port p)
        {
            String family = FPGA.FPGA.Instance.Family.ToString();

            if (!this.m_portFilterRegexps.ContainsKey(family))
            {
                this.m_portFilterRegexps.Add(family, new List<Regex>());
                foreach (KeyValuePair<String, List<String>> tupel in this.m_portFilter.Where(tupel => Regex.IsMatch(family, tupel.Key)))
                {
                    foreach (String filter in tupel.Value)
                    {
                        Regex regexp = new Regex(filter, RegexOptions.Compiled);
                        this.m_portFilterRegexps[family].Add(regexp);
                    }
                }
            }

            foreach (Regex r in this.m_portFilterRegexps[family])
            {
                if (r.IsMatch(p.Name))
                {
                    return true;
                }
            }

            // no filter matches
            return false;
        }

        public void AddTileFilter(String family, String regexp)
        {
            if (!this.m_tileFilter.ContainsKey(family))
            {
                this.m_tileFilter.Add(family, new List<String>());
            }

            this.m_tileFilter[family].Add(regexp);
        }

        public bool SkipTile(Tile t)
        {
            String family = FPGA.FPGA.Instance.Family.ToString();

            if (!this.m_tileFilterRegexps.ContainsKey(family))
            {
                this.m_tileFilterRegexps.Add(family, new List<Regex>());
                foreach (KeyValuePair<String, List<String>> tupel in this.m_tileFilter.Where(tupel => Regex.IsMatch(family, tupel.Key)))
                {
                    foreach (String filter in tupel.Value)
                    {
                        Regex regexp = new Regex(filter, RegexOptions.Compiled);
                        this.m_tileFilterRegexps[family].Add(regexp);
                    }
                }
            }

            foreach (Regex r in this.m_tileFilterRegexps[family])
            {
                if (r.IsMatch(t.Location))
                {
                    return true;
                }
            }

            // no filter matches
            return false;
        }

        public void AddPrimitveTemplate(String family, String primitiveTypeRegexp, String sliceNumberPattern, String template)
        {
            if (!this.m_mappings.ContainsKey(family))
            {
                this.m_mappings.Add(family, new List<ReplacementInfo>());
            }

            ReplacementInfo ri = new ReplacementInfo();
            ri.PrimitveRegexp = primitiveTypeRegexp;
            ri.SliceNumberPattern = sliceNumberPattern;
            ri.Template = template;

            this.m_mappings[family].Add(ri);
        }

        /// <summary>
        /// Wheter to insert a template considering the SliceNumberPattern
        /// </summary>
        /// <param name="primitiveIdentifier"></param>
        /// <param name="primitiveIndex"></param>
        /// <param name="template"></param>
        /// <returns></returns>
        public bool InsertTemplate(String primitiveIdentifier, bool ignoreSliceNumber, int primitiveIndex, out String template)
        {
            template = "";

            String family = FPGA.FPGA.Instance.Family.ToString();

            foreach (KeyValuePair<String, List<ReplacementInfo>> tupel in this.m_mappings.Where(tupel => Regex.IsMatch(family, tupel.Key)))
            {
                foreach (ReplacementInfo ri in tupel.Value)
                {
                    if (Regex.IsMatch(primitiveIdentifier, ri.PrimitveRegexp) && (ignoreSliceNumber || Regex.IsMatch(primitiveIndex.ToString(), ri.SliceNumberPattern)))
                    {
                        template = ri.Template;
                        return true;
                    }
                }
            }

            // no match
            template = "";
            return false;
        }

        public void AddEndPipRegexp(String familyRegexp, String endPipRegexp)
        {
            if (!this.m_endPipsRegexps.ContainsKey(familyRegexp))
            {
                this.m_endPipsRegexps.Add(familyRegexp, new List<String>());
            }

            this.m_endPipsRegexps[familyRegexp].Add(endPipRegexp);
        }

        public void AddBlockerOrder(String familyRegexp, String driverRegexp, String sinkRegexp, bool connectAll, bool endPip, string vivadoPipConnector)
        {
            if (!this.m_blockerOrder.ContainsKey(familyRegexp))
            {
                this.m_blockerOrder.Add(familyRegexp, new List<BlockerOrderElement>());
            }

            BlockerOrderElement el = new BlockerOrderElement();
            el.ConnectAll = connectAll;
            el.DriverRegexp = driverRegexp;
            el.SinkRegexp = sinkRegexp;
            el.EndPip = endPip;
            el.VivadoPipConnector = vivadoPipConnector;
            this.m_blockerOrder[familyRegexp].Add(el);
        }

        public IEnumerable<BlockerOrderElement> GetBlockerOrder()
        {
            String family = FPGA.FPGA.Instance.Family.ToString();

            foreach (KeyValuePair<String, List<BlockerOrderElement>> tupel in this.m_blockerOrder.Where(tupel => Regex.IsMatch(family, tupel.Key)))
            {
                foreach (BlockerOrderElement el in tupel.Value)
                {
                    yield return el;
                }
            }
        }

        /// <summary>
        /// map e.g. FPGA family to SLICE and template
        /// </summary>
        private Dictionary<String, List<ReplacementInfo>> m_mappings = new Dictionary<String, List<ReplacementInfo>>();

        private Dictionary<String, List<String>> m_tileFilter = new Dictionary<String, List<String>>();
        private Dictionary<String, List<Regex>> m_tileFilterRegexps = new Dictionary<String, List<Regex>>();

        private Dictionary<String, List<String>> m_portFilter = new Dictionary<String, List<String>>();
        private Dictionary<String, List<Regex>> m_portFilterRegexps = new Dictionary<String, List<Regex>>();

        /// <summary>
        /// map e.g. FPGA family to SLICE and template
        /// </summary>
        private Dictionary<String, List<BlockerPath>> m_blockerPaths = new Dictionary<String, List<BlockerPath>>();

        private Dictionary<String, List<String>> m_endPipsRegexps = new Dictionary<String, List<String>>();

        private Dictionary<String, List<BlockerOrderElement>> m_blockerOrder = new Dictionary<String, List<BlockerOrderElement>>();
    }
}
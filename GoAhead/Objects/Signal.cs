﻿using System;
using System.ComponentModel;
using GoAhead.FPGA;

namespace GoAhead.Objects
{
    [Serializable]
    public class Signal : INotifyPropertyChanged
    {
        public Signal()
        {
            this.m_signalName = InterfaceManager.Instance.GetNewSignalName();
            this.m_signalMode = "in";
            this.m_signalDirection = FPGATypes.InterfaceDirection.East;
        }

        /// <summary>
        /// Parse SignalEntry members from atoms which must contain 7 entries
        /// </summary>
        /// <param name="atoms"></param>
        public Signal(String signalName, String signalMode, FPGATypes.InterfaceDirection signalDirection, String partialRegion, int column)
        {
            this.m_signalName = signalName;
            this.m_signalMode = signalMode;
            this.m_signalDirection = signalDirection;
            this.m_partialRegion = partialRegion;
            this.m_column = column;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        public String SignalName
        {
            get { return this.m_signalName; }
            set { this.m_signalName = value; NotifyPropertyChanged("SignalName"); }
        }

        public String SignalNameWithoutBraces
        {
            get
            {
                // Video_Bar(31) -> VideoBar.
                // Assume only one brace
                String[] braces = { "<", "{", "[", "(" };
                foreach (String brace in braces)
                {
                    if (this.SignalName.Contains(brace))
                    {
                        int index = this.SignalName.IndexOf(brace);
                        if (index > 0)
                        {
                            return this.SignalName.Substring(0, index);
                        };
                    };
                }
                return this.SignalName;
            }
        }

        /// <summary>
        /// in or out
        /// </summary>
        public String SignalMode
        {
            get { return m_signalMode; }
            set { this.m_signalMode = value; NotifyPropertyChanged("SignalMode"); }
        }

        /// <summary>
        /// East, West, South or North
        /// </summary>
        public FPGATypes.InterfaceDirection SignalDirection
        {
            get { return m_signalDirection; }
            set { this.m_signalDirection = value; NotifyPropertyChanged("SignalDirection"); }
        }

        public String PartialRegion
        {
            get { return this.m_partialRegion; }
            set { this.m_partialRegion = value; NotifyPropertyChanged("PartialRegion"); }
        }

        public int Column
        {
            get { return this.m_column; }
            set { this.m_column = value; NotifyPropertyChanged("Column"); }
        }

        public override string ToString()
        {
            return
                "SignalName: " + this.SignalName + " " +
                "SignalMode: " + this.SignalMode + " " +
                "SignalDirection: " + this.SignalDirection + " " +
                "PartialRegion: " + this.m_partialRegion + " " +
                "Column:" + this.m_column;
        }

        /// <summary>
        /// Return a comms separated value representation of this signal
        /// </summary>
        /// <returns></returns>
        public String ToCSV()
        {
            return this.SignalName + "," + this.SignalMode + "," + this.SignalDirection + "," + this.PartialRegion + "," + this.Column;
        }

        /// <summary>
        /// addr_in
        /// </summary>
        private String m_signalName = "default signal name";

        /// <summary>
        /// in, out, streaming
        /// </summary>
        private String m_signalMode = "in or out";

        /// <summary>
        /// East, West, ...
        /// </summary>
        private FPGA.FPGATypes.InterfaceDirection m_signalDirection = FPGA.FPGATypes.InterfaceDirection.East;

        /// <summary>
        /// an optional tag for this signal
        /// </summary>
        private String m_partialRegion = "";

        /// <summary>
        /// For interleaving (currently we support 1 and 2)
        /// </summary>
        private int m_column = 1;
    }
}
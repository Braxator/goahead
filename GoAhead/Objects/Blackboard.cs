﻿using System;
using System.Collections.Generic;
using GoAhead.FPGA;

namespace GoAhead.Objects
{
    public class Blackboard
    {
        private Blackboard()
        {
        }

        public static Blackboard Instance = new Blackboard();

        public void ClearToolTipInfo(Tile t)
        {
            if (this.m_toolTipInfo.ContainsKey(t))
            {
                this.m_toolTipInfo[t] = "";
            }
        }

        public void AddToolTipInfo(Tile t, String info)
        {
            if (!this.m_toolTipInfo.ContainsKey(t))
            {
                this.m_toolTipInfo.Add(t, info);
            }
            else
            {
                this.m_toolTipInfo[t] += info;
            }
        }

        public bool HasToolTipInfo(Tile t)
        {
            return this.m_toolTipInfo.ContainsKey(t);
        }

        public String GetToolTipInfo(Tile t)
        {
            return this.m_toolTipInfo[t];
        }

        public String LastLoadCommandForFPGA = "";

        private Dictionary<Tile, String> m_toolTipInfo = new Dictionary<Tile, String>();
    }
}
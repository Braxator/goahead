﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using GoAhead.Code;
using GoAhead.FPGA;

namespace GoAhead.Objects
{
    public class LibElemInst
    {
        public LibElemInst()
        {
            this.LibraryElementName = "";
            this.InstanceName = "";
            this.AnchorLocation = "";
            this.SliceName = "";
            this.SliceNumber = -1;
            this.m_portMapper = new PortMapper();
        }

        public String LibraryElementName { get; set; }
        public String InstanceName { get; set; }
        public String AnchorLocation { get; set; }
        public String SliceName { get; set; }
        public int SliceNumber { get; set; }
        public String TunnelDirection { get; set; }
        public String InputDirection { get; set; }
        public String OutputDirection { get; set; }

        public PortMapper PortMapper
        {
            get { return this.m_portMapper; }
        }

        public override string ToString()
        {
            return
                "InstanceName " + this.InstanceName + Environment.NewLine +
                "LibraryElementName: " + this.LibraryElementName + Environment.NewLine +
                "AnchorLocation " + this.AnchorLocation + Environment.NewLine +
                "SliceName " + this.SliceName + Environment.NewLine +
                "SliceNumber " + this.SliceNumber;
        }

        public LibraryElement GetLibraryElement()
        {
            return Library.Instance.GetElement(this.LibraryElementName);
        }

        public IEnumerable<Tuple<Tile, Slice>> GetAllUsedSlices()
        {
            LibraryElement libElement = Library.Instance.GetElement(this.LibraryElementName);

            Tile anchor = FPGA.FPGA.Instance.GetTile(this.AnchorLocation);

            foreach (Tuple<Instance, Tile> instanceTileTupel in libElement.GetInstanceTiles(anchor, libElement))
            {
                // placement of the current instance;
                Tile targetTile = instanceTileTupel.Item2;

                yield return new Tuple<Tile, Slice>(targetTile, targetTile.Slices[(int)instanceTileTupel.Item1.SliceNumber]);
            }
        }

        private readonly PortMapper m_portMapper;
    }

    public class PortMapper
    {
        public enum MappingKind { NoVector = 0, Internal = 1, External = 2 }

        public void AddMapping(String portName, String mappedSignal, MappingKind kind, int index)
        {
            this.m_kindMapping[portName] = kind;
            this.m_signalMapping[portName] = mappedSignal;
            this.m_signalIndex[portName] = index;
        }

        public int GetIndex(String portName)
        {
            if (!this.m_signalIndex.ContainsKey(portName))
            {
                throw new ArgumentException("No index found for signal " + portName);
            }
            else
            {
                return this.m_signalIndex[portName];
            }
        }

        public bool HasIndex(String portName)
        {
            return this.m_signalIndex.ContainsKey(portName);
        }

        public IEnumerable<Tuple<String, String, MappingKind>> GetMappings()
        {
            foreach (KeyValuePair<String, MappingKind> tuple in this.m_kindMapping)
            {
                Tuple<String, String, MappingKind> next = new Tuple<String, String, MappingKind>(tuple.Key, this.m_signalMapping[tuple.Key], tuple.Value);
                yield return next;
            }
        }

        public MappingKind GetMapping(String portName)
        {
            return this.m_kindMapping[portName];
        }

        public String GetSignalName(String portName)
        {
            return this.m_signalMapping[portName];
        }

        public bool HasKindMapping(String portName)
        {
            return this.m_kindMapping.ContainsKey(portName);
        }

        public bool HasSignalMapping(String portName)
        {
            return this.m_signalMapping.ContainsKey(portName);
        }

        public void Clear()
        {
            this.m_kindMapping.Clear();
            this.m_signalMapping.Clear();
            this.m_signalIndex.Clear();
        }

        // alles per Instantiation Filter
        private Dictionary<String, MappingKind> m_kindMapping = new Dictionary<String, MappingKind>();

        private Dictionary<String, String> m_signalMapping = new Dictionary<String, String>();
        private Dictionary<String, int> m_signalIndex = new Dictionary<String, int>();
    }

    public class LibraryElementInstanceManager : Interfaces.IResetable
    {
        private LibraryElementInstanceManager()
        {
            GoAhead.Commands.Debug.PrintGoAheadInternals.ObjectsToPrint.Add(this);
        }

        public void Add(LibElemInst instantiation)
        {
            if (this.m_instanceNames.ContainsKey(instantiation.InstanceName))
            {
                throw new ArgumentException("Instance name " + instantiation.InstanceName + " already used");
            }

            this.m_instantiations.Add(instantiation);
            this.m_instanceNames.Add(instantiation.InstanceName, true);
        }

        public void Remove(String instanceName)
        {
            if (!this.m_instanceNames.ContainsKey(instanceName))
            {
                throw new ArgumentException("Instance name " + instanceName + " already used");
            }

            // there should be only one element
            LibElemInst instToDelete = this.m_instantiations.FirstOrDefault(element => element.InstanceName == instanceName);

            this.m_instantiations.Remove(instToDelete);
            this.m_instanceNames.Remove(instanceName);
        }

        public void Reset()
        {
            this.m_instanceNames.Clear();
            this.m_instantiations.Clear();
        }

        public bool HasInstanceName(String instanceName)
        {
            return this.m_instanceNames.ContainsKey(instanceName);
        }

        public String ProposeInstanceName(String prefix)
        {
            int suffix = this.m_instanceNames.Keys.Count(str => Regex.IsMatch(str, prefix));

            return prefix + "_" + suffix;
        }

        public IEnumerable<LibElemInst> GetAllInstantiations()
        {
            foreach (LibElemInst instantiation in this.m_instantiations)
            {
                yield return instantiation;
            }
        }

        public LibElemInst GetInstantiation(int index)
        {
            return this.m_instantiations[index];
        }

        public LibElemInst GetInstantiation(Tile t)
        {
            foreach (LibElemInst instantiation in this.m_instantiations)
            {
                if (instantiation.AnchorLocation.Equals(t.Location))
                {
                    return instantiation;
                }
            }
            return null;
        }

        public LibElemInst GetInstantiation(String instanceName)
        {
            if (!this.HasInstanceName(instanceName))
            {
                throw new ArgumentException("Instance name " + instanceName + " not found");
            }

            // InstanceName is unique
            return this.m_instantiations.First(macro => macro.InstanceName.Equals(instanceName));
        }

        public BindingList<LibElemInst> Instances
        {
            get { return m_instantiations; }
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            if (this.m_instanceNames.Count == 0)
            {
                result.AppendLine("No instances added");
            }
            else
            {
                result.AppendLine("Added instances are");
                foreach (LibElemInst m in this.GetAllInstantiations())
                {
                    result.AppendLine(m.ToString());
                }
            }
            return result.ToString();
        }

        public static LibraryElementInstanceManager Instance = new LibraryElementInstanceManager();

        private Dictionary<String, bool> m_instanceNames = new Dictionary<String, bool>();
        private BindingList<LibElemInst> m_instantiations = new BindingList<LibElemInst>();
    }
}
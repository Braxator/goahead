using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GoAhead.Code;
using GoAhead.FPGA;
using GoAhead.Commands.Data;

namespace GoAhead.Objects
{
    [Serializable]
    public class NetlistContainer
    {
        public NetlistContainer()
        {
            this.Name = NetlistContainerManager.Instance.GetName();
        }

        public NetlistContainer(String netlistContainerName)
        {
            this.Name = netlistContainerName;
        }

        public void Add(Slice slice)
        {
            // overwrite
            if (!this.m_slices.ContainsKey(slice))
            {
                this.m_slices.Add(slice, slice.SliceName);
            }
            else
            {
                this.m_slices[slice] = slice.SliceName;
            }
        }

        public void Add(Instance instance)
        {
            if (this.m_instances.ContainsKey(instance.Name))
            {
                throw new ArgumentException("An instance named " + instance.Name + " has already been added");
            }

            instance.InstanceIndex = this.m_instances.Count;

            // add instance
            this.m_instances.Add(instance.Name, instance);

            // add location->instance mapping
            if (!this.m_locationInstanceMapping.ContainsKey(instance.Location))
            {
                this.m_locationInstanceMapping.Add(instance.Location, new List<Instance>());
            }
            this.m_locationInstanceMapping[instance.Location].Add(instance);
            
            // add slice->instance mapping
            if (!this.m_sliceInstanceMapping.ContainsKey(instance.SliceName))
            {
                this.m_sliceInstanceMapping.Add(instance.SliceName, new List<Instance>());
            }
            this.m_sliceInstanceMapping[instance.SliceName].Add(instance);
        }

        public void Add(Net net)
        {
            if (this.m_nets.ContainsKey(net.Name))
            {
                return;
                throw new ArgumentException("A net named " + net.Name + " has already been added");
            }
            this.m_nets.Add(net.Name, net);
            this.m_lastNetAdded = net;
        }

        public void Add(NetlistPort port)
        {
            if (this.m_ports.Contains(port))
            {
                return;
                throw new ArgumentException("A port named " + port.ExternalName + " has already been added");
            }

            this.m_ports.Add(port);
        }

        public IEnumerable<Slice> GetAllSlicesTemplates()
        {
            foreach (KeyValuePair<Slice, String> next in this.m_slices)
            {
                yield return next.Key;
            }
        }

        public Net GetNet(String netName)
        {
            if (!this.m_nets.ContainsKey(netName))
            {
                throw new ArgumentException("A net with the name " + netName + " has not been added");
            }
            else
            {
                return this.m_nets[netName];
            }
        }

        public virtual Net GetAnyNet()
        {
            if (this.m_nets.Count == 0)
            {
                throw new ArgumentException("GetAnyNet: No nets found in netlist container " + this.Name);
            }
            else
            {
                return this.m_nets.Values.First();
            }
        }

        public Slice GetTopLeftSlice()
        {
            int topLeftX = Int32.MaxValue;
            int topLeftY = Int32.MaxValue;

            Slice topLeftSlice = null;

            // the anchor must reside on an instantiated slice within a CLB
            foreach (Instance inst in this.Instances.Where(i => IdentifierManager.Instance.IsMatch(i.Location, IdentifierManager.RegexTypes.CLBRegex)))
            {
                Tile t = FPGA.FPGA.Instance.GetTile(inst.Location);
                Slice s = t.GetSliceByName(inst.SliceName);
                //Slice s = FPGA.FPGA.Instance.GetSlice(inst.SliceName);
                if (s.ContainingTile.TileKey.X < topLeftX)
                {
                    topLeftX = s.ContainingTile.TileKey.X;
                    topLeftY = s.ContainingTile.TileKey.Y;
                    //used this slice as an anchor
                    topLeftSlice = s;
                }
                else if (s.ContainingTile.TileKey.X == topLeftX && s.ContainingTile.TileKey.Y < topLeftY)
                {
                    topLeftX = s.ContainingTile.TileKey.X;
                    topLeftY = s.ContainingTile.TileKey.Y;
                    //used this slice as an anchor
                    topLeftSlice = s;
                }
            }
            return topLeftSlice;
        }

        public Instance GetInstanceByName(String instanceName)
        {
            if (!this.m_instances.ContainsKey(instanceName))
            {
                //throw new ArgumentException("Instance " + instanceName + " not known in " + this.ToString());
                return null;
            }
            else
            {
                return this.m_instances[instanceName];
            }
        }

        public bool HasMappedInstances(String location)
        {
            return this.m_locationInstanceMapping.ContainsKey(location);
        }

        public IEnumerable<NetlistPort> Ports
        {
            get { return this.m_ports; }
        }

        public IEnumerable<Net> Nets
        {
            get { return this.m_nets.Values; }
        }

        public Net LastNetAdded
        {
            get { return this.m_lastNetAdded; }
        }

        public void RemoveNet(string netName)
        {
            if (!this.m_nets.ContainsKey(netName))
            {
                throw new ArgumentException("Net " + netName + " not found");
            }
            this.UnblockResourcesAfterRemoval(netName);

            this.m_nets.Remove(netName);
        }

        protected virtual void UnblockResourcesAfterRemoval(string netName)
        {
            // TODO TCL
        }

        public virtual NetlistContainer GetSelectedDesignElements()
        {
            throw new NotImplementedException();
        }

        public virtual void WriteCodeToFile(StreamWriter sw)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get all instances
        /// </summary>
        public IEnumerable<Instance> Instances
        {
            get { return this.m_instances.Values; }
        }

        public int InstanceCount
        {
            get { return this.m_instances.Count; }
        }

        public int NetCount
        {
            get { return this.m_nets.Count; }
        }

        public void Remove(Predicate<Net> p)
        {
            List<String> keysToDelete = new List<String>();
            foreach (KeyValuePair<String, Net> tupel in this.m_nets.Where(t => p(t.Value)))
            {
                keysToDelete.Add(tupel.Key);
                this.UnblockResourcesAfterRemoval(tupel.Key);
            }
            foreach (String s in keysToDelete)
            {
                this.m_nets.Remove(s);
            }
        }

        public void Remove(Predicate<Instance> p)
        {
            List<String> keysToDelete = new List<String>();
            foreach (KeyValuePair<String, Instance> tupel in this.m_instances.Where(t => p(t.Value)))
            {
                Tile t = FPGA.FPGA.Instance.GetTile(tupel.Value.Location);
                if (t.HasSlice(tupel.Value.SliceName))
                {
                    Slice s = t.GetSliceByName(tupel.Value.SliceName);
                    s.Usage = FPGATypes.SliceUsage.Free;
                }
                keysToDelete.Add(tupel.Key);
            }
            foreach (String s in keysToDelete)
            {
                this.m_instances.Remove(s);
            }
        }

        public override string ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// How to identify this netlist container (e.g. default_netlist_container)
        /// </summary>
        public String Name { get; set; }

        protected Dictionary<String, List<Instance>> m_locationInstanceMapping = new Dictionary<String, List<Instance>>();
        protected Dictionary<String, List<Instance>> m_sliceInstanceMapping = new Dictionary<string, List<Instance>>();
        protected Dictionary<String, Instance> m_instances = new Dictionary<String, Instance>();
        protected Dictionary<String, Net> m_nets = new Dictionary<String, Net>();
        protected Dictionary<Slice, String> m_slices = new Dictionary<Slice, String>();
        protected Net m_lastNetAdded = null;
        private List<NetlistPort> m_ports = new List<NetlistPort>();

    }
}
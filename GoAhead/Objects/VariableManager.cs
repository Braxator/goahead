﻿using System;
using System.Collections.Generic;

namespace GoAhead.Objects
{
    public class VariableManager : Interfaces.Subject
    {
        private VariableManager()
        {
        }

        public static VariableManager Instance = new VariableManager();

        public void Set(String variable, String value)
        {
            this.m_variables[variable] = value;

            // notify observers about change
            this.Notfiy(null);
        }

        public void Unset(String variable)
        {
            if (!this.IsSet(variable))
            {
                throw new ArgumentException("A variable named " + variable + " has not been set");
            }
            this.m_variables.Remove(variable);

            // notify observers about change
            this.Notfiy(null);
        }

        public IEnumerable<String> GetAllVariableNames()
        {
            return this.m_variables.Keys;
        }

        public IEnumerable<String> GetAllVariableValues()
        {
            return this.m_variables.Values;
        }

        public String GetValue(String variable)
        {
            return this.m_variables[variable];
        }

        public bool IsSet(String variable)
        {
            return this.m_variables.ContainsKey(variable);
        }

        /// <summary>
        /// translate value to a before set value and return the translation. if no variable is found, value is returned
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public String Resolve(String value)
        {
            String resolvedValue = value;
            foreach (KeyValuePair<String, String> entry in this.m_variables)
            {
                resolvedValue = resolvedValue.Replace("%" + entry.Key + "%", entry.Value);
                //resolvedValue = Regex.Replace(resolvedValue, "%" + entry.Key + "%", entry.Value);
            }

            return resolvedValue;
        }

        private Dictionary<String, String> m_variables = new Dictionary<String, String>();
    }
}
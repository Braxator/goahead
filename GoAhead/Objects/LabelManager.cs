﻿using System;
using System.Collections.Generic;

namespace GoAhead.Objects
{
    public class LabelManager
    {
        private LabelManager()
        {
        }

        public static LabelManager Instance = new LabelManager();

        public bool Contains(String labelName)
        {
            return this.m_labels.ContainsKey(labelName);
        }

        public int GetCommandListIndex(String labelName)
        {
            return this.m_labels[labelName];
        }

        /// <summary>
        /// Set a named label pointing into the command executer command list index
        /// </summary>
        /// <param name="labelName"></param>
        /// <param name="commandListIndex"></param>
        public void SetLabel(String labelName, int commandListIndex)
        {
            this.m_labels[labelName] = commandListIndex;
        }

        private Dictionary<String, int> m_labels = new Dictionary<String, int>();
    }
}
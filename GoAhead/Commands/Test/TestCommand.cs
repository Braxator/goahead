﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GoAhead.Commands.Test
{
    class TestCommand : Command
    {
        protected override void DoCommandAction()
        {
            MessageBox.Show("Executing TestCommand.");
        }

        public override void Undo()
        {
            throw new NotImplementedException();
        }
    }
}

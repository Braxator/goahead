﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoAhead.FPGA;
using GoAhead.Commands;

namespace GoAhead.Commands.Debug
{
    [CommandDescription(Description = "Print the identifiers of all currently selected tiles with blocked ports", Wrapper = false, Publish = true)]
    class PrintSelectedTilesWithBlockedPorts : Command
    {
        protected override void DoCommandAction()
        {
            foreach (Tile tile in FPGA.TileSelectionManager.Instance.GetSelectedTiles().Where(t => t.HasBlockedPorts))
            {
                this.OutputManager.WriteOutput(tile.Location);
            }
        }

        public override void Undo()
        {
            throw new NotImplementedException();
        }
    }
}

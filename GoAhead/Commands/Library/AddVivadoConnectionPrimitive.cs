﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using GoAhead.Code.XDL;
using GoAhead.FPGA;
using GoAhead.Objects;

namespace GoAhead.Commands.Library
{
    [CommandDescription(Description = "Add a Vivado connection primitive.", Wrapper = false, Publish = true)]
    class AddVivadoConnectionPrimitive : Command
    {
        protected override void DoCommandAction()
        {
            // check whether relavant parameters are non-empty and correct
            this.CheckParameter();

            
            LibraryElement main;

            // check if the library element already is present in the Library, otherwise create a new library element
            if(Objects.Library.Instance.Contains(this.Name))
            {
                main = Objects.Library.Instance.GetElement(this.Name);
            }
            else
            {
                main = new LibraryElement();
                main.Name = this.Name; // example; "VivadoConnectionPrimitive"
                main.VivadoConnectionPrimitve = true; // mark as vivado connection primitive
                main.Containter = new NetlistContainer(); // TODO: what is the purpose of this netlist?
                Objects.Library.Instance.Add(main);
            }

            // loop through the list of provided BELs (example; {"A6LUT", "B6LUT", "C6LUT", "D6LUT"})
            for (int i = 0; i < this.BELs.Count; i++)
            {
                string bel = this.BELs[i]; // example: "A6LUT"                
                string prefix = this.InputBELinputPortPrefix[i]; // example: "_L_A"
                string outputPort = this.BELOutputPorts[i]; // example: "A"

                // the "!bel.Equals(this.InputBEL)" checks whether the BEL is the BEL where the inputs are connected to. If this is not the case
                // the inputs of this BEL are made constant ('0' or '1')
                // create library element
                LibraryElement elem = this.GetLibraryElement(bel, !bel.Equals(this.InputBEL), prefix, outputPort);

                // add the library element to the Library
                main.Add(elem);
                Objects.Library.Instance.Add(elem);
            }

            /*
            LibElem a = this.GetLUT6LibraryElement("A", true);
            LibElem b = this.GetLUT6LibraryElement("B", true);
            LibElem c = this.GetLUT6LibraryElement("C", false);
            LibElem d = this.GetLUT6LibraryElement("D", true);
            
            main.Add(a);
            main.Add(b);
            main.Add(c);
            main.Add(d);

            Objects.Library.Instance.Add(main);
            Objects.Library.Instance.Add(a);
            Objects.Library.Instance.Add(b);
            Objects.Library.Instance.Add(c);
            Objects.Library.Instance.Add(d);
             * */
        }

        private void CheckParameter()
        {
            // the name may not be empty
            if (string.IsNullOrEmpty(this.Name))
            {
                throw new ArgumentException("Name must not be empty");
            }

            // there must be provided BELs (example; {"A6LUT", "B6LUT", "C6LUT", "D6LUT"}
            if (this.BELs.Count == 0)
            {
                throw new ArgumentException("No BELs provided");
            }

            // the input BEL (the BEL where the input signals are connected to, example; "C6LUT") must be included in the provided BELs 
            if (!String.IsNullOrEmpty(this.InputBEL))
            {
                if (this.BELs.IndexOf(this.InputBEL) < 0)
                {
                    throw new ArgumentException("InputBEL must be included in BELs");
                }
            }

            // Any<TSource>
            // Determines whether any element of a sequence exists or satisfies a condition

            // check of one of the provided BELs is a [A..D]6LUT
            if(this.BELs.Any(s => Regex.IsMatch(s, @"[A-D]LUT6$^")))
            {
                throw new ArgumentException("InputBEL must be included in A..DLUT6");
            }

            // the number of provided BELs must be the same as the number of input BEL input port prefix
            if (this.BELs.Count != this.InputBELinputPortPrefix.Count)
            {
                throw new ArgumentException("All list arguments must be of the same size (1)");
            }

            /*
            if (this.BELs.Count != this.InputBELPorts.Count)
            {
                throw new ArgumentException("All list arguments must be of the same size (2)");
            }
             * */

            // the number of provided BELS must be the same as the number of output ports of BELs  
            if (this.BELs.Count != this.BELOutputPorts.Count)
            {
                throw new ArgumentException("All list arguments must be of the same size (3)");
            }
        }

        private LibraryElement GetLibraryElement(string belName, bool makeInputsConstant, string inputPortPrefix, string outputPort)
        {
            if (this.BELType.Equals("LUT6")) // if the BEL type is LUT6
            {
                return GetLUT(belName, makeInputsConstant, inputPortPrefix, outputPort); // example; GetLut("A6LUT", true, "_L_A", "A")
            }
            else if (this.BELType.Equals("FDRE")) // if the BEL type is FDRE (flip-flop)
            {
                return GetFF(belName, makeInputsConstant, inputPortPrefix, outputPort);
            }
            else
            {
                throw new ArgumentException("Unsupportd BEL " + this.BELType);
            }
        }

        private LibraryElement GetLUT(string belName, bool makeInputsConstant, string inputPortPrefix, string outputPort)
        {
            int lutSize = 6; // the LUTs have 6 inputs
            LibraryElement el = new LibraryElement();
            el.SliceNumber = this.SliceNumber; // example; 1 (for Zynq; the lower slice is 0 and the upper slice is 1 in a CLB tile)
            el.Name = belName; // example; "A6LUT"
            el.PrimitiveName = this.BELType; // example; "LUT6"
            el.BEL = belName; // example; "A6LUT"
            el.LoadCommand = this.ToString(); // the complete command AddVivadoConnectionPrimitive (so including the parameters)
            el.Containter = new NetlistContainer(); // TODO: what is the purpose of the netlist container?
            el.VHDLGenericMap = "generic map ( INIT => X\"ABCDABCDABCDABCD\" )"; // TODO: what is the purpose of this string?
            el.Containter = new XDLModule(); // TODO: what is the purpose of the XDL module?
            el.VivadoConnectionPrimitve = true; // mark as vivado connection primitive

            // one output per LUT
            XDLPort outPort = new XDLPort();
            outPort.Direction = FPGATypes.PortDirection.Out; 
            outPort.ExternalName = "O";
            outPort.InstanceName = "unknown";
            outPort.SlicePort = "unknown";

            // add the output port to the netlist container
            el.Containter.Add(outPort);

            // six inputs I=I0..I5
            for (int i = 0; i < lutSize; i++)
            {
                // add input port to the netlist container
                this.AddXDLPort(el, "I", i, FPGATypes.PortDirection.In, makeInputsConstant);
            }

            // So far, we added 1 output port and 6 input ports of the LUTs to the netlist container (el.Container)




            // get the very first CLB tile of the FPGA fabric (this is just used as dummy)
            Tile clb = FPGA.FPGA.Instance.GetAllTiles().FirstOrDefault(t => IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.CLBRegex));

            // get the corresponding interconnect tile
            Tile interconnect = FPGA.FPGATypes.GetInterconnectTile(clb);

            List<string> lutPortNames = new List<string>();
            // see LUTRouting tab
            for (int i = 1; i <= lutSize; i++)
            {
                lutPortNames.Add(inputPortPrefix + i); // example; {_L_A[0..5]}
            }

            // Next we going to block ports of switch boxes that route to/from the LUTs

            foreach (string s in lutPortNames) // example lutPortNames; {"_L_A0","_L_A1","_L_A2","_L_A3","_L_A4","_L_A5"}
            {
                // Here we start with the CLB
                // Each CLB has its belonging switch box (it is part of the CLB)

                // original; travers backwards into INT
                // this means that we start at the LUT, and we route to the INT tile
                foreach (Tuple<Port, Port> t in clb.SwitchMatrix.GetAllArcs().Where(a => a.Item2.Name.EndsWith(s)))
                {
                    // example of t; CLBLM_IMUX6, CLBLM_L_A1

                    // here we block ports in the switch box belonging to the CLB
                    // here we block the two ports that corresponds to one of the input signals of the LUTs

                    el.AddPortToBlock(clb, t.Item1); // example t.Item1; CLBLM_IMUX6
                    el.AddPortToBlock(clb, t.Item2); // example t.Item2; CLBLM_L_A1
                    if (interconnect.WireList == null)
                    {
                        this.OutputManager.WriteWarning("No wire list found on " + interconnect.Location);
                    }
                    else
                    {
                        // here we look in the switch matrix (NOT part of the CLB)
                        // here we block 1 port from the switch matrix (NOT part of the CLB) that connects to the input of the switch box (IS part of the CLB) of the CLB

                        foreach (Wire w in interconnect.WireList.Where(w => w.PipOnOtherTile.Equals(t.Item1.Name)))
                        {
                            el.AddPortToBlock(interconnect, new FPGA.Port(w.LocalPip));
                        }
                    }
                }
            }

            // we always need to exclude the port from the LUT output from blocking
            // assuming name
            foreach (Tuple<Port, Port> t in clb.SwitchMatrix.GetAllArcs().Where(a => a.Item1.Name.EndsWith(outputPort))) // example outputPort; "A"
            {
                // no realy neccessary
                el.AddPortToBlock(clb, t.Item1); // example; CLBLM_L_A
                el.AddPortToBlock(clb, t.Item2); // example; CLBLM_LOGIC_OUTS8
                foreach (Wire w in clb.WireList.Where(w => w.LocalPip.Equals(t.Item2.Name)))
                {
                    el.AddPortToBlock(interconnect, new FPGA.Port(w.PipOnOtherTile));
                }
            }

            // EE2END1 cannot be directly connect to a port that routes to the switch box of the CLB. Therefor we need stop over ports.
            // example StopOverPorts; {"BYP_ALT4", "BYP_BOUNCE4"}
            foreach (string stopOverPortName in this.StopOverPorts)
            {
                el.AddPortToBlock(interconnect, new Port(stopOverPortName));
            }

            return el;
        }

        private LibraryElement GetFF(string belName, bool makeInputsConstant, string inputPortPrefix, string outputPort)
        {
            LibraryElement el = new LibraryElement();
            el.SliceNumber = this.SliceNumber;
            el.Name = belName;
            el.PrimitiveName = this.BELType;
            el.BEL = belName;
            el.LoadCommand = this.ToString();
            el.Containter = new NetlistContainer();
            el.VHDLGenericMap = "generic map ( INIT => '0' )";
            el.Containter = new XDLModule();
            el.VivadoConnectionPrimitve = true;

            // Q output
            XDLPort q = new XDLPort();
            q.Direction = FPGATypes.PortDirection.Out;
            q.ExternalName = "Q";
            q.InstanceName = "unknown";
            q.SlicePort = "unknown";
            el.Containter.Add(q);

            List<string> inputs = new List<string>();
            inputs.Add("D");
            inputs.Add("C");
            inputs.Add("CE");
            inputs.Add("R");

            foreach (string i in inputs)
            {
                XDLPort p = new XDLPort();
                p.Direction = FPGATypes.PortDirection.In;
                p.ExternalName = i;
                p.InstanceName = "unknown";
                p.SlicePort = "unknown";
                p.ConstantValuePort = false;
                el.Containter.Add(p);
            };

            Tile clb = FPGA.FPGA.Instance.GetAllTiles().FirstOrDefault(t => IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.CLBRegex));
            Tile interconnect = FPGA.FPGATypes.GetInterconnectTile(clb);

            /*
             * TODO
            // travers backwards into INT
            foreach (Tuple<Port, Port> t in clb.SwitchMatrix.GetAllArcs().Where(a => a.Item2.Name.EndsWith(s)))
            {
                el.AddPortToBlock(clb, t.Item1);
                el.AddPortToBlock(clb, t.Item2);
                if (interconnect.WireList == null)
                {
                    this.OutputManager.WriteWarning("No wire list found on " + interconnect.Location);
                }
                else
                {
                    foreach (Wire w in interconnect.WireList.Where(w => w.PipOnOtherTile.Equals(t.Item1.Name)))
                    {
                        el.AddPortToBlock(interconnect, new FPGA.Port(w.LocalPip));
                    }
                }
            }
            

            // we always need to exclude the port from the LUT output from blocking
            // assuming name
            foreach (Tuple<Port, Port> t in clb.SwitchMatrix.GetAllArcs().Where(a => a.Item1.Name.EndsWith(outputPort)))
            {
                // no realy neccessary
                el.AddPortToBlock(clb, t.Item1);
                el.AddPortToBlock(clb, t.Item2);
                foreach (Wire w in clb.WireList.Where(w => w.LocalPip.Equals(t.Item2.Name)))
                {
                    el.AddPortToBlock(interconnect, new FPGA.Port(w.PipOnOtherTile));
                }
            }
             * */

            foreach (string stopOverPortName in this.StopOverPorts)
            {
                el.AddPortToBlock(interconnect, new Port(stopOverPortName));
            }
            
            return el;
        }

        private void AddXDLPort(LibraryElement el, string prefix, int index, FPGATypes.PortDirection dir, bool makeInputsConstant) // example; AddXDLPort(el, "I", 0, IN, true)
        {
            XDLPort p = new XDLPort();
            p.Direction = dir; // example; IN
            p.ExternalName = prefix + index; // example; I0
            p.InstanceName = "unknown";
            p.SlicePort = "unknown";
            p.ConstantValuePort = makeInputsConstant;
            el.Containter.Add(p);
        }

        public override void Undo()
        {
        }

        [Parameter(Comment = "The name of ths connection primitive")]
        public string Name = "VivadoConnectionPrimitive";

        [Parameter(Comment = "The type of BEl that is instantiated (currently only LUT6 is supported/tested")]
        public String BELType = "LUT6";

        [Parameter(Comment = "The list of BELs incorporated in this connection primitive, used for conected wires (currently only the default is supported")]
        public List<String> BELs = new List<string> { "A6LUT", "B6LUT", "C6LUT", "D6LUT" };

        [Parameter(Comment = "We need to provide the prefix for the input bel input ports (namely LUT input ports)")]
        public List<String> InputBELinputPortPrefix = new List<string> { "_L_A", "_L_B", "_L_C", "_L_D" };

        [Parameter(Comment = "Which BEL out of BELs shall be used for connecting the four wires (e.g. EE2END0..3). See command PrintLUTRouting for finding a suitable LUT")]
        public string InputBEL = "C6LUT";

        [Parameter(Comment = "Which ports in the interconnect tile shall be blocked to allow stop over routing, e.g. WW2END3 -> FAN_ALT3 -> FAN_BOUNCE3 -> IMUX_L21 -> CLBLM_IMUX21 -> CLBLM_L_C4. These port will be excluded from blocking as they are needed by this connection primitive ")]
        public List<String> StopOverPorts = new List<string> { "WW2END3", "FAN_ALT3", "FAN_BOUNCE3", "IMUX_L21" };
        
        [Parameter(Comment = "The name of the BEl (currently only 6LUT) output ports")]
        public List<String> BELOutputPorts = new List<string> { "A", "B", "C", "D" };

        [Parameter(Comment = "The slice number where to place the BELs")]
        public int SliceNumber = 0;

    }
}

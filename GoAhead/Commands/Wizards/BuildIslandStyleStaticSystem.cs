﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using GoAhead.Commands.Data;
using GoAhead.Commands.Debug;
using GoAhead.Commands.NetlistContainerGeneration;
using GoAhead.Commands.UCF;
using GoAhead.Objects;

namespace GoAhead.Commands.Wizards
{
    public class BuildIslandStyleStaticSystem : Command
    {
        protected override void DoCommandAction()
        {
            if (this.PartialAreas.Count != this.ConnectionPrimitives.Count)
            {
                throw new ArgumentException("Number of partial areas must match number of macro names");
            }

            String staticUCFFile = this.ProjectDirectory + Path.DirectorySeparatorChar + "static.ucf";
            String staticScript = this.ProjectDirectory + Path.DirectorySeparatorChar + "static.goa";
            String verificationScript = this.ProjectDirectory + Path.DirectorySeparatorChar + "check.goa";
            String staticBlocker = this.ProjectDirectory + Path.DirectorySeparatorChar + "static_blocker.xdl";

            String globalBlockerFE = Regex.Replace(staticBlocker, @"\.xdl", "_FE.xdl");

            TextWriter goaScript = new StreamWriter(staticScript, false);
            goaScript.WriteLine("# " + staticScript + " for building a static system. Generated by GoAhead");

            goaScript.WriteLine("# open the device description");
            goaScript.WriteLine(Objects.Blackboard.Instance.LastLoadCommandForFPGA);

            goaScript.WriteLine("# open the interface description(s) for the partial area(s)");
            foreach (String loadCommand in Objects.InterfaceManager.Instance.LoadCommands)
            {
                goaScript.WriteLine(loadCommand);
            }

            // there might be several forms using the same connection primitives, however the connection primitive may only be added once per script -> collect unique names
            goaScript.WriteLine("# load one or more connection primitives");
            List<String> connectionPrimitveLoadCommand = new List<String>();
            foreach (String macroName in this.ConnectionPrimitives)
            {
                if (!connectionPrimitveLoadCommand.Contains(macroName))
                {
                    LibraryElement libElement = Objects.Library.Instance.GetElement(macroName);
                    goaScript.WriteLine(libElement.LoadCommand);
                    connectionPrimitveLoadCommand.Add(macroName);
                }
            }

            // the first PrintLocationConstraintsForPlacedMacros must run with Append=false to overwrite
            bool printLocCmdFound = false;
            List<String> blockerMacroNames = new List<String>();
            for (int i = 0; i < this.PartialAreas.Count; i++)
            {
                IslandCommandBuilder builder = new IslandCommandBuilder();
                builder.IslandName = this.PartialAreas[i];
                builder.VHDLWrapper = this.PartialAreaPlaceholderVHDLFiles[i];
                builder.UCFFile = staticUCFFile;
                builder.ConnectionPrimitiveName = this.ConnectionPrimitives[i];
                builder.BuildTarget = IslandCommandBuilder.Target.Static;

                goaScript.WriteLine("##############################################################################");
                goaScript.WriteLine("# GOA Commands for the partial area " + builder.IslandName);
                goaScript.WriteLine("##############################################################################");
                foreach (Command cmd in builder.GetCommands())
                {
                    // collect blocker macros for global blocker
                    if (cmd is AddNetlistContainer)
                    {
                        AddNetlistContainer addMacroCmd = (AddNetlistContainer)cmd;
                        blockerMacroNames.Add(addMacroCmd.NetlistContainerName);
                    }
                    if (!printLocCmdFound && cmd is PrintProhibitStatementsForSelection)
                    {
                        ((PrintProhibitStatementsForSelection)cmd).Append = false;
                        ((PrintProhibitStatementsForSelection)cmd).Comment += ". this command will truncate the UCF file";
                        // following PrintLocationConstraintsForPlacedMacros will append to printCMD.FileName
                        printLocCmdFound = true;
                    }
                    // dump command to file
                    goaScript.WriteLine(cmd.ToString());
                }
                goaScript.WriteLine("# " + builder.IslandName + " done");
            }

            goaScript.WriteLine("##############################################################################");
            goaScript.WriteLine("# write the blocker XDL ");
            goaScript.WriteLine("##############################################################################");

            // generate a global blocker
            GenerateXDL genBlocker = new GenerateXDL();
            genBlocker.FileName = staticBlocker;
            genBlocker.NetlistContainerNames = blockerMacroNames;
            genBlocker.IncludePorts = false;
            genBlocker.IncludeDummyNets = true;
            genBlocker.IncludeModuleHeader = false;
            genBlocker.IncludeModuleFooter = false;
            genBlocker.Comment = "save the blocker for routing to an XDL file";
            goaScript.WriteLine(genBlocker.ToString());

            genBlocker.FileName = globalBlockerFE;
            genBlocker.IncludeModuleHeader = true;
            genBlocker.IncludeModuleFooter = true;
            genBlocker.IncludeDesignStatement = true;
            genBlocker.Comment = "save the blocker for conversions wiht xdl -xdl2ncd " + genBlocker.FileName + " -nodrc";
            goaScript.WriteLine(genBlocker.ToString());

            String expectedPortFileName = Path.GetDirectoryName(staticScript) + Path.DirectorySeparatorChar + "expected_ports.txt";
            PrintExpectedPorts printExpectedPorts = new PrintExpectedPorts();
            printExpectedPorts.Comment = "print the expected ports to a file that will be used to verify the interface implementation after route";
            printExpectedPorts.InstantiationFilter = ".*";
            printExpectedPorts.Append = false;
            printExpectedPorts.FileName = expectedPortFileName;
            goaScript.WriteLine(printExpectedPorts.ToString());

            goaScript.WriteLine("# close GoAhead");
            goaScript.WriteLine("Exit;");

            goaScript.Close();

            TextWriter checkScript = new StreamWriter(verificationScript, false);
            checkScript.WriteLine("# for verifying the static system. Generated by GoAhead");
            checkScript.WriteLine(Objects.Blackboard.Instance.LastLoadCommandForFPGA);

            OpenDesign openCmd = new OpenDesign();
            openCmd.Comment = "open the routed netlist of the static system ...";
            openCmd.FileName = "top_routed.xdl";
            checkScript.WriteLine(openCmd.ToString());

            CheckExpectedPorts checkCmd = new CheckExpectedPorts();
            checkCmd.Comment = "and check the interface: if no print outs are generated, everything is OK!";
            checkCmd.ExpectFile = expectedPortFileName;
            checkScript.WriteLine(checkCmd.ToString());

            CheckDesignAgainstBlocker checkAgainsBlocker = new CheckDesignAgainstBlocker();
            checkAgainsBlocker.Comment = "check that the blocker and static system are disjoint. Once again: if no print outs are generated, everything is OK!;";
            checkAgainsBlocker.Blocker = staticBlocker;
            checkScript.WriteLine(checkAgainsBlocker);

            checkScript.Close();
        }

        public override void Undo()
        {
            throw new NotImplementedException();
        }

        [Parameter(Comment = "The directory in which to save generated scripts")]
        public String ProjectDirectory = "";

        [Parameter(Comment = "A list of partial areas (user selections")]
        public List<String> PartialAreas = new List<String>();

        [Parameter(Comment = "A list of connections macro (one for each partial area")]
        public List<String> ConnectionPrimitives = new List<String>();

        [Parameter(Comment = "A list of VHDL files (one for each partial area")]
        public List<String> PartialAreaPlaceholderVHDLFiles = new List<String>();
    }
}
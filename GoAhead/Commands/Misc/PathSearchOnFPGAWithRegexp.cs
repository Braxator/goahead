﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using GoAhead.FPGA;
using GoAhead.Objects;

namespace GoAhead.Commands.Misc
{
    [CommandDescription(Description = "Helper command for PrintLUTRouting", Wrapper = true, Publish = true)]
    class PathSearchOnFPGAWithRegexp : CommandWithFileOutput
    {
        protected override void DoCommandAction()
        {
            Tile startTile = FPGA.FPGA.Instance.GetTile(this.StartLocation);
            Tile targetTile = FPGA.FPGA.Instance.GetTile(this.TargetLocation);

            List<List<Location>> m_paths = new List<List<Location>>();

            string startPortRegexp = "";
            foreach(string s in this.StartPortRegexps)
            {
                startPortRegexp += "(" + s + ")|";
            }
            startPortRegexp = startPortRegexp.Remove(startPortRegexp.Length - 1);

            foreach (Port startPort in startTile.SwitchMatrix.GetAllDrivers().Where(p => Regex.IsMatch(p.Name, startPortRegexp)).OrderBy(p => p.Name))
            {
                foreach (Port targetPort in targetTile.SwitchMatrix.GetDrivenPorts().Where(p => Regex.IsMatch(p.Name, this.TargetPortRegexp)).OrderBy(p => p.Name))
                {
                    PathSearchOnFPGA searchCmd = new PathSearchOnFPGA();
                    searchCmd.Forward = true;
                    searchCmd.MaxDepth = this.MaxDepth;
                    searchCmd.MaxSolutions = 1;
                    searchCmd.SearchMode = "BFS";
                    searchCmd.StartLocation = startTile.Location;
                    searchCmd.StartPort = startPort.Name;
                    searchCmd.TargetLocation = targetTile.Location;
                    searchCmd.TargetPort = targetPort.Name;
                    searchCmd.PrintBanner = false;
                    CommandExecuter.Instance.Execute(searchCmd);
                    m_paths.AddRange(searchCmd.m_paths);
                    // copy output
                    if (searchCmd.OutputManager.HasOutput)
                    {
                        this.OutputManager.WriteWrapperOutput(searchCmd.OutputManager.GetOutput());
                    }
                }
                // one blank for readability
                this.OutputManager.WriteWrapperOutput("");
            }

            return;
        }

        private bool IsUnique(IEnumerable<Tuple<Tuple<Location, Location>, List<Location>>> set)
        {
            List<Location> startPoints = new List<Location>();
            foreach (Tuple<Tuple<Location, Location>, List<Location>> c in set)
            {
                startPoints.Add(c.Item1.Item1);
            }
            int distinctCount = startPoints.Distinct().Count();
            if (distinctCount != 4) {
                return false;
            }

            List<Location> endPoints = new List<Location>();
            foreach (Tuple<Tuple<Location, Location>, List<Location>> c in set)
            {
                endPoints.Add(c.Item1.Item2);
            }
            distinctCount = endPoints.Distinct().Count();
            if (distinctCount != 4)
            {
                return false;
            }


            return true;
        }


        public override void Undo()
        {
            throw new NotImplementedException();
        }

        [Parameter(Comment = "Location string where to start")]
        public String StartLocation = "INT_X9Y39";

        [Parameter(Comment = "Start ports")]
        public List<String> StartPortRegexps = new List<string>();

        [Parameter(Comment = "Location string where to go")]
        public String TargetLocation = "INT_X11Y39";

        [Parameter(Comment = "Target port")]
        public String TargetPortRegexp = "_L_C";


        [Parameter(Comment = "The max path length")]
        public int MaxDepth = 5;
    }
}

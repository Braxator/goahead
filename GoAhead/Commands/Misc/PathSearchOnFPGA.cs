using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using GoAhead.FPGA;
using GoAhead.Objects;
using GoAhead.Code;
using GoAhead.Code.XDL;

namespace GoAhead.Commands
{
    [CommandDescription(Description = "Look for the specified path and write it to file", Wrapper = true)]
    class PathSearchOnFPGA : CommandWithFileOutput
	{
        public List<List<Location>> m_paths = new List<List<Location>>();


        protected override void DoCommandAction()
        {
            Tile startTile = FPGA.FPGA.Instance.GetTile(this.StartLocation);
            Port startPip = new Port(this.StartPort);

            Tile targetTile = FPGA.FPGA.Instance.GetTile(this.TargetLocation);
            Port targetPip = new Port(this.TargetPort);
            

            Location startLocation = new Location(startTile, startPip);
            Location targetLocation = new Location(targetTile, targetPip);

            PathSearchOnFPGA.CheckExistence(startLocation);
            PathSearchOnFPGA.CheckExistence(targetLocation);

            // print results
            if (this.PrintBanner)
            {
                this.OutputManager.WriteOutput(PathSearchOnFPGA.GetBanner(startLocation, targetLocation));
            }

            RouteNet routeCmd = new RouteNet();
            routeCmd.Watch = this.Watch;

            foreach (List<Location> path in routeCmd.Route(this.SearchMode, this.Forward, Enumerable.Repeat(startLocation, 1), targetLocation, 100, this.MaxDepth, this.KeepPathsIndependet))
            {
                if (!PathSearchOnFPGA.PathAlreadyFound(path, m_paths))
                {
                    m_paths.Add(path);

                    if (this.BlockUsedPorts)
                    {
                        foreach (Location l in path)
                        {
                            if (!l.Tile.IsPortBlocked(l.Pip))
                            {
                                l.Tile.BlockPort(l.Pip, Tile.BlockReason.Blocked);
                            }
                        }
                    }

                    this.ProgressInfo.Progress = this.ProgressStart + (int)((double)m_paths.Count / (double)this.MaxSolutions * this.ProgressShare);

                    if (m_paths.Count >= this.MaxSolutions)
                    {
                        break;
                    }
                }
            }

            if (m_paths.Count == 0)
            {
                //this.OutputManager.WriteOutput("No path found");
            }
            else if (this.OutputMode.ToUpper().Equals("CHAIN"))
            {
                this.PrintAsChain(m_paths);
            }
            else if (this.OutputMode.ToUpper().Equals("XDL"))
            {
                this.OutputManager.WriteOutput(PathToString(startLocation, targetLocation, m_paths));
            }
        }

        public static String GetBanner(Location start, Location sink)
        {
            String result = "";
            result += "---------------------------------------------------------------------------------" + Environment.NewLine;
            result += "--From " + start.Tile.Location + "." + start.Pip.Name + " to " + sink.Tile.Location + "." + sink.Pip.Name + Environment.NewLine;
            result += "---------------------------------------------------------------------------------" + Environment.NewLine;

            return result;

        }

        public static void CheckExistence(Location startLocation)
        {
            if (!startLocation.Tile.SwitchMatrix.Contains(startLocation.Pip))
            {
                throw new ArgumentException("Tile " + startLocation.Tile.Location + " does not contain port " + startLocation.Pip);
            }
        }

        public static String PathToString(Location start, Location sink, IEnumerable<List<Location>> paths)
        {
            Tile startTile = FPGA.FPGA.Instance.GetTile(start.Tile.Location);
            Port startPip = new Port(start.Pip.Name);

            Tile targetTile = FPGA.FPGA.Instance.GetTile(sink.Tile.Location);
            Port targetPip = new Port(sink.Pip.Name);

            NetOutpin op = new NetOutpin();
            op.InstanceName = start.Tile.Location;
            op.SlicePort = start.Pip.Name;

            NetInpin ip = new NetInpin();
            ip.InstanceName = sink.Tile.Location;
            ip.SlicePort = sink.Pip.Name;

            foreach (Slice s in startTile.Slices)
            {
                if (s.PortMapping.Contains(startPip))
                {
                    op.InstanceName = s.SliceName;
                }
                if (s.PortMapping.Contains(targetPip))
                {
                    ip.InstanceName = s.SliceName;
                }
            }         

            int netCount = 0;
            StringBuilder buffer = new StringBuilder();
            foreach (List<Location> path in paths.OrderBy(l => l.Count))
            {
                XDLNet n = new XDLNet(path);
                n.Name = "path_" + netCount++;
                n.Add(op);
                n.Add(ip);

                buffer.AppendLine(n.ToString());
            }
            return buffer.ToString();
        }

        private void PrintAsChain(List<List<Location>> paths)
        {
            Dictionary<int, int> maxSegmentLength = new Dictionary<int, int>();
            foreach (List<Location> path in paths)
            {
                for (int i = 0; i < path.Count; i++)
                {
                    if (!maxSegmentLength.ContainsKey(i))
                    {
                        maxSegmentLength.Add(i, 0);
                    }
                    maxSegmentLength[i] = Math.Max(maxSegmentLength[i], path[i].ToString().Length);
                }
            }

            foreach (List<Location> path in paths.OrderBy(l => l.Count))
            {
                StringBuilder buffer = new StringBuilder();
                for (int i = 0; i < path.Count; i++)
                {
                    String nextLine = path[i].ToString();
                    nextLine = nextLine.PadRight(maxSegmentLength[i]);
                    nextLine += (i < path.Count - 1 ? " -> " : "");

                    // UltraScale mapping
                    if (i == path.Count - 1)
                    {
                        foreach (string v in VariableManager.Instance.GetAllVariableNames())
                        {
                            try
                            {
                                Regex r = new Regex(VariableManager.Instance.GetValue(v));
                                if (r.IsMatch(path[i].Pip.Name))
                                {
                                    nextLine += " (" + v + ")";
                                }
                            }
                            catch
                            {

                            }
                        }
                    }
                    buffer.Append(nextLine);

                }
                if (path.Count != 0)
                {
                    this.OutputManager.WriteOutput(buffer.ToString());
                }
            }
        }

        public static bool PathAlreadyFound(List<Location> path, List<List<Location>> foundPaths)
        {
            foreach (List<Location> fp in foundPaths.Where(p => p.Count == path.Count))
            {
                bool equal = true;
                for (int i = 0; i < path.Count; i++)
                {
                    if (!fp[i].Equals(path[i]))
                    {
                        equal = false;
                        break;
                    }
                }
                if (equal)
                {
                    return true;
                }
            }

            return false;
        }

        public override void Undo()
        {
            throw new ArgumentException("The method or operation is not implemented.");
        }

        [Parameter(Comment = "The path search modue (BFS, DFS, A*)")]
        public String SearchMode = "BFS";

        [Parameter(Comment = "Whether to search in forward (true) or backward direction (false)")]
        public bool Forward = true;

        [Parameter(Comment = "Dont use!")]
        public bool KeepPathsIndependet = false;

        [Parameter(Comment = "Whether to block used ports after finding a path (set to true for incremental search)")]
        public bool BlockUsedPorts = false;      
        
        [Parameter(Comment = "How to print out the results (XDL, CHAIN)")]
        public String OutputMode = "CHAIN";

        [Parameter(Comment = "Location string where to start")]
        public String StartLocation = "INT_X9Y39";

        [Parameter(Comment = "Start port")]
        public String StartPort = "EL2BEG0";

        [Parameter(Comment = "Location string where to go")]
        public String TargetLocation = "INT_X11Y39";

        [Parameter(Comment = "Target port")]
        public String TargetPort = "EL2BEG0";

        [Parameter(Comment = "Stop after the first MaxSolutions path")]
        public int MaxSolutions = 5;

        [Parameter(Comment = "The max path length")]
        public int MaxDepth = 5;

        [Parameter(Comment = "Print Banner")]
        public bool PrintBanner = true;
	}

 
}



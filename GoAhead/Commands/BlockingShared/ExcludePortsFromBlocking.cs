﻿using System;
using System.Linq;
using GoAhead.FPGA;
using GoAhead.Objects;

namespace GoAhead.Commands.BlockingShared
{
    [CommandDescription(Description = "Prevent future blocking of the specified port on a specific tile")]
    class ExcludePortsFromBlocking : Command
    {
        protected override void DoCommandAction() 
        {
            ExcludePortsFromBlocking.BlockPortAndReachablePorts(this.Location, this.PortName, this.CheckForExistence, this.IncludeAllPorts, this.TunnelDirection);
        }

        public static void BlockPortAndReachablePorts(String location, String portName, bool checkForExistence, bool includeAllPorts, string tunnel)
        {
            Tile where = FPGA.FPGA.Instance.GetTile(location);

            // block 
            ExcludePortsFromBlocking.BlockPort(where, portName, checkForExistence, tunnel);

            // follow wire if required
            if (includeAllPorts && where.WireList != null)
            {
                foreach (Location l in Navigator.GetDestinations(where.Location, portName))
                {
                    ExcludePortsFromBlocking.BlockPort(l.Tile, l.Pip.Name, checkForExistence, tunnel);
                }

            }
        }

        public static void BlockPort(Tile where, String portName, bool checkForExistence, String tunnel)
        {
            if (checkForExistence && !where.SwitchMatrix.Contains(portName))
            {
                throw new ArgumentException("Tile " + where.Location + " does not contain port " + portName);
            }
            else if (where.SwitchMatrix.Contains(portName) && !where.IsPortBlocked(portName))
            {
                switch (tunnel)
                {
                    case "North":
                        where.BlockPort(portName, Tile.BlockReason.UsedByNorthernTunnel);
                        break;
                    case "East":
                        where.BlockPort(portName, Tile.BlockReason.UsedByEasternTunnel);
                        break;
                    case "South":
                        where.BlockPort(portName, Tile.BlockReason.UsedBySouthernTunnel);
                        break;
                    case "West":
                        where.BlockPort(portName, Tile.BlockReason.UsedByWesternTunnel);
                        break;
                    default:
                        where.BlockPort(portName, Tile.BlockReason.ExcludedFromBlocking);
                        break;
                }
            }                     

        }

        public override void Undo() 
        {
            throw new NotImplementedException();
        }

        [Parameter(Comment = "The port name to be blocked, e.g. E2BEG5")]
        public String PortName = "E2BEG5";

        [Parameter(Comment = "The location string, e.g. INT_X2Y34")]
        public String Location = "INT_X2Y34";

        [Parameter(Comment = "Check for existence of ports")]
        public bool CheckForExistence = false;

        [Parameter(Comment = "Whether to follow up wires on a port and also block the reachable ports in other tiles")]
        public bool IncludeAllPorts = true;

        [Parameter(Comment = "The direction of the tunnel: North, East, South or West.")]
        public String TunnelDirection = "None";
    }
}

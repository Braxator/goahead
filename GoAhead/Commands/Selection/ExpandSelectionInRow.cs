﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoAhead.FPGA;

namespace GoAhead.Commands.Selection
{
    abstract class ExpandSelectionInRow : Command
    {
        protected void ExpandSelection(Tile start, int increment, bool startInUserSelection)
        {
            int x = start.TileKey.X;
            int y = start.TileKey.Y;

            // get the lower left and lower right boundary
            Tile ll = FPGA.TileSelectionManager.Instance.GetUserSelectedTile(".*", this.UserSelectionType, FPGATypes.Placement.LowerLeft);
            Tile lr = FPGA.TileSelectionManager.Instance.GetUserSelectedTile(".*", this.UserSelectionType, FPGATypes.Placement.LowerRight);

            // 1) if the start tile is in the UserSelection, loop as long till the boundaries of the UserSelection are reached (inside)
            // 2) if the start tile is outside the UserSelection, loop as long till the boundaries of the UserSelection are reached (outside)
            while (FPGA.FPGA.Instance.Contains(x, y))
            {
                // get the tile based on (x,y) position
                Tile other = FPGA.FPGA.Instance.GetTile(x, y);

                bool tileStillInUserSelection = FPGA.TileSelectionManager.Instance.IsUserSelected(other.TileKey, this.UserSelectionType) && startInUserSelection;
                bool tileStillOutUserSelection = !FPGA.TileSelectionManager.Instance.IsUserSelected(other.TileKey, this.UserSelectionType) && !startInUserSelection;

                if (tileStillInUserSelection || tileStillOutUserSelection)
                {
                    this.m_expansion.Add(other);
                }

                bool userSelectionBorderCrossed =
                   !FPGA.TileSelectionManager.Instance.IsUserSelected(other.TileKey, this.UserSelectionType) && startInUserSelection ||
                    FPGA.TileSelectionManager.Instance.IsUserSelected(other.TileKey, this.UserSelectionType) && !startInUserSelection;

                if (userSelectionBorderCrossed)
                {
                    break;
                }

                x += increment;
            }

            FPGA.TileSelectionManager.Instance.SelectionChanged();
        }

        protected bool StartInUserSelection()
        {
            // get the number of selected tiles
            // note that when we select 3x3 CLB tiles (9 tiles), we have a total of 18 tiles, since the CLB tiles come in pairs with INT tiles
            int selectTiles = FPGA.TileSelectionManager.Instance.NumberOfSelectedTiles;
            if (selectTiles == 0)
            {
                throw new ArgumentException("No tiles selected");
            }

            // check if the current selection (example; ConnMacroPlace) are all located within the given UserSelectionType (Example; PartialArea_All)
            bool allUserSelected = FPGA.TileSelectionManager.Instance.GetSelectedTiles().All(t => FPGA.TileSelectionManager.Instance.IsUserSelected(t.TileKey, this.UserSelectionType));
            bool noneUserSelected = FPGA.TileSelectionManager.Instance.GetSelectedTiles().All(t => !FPGA.TileSelectionManager.Instance.IsUserSelected(t.TileKey, this.UserSelectionType));

            if (!allUserSelected && !noneUserSelected)
            {
                throw new ArgumentException("Ensure that either all or none tile are user selected");
            }

            return allUserSelected;
        }


        protected void AddTiles()
        {
            foreach (Tile t in this.m_expansion)
            {
                if (!FPGA.TileSelectionManager.Instance.IsSelected(t.TileKey))
                {
                    FPGA.TileSelectionManager.Instance.AddToSelection(t.TileKey, false);
                }
            }
            FPGA.TileSelectionManager.Instance.SelectionChanged();
        }


        protected List<Tile> m_expansion = new List<Tile>();

        [Parameter(Comment = "The name of the user selection that limits the extent of the selection expanding")]
        public String UserSelectionType = "PartialArea";
    }
}

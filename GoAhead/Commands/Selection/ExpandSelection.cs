﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using GoAhead.FPGA;
using GoAhead.Objects;

namespace GoAhead.Commands.Selection
{
    [CommandDescription(Description = "Expand the current selection to pairs of CLB/CLEXLM and INT tiles", Wrapper = false)]
    class ExpandSelection : AddToSelectionCommand
    {
        protected override void DoCommandAction()
        {
            // TODO: what does this if-block ?
            if (!FPGA.RAMSelectionManager.Instance.HasMappings)
            {
                FPGA.RAMSelectionManager.Instance.UpdateMapping();
            }

            List<Tile> expansion = new List<Tile>();

            // get tiles to add
            foreach (Tile t in FPGA.TileSelectionManager.Instance.GetSelectedTiles())
            {
                if (IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.CLBRegex))
                {
                    Tile intTile = FPGA.FPGATypes.GetInterconnectTile(t);

                    if (intTile.LocationX == t.LocationX && intTile.LocationY == t.LocationY && IdentifierManager.Instance.IsMatch(intTile.Location, IdentifierManager.RegexTypes.InterconnectRegex))
                    {
                        if (this.AddToSelection(intTile))
                        {
                            expansion.Add(intTile);
                        }
                    }
                }

                if (IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.InterconnectRegex))
                {
                    foreach (Tile clbTile in FPGA.FPGATypes.GetCLTile(t))
                    {
                        if (clbTile.LocationX == t.LocationX && clbTile.LocationY == t.LocationY && IdentifierManager.Instance.IsMatch(clbTile.Location, IdentifierManager.RegexTypes.CLBRegex))
                        {
                            if (this.AddToSelection(clbTile))
                            {
                                expansion.Add(clbTile);
                            }
                        }
                    }
                }

                //TODO: What does this if-block? How are the BRAM tiles organized?
                if (FPGA.RAMSelectionManager.Instance.HasMapping(t))
                {
                    foreach (Tile ramBlockMember in FPGA.RAMSelectionManager.Instance.GetRamBlockMembers(t))
                    {
                        if (this.AddToSelection(ramBlockMember))
                        {
                            expansion.Add(ramBlockMember);
                        }
                    }
                }
            }

            // expand selection
            foreach (Tile t in expansion)
            {
                FPGA.TileSelectionManager.Instance.AddToSelection(t.TileKey, false);
            }

            FPGA.TileSelectionManager.Instance.SelectionChanged();
        }

        private bool AddToSelection(Tile where)
        {
            if (FPGA.FPGA.Instance.Contains(where.Location))
            {
                return (!FPGA.TileSelectionManager.Instance.IsSelected(where.TileKey));
            }
            else
            {
                return false;
            }
        }

        public override void Undo()
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoAhead.FPGA;

namespace GoAhead.Commands.Selection
{
    class ExpandSelectionLeftAndRight : ExpandSelectionInRow
    {
        protected override void DoCommandAction()
        {
            bool startInUserSelection = this.StartInUserSelection();

            // expand for each selected tile (for example in ConnMacroPlace) the selection to left (-1) and right (1) in the given user selection (for example in PartialArea_All)
            foreach (Tile t in FPGA.TileSelectionManager.Instance.GetSelectedTiles())
            {
                this.ExpandSelection(t, -1, startInUserSelection);
                this.ExpandSelection(t, 1, startInUserSelection);
            }

            this.AddTiles();

            FPGA.TileSelectionManager.Instance.SelectionChanged();
        }

        public override void Undo()
        {
            throw new NotImplementedException();
        }
    }
}

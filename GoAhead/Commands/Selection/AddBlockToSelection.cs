﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using GoAhead.FPGA;
using GoAhead.Objects;

namespace GoAhead.Commands.Selection
{
    [CommandDescription(Description = "Add CLBS, INT, DSP, and BRAM tiles in the specified rectangle to the current selection", Wrapper = true)]
    class AddBlockToSelection : AddToSelectionCommand // AddToSelectionCommand is empty and inherits from Command
    {
        public AddBlockToSelection()
        { 
        }

        public AddBlockToSelection(int x1, int y1, int x2, int y2)
        {
            // run form min to max
            int startX = Math.Min(x1, x2);
            int endX = Math.Max(x1, x2);

            int startY = Math.Min(y1, y2);
            int endY = Math.Max(y1, y2);

            int maxX = Int32.MinValue;
            int minX = Int32.MaxValue;
            int maxY = Int32.MinValue;
            int minY = Int32.MaxValue;

            Tile ul = null;
            Tile lr = null;

            for (int x = startX; x <= endX; x++)
            {
                for (int y = startY; y <= endY; y++)
                {
                    // click done out of FPGA range
                    if (!FPGA.FPGA.Instance.Contains(x, y))
                    {
                        continue;
                    }

                    Tile t = FPGA.FPGA.Instance.GetTile(x, y);
                    this.UpdateUpperLeftAndLowerRightCorner(ref maxX, ref minX, ref maxY, ref minY, ref ul, ref lr, t);
                }
            }

            // clicked in BRAM/DSP block?
            if( (ul == null || lr == null) && FPGA.FPGA.Instance.Contains(startX, endY))
            {
                Tile t = FPGA.FPGA.Instance.GetTile(startX, endY);
                if (FPGA.RAMSelectionManager.Instance.HasMapping(t))
                {
                    foreach (Tile member in FPGA.RAMSelectionManager.Instance.GetRamBlockMembers(t))
                    {
                        this.UpdateUpperLeftAndLowerRightCorner(ref maxX, ref minX, ref maxY, ref minY, ref ul, ref lr, member);
                    }
                }
            }
            
            if(ul == null || lr == null)
            {
                throw new ArgumentException("Could not derive upper left and lower right anchor");
            }


            // go from CLB to INT
            if (IdentifierManager.Instance.IsMatch(ul.Location, IdentifierManager.RegexTypes.CLBRegex))
            {
                ul = FPGATypes.GetInterconnectTile(ul);
            }
            if (IdentifierManager.Instance.IsMatch(lr.Location, IdentifierManager.RegexTypes.CLBRegex))
            {
                lr = FPGATypes.GetInterconnectTile(lr);
            }


            this.UpperLeftTile = ul.Location;
            this.LowerRightTile = lr.Location;
        }

        private void UpdateUpperLeftAndLowerRightCorner(ref int maxX, ref int minX, ref int maxY, ref int minY, ref Tile ul, ref Tile lr, Tile t)
        {
            if (this.Consider(t))
            {
                if (t.LocationX <= minX && t.LocationY >= maxY)
                {
                    minX = t.LocationX;
                    maxY = t.LocationY;
                    ul = t;
                }
                if (t.LocationX >= maxX && t.LocationY <= minY)
                {
                    maxX = t.LocationX;
                    minY = t.LocationY;
                    lr = t;
                }
            }
        }

        protected override void DoCommandAction()
        {
            Tile ul = this.GetCorner(this.UpperLeftTile, FPGATypes.Direction.West);
            Tile lr = this.GetCorner(this.LowerRightTile, FPGATypes.Direction.East);

            // return when either the upper-left or lower-right tile cannot be resolved
            if (ul == null || lr == null)
            {
                this.OutputManager.WriteWarning("Can not resolve " + this.UpperLeftTile + " or " + this.LowerRightTile);
                return;
            }

            // at this moment only upper-left and lower-right is supported, not lower-left and upper-right

            // TODO sort lu kann auch unten links und lr kann auch oben rechts sein
            /*
            ClearSelection;
            AddBlockToSelection UpperLeftTile=INT_X21Y174 LowerRightTile=INT_X23Y170;
            ExpandSelection;
            [12:01:34 PM] diko: ClearSelection;
            AddBlockToSelection UpperLeftTile=INT_X23Y174 LowerRightTile=INT_X21Y170;
            ExpandSelection;
            */

            // get start and end position of x-axis
            int startX = Math.Min(ul.TileKey.X, lr.TileKey.X);
            int endX = Math.Max(ul.TileKey.X, lr.TileKey.X);

            // get start and end position of y-axis
            int startY = Math.Min(ul.TileKey.Y, lr.TileKey.Y);
            int endY = Math.Max(ul.TileKey.Y, lr.TileKey.Y);

            Regex filter = new Regex(this.Filter);

            // loop through the XY-plane
            // note that we gave only two tiles (upper-left and lower-right) and it makes a squared form out of it
            // thus, only squared reconfigurable regions are supported right now
            for (int x = startX; x <= endX; x++)
            {
                for (int y = startY; y <= endY; y++)
                {
                    // original comment: click done out of FPGA range 
                    // selected outside of the FPGA range
                    if (!FPGA.FPGA.Instance.Contains(x, y))
                    {
                        continue;
                    }

                    // get tile based on the X and Y position 
                    Tile t = FPGA.FPGA.Instance.GetTile(x, y);

                    // check if the tile fullfills the filter requirements (by default it allows all tiles)
                    if (!filter.IsMatch(t.Location))
                    {
                        continue;
                    }

                    // check if the tile is either a CLB, BRAM, DSP or Interconnect
                    if (this.Consider(t))
                    {
                        // deselect or add the selected tile 
                        if (FPGA.TileSelectionManager.Instance.IsSelected(x, y))
                        {
                            FPGA.TileSelectionManager.Instance.RemoveFromSelection(t.TileKey, false);
                        }
                        else
                        {
                            FPGA.TileSelectionManager.Instance.AddToSelection(t.TileKey, false);
                        }
                    }                
                }
            }

            // Notify to registered classes that the selection has changed
            FPGA.TileSelectionManager.Instance.SelectionChanged();
        }

        private Tile GetCorner(String identifier, FPGATypes.Direction dir)
        {
            Tile tile = null;

            // check if it is a valid identifier, example; "INT_L_X40Y44"
            if (FPGA.FPGA.Instance.Contains(identifier))
            {      
                // get the tile
                tile = FPGA.FPGA.Instance.GetTile(identifier);
            }
            else if (identifier.Contains("_"))
            {
                int split = identifier.IndexOf('_');
                String prefix = identifier.Substring(0, split + 1);
                String suffix = identifier.Substring(split, identifier.Length - split);
                tile = FPGA.FPGA.Instance.GetAllTiles().FirstOrDefault(t => t.Location.StartsWith(prefix) && t.Location.EndsWith(suffix));
                // if we can not resolve the identifer, triggger error handling in Do
                if(tile==null)
                {
                    return null;
                }
            }
            else 
            {
                return null;
            }

            List<Tile> otherTiles = new List<Tile>();            
            otherTiles.Add(tile);

            // original comment: there might be more left interconnects
            //
            // A CLB tile and switch matrix come in pairs in the Zynq architecture. In this if-else statement the 
            // corresponding switch matrix/CLB tile is added to the tile list to check the most right or left block (either switch matrix or CLB tile)
            if (IdentifierManager.Instance.IsMatch(tile.Location, IdentifierManager.RegexTypes.CLBRegex))
            {
                otherTiles.Add(FPGATypes.GetInterconnectTile(tile));
            }
            else if (IdentifierManager.Instance.IsMatch(tile.Location, IdentifierManager.RegexTypes.InterconnectRegex))
            {
                foreach(Tile o in FPGATypes.GetCLTile(tile))
                {
                    otherTiles.Add(o);
                }
            }

            // Lambda expressions
            // (input-parameters) => expression
            // Example
            // double lowest_price = list1.Min(car => car.price);
            // 
            // The statement loops throught the list (list1) and for every element is checks the price (car.price), the lowest
            // value is the minimum and thus that value will be returned

            if(dir == FPGATypes.Direction.West)
            {
                //get the minimum X position 
                int min = otherTiles.Min(t => t.TileKey.X);

                //return the minimum X position, or null when the list is empty
                return otherTiles.FirstOrDefault(t => t.TileKey.X == min);
            }
            else if (dir == FPGATypes.Direction.East)
            {
                //get the maximum X position 
                int max = otherTiles.Max(t => t.TileKey.X);

                //return the maximum X position, or null when the list is empty
                return otherTiles.FirstOrDefault(t => t.TileKey.X == max);
            }
            else
            {
                throw new ArgumentException(this.GetType().Name + ".GetCorner only supports East and West");
            }
        }

        private bool Consider(Tile t)
        {
            return 
                IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.CLBRegex) ||
                IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.InterconnectRegex) ||
                IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.BRAMRegex) ||
                IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.DSPRegex);
        }

        public override void Undo()
        {
            throw new NotImplementedException();
        }

        [Parameter(Comment = "Only selected those tiles in the given range that match this filter", PrintParameter=false)]
        public String Filter = ".*";
        
        [Parameter(Comment = "The upper left tile")]
        public String UpperLeftTile = "";

        [Parameter(Comment = "The lower right tile")]
        public String LowerRightTile = "";
    }
}

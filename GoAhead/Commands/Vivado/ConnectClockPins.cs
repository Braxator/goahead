﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using GoAhead.Commands.NetlistContainerGeneration;
using GoAhead.Objects;
using GoAhead.Code;
using GoAhead.FPGA;

namespace GoAhead.Commands.Vivado
{
    [CommandDescription(Description = "Generate TCL code to connect all clock pins of all currently selected tiles to the given clock net", Wrapper = false, Publish = true)]
    class ConnectClockPins : CommandWithFileOutput
    {
        protected override void DoCommandAction()
        {
            // Example
            // [A-D]FF
            // Means that the string starts with A,B,C or D, and ends with "FF"
            // As consequence, only the flip-flops are connected to the clock   
            Regex BELRegexp = new Regex(this.BELs, RegexOptions.Compiled);

            // TODO: what about RAM/DSP, do they need any clock signal?

            //loop through all the selected tiles 
            foreach (Tile tile in FPGA.TileSelectionManager.Instance.GetSelectedTiles())
            {
                // each CLB tile has 2 slices (in Zynq architecture)
                foreach (Slice slice in tile.Slices)
                {
                    // connect the clock signal to the BELs that match the regex expression (in the example above, only consider flip-flops)
                    foreach (string bel in slice.Bels.Where(b => BELRegexp.IsMatch(b)))
                    {
                        // instantiation name
                        string instName = slice.SliceName + "_" + bel;

                        // create_cell: create cell in the current design
                        // -reference: library cell or design which cells reference
                        // FDRE: Flip-Flop with Clock Enable and Synchronous Reset
                        // Lastly, the name of the cell to create
                        this.OutputManager.WriteOutput("create_cell -reference FDRE	" + instName);

                        // place_cell: move or place one or more instances to new locations. Sites and cells are required to be listed in the right order
                        // and there should be same number of sites as number of cells
                        // Argument: A list of cells and sites in the interleaved order
                        this.OutputManager.WriteOutput("place_cell " + instName + " " + slice.SliceName + "/" + bel);

                        // create_pin: create pins in the current design
                        // -direction: pin direction, values: IN, OUT, INOUT
                        // Lastly, the names of pins to create
                        this.OutputManager.WriteOutput("create_pin -direction IN " + instName + "/" + this.ClockPin);

                        // connect_net: connect a net to pins or ports
                        // -net: net to connect the given objects
                        // -objects: list of pin and port objects to connect
                        this.OutputManager.WriteOutput("connect_net -hier -net " + this.ClockNetName + " -objects {" + instName + "/" + this.ClockPin + "}");
                    }
                }
            }
        }

        public override void Undo()
        {
            throw new NotImplementedException();
        }

        // TODO:
        // In our current script we are using "C" as clock name, however the flip-flop has "CK" as clock pin name
        // Is this a problem? Apparently not, as the flip-flops gets connected to clock net

        [Parameter(Comment = "The name of the clock pin to connect")]
        public String ClockPin = "CK";

        // BEL
        // Basic ELement
        // Examples; flip-flop, LUT

        [Parameter(Comment = "The slice bels")] // The BELs that should be connected to the clock net (flip-flops!)
        public String BELs = "[A-D]FF";

        [Parameter(Comment = "The name of the clock net that the clock pins will be added to")]
        public String ClockNetName = "clk";
    }
}

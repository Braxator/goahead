﻿using System;

namespace GoAhead.Commands
{
    [CommandDescription(Description = "Create an alias for which upon call the semicolon seperated commands are executed", Wrapper = false)]
    public class AddAlias : Command
    {
        // AddAlias is used to create "new" commands. We pass a name as argument. This name is the name of the "new" command. We also pass a 
        // list of commands. When the new command name is executed, the list of commands will be executed.

        protected override void DoCommandAction()
        {
            Objects.AliasManager.Instance.AddAlias(this.AliasName, this.Commands);
        }

        public override void Undo()
        {
            throw new NotImplementedException();
        }

        [Parameter(Comment = "The name of alias")]
        public String AliasName = "select_an_area";

        [Parameter(Comment = "A semicolon seperated list of GoAhead commands that will be executed if the alias name is used as a command.")]
        public String Commands = "ClearSelection;AddToSelectionXY UpperLeftX=138 UpperLeftY=85 LowerRightX=145 LowerRightY=125;ExpandSelection;";
    }
}
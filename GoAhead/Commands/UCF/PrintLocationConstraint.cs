﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using GoAhead.FPGA;
using GoAhead.Objects;

namespace GoAhead.Commands.UCF
{
    [CommandDescription(Description = "Output a location constraint")]
    class PrintLocationConstraint : UCFCommand
    {
        public PrintLocationConstraint()
        {
        }

        protected override void DoCommandAction()
        {
            // check if the location corresponds with a CLB tile
            if (IdentifierManager.Instance.IsMatch(this.Location, IdentifierManager.RegexTypes.CLBRegex))
            {
                Tile where = FPGA.FPGA.Instance.GetTile(this.Location);

                // SliceNumber cannot be equal (due to index starts with 0) or larger than the total number of slices in the CLB
                if (this.SliceNumber >= where.Slices.Count)
                {
                    throw new ArgumentException("Slice index exceeded on " + where);
                }

                String constraint = "";
                if (FPGA.FPGA.Instance.BackendType == FPGATypes.BackendType.ISE)
                {
                    this.OutputManager.WriteUCFOutput("INST \"" + this.InstanceName + "\" LOC = \"" + where.Slices[this.SliceNumber] + "\"; # generated_by_GoAhead");
                }
                else if (FPGA.FPGA.Instance.BackendType == FPGATypes.BackendType.Vivado)
                {
                    // original comment: place_cell inst_PartialSubsystem/inst_3 SLICE_X6Y66/C6LUT
                    //
                    // place_cell: Move or place on or more instances to new locations. Sites and cells are required to be listed in the right order and there should
                    // be same number of sites as number of cells
                    //
                    // arguments: a list of cells and sites in the interleaved order
                    //
                    // With this command we make sure that the LUT is binded to the one that we specify here
                    this.OutputManager.WriteUCFOutput("place_cell " + this.InstanceName + " " + where.Slices[this.SliceNumber] + "/" + this.BEL + "; # generated_by_GoAhead");
                    // LOCK_PINS can only be applied to LUTs, for UltraScale we used FlipFops as well 
                    if (this.BEL.EndsWith("LUT"))
                    {
                        // LOCK_PINS is a cell property used to specify the mapping of the logic LUT inputs (i0,i1,i2,..) to physical LUT inputs (A6,A5,A4,..) on Xilinx device resource. A common
                        // use is to force timing-critical LUT inputs to be mapped to the fastest A6 and A5 physical LUT inputs.
                        this.OutputManager.WriteUCFOutput("set_property LOCK_PINS {I0:A1 I1:A2 I2:A3 I3:A4 I4:A5 I5:A6} [get_cells " + this.InstanceName + "]; # generated_by_GoAhead");
                    }
                    // DONT_TOUCH directs the tool to not optimize a user hierarchy or instantiated component so that optimization does not occur accross it boundary. While this can assist
                    // floorplanning, analysis, and debugging, it may inhibit optimization, resulting in a larger, slower design
                    this.OutputManager.WriteUCFOutput("set_property DONT_TOUCH TRUE [get_cells " + this.InstanceName + "]; # generated_by_GoAhead");
                    /*
                    // set_property LOC SLICE_X33Y15 [get_cells My_LUT6_inst]
                    this.OutputManager.WriteUCFOutput("set_property LOC " + where.Slices[this.SliceNumber] + " [get_cells " + this.InstanceName + "]; # generated_by_GoAhead");
                    // set_property LOCK_PINS {I0:A1 I1:A2 I2:A3 I3:A4} [get_cells inst_PartialSubsystem/inst_0]
                    this.OutputManager.WriteUCFOutput("set_property LOCK_PINS {I0:A1 I1:A2 I2:A3 I3:A4 I4:A5 I5:A6} [get_cells " + this.InstanceName + "]; # generated_by_GoAhead");
                     * */
                }

                // promt ucf to text box
                this.OutputManager.WriteUCFOutput(constraint);
            }
        }

        public override void Undo()
        {
        }
      
        [Parameter(Comment = "The location of the file where toplace the ucf in")]
        public String Location = "";

        [Parameter(Comment = "The index of the slice the blocker will use")]
        public int SliceNumber = 0;

        [Parameter(Comment = "The instance name")]
        public String InstanceName= "";

        [Parameter(Comment = "The slice element name (Vivado only)")]
        public String BEL = "";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using GoAhead.Objects;
using GoAhead.FPGA;

namespace GoAhead.Commands.UCF
{
    [CommandDescription(Description="Print UCF (ISE) or TCL (Vivado) placement constraints for all instantiated library elements", Wrapper=true)]
    class PrintLocationConstraints : UCFCommand
    {
        protected override void DoCommandAction()
        {
            // original comment: UCF/TCL location constraints
            //
            // loop through all the library instantiations
            // when using VivadoConnectionPrimitive, and 3x3, we have 36 library instantiations, since each for each CLB 4 LUTs are used
            foreach (LibElemInst inst in Objects.LibraryElementInstanceManager.Instance.GetAllInstantiations().Where(i => Regex.IsMatch(i.InstanceName, this.InstantiationFilter)))
            {
                LibraryElement libEl = Objects.Library.Instance.GetElement(inst.LibraryElementName);

                PrintLocationConstraint getLoc = new PrintLocationConstraint();
                getLoc.Location = inst.AnchorLocation; // example: "CLBLL_L_X40Y17"
                getLoc.SliceNumber = inst.SliceNumber; // example: 1
                getLoc.InstanceName = this.HierarchyPrefix + inst.InstanceName; // example: inst_ConnMacro/inst_0
                getLoc.BEL = libEl.BEL; // example: "A6LUT"
                getLoc.Mute = this.Mute; // example: false

                // This command is executed for each instantiated library element and the tcl command are added to the static_location_constraint.tcl file
                CommandExecuter.Instance.Execute(getLoc); 

                // copy output
                if (getLoc.OutputManager.HasUCFOutput)
                {
                    this.OutputManager.WriteWrapperOutput(getLoc.OutputManager.GetUCFOuput());
                }
            }
        }

        public override void Undo()
        {
        }

        [Parameter(Comment = "Only consider those macro instantiations with this prefix")]
        public String InstantiationFilter = "";

        [Parameter(Comment = "The prefix to insert before each instance name. The prefix can be used to insert hierarchies, e.g. partial_subsystem/")]
        public String HierarchyPrefix = "";
    }
}

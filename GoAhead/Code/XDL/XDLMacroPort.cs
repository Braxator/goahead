using System;
using System.Text.RegularExpressions;
using GoAhead.FPGA;

namespace GoAhead.Code.XDL
{
    public class XDLMacroPort
    {
        /// <summary>
        /// port "$extpin" "SLICE_X57Y77" "B6";
        /// </summary>
        /// <param name="portName">$extpin</param>
        /// <param name="port">SLICE_X57Y77</param>
        /// <param name="where">B6</param>
        public XDLMacroPort(String portName, Port port, Slice where)
        {
            this.m_portName = portName;
            this.m_port = port;
            this.m_where = where;
            this.m_sliceName = "";
            //if removing L_ or M_ does not work, promote m_dummyNetPortName to the addport command
            if (Regex.IsMatch(port.ToString(), "^(L|M)_"))
            {
                this.m_dummyNetPortName = port.ToString().Substring(2, port.ToString().Length - 2);
            }
            else if (Regex.IsMatch(port.ToString(), "^CLB[L|M][L|M]_([L|M])+_"))
            {
                // virtex 6
                this.m_dummyNetPortName = Regex.Replace(port.ToString(), "^CLB[L|M][L|M]_([L|M])+_", "");
            }
            else
            {
                this.m_dummyNetPortName = port.ToString();
                // throw new NotImplementedException("Removing L_ or M_ failed in XDLPort");
            }
        }

        /// <summary>
        /// Overwrite the instance name on which this port resides
        /// </summary>
        /// <param name="portName"></param>
        /// <param name="port"></param>
        /// <param name="where"></param>
        /// <param name="sliceName"></param>
        public XDLMacroPort(String portName, Port port, Slice where, String sliceName)
        {
            this.m_portName = portName;
            this.m_port = port;
            this.m_where = where;
            this.m_sliceName = sliceName;
            this.m_dummyNetPortName = port.ToString();
        }

        /// <summary>
        /// might be changed to add indeces
        /// </summary>
        public String PortName
        {
            get { return m_portName; }
            set { m_portName = value; }
        }

        public Slice Slice
        {
            get { return this.m_where; }
        }

        public String SliceName
        {
            get { return String.IsNullOrEmpty(this.m_sliceName) ? this.Slice.ToString() : this.m_sliceName; }
        }

        public Port Port
        {
            get { return this.m_port; }
        }

        public String DummyNetPortName
        {
            get { return this.m_dummyNetPortName; }
        }

        private String m_portName;
        private readonly Port m_port;
        private readonly Slice m_where;
        private readonly String m_sliceName;
        private readonly String m_dummyNetPortName;
    }
}
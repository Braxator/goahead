﻿using System;

namespace GoAhead.Code.XDL
{
    [Serializable]
    public class XDLPip
    {
        public XDLPip(String location, String from, String connectionOperator, String to)
        {
            this.m_location = location;
            this.m_from = from;
            this.m_to = to;
            this.m_connectionOperator = connectionOperator;

            /*
            this.m_locationKey = FPGA.FPGA.Instance.IdentifierListLookup.GetKey(location);
            this.m_fromKey = FPGA.FPGA.Instance.IdentifierListLookup.GetKey(from);
            this.m_connectionOperatorKey = FPGA.FPGA.Instance.IdentifierListLookup.GetKey(connectionOperator);
            this.m_toKey = FPGA.FPGA.Instance.IdentifierListLookup.GetKey(to);
            this.m_comment = comment;
             * */

            //Tile t = FPGA.FPGA.Instance.GetTile(location);
            //this.m_tileKey = t.TileKey;
        }

        /*
        public TileKey TileKey
        {
            get { return this.m_tileKey; }
        }
        */

        public String Location
        {
            get { return this.m_location; }
            //get { return FPGA.FPGA.Instance.IdentifierListLookup.GetIdentifier(this.m_locationKey); }
        }

        public String From
        {
            get { return this.m_from; }
            //get { return FPGA.FPGA.Instance.IdentifierListLookup.GetIdentifier(this.m_fromKey); }
        }

        public String To
        {
            get { return this.m_to; }
            //get { return FPGA.FPGA.Instance.IdentifierListLookup.GetIdentifier(this.m_toKey); }
        }

        public uint FromKey
        {
            get { return FPGA.FPGA.Instance.IdentifierListLookup.GetKey(this.m_from); }
        }

        public uint ToKey
        {
            get { return FPGA.FPGA.Instance.IdentifierListLookup.GetKey(this.m_to); }
        }

        /// <summary>
        /// The mux operator that connects the left hand with the right hand pip (either -> or -=)
        /// </summary>
        public String Operator
        {
            get { return this.m_connectionOperator; }
            //get { return FPGA.FPGA.Instance.IdentifierListLookup.GetIdentifier(this.m_connectionOperatorKey); }
        }

        public override bool Equals(object obj)
        {
            if (obj is XDLPip)
            {
                XDLPip other = (XDLPip)obj;
                if (other.FromKey != this.FromKey || other.ToKey != this.ToKey)
                {
                    return false;
                }
                return other.Location.Equals(this.Location);
                /*
                return
    other.Location.Equals(this.Location) &&
    other.From.Equals(this.From) &&
    other.To.Equals(this.To);*/
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        public override string ToString()
        {
            if (this.Location.StartsWith("#"))
            {
                // pseudo pip serves as comment
                return this.Location;
            }
            else
            {
                return "pip " + this.Location + " " + this.From + " " + this.Operator + " " + this.To + ",";
            }
        }

        private readonly String m_location;
        private readonly String m_from;
        private readonly String m_to;
        private readonly String m_connectionOperator;
        /*
                private readonly ushort m_locationKey;
                private readonly ushort m_fromKey;
                private readonly ushort m_toKey;
                private readonly ushort m_connectionOperatorKey;*/
    }
}
﻿using System;
using System.Text;

namespace GoAhead.Code.XDL
{
    [Serializable]
    public class XDLModule : XDLContainer
    {
        public void AddCode(String line)
        {
            this.m_xdlCode.Append(line);
        }

        public void AddCode(char c)
        {
            this.m_xdlCode.Append(c);
        }

        public void Clear()
        {
            this.m_xdlCode.Clear();
        }

        public void SetCode(String xdlCode)
        {
            this.Clear();
            this.AddCode(xdlCode);
        }

        public override string ToString()
        {
            return this.m_xdlCode.ToString();
        }

        protected StringBuilder m_xdlCode = new StringBuilder();
    }
}
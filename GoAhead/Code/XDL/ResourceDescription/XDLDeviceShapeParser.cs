using System;

namespace GoAhead.Code.XDL.ResourceDescription
{
    public class XDLDeviceShapeParser
    {
        public static void Parse(String line)
        {
            String[] atoms = line.Split(' ');
            int x = Int32.Parse(atoms[1]);
            int y = Int32.Parse(atoms[2]);

            FPGA.FPGA.Instance.NumberOfExpectedTiles = x * y;
        }
    }
}
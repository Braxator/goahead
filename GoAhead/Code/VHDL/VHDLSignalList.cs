﻿using System;
using System.Collections.Generic;
using GoAhead.Objects;

namespace GoAhead.Code.VHDL
{
    public abstract class VHDLSignalList
    {
        public void Add(String signalName, int width, PortMapper.MappingKind mappingKind)
        {
            if (this.m_signals.ContainsKey(signalName))
            {
                throw new ArgumentException("Entity signal " + signalName + " exists already");
            }

            this.m_signals[signalName] = width;
            this.m_mappings[signalName] = mappingKind;
        }

        public void Add(String signalName, int width)
        {
            if (this.m_signals.ContainsKey(signalName))
            {
                throw new ArgumentException("Entity signal " + signalName + " exists already");
            }

            this.m_signals[signalName] = width;
        }

        public bool HasSignal(String signalName)
        {
            return this.m_signals.ContainsKey(signalName);
        }

        public bool HasMapping(String signalName)
        {
            return this.m_mappings.ContainsKey(signalName);
        }

        public PortMapper.MappingKind GetMapping(String signalName)
        {
            return this.m_mappings[signalName];
        }

        public int GetSignalWidth(String signalName)
        {
            if (!this.m_signals.ContainsKey(signalName))
            {
                throw new ArgumentException("Entity signal " + signalName + " does not exist");
            }

            return this.m_signals[signalName];
        }

        public void SetSignalWidth(String signalName, int width)
        {
            if (!this.m_signals.ContainsKey(signalName))
            {
                throw new ArgumentException("Entity signal " + signalName + " does not exist");
            }

            this.m_signals[signalName] = width;
        }

        public IEnumerable<Tuple<String, int>> GetSignals()
        {
            foreach (KeyValuePair<String, int> s in this.m_signals)
            {
                yield return new Tuple<String, int>(s.Key, s.Value);
            }
        }

        protected Dictionary<String, int> m_signals = new Dictionary<String, int>();
        protected Dictionary<String, PortMapper.MappingKind> m_mappings = new Dictionary<String, PortMapper.MappingKind>();
    }
}
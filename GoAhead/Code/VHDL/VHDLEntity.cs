﻿using System;
using System.Collections.Generic;
using System.Text;
using GoAhead.FPGA;
using GoAhead.Objects;

namespace GoAhead.Code.VHDL
{
    public class VHDLEntity : VHDLSignalList
    {
        public VHDLEntity()
        {
        }

        public void SetDirection(String signalName, FPGATypes.PortDirection direction)
        {
            this.m_directions[signalName] = direction;
        }

        public FPGATypes.PortDirection GetDirection(String signalName)
        {
            if (!this.m_directions.ContainsKey(signalName))
            {
                throw new ArgumentException("Entity signal " + signalName + " does not exist");
            }

            return this.m_directions[signalName];
        }

        public override string ToString()
        {
            StringBuilder buffer = new StringBuilder();

            // we need signalWidths as a list ...
            List<KeyValuePair<String, int>> interfaceSignals = new List<KeyValuePair<String, int>>();
            foreach (KeyValuePair<String, int> tupel in this.m_signals)
            {
                interfaceSignals.Add(tupel);
            }
            for (int i = 0; i < interfaceSignals.Count; i++)
            {
                String signalName = interfaceSignals[i].Key;
                String line = "";

                PortMapper.MappingKind mappingKind = this.m_mappings.ContainsKey(signalName) ? this.m_mappings[signalName] : PortMapper.MappingKind.External;

                if (mappingKind == PortMapper.MappingKind.NoVector)
                {
                    line = "\t" + signalName + " : " + this.m_directions[signalName].ToString().ToLower() + " std_logic";
                }
                else
                {
                    line = "\t" + signalName + " : " + this.m_directions[signalName].ToString().ToLower() + " std_logic_vector(" + (interfaceSignals[i].Value - 1) + " downto 0)";
                }
                // ... to find the last index
                if (i < interfaceSignals.Count - 1)
                {
                    line += ";";
                }
                else
                {
                    line += ");";
                }
                buffer.AppendLine(line);
            }

            return buffer.ToString();
        }

        private Dictionary<String, FPGATypes.PortDirection> m_directions = new Dictionary<String, FPGATypes.PortDirection>();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using GoAhead.Code.XDL;
using GoAhead.Objects;

namespace GoAhead.Code.VHDL
{
    public class VHDLComponent
    {
        public VHDLComponent(LibraryElement libraryElement)
        {
            this.m_libraryElement = libraryElement;
        }

        public override string ToString()
        {
            if (this.m_libraryElement.SubElements.Count() > 0)
            {
                return "Composed element, no code available";
            }
            List<String> buffer = new List<String>();
            buffer.Add("component " + this.m_libraryElement.PrimitiveName + " is Port (");

            if (this.m_libraryElement.Containter != null)
            {
                foreach (NetlistPort port in this.m_libraryElement.Containter.Ports)
                {
                    buffer.Add("\t" + port.ExternalName + " : " + port.Direction.ToString().ToLower() + " std_logic;");
                }
            }

            buffer[buffer.Count - 1] = Regex.Replace(buffer[buffer.Count - 1], ";", "");
            buffer.Add(");");
            buffer.Add("end component " + this.m_libraryElement.PrimitiveName + ";");

            StringBuilder all = new StringBuilder();
            foreach (String s in buffer)
            {
                all.AppendLine(s);
            }
            return all.ToString();
        }

        public String Name
        {
            get { return this.m_libraryElement.PrimitiveName; }
        }

        private readonly LibraryElement m_libraryElement;
    }
}
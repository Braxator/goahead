﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using GoAhead.Code.XDL;
using GoAhead.Commands;
using GoAhead.FPGA;
using GoAhead.Objects;

namespace GoAhead.Code.VHDL
{
    public class VHDLInstantiation
    {
        public VHDLInstantiation(VHDLFile container, LibElemInst instantiation, LibraryElement libElement, Command callee)
        {
            this.m_container = container;
            this.m_instantation = instantiation;
            this.m_libraryElement = libElement;
            this.IntergrateIntoContainer(callee);
        }

        public override string ToString()
        {
            return this.m_instanceCode.ToString();
        }

        private void IntergrateIntoContainer(Command callee)
        {
            this.m_instanceCode.AppendLine("-- instantiation of " + this.m_instantation.InstanceName);
            this.m_instanceCode.AppendLine(this.m_instantation.InstanceName + " : " + this.m_instantation.GetLibraryElement().PrimitiveName);
            if (!String.IsNullOrEmpty(this.m_libraryElement.VHDLGenericMap))
            {
                this.m_instanceCode.AppendLine(this.m_libraryElement.VHDLGenericMap);
            }
            this.m_instanceCode.AppendLine("port map (");

            List<String> mappings = new List<String>();

            foreach (XDLPort port in ((XDLContainer)this.m_libraryElement.Containter).Ports)
            {
                if (!this.m_instantation.PortMapper.HasKindMapping(port.ExternalName))
                {
                    callee.OutputManager.WriteOutput("Warning: Could not find a signal mapping for port " + port.ExternalName + ". Misspelled mapping? Note that the mapping is case sensitive.");
                    continue;
                }

                PortMapper mapper = this.m_instantation.PortMapper;
                PortMapper.MappingKind mappingKind = mapper.GetMapping(port.ExternalName);
                String rightHandSide = mapper.GetSignalName(port.ExternalName);

                // do not vectorize already indeced ports
                //bool vector = Regex.IsMatch(port.ExternalName, @"\d+$") && !Regex.IsMatch(rightHandSide, @"\(\d+\)$");

                if (rightHandSide.Equals("0") || rightHandSide.Equals("1"))
                {
                    mappings.Add("\t" + port.ExternalName + " => " + "'" + rightHandSide + "',");
                }
                else if (rightHandSide.Equals("open"))
                {
                    mappings.Add("\t" + port.ExternalName + " => open,");
                }
                else if (!rightHandSide.Equals("open"))
                {
                    VHDLSignalList signalList = null;
                    switch (mappingKind)
                    {
                        case PortMapper.MappingKind.NoVector:
                        case PortMapper.MappingKind.External:
                            {
                                // in case of entity signals add direction
                                signalList = this.m_container.Entity;
                                this.m_container.Entity.SetDirection(rightHandSide, port.Direction);
                                break;
                            }
                        case PortMapper.MappingKind.Internal:
                            {
                                signalList = this.m_container.SignalDeclaration;
                                break;
                            }

                        default:
                            throw new ArgumentException("Found port mapped to " + mappingKind + ". This mapping is not supported. Use either public or external, see command AddPortMapping");
                    }

                    if (!signalList.HasSignal(rightHandSide))
                    {
                        if (this.m_instantation.PortMapper.HasKindMapping(port.ExternalName))
                        {
                            signalList.Add(rightHandSide, 1, this.m_instantation.PortMapper.GetMapping(port.ExternalName));
                        }
                        else
                        {
                            signalList.Add(rightHandSide, 1);
                        }
                    }

                    switch (mappingKind)
                    {
                        case PortMapper.MappingKind.Internal:
                        case PortMapper.MappingKind.External:
                            {
                                //int index = signalList.GetSignalWidth(rightHandSide) - 1;
                                int index = mapper.GetIndex(port.ExternalName);
                                mappings.Add("\t" + port.ExternalName + " => " + rightHandSide + "(" + index + "),");

                                if (!signalList.HasSignal(rightHandSide))
                                {
                                    signalList.Add(rightHandSide, -1);
                                }
                                if (signalList.GetSignalWidth(rightHandSide) <= index)
                                {
                                    signalList.SetSignalWidth(rightHandSide, index + 1);
                                }

                                // store the index generated during VHDL generation for interface checks
                                //this.m_instantation.GetPortMapper().SetIndex(port.ExternalName, index);
                                break;
                            }
                        case PortMapper.MappingKind.NoVector:
                            {
                                mappings.Add("\t" + port.ExternalName + " => " + rightHandSide + ",");
                                break;
                            }

                        default:
                            throw new ArgumentException("Found port mapped to " + mappingKind + ". This mapping is not supported. Use either public or external, see command AddPortMapping");
                    }
                }
            }

            if (mappings.Count > 0)
            {
                mappings[mappings.Count - 1] = Regex.Replace(mappings[mappings.Count - 1], ",$", "");
            }

            // update tool info
            //Tile t = FPGA.FPGA.Instance.GetTile(this.m_instantation.AnchorLocation);
            //Objects.Blackboard.Instance.ClearToolTipInfo(t);

            foreach (String str in mappings)
            {
                this.m_instanceCode.AppendLine(str);

                // update tool info
                if (!str.EndsWith(" => ,"))
                {
                    String toolTip = str;
                    toolTip = Regex.Replace(toolTip, @"^\s+", "");
                    toolTip = Regex.Replace(toolTip, ",", "");
                    toolTip += Environment.NewLine;
                    //Objects.Blackboard.Instance.AddToolTipInfo(t, toolTip);
                }
            }

            this.m_instanceCode.AppendLine(");");
        }

        private StringBuilder m_instanceCode = new StringBuilder();
        private readonly LibElemInst m_instantation;
        private readonly LibraryElement m_libraryElement;
        private readonly VHDLFile m_container;
    }
}
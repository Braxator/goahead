﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace GoAhead.Code.VHDL
{
    public class VHDLParserEntity
    {
        public String WholeEntityAsString = "";
        public String EntityName = "";
        public List<HDLEntitySignal> InterfaceSignals = new List<HDLEntitySignal>();
    }

    public class HDLEntitySignalMetaData
    {
        /// <summary>
        /// Where to place the signal in an island
        /// </summary>
        public FPGA.FPGATypes.Direction Direction = FPGA.FPGATypes.Direction.East;

        public int Column = 0;
        public String WholeMetaDataStrig = "";
    }

    public class HDLEntitySignal
    {
        public String SignalName = "";
        public String SigalType = "";
        public String Range = "";

        /// <summary>
        /// in our out
        /// </summary>
        public String SignalDirection = "";

        public int MSB = 0;
        public int LSB = 0;
        public String WholeSignalDeclaration = "";
        public HDLEntitySignalMetaData MetaData = null;
        public bool IsClock = false;
    }

    public class VHDLParser
    {
        public VHDLParser(String fileName)
        {
            this.m_fileName = fileName;
        }

        private void Parse()
        {
            StreamReader sr = new StreamReader(this.m_fileName);
            String wholeFile = sr.ReadToEnd();
            sr.Close();

            Regex regExpEntities = new Regex(@"(entity[\s\S]*?end[\s\S]*?;)", RegexOptions.IgnorePatternWhitespace & RegexOptions.CultureInvariant);
            MatchCollection entityMatches = regExpEntities.Matches(wholeFile);
            foreach (Match entityMatch in entityMatches)
            {
                VHDLParserEntity entity = new VHDLParserEntity();

                String entityCode = entityMatch.Groups[1].Value;
                String[] atoms = entityCode.Split(' ');

                entity.WholeEntityAsString = entityCode;
                entity.EntityName = atoms[1];
                entity.InterfaceSignals.AddRange(this.GetSignals(entityCode));

                this.m_entities.Add(entity);
            }
        }

        private IEnumerable<HDLEntitySignal> GetSignals(String entityCode)
        {
            int openBR = 0;
            int pos = entityCode.IndexOf('(') + 1;
            String signalString = "";
            while (pos < entityCode.Length)
            {
                if (entityCode[pos] == '(')
                {
                    openBR++;
                }
                if (entityCode[pos] == ')')
                {
                    if (openBR == 0)
                    {
                        yield return this.GetHDLEntitySignal(signalString, "");
                        break;
                    }
                    else
                    {
                        openBR--;
                    }
                }
                if (entityCode[pos] == ';')
                {
                    // read until end of line
                    String metaData = "";
                    pos++; // but skip the semicolon
                    while (pos < entityCode.Length)
                    {
                        char c = entityCode[pos];
                        metaData += entityCode[pos];

                        pos++;
                        if (metaData.Contains('\n'))
                        {
                            break;
                        }
                    }
                    metaData = Regex.Replace(metaData, Environment.NewLine, "");
                    yield return this.GetHDLEntitySignal(signalString, metaData);
                    signalString = "";
                }
                else
                {
                    signalString += entityCode[pos];
                }
                pos++;
            }
        }

        private HDLEntitySignal GetHDLEntitySignal(String signalString, String metaData)
        {
            HDLEntitySignal signal = new HDLEntitySignal();

            signal.WholeSignalDeclaration = Regex.Replace(signalString, @"^\s*", "");
            String[] atoms = signal.WholeSignalDeclaration.Split(':');
            signal.SignalName = Regex.Replace(atoms[0], @"(^\s*)|(\s*$)", "");
            atoms = Regex.Split(signal.WholeSignalDeclaration, @"(:\s+in)|(:\s+out)");
            signal.SigalType = Regex.Replace(atoms[2], @"(^\s*)|(\s*$)", "");

            signal.SignalDirection = Regex.IsMatch(signalString, @":\s*in\s*") ? "in" : Regex.IsMatch(signalString, @":\s*out\s*") ? "out" : "inout";

            if (signal.WholeSignalDeclaration.Contains('('))
            {
                // vector
                int from = signal.WholeSignalDeclaration.IndexOf('(');
                int length = signal.WholeSignalDeclaration.Length - from;
                signal.Range = signal.WholeSignalDeclaration.Substring(from, length);
                // extract digits from range
                String digtis = Regex.Replace(signal.Range, @"(downto)|(to)|(\()|(\))", "");
                String[] digitAtoms = digtis.Split(' ');
                signal.MSB = Int32.Parse(digitAtoms[0]);
                signal.LSB = Int32.Parse(digitAtoms[2]);
                //signal.Width
            }
            else
            {
                // std_logic
                signal.LSB = 0;
                signal.MSB = 0;
            }

            if (!String.IsNullOrEmpty(metaData))
            {
                HDLEntitySignalMetaData hdlMetaData = new HDLEntitySignalMetaData();
                hdlMetaData.WholeMetaDataStrig = metaData;
                if (metaData.ToLower().Contains("east")) { hdlMetaData.Direction = FPGA.FPGATypes.Direction.East; }
                if (metaData.ToLower().Contains("west")) { hdlMetaData.Direction = FPGA.FPGATypes.Direction.West; }
                if (metaData.ToLower().Contains("south")) { hdlMetaData.Direction = FPGA.FPGATypes.Direction.South; }
                if (metaData.ToLower().Contains("north")) { hdlMetaData.Direction = FPGA.FPGATypes.Direction.North; }

                Regex regExpModules = new Regex(@"column=[0-9]+(\s|$)");
                MatchCollection modulesMatches = regExpModules.Matches(metaData.ToLower());
                foreach (Match match in modulesMatches)
                {
                    String columnCode = match.Groups[0].Value;
                    columnCode = Regex.Replace(columnCode, "column=", "");
                    hdlMetaData.Column = Int32.Parse(columnCode);
                }

                signal.MetaData = hdlMetaData;
            }

            return signal;
        }

        public VHDLParserEntity GetEntity(int i)
        {
            if (this.m_entities.Count == 0)
            {
                this.Parse();
            }
            return this.m_entities[i];
        }

        public IEnumerable<VHDLParserEntity> GetEntities()
        {
            if (this.m_entities.Count == 0)
            {
                this.Parse();
            }

            return this.m_entities;
        }

        private List<VHDLParserEntity> m_entities = new List<VHDLParserEntity>();
        private readonly String m_fileName = "";
    }
}
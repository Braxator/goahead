﻿using System;
using System.Text;

namespace GoAhead.Code
{
    [Serializable]
    public abstract class NetlistElement
    {
        virtual public void AddCode(String line)
        {
            this.m_code.Append(line);
        }

        virtual public void AddCode(char c)
        {
            this.m_code.Append(c);
        }

        public string GetCode()
        {
            return this.m_code.ToString();
        }

        public override string ToString()
        {
            return this.m_code.ToString();
        }

        public virtual void Clear()
        {
            this.m_code.Clear();
        }

        public void SetCode(String code)
        {
            this.Clear();
            this.AddCode(code);
        }

        protected StringBuilder m_code = new StringBuilder();
    }
}
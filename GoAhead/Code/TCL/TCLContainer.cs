﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using GoAhead.FPGA;
using GoAhead.Objects;
using GoAhead.Commands.Data;

namespace GoAhead.Code.TCL
{
    class TCLContainer : NetlistContainer
    {
        public TCLContainer(string name)
        {
            this.Name = name;
        }

        public void Add(TCLPin pin)
        {
            if (this.m_pins.ContainsKey(pin.Name))
            {
                return;
                throw new ArgumentException("A pin named " + pin.Name + " has already been added");
            }

            this.m_pins.Add(pin.Name, pin);
        }

        public void Add(TCLDesignHierarchy hier)
        {
            if (this.m_hier.ContainsKey(hier.Name))
            {
                throw new ArgumentException("A hierarchy named " + hier.Name + " has already been added");
            }

            this.m_hier.Add(hier.Name, hier);
        }

        public bool AddGndPrimitive(Net net)
        {
            TCLInstance gnd = new TCLInstance(); 
            gnd.Name = "gnd_for_" + net.Name; // example; gnd_for_BlockSelection
            // get the first tile from the selection
            Tile any = FPGA.TileSelectionManager.Instance.GetSelectedTiles().Where(t => t.Slices.Count > 0).First(); // example; {X99Y136:CLBLL_L_X38Y19} (the first tile in the selection)
            gnd.Location = any.Location; // example; "CLBLL_L_X38Y19"
            gnd.SliceName = any.Slices[0].SliceName; // example; "SLICE_X56Y19", TODO: is is necessary to use slice index 0? or is doesnt matter anyway?
            gnd.BELType = "GND";
            gnd.OmitPlaceCommand = true;
            this.Add(gnd); // add to netlist container

            // add output pin to the net
            NetOutpin outpin = new NetOutpin();
            outpin.InstanceName = gnd.Name; // example; "gnd_for_BlockSelection"
            outpin.SlicePort = "G";
            net.Add(outpin);

            return true;
            /*
            TCLInstance vcc = new TCLInstance();
            vcc.Name = "vcc_for_" + blockerNet.Name;
            Tile any = FPGA.TileSelectionManager.Instance.GetSelectedTiles().First();
            vcc.Location = any.Location;
            vcc.BELType = "VCC";
            vcc.OmitPlaceCommand = true;
            nlc.Add(vcc);

            NetOutpin outpin = new NetOutpin();
            outpin.InstanceName = vcc.Name;
            outpin.SlicePort = "P";
            blockerNet.Add(outpin);

            return true;
             * */
        }

        public IEnumerable<TCLPin> Pins
        {
            get { return this.m_pins.Values; }
        }

        public IEnumerable<TCLPort> Ports
        {
            get { return this.m_ports.Values; }
        }

        public IEnumerable<TCLDesignHierarchy> Hierarchies
        {
            get { return this.m_hier.Values; }
        }

        public override NetlistContainer GetSelectedDesignElements()
        {
            // resulting design
            TCLContainer result = new TCLContainer("GetSelectedDesignElements");
            result.Name = this.Name;

            // filter instances
            foreach (Instance inst in this.Instances)
            {
                if (FPGA.TileSelectionManager.Instance.IsSelected(inst.TileKey))
                {
                    result.Add(inst);
                }
            }
             
            // filter nets
            foreach (TCLNet inNet in this.Nets)
            {
                // create copy
                TCLNet outNet = TCLNet.Copy(inNet);
                outNet.NodeNet = true;

                // cleat net pins and readd only he selected ones in the for each loop
                outNet.ClearNetPins();
                 
                foreach (NetPin pin in inNet.NetPins)
                {
                    if (!this.m_instances.ContainsKey(pin.InstanceName))
                    {
                        if (FPGA.TileSelectionManager.Instance.IsSelected(FPGA.FPGA.Instance.GetTile(pin.TileName)))
                        {
                            outNet.Add(pin);
                        }
                    }
                    else
                    {
                        if (FPGA.TileSelectionManager.Instance.IsSelected( this.GetInstanceByName(pin.InstanceName).TileKey))
                        {
                            outNet.Add(pin);
                        }
                    }
                }

                if (outNet.RoutingTree.GetAllRoutingNodes().Any(n => !n.VirtualNode && !FPGA.TileSelectionManager.Instance.IsSelected(n.Tile)))
                {
                    outNet.FlattenNet();
                    outNet.Remove(node => !node.VirtualNode && !FPGA.TileSelectionManager.Instance.IsSelected(node.Tile));
                }

                if (outNet.RoutingTree.GetAllRoutingNodes().Any(n => !n.VirtualNode))
                {
                    result.Add(outNet);
                }
            }
            return result;
        }

        public TCLInstance GetInstanceBySliceName(String sliceName, String belName)
        {
            if (!this.m_sliceInstanceMapping.ContainsKey(sliceName))
            {
                return null;                    
                //throw new ArgumentException("No instance with slice name" + sliceName + " known in " + this.ToString());
            }
            else
            {
                if (this.m_sliceInstanceMapping[sliceName].Count != 1)
                {
                    List<Instance> instances = this.m_sliceInstanceMapping[sliceName];
                }
                return (TCLInstance)this.m_sliceInstanceMapping[sliceName].FirstOrDefault(i => ((TCLInstance)i).Properties.GetValue("BEL").EndsWith(belName));
            }
        }

        public void RemoveAllPins()
        {
            this.m_pins.Clear();
        }

        public override void WriteCodeToFile(StreamWriter sw)
        {
            sw.WriteLine("##########################################");
            sw.WriteLine("# vivado netlist description for GoAhead #");
            sw.WriteLine("##########################################");
            foreach (TCLInstance cell in this.Instances)
            {
                sw.Write("Cell=" + cell.Name);
                foreach(TCLProperty prop in cell.Properties)
                {
                    sw.Write(";" + prop.Name + "=" + prop.Value + "(" + (prop.ReadOnly ? "1" : "0" ) + ")");
                }
                sw.WriteLine("");
            }

            foreach (TCLNet net in this.Nets)
            {
                sw.Write("Net=" + net.Name);
                foreach (NetPin np in net.NetPins)
                {
                    // no (readonly) forBelPins
                    sw.Write(";BelPin=" + np.Code);

                }
                if (net.NodeNet)
                {
                    string nodes = String.Join(" ", net.RoutingTree.Root.Children.Select(n => n.ToString()));
                    sw.Write(";Nodes=" + nodes);
                }
                foreach (TCLProperty prop in net.Properties)
                {
                    sw.Write(";" + prop.Name + "=" + prop.Value + "(" + (prop.ReadOnly ? "1" : "0") + ")");
                }
                sw.WriteLine("");
            }


            sw.WriteLine("# end of file");
        }

        protected Dictionary<string, TCLPin> m_pins = new Dictionary<string, TCLPin>();
        protected Dictionary<string, TCLPort> m_ports = new Dictionary<string, TCLPort>();
        protected Dictionary<string, TCLDesignHierarchy> m_hier = new Dictionary<string, TCLDesignHierarchy>();
    }
}

﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.ComponentModel;

namespace GoAhead.Code.TCL
{
    [Serializable]
    public class TCLInstance : Instance
    {
        public TCLInstance()
        {
        }

        public TCLInstance(TCLInstance other)
        {
            this.BELType = other.BELType;
            this.InstanceIndex = other.InstanceIndex;
            this.Location = other.Location;
            this.LocationX = other.LocationX;
            this.LocationY = other.LocationY;
            this.m_code.Append(other.m_code.ToString());
            this.m_sliceNumber = other.m_sliceNumber;
            this.m_tileKey = new FPGA.TileKey(other.m_tileKey.X, other.m_tileKey.Y);
            this.Name = other.Name;
            this.OmitPlaceCommand = other.OmitPlaceCommand;
            this.Properties = new TCLProperties(other.Properties);
            this.SliceName = other.SliceName;
            this.SliceNumber = other.SliceNumber;
            this.SliceType = other.SliceType;
        }

        [DefaultValue("undefined bel type"), DataMember]
        public String BELType { get; set; }

        [DefaultValue(false), DataMember]
        public bool OmitPlaceCommand { get; set; }

        public TCLProperties Properties = new TCLProperties();
    }
}
﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using GoAhead.FPGA;

namespace GoAhead.Code
{
    [Serializable]
    public abstract class Instance : NetlistElement
    {
        public override bool Equals(object obj)
        {
            if (obj is Instance)
            {
                Instance other = (Instance)obj;
                return
                    other.Location.Equals(this.Location) &&
                    other.SliceName.Equals(this.SliceName);
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            // we compare via SliceName
            return this.SliceName.GetHashCode();
        }

        public TileKey TileKey
        {
            get
            {
                if (this.m_tileKey == null)
                {
                    this.DeriveTileKeyAndSliceNumber();
                }

                return this.m_tileKey;
            }
            set { this.m_tileKey = value; }
        }

        public int SliceNumber
        {
            get
            {
                if (this.m_sliceNumber == -1)
                {
                    this.DeriveTileKeyAndSliceNumber();
                }
                return this.m_sliceNumber;
            }
            set { this.m_sliceNumber = value; }
        }

        public void DeriveTileKeyAndSliceNumber()
        {
            Tile tile = FPGA.FPGA.Instance.GetTile(this.Location);
            this.m_tileKey = tile.TileKey;
            // sideeffect with get!
            TileKey key = this.TileKey;
            Tile where = FPGA.FPGA.Instance.GetTile(this.Location);
            if (where.HasSlice(this.SliceName))
            {
                this.m_sliceNumber = where.GetSliceNumberByName(this.SliceName);
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Warning: Can not find " + this.SliceName + " on tile " + where.Location);
                Console.ResetColor();
                this.m_sliceNumber = 0;
            }
        }

        public override string ToString()
        {
            // TODO XDL needs slice configuration??
            return this.Name;
        }

        [DefaultValue("undefined name"), DataMember]
        public String Name { get; set; }

        [DefaultValue(-1), DataMember]
        public int InstanceIndex { get; set; }

        [DefaultValue(-1), DataMember]
        public int LocationX { get; set; }

        [DefaultValue(-1), DataMember]
        public int LocationY { get; set; }

        [DefaultValue("undefined location"), DataMember]
        public String Location { get; set; }

        [DefaultValue("undefined slice"), DataMember]
        public String SliceName { get; set; }

        [DefaultValue("undefined slice type"), DataMember]
        public String SliceType { get; set; }

        [DataMember]
        protected TileKey m_tileKey = null;

        [DataMember]
        protected int m_sliceNumber = -1;
    }
}
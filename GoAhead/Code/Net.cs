﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace GoAhead.Code
{
    [Serializable]
    public abstract class Net : NetlistElement
    {
        public static Net CreateNet(string name)
        {
            switch (FPGA.FPGA.Instance.BackendType)
            {
                case GoAhead.FPGA.FPGATypes.BackendType.ISE:
                    return new XDL.XDLNet(name);
                case GoAhead.FPGA.FPGATypes.BackendType.Vivado:
                    return new TCL.TCLNet(name);
                default:
                    throw new ArgumentException("Unsupported backend type " + FPGA.FPGA.Instance.BackendType);
            }
        }

        public abstract int PipCount
        {
            get;
        }

        public int OutpinCount
        {
            get { return (this.m_netPins == null ? 0 : this.m_netPins.Count(np => np is NetOutpin)); }
        }

        public int InpinCount
        {
            get { return (this.m_netPins == null ? 0 : this.m_netPins.Count(np => np is NetInpin)); }
        }

        public int NetPinCount
        {
            get { return (this.m_netPins == null ? 0 : this.m_netPins.Count); }
        }

        /// <summary>
        /// Add inpin and outpins
        /// </summary>
        /// <param name="pin"></param>
        public void Add(NetPin pin)
        {
            if (this.m_netPins == null)
            {
                this.m_netPins = new List<NetPin>();
            }
        
            this.m_netPins.Add(pin);
        }

        public override string ToString()
        {
            return this.Name;
        }

        public void ClearNetPins()
        {
            this.m_netPins = null;
        }

        public void Remove(Predicate<NetPin> p)
        {
            if (this.m_netPins != null)
            {
                this.m_netPins.RemoveAll(p);
            }
        }

        public virtual IEnumerable<NetPin> NetPins
        {
            get 
            {
                if (this.m_netPins != null)
                {
                    foreach (NetPin np in this.m_netPins)
                    {
                        yield return np;
                    }
                }
            }
        }

        public abstract void BlockUsedResources();

        [DefaultValue(false)]
        public bool ReadOnly { get; set; }

        [DefaultValue("undefined net name")]
        public String Name { get; set; }

        /// <summary>
        /// Inpin and Outpins
        /// </summary>
        protected List<NetPin> m_netPins = null;
    }
}
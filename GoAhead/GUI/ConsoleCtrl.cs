﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using GoAhead.Commands;

namespace GoAhead.GUI
{
    public partial class ConsoleCtrl : UserControl, Interfaces.IResetable
    {
        public ConsoleCtrl()
        {
            InitializeComponent();

            Commands.Reset.ObjectsToReset.Add(this);

            // geht das nicht im Wizard?
            this.m_txtInput.AllowDrop = true;
            this.m_txtInput.DragEnter += new DragEventHandler(this.m_txtInput_DragEnter);
            this.m_txtInput.DragDrop += new DragEventHandler(this.m_txtInput_DragDrop);    
        }

        /// <summary>
        /// Clear all text boxes (expect commmand trace and input) upon reset
        /// </summary>
        public void Reset()
        {
            this.m_txtErrorTrace.Clear();
            this.m_txtOutputTrace.Clear();
            this.m_txtUCF.Clear();
            this.m_txtVHDL.Clear();
            this.m_txtTCL.Clear();
        }

        new public Form Parent = null;

        #region DragAndDrop
            
        private void m_txtInput_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else if (e.Data.GetDataPresent(DataFormats.Text))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void m_txtInput_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop, false);

                this.m_txtInput.Clear();
                foreach (String file in files)
                {
                    this.m_txtInput.AppendText(File.ReadAllText(file));
                }
            }
            else if (e.Data.GetDataPresent(DataFormats.Text))
            {
                String line = e.Data.GetData(DataFormats.Text).ToString();
                this.m_txtInput.AppendText(line);
            }
        }

        #endregion DrapAndDrop

        #region AddText
        public void AddToCommandTrace(Command cmd)
        {
            CommandStringParser parser = new CommandStringParser("");

            String cmdTag;
            String argumentPart;
            bool valid = parser.SplitCommand(cmd.ToString(), out cmdTag, out argumentPart);

            int offset = this.m_txtCommandTrace.TextLength;

            String cmdString = cmd.ToString() + Environment.NewLine;
            // dump command
            this.m_txtCommandTrace.AppendText(cmdString);
            
            if(!valid)
            {
                return;
            }

            // color command name red
            this.m_txtCommandTrace.Select(offset, offset + cmdTag.Length);
            this.m_txtCommandTrace.SelectionColor = Color.Red;

            // let offset point right behind the command
            offset += cmdTag.Length;

            // color argument names blue and argument values black
            if (!String.IsNullOrEmpty(argumentPart))
            {
                foreach (NameValuePair nameValuePair in parser.GetNameValuePairs(argumentPart))
                {
                    this.m_txtCommandTrace.Select(offset + nameValuePair.NameFrom, offset + nameValuePair.NameTo);
                    this.m_txtCommandTrace.SelectionColor = Color.Blue;

                    this.m_txtCommandTrace.Select(offset + nameValuePair.ValueFrom, offset + nameValuePair.ValueTo);
                    this.m_txtCommandTrace.SelectionColor = Color.Black;
                }
            }
            this.m_txtCommandTrace.DeselectAll();
            this.m_txtCommandTrace.ScrollToCaret();
            return;
           
            /*
            int index = 0;
            while (index < cmdString.Length && cmdString[index] != ' ' && cmdString[index] != ';')
            {
                index++;
            }
            this.m_txtCommandTrace.Select(offset, offset + index);
            this.m_txtCommandTrace.SelectionColor = Color.Red;

            this.m_txtCommandTrace.Select(offset + index, this.m_txtCommandTrace.TextLength);
            this.m_txtCommandTrace.SelectionColor = Color.Blue;
            this.m_txtCommandTrace.DeselectAll();
            this.m_txtCommandTrace.ScrollToCaret();*/
        }
        public void AddToErrorTrace(String msg)
        {
            this.m_txtErrorTrace.AppendText(msg);
            this.m_txtErrorTrace.ScrollToCaret();
        }
        public void AddToOutputTrace(String msg)
        {
            this.m_txtOutputTrace.AppendText(msg);
            this.m_txtOutputTrace.ScrollToCaret();
        }
        public void AddToWarningsTrace(String msg)
        {
            this.m_txtWarningsTrace.AppendText(msg);
            this.m_txtWarningsTrace.ScrollToCaret();
        }
        public void AddToUCFTrace(String msg)
        {
            this.m_txtUCF.AppendText(msg);
            this.m_txtUCF.ScrollToCaret();
        }
        public void AddToVHDLTrace(String msg)
        {
            this.m_txtVHDL.AppendText(msg);
            this.m_txtVHDL.ScrollToCaret();
        }
        public void AddToTCLTrace(String msg)
        {
            this.m_txtTCL.AppendText(msg);
            this.m_txtTCL.ScrollToCaret();
        }
        #endregion AddText

        #region ContextMenu
        private void m_ctxtMenuSelectAll_Click(object sender, EventArgs e)
        {
            this.m_origionOfRightClick.SelectAll();
        }

        private void m_ctxtMenuCopy_Click(object sender, EventArgs e)
        {
            this.m_origionOfRightClick.Copy();
        }

        private void m_ctxtMenuCopyAll_Click(object sender, EventArgs e)
        {
            this.m_origionOfRightClick.SelectAll();
            this.m_origionOfRightClick.Copy();
        }

        private void m_ctxtMenuDelete_Click(object sender, EventArgs e)
        {
            this.m_origionOfRightClick.Clear();
        }

        private void m_ctxtMenuPaste_Click(object sender, EventArgs e)
        {
            this.m_origionOfRightClick.Paste();
        }

        private void ShowContextMenu(object sender, MouseEventArgs e)
        {            
            this.m_ctxtMenu.Show(this, new Point(e.X, e.Y));
        }
        #endregion ContextMenu

        /// <summary>
        /// handle user cmds
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_txtInputTrace_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                try
                {
                    CommandStringParser parser = new CommandStringParser(this.m_txtInput.Text);
                    foreach (String cmdString in parser.Parse())
                    {
                        Command cmd;
                        String errorDescr;
                        bool valid = parser.ParseCommand(cmdString, true, out cmd, out errorDescr);
                        if (valid)
                        {
                            CommandExecuter.Instance.Execute(cmd);
                        }
                        else
                        {
                            MessageBox.Show("Could not parse command " + Environment.NewLine + cmdString + Environment.NewLine + "Error: " + errorDescr, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }

                        // update shell
                        Objects.CommandStackManager.Instance.Execute();

                        if (this.Parent != null)
                        {
                            this.Parent.Invalidate();
                        }
                    }
                
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message, "Error", MessageBoxButtons.OK);
                }
                this.m_txtInput.Clear();
            }

        }

        #region ContextMenu
        private void m_txtCommandTrace_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.m_origionOfRightClick = this.m_txtCommandTrace;
                this.m_ctxtMenuPaste.Visible = false;
                this.ShowContextMenu(sender, e);
            }
        }

        private void m_txtErrorTrace_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.m_origionOfRightClick = this.m_txtErrorTrace;
                this.m_ctxtMenuPaste.Visible = false;
                this.ShowContextMenu(sender, e);
            }
        }

        private void m_txtOutputTrace_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.m_origionOfRightClick = this.m_txtOutputTrace;
                this.m_ctxtMenuPaste.Visible = false;
                this.ShowContextMenu(sender, e);
            }
        }

        private void m_txtWarningsTrace_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.m_origionOfRightClick = this.m_txtWarningsTrace;
                this.m_ctxtMenuPaste.Visible = false;
                this.ShowContextMenu(sender, e);
            }
        }

        private void m_txtInput_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.m_origionOfRightClick = this.m_txtInput;
                this.m_ctxtMenuPaste.Visible = true;
                this.ShowContextMenu(sender, e);
            }
        }

        private void m_txtUCF_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.m_origionOfRightClick = this.m_txtUCF;
                this.m_ctxtMenuPaste.Visible = false;
                this.ShowContextMenu(sender, e);
            }
        }

        private void m_txtVHDL_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.m_origionOfRightClick = this.m_txtVHDL;
                this.m_ctxtMenuPaste.Visible = false;
                this.ShowContextMenu(sender, e);
            }
        }

        private void m_txtTCL_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.m_origionOfRightClick = this.m_txtTCL;
                this.m_ctxtMenuPaste.Visible = false;
                this.ShowContextMenu(sender, e);
            }
        }


        #endregion ContextMenu

        private void m_txtInput_KeyDown(object sender, KeyEventArgs e)
        {
            String command = "";

            if (e.KeyValue == (char)Keys.Up)
            {
                command = Objects.CommandStackManager.Instance.Up();
            }
            else if (e.KeyValue == (char)Keys.Down)
            {
                command = Objects.CommandStackManager.Instance.Down();
            }

            if(!String.IsNullOrEmpty(command))
            {
                this.m_txtInput.Clear();
                this.m_txtInput.Text = command;
            }
        }

        /// <summary>
        /// current context menu txt bos
        /// </summary>
        private RichTextBox m_origionOfRightClick;




    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using GoAhead.FPGA;
using GoAhead.Code;
using GoAhead.Code.XDL;
using GoAhead.Commands.Debug;
using GoAhead.Objects;

namespace GoAhead.GUI
{
    public partial class TileViewCtrl : UserControl
    {
        public TileViewCtrl(Tile tile)
        {
            InitializeComponent();

            this.m_tile = tile;

            this.InitText();
            this.InitSwitchMatrix();
            this.InitWires();
        }

        private void InitText()
        {
            this.Text = this.m_tile.Location;

            PrintTileInfo printTileInfoCommand = new PrintTileInfo();
            printTileInfoCommand.Location = this.m_tile.Location;
            printTileInfoCommand.Do();

            this.m_txtBox.AppendText(printTileInfoCommand.OutputManager.GetOutput());

            // scroll up
            this.m_txtBox.SelectionStart = 0;
            this.m_txtBox.ScrollToCaret();
        }

        /// <summary>
        /// 2nd Tab
        /// </summary>
        private void InitSwitchMatrix()
        {
            this.m_grdViewSwitchMatrix.Rows.Clear();

            if (!FPGA.FPGA.Instance.ContainsSwitchMatrix(this.m_tile.SwitchMatrixHashCode))
            {
                return;
            }

            Regex inFilter = null;
            bool inFilterValid = false;
            this.GetFilter(this.m_txtInFilter.Text,out inFilter, out inFilterValid);
            this.m_lblInFilterValid.Text = inFilterValid ? "" : "Invalid regular expression";
            
            Regex outFilter = null;
            bool outFilterValid = false;
            this.GetFilter(this.m_txtOutFilter.Text, out outFilter, out outFilterValid);
            this.m_lblOutFilterValid.Text = outFilterValid ? "" : "Invalid regular expression";
            
            if(!inFilterValid || !outFilterValid)
            {
                return;
            }

            foreach (Tuple<Port, Port> arc in this.m_tile.SwitchMatrix.GetAllArcs().Where(a => inFilter.IsMatch(a.Item1.Name) && outFilter.IsMatch(a.Item2.Name)))
            {
                this.m_grdViewSwitchMatrix.Rows.Add(arc.Item1.Name, arc.Item2.Name);
            }

            this.InitLUTRouting();
        }

        private void InitLUTRouting()
        {
            this.m_grdViewLUTRouting.Rows.Clear();

            // LUT routing requires wire lists
            if (FPGA.FPGA.Instance.WireListCount == 0)
            {
                return;
            }
            if (IdentifierManager.Instance.IsMatch(this.m_tile.Location, IdentifierManager.RegexTypes.CLBRegex))
            {
                Regex filter1 = null;
                Regex filter2 = null;
                Regex filter3 = null;
                Regex filter4 = null;
                bool filter1Valid = false;
                bool filter2Valid = false;
                bool filter3Valid = false;
                bool filter4Valid = false;
                this.GetFilter(this.m_txtLRLutOutFilter.Text, out filter1, out filter1Valid);
                this.GetFilter(this.m_txtLREndFilter.Text, out filter2, out filter2Valid);
                this.GetFilter(this.m_txtLRBegFilter.Text, out filter3, out filter3Valid);
                this.GetFilter(this.m_txtLRLUTInFilter.Text, out filter4, out filter4Valid);

                if (!filter1Valid || !filter2Valid || !filter3Valid || !filter4Valid)
                {
                    return;
                }

                foreach (LUTRoutingInfo info in FPGA.FPGATypes.GetLUTRouting(this.m_tile))
                {
                    string port1 = info.Port1 != null ? info.Port1.Name : "";
                    string port2 = info.Port2 != null ? info.Port2.Name : "";
                    string port3 = info.Port3 != null ? info.Port3.Name : "";
                    string port4 = info.Port4 != null ? info.Port4.Name : "";

                    if (filter1.IsMatch(port1) && filter2.IsMatch(port2) && filter3.IsMatch(port3) && filter4.IsMatch(port4))
                    {
                        this.m_grdViewLUTRouting.Rows.Add(port1, port2, port3, port4);
                    }
                }
            }
        }

        private void GetFilter(String regexp, out Regex filter, out bool valid)
        {
            filter = null;
            valid = false;

            if (String.IsNullOrEmpty(regexp))
            {
                filter = new Regex("", RegexOptions.Compiled);
                valid = true;
            }
            else
            {
                valid = true;
                try
                {
                    filter = new Regex(regexp, RegexOptions.Compiled);
                }
                catch (Exception)
                {
                    valid = false;
                }
            }
        }

        /// <summary>
        /// 3rd Tab
        /// </summary>
        private void InitWires()
        {
            if (!FPGA.FPGA.Instance.ContainsWireList(this.m_tile.WireListHashCode))
            {
                return;
            }
            this.m_grdViewWires.Rows.Clear();

            foreach (Wire wire in this.m_tile.WireList)
            {
                Tile target = Navigator.GetDestinationByWire(this.m_tile, wire);

                this.m_grdViewWires.Rows.Add(wire.LocalPip, wire.LocalPipIsDriver, wire.PipOnOtherTile, wire.XIncr, wire.YIncr, target);
            }
        }

        private void m_txtInFilter_TextChanged(object sender, EventArgs e)
        {
            this.InitSwitchMatrix();
        }

        private void m_txtOutFilter_TextChanged(object sender, EventArgs e)
        {
            this.InitSwitchMatrix();
        }

        private void m_txtLRLutOutFilter_TextChanged(object sender, EventArgs e)
        {
            this.InitLUTRouting();
        }

        private void m_txtLREndFilter_TextChanged(object sender, EventArgs e)
        {
            this.InitLUTRouting();
        }

        private void m_txtLRBegFilter_TextChanged(object sender, EventArgs e)
        {
            this.InitLUTRouting();
        }

        private void m_txtLRLUTInFilter_TextChanged(object sender, EventArgs e)
        {
            this.InitLUTRouting();
        }

        private void m_tabSwitchMatrix_Resize(object sender, EventArgs e)
        {
            this.m_grdViewSwitchMatrix.Width = this.m_tabSwitchMatrix.Width - 7;
            this.m_grdViewSwitchMatrix.Top = this.m_grpFilter.Height + 5;
            this.m_grdViewSwitchMatrix.Height = (this.m_tabSwitchMatrix.Height - this.m_grpFilter.Height) - 7;
        }

        private readonly Tile m_tile;


    }
}

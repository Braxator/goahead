﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using GoAhead.FPGA;
using GoAhead.Objects;

namespace GoAhead.GUI.TileView
{
    public partial class TileViewForm : Form
    {
        public TileViewForm(Tile tile)
        {
            InitializeComponent();

            this.m_tile = tile;
            Settings.StoredPreferences.Instance.GUISettings.Open(this);

            List<Tile> tiles = new List<Tile>();
            tiles.Add(this.m_tile);
            if (IdentifierManager.Instance.IsMatch(this.m_tile.Location, IdentifierManager.RegexTypes.InterconnectRegex))
            {
                tiles.AddRange(FPGA.FPGATypes.GetCLTile(this.m_tile));
            }
            if (IdentifierManager.Instance.IsMatch(this.m_tile.Location, IdentifierManager.RegexTypes.CLBRegex))
            {
                Tile interconnect = FPGA.FPGATypes.GetInterconnectTile(this.m_tile);
                tiles.Add(interconnect);
            }
            foreach(Tile t in tiles)
            {
                TabPage page = new TabPage();
                page.Text = t.Location;
                TileViewCtrl viewCtrl = new TileViewCtrl(t);
                viewCtrl.Dock = DockStyle.Fill;
                page.Controls.Add(viewCtrl);
                this.m_tabTop.TabPages.Add(page);
            }
        }

        private void TileViewForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Settings.StoredPreferences.Instance.GUISettings.Close(this);
        }

        private readonly Tile m_tile;
    }
}

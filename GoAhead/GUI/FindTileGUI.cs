﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GoAhead.Commands;
using GoAhead.FPGA;
using GoAhead.Settings;

namespace GoAhead.GUI
{
    public partial class FindTileGUI : Form
    {
        public FindTileGUI(Form invalidateMeAfterEachCommand)
        {
            InitializeComponent();

            this.m_formToInvalidateAfterEachSearch = invalidateMeAfterEachCommand;

            Settings.StoredPreferences.Instance.GUISettings.Open(this);
        }

        public FindTileGUI(UserControl invalidateMeAfterEachCommand)
        {
            InitializeComponent();

            this.m_userCtrlToInvalidateAfterEachSearch = invalidateMeAfterEachCommand;
        }

        private void InvalidateCalles()
        {
            if (this.m_formToInvalidateAfterEachSearch != null)
            {
                this.m_formToInvalidateAfterEachSearch.Invalidate();
            }
            if (this.m_userCtrlToInvalidateAfterEachSearch != null)
            {
                if (this.m_userCtrlToInvalidateAfterEachSearch is FPGAViewCtrl)
                {
                    FPGAViewCtrl fpgaView = (FPGAViewCtrl)this.m_userCtrlToInvalidateAfterEachSearch;
                    fpgaView.PointToSelection = true;
                }

                this.m_userCtrlToInvalidateAfterEachSearch.Invalidate();
            }
        }

        private void m_btnFindXY_Click(object sender, EventArgs e)
        {
            try
            {
                int x = Int32.Parse(this.m_txtX.Text);
                int y = Int32.Parse(this.m_txtY.Text);

                CommandExecuter.Instance.Execute(new Commands.Selection.ClearSelection());
                CommandExecuter.Instance.Execute(new Commands.Selection.AddToSelectionXY(x, y, x, y));
                if (StoredPreferences.Instance.ExecuteExpandSelection)
                {
                    Commands.CommandExecuter.Instance.Execute(new Commands.Selection.ExpandSelection());
                }

                this.InvalidateCalles();
            }
            catch (Exception error)
            {
                MessageBox.Show("Input error", "Errror: " + error.Message);
            }
        }

        private void m_btnFindSlice_Click(object sender, EventArgs e)
        {
            String sliceName = this.m_txtSliceName.Text.Replace(" ", String.Empty);
            foreach (Tile t in FPGA.FPGA.Instance.GetAllTiles())
            {
                foreach (Slice s in t.Slices)
                {
                    if (s.SliceName.Equals(sliceName))
                    {
                        Commands.CommandExecuter.Instance.Execute(new Commands.Selection.ClearSelection());
                        Commands.CommandExecuter.Instance.Execute(new Commands.Selection.AddToSelectionXY(t.TileKey.X, t.TileKey.Y, t.TileKey.X, t.TileKey.Y));
                        if (StoredPreferences.Instance.ExecuteExpandSelection)
                        {
                            Commands.CommandExecuter.Instance.Execute(new Commands.Selection.ExpandSelection());
                        }

                        this.InvalidateCalles();
                        return;
                    }
                }
            }
        }

        private void m_btnFindLocation_Click(object sender, EventArgs e)
        {
            String location = this.m_txtLocation.Text.Replace(" ", String.Empty);

            if (!FPGA.FPGA.Instance.Contains(location))
            {
                MessageBox.Show("Location not found", "Error", MessageBoxButtons.OK);
                return;
            }

            Tile t = FPGA.FPGA.Instance.GetTile(location);

            Commands.CommandExecuter.Instance.Execute(new Commands.Selection.ClearSelection());
            Commands.CommandExecuter.Instance.Execute(new Commands.Selection.AddToSelectionXY(t.TileKey.X, t.TileKey.Y, t.TileKey.X, t.TileKey.Y));
            if (StoredPreferences.Instance.ExecuteExpandSelection)
            {
                Commands.CommandExecuter.Instance.Execute(new Commands.Selection.ExpandSelection());
            }
            this.InvalidateCalles();
        }

        private void FindTileGUI_FormClosed(object sender, FormClosedEventArgs e)
        {
            Settings.StoredPreferences.Instance.GUISettings.Close(this);
        }


        private Form m_formToInvalidateAfterEachSearch = null;
        private UserControl m_userCtrlToInvalidateAfterEachSearch = null;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GoAhead.Objects;
using GoAhead.Interfaces;

namespace GoAhead.GUI.Watch
{
    public partial class VariableWatchCtrl : UserControl, IObserver
    {
        public VariableWatchCtrl()
        {
            InitializeComponent();
            Objects.VariableManager.Instance.Add(this);
        }

        private void UpdateValues()
        {
            this.m_dataGrdArguments.Columns.Clear();
            this.m_dataGrdArguments.Columns.Add("Name", "Name");
            this.m_dataGrdArguments.Columns.Add("Value", "Value");

            this.m_dataGrdArguments.Columns[0].ReadOnly = true;
            // the user will edit this column
            this.m_dataGrdArguments.Columns[1].ReadOnly = false;

            foreach (String varName in VariableManager.Instance.GetAllVariableNames())
            {
                String varValue = VariableManager.Instance.GetValue(varName);

                this.m_dataGrdArguments.Rows.Add(varName, varValue);
            }            
        }

        public void Notify(Object obj)
        {
            this.UpdateValues();
        }

    }
}

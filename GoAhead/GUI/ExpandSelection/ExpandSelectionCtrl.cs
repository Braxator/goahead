﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GoAhead.FPGA;
using GoAhead.Commands;
using GoAhead.Commands.Selection;

namespace GoAhead.GUI.ExpandSelection
{
    public partial class ExpandSelectionCtrl : UserControl
    {
        public ExpandSelectionCtrl()
        {
            InitializeComponent();

            BindingSource selBsrc = new BindingSource();
            selBsrc.DataSource = FPGA.TileSelectionManager.Instance.UserSelectionTypes;
            this.m_cmbBoxUserSel.DataSource = selBsrc;

        }

        private void m_btnOK_Click(object sender, EventArgs e)
        {
            if (this.m_cmbBoxUserSel.SelectedItem == null)
            {
                MessageBox.Show("No user selection selected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            String userSel = this.m_cmbBoxUserSel.SelectedItem.ToString();
            ExpandSelectionInRow expandCmd = null;
            if (this.m_rbBtnLeft.Checked)
            {
                expandCmd = new ExpandSelectionLeft();
            }
            else if (this.m_rbBtnRight.Checked)
            {
                expandCmd = new ExpandSelectionRight();
            }
            else if (this.m_rbBtnLeftRight.Checked)
            {
                expandCmd = new ExpandSelectionLeftAndRight();
            }
            expandCmd.UserSelectionType = userSel;
            CommandExecuter.Instance.Execute(expandCmd);

            if (this.Gui != null && FPGA.TileSelectionManager.Instance.NumberOfSelectedTiles > 0)
            {
                Tile t = FPGA.TileSelectionManager.Instance.GetSelectedTile(".*", FPGATypes.Placement.UpperLeft);
                this.Gui.FPGAView.UpdateStatusStrip(t.TileKey);
                this.Gui.FPGAView.Invalidate();
            }

            this.ParentForm.Close();
        }

        public GUI Gui { get; set; }
    }
}

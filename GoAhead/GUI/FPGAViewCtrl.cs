﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using GoAhead.FPGA;
using GoAhead.Commands;
using GoAhead.Commands.Selection;
using GoAhead.Commands.CommandExecutionSettings;
using GoAhead.Settings;
using GoAhead.Objects;
using GoAhead.GUI.TileView;
using GoAhead.GUI.FPGAView;

namespace GoAhead.GUI
{
    public partial class FPGAViewCtrl : UserControl, Interfaces.IResetable
    {
        public FPGAViewCtrl()
        {
            InitializeComponent();

            this.UpdateDrawingOptionsMenu();

            BindingSource macroBsrc = new BindingSource();
            macroBsrc.DataSource = Objects.Library.Instance.LibraryElements;
            this.m_toolStripDrpDownMacro.ComboBox.DisplayMember = "Name";
            this.m_toolStripDrpDownMacro.ComboBox.ValueMember = "Name";
            this.m_toolStripDrpDownMacro.ComboBox.DataSource = macroBsrc;

            BindingSource selBsrc = new BindingSource();
            selBsrc.DataSource = FPGA.TileSelectionManager.Instance.UserSelectionTypes;

            this.m_toolStripTopDrpDownSelect.ComboBox.DataSource = selBsrc;
            // be notified about reset
            Commands.Reset.ObjectsToReset.Add(this);

            this.m_timer.Interval = 1000;
            this.m_timer.Tick += this.ShowToolTipAfterTimerFired;              

            this.Reset();

            PrintProgressToGUIHook hook = new PrintProgressToGUIHook();
            hook.ProgressBar = this.m_toolStripProgressBar;
            CommandExecuter.Instance.AddHook(hook);
        }

        private void UpdateDrawingOptionsMenu()
        {
            this.m_toolStripDrpDownMenuPaintingClockRegion.Checked = Settings.StoredPreferences.Instance.HighLightClockRegions;
            this.m_toolStripDrpDownMenuPaintingMacros.Checked = Settings.StoredPreferences.Instance.HighLightPlacedMacros;
            this.m_toolStripDrpDownMenuPaintingRAM.Checked = Settings.StoredPreferences.Instance.HighLightRAMS;
            this.m_toolStripDrpDownMenuPaintingSelection.Checked = Settings.StoredPreferences.Instance.HighLightSelection;
            this.m_toolStripDrpDownMenuPaintingPossibleMacroPlacements.Checked = Settings.StoredPreferences.Instance.HighLightPossibleMacroPlacements;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if(FPGA.FPGA.Instance.Family.Equals(FPGA.FPGATypes.FPGAFamily.Undefined))
            {
                return;
            }

            if (this.TilePaintStrategy == null)
            {
                return;
            }

            // call stratey
            this.TilePaintStrategy.Invalidate();

            if (this.TilePaintStrategy != null)
            {
                if (this.TilePaintStrategy.TileBitmap != null)
                {
                    this.m_zoomPictBox.Image = this.TilePaintStrategy.TileBitmap;
                }
            }
            this.m_zoomPictBox.Invalidate();
            this.m_panelSelection.Invalidate();

            if (this.m_zoomPictBox.Image != null)
            {
                float panelWidth = (float)this.m_zoomPictBox.Image.Width * this.m_zoomPictBox.Zoom;
                float panelHeight = (float)this.m_zoomPictBox.Image.Height * this.m_zoomPictBox.Zoom;
            }
            
            base.OnPaint(e);
        }

        private void m_panelTop_Paint(object sender, PaintEventArgs e)
        {
            if (this.RectangleSelect || this.ZoomSelectOngoing)
            {
                int x = Math.Min(this.m_mouseDownPosition.X, this.m_currentMousePositionWithRectangleSelect.X);
                x += this.m_zoomPictBox.HorizontalScroll.Value;
                int y = Math.Min(this.m_mouseDownPosition.Y, this.m_currentMousePositionWithRectangleSelect.Y);
                y += this.m_zoomPictBox.VerticalScroll.Value;
                int width = Math.Abs(this.m_mouseDownPosition.X - this.m_currentMousePositionWithRectangleSelect.X);
                int height = Math.Abs(this.m_mouseDownPosition.Y - this.m_currentMousePositionWithRectangleSelect.Y);

                if (StoredPreferences.Instance.RectangleWidth != this.RectanglePenWidth)
                {
                    this.RectanglePenWidth = StoredPreferences.Instance.RectangleWidth;
                    this.m_rectanglePen = new Pen(Color.Black, this.RectanglePenWidth);
                }
                

                //e.Graphics.DrawRectangle(Pens.Black, x, y, width, height);
                e.Graphics.DrawRectangle(m_rectanglePen, x, y, width, height);
            }

            if (this.m_zoomPictBox.Image != null)
            {
                float panelWidth = (float)this.m_zoomPictBox.Image.Width * this.m_zoomPictBox.Zoom;
                float panelHeight = (float)this.m_zoomPictBox.Image.Height * this.m_zoomPictBox.Zoom;
                this.m_panelSelection.Width = (int)panelWidth;
                this.m_panelSelection.Height = (int)panelHeight;
            }

            if (this.PointToSelection)
            {
                double x, y;
                FPGA.TileSelectionManager.Instance.GetCenterOfSelection(p => true, out x, out y);
                float xf = (float)x * this.TileSize * this.ZoomPictureBox.Zoom;
                float yf = (float)y * this.TileSize * this.ZoomPictureBox.Zoom;
                Pen pen = new Pen(Brushes.Black, 4);
                e.Graphics.DrawLine(pen, 0, 0, xf, yf);
            }
        }

        public void SaveFPGAViewAsPNG(String fileName)
        {
            this.m_zoomPictBox.Image.Save(fileName, System.Drawing.Imaging.ImageFormat.Png);
        }
      
        public Regex TileRegex
        {
            get { return this.m_tileFilter; }
        }

        private String GetTileFilter()
        {
            // check wether filter is valid regex
            String filter = this.m_toolStripTxtBoxTileFilter.Text;
            try
            {
                bool test = Regex.IsMatch("test", filter);
            }
            catch (Exception)
            {
                // invalid
                filter = "";
            }
            return filter;
        }

        public Color GetColor(Tile tile, bool addIncrementForSelectedTiles, bool addIncrementForUserSelectedTiles) 
        {
            Color c = Settings.ColorSettings.Instance.GetColor(tile);

            // shift colors for selected tiles
            int incR = 0;
            int incG = 0;
            int incB = 0;

            if (addIncrementForSelectedTiles && FPGA.TileSelectionManager.Instance.IsSelected(tile.TileKey))
            {
                incR = Settings.ColorSettings.Instance.SelectionIncrement.R;
                incG = Settings.ColorSettings.Instance.SelectionIncrement.G;
                incB = Settings.ColorSettings.Instance.SelectionIncrement.B;
            }
            else if (addIncrementForUserSelectedTiles && FPGA.TileSelectionManager.Instance.IsUserSelected(tile.TileKey))
            {
                incR = Settings.ColorSettings.Instance.UserSelectionIncrement.R;
                incG = Settings.ColorSettings.Instance.UserSelectionIncrement.G;
                incB = Settings.ColorSettings.Instance.UserSelectionIncrement.B;
            }
            else if (tile.HasBlockedPorts || tile.HasUsedSlice)
            {
                incR = Settings.ColorSettings.Instance.BlockedPortsColor.R;
                incG = Settings.ColorSettings.Instance.BlockedPortsColor.G;
                incB = Settings.ColorSettings.Instance.BlockedPortsColor.B;
            }

            return Color.FromArgb((c.R + incR) % 256, (c.G + incG) % 256, (c.B + incB) % 256);
            // saturation
            //return Color.FromArgb(
                //((c.R + incR) < 255 ? c.R + incR : 255),
                //((c.G + incG) < 255 ? c.G + incG : 255),
                //((c.B + incB) < 255 ? c.B + incB : 255));
        }

        public TileKey GetClickedKey(int x, int y)
        {
            return this.XYConverter.GetClickedKey(x, y);
        }

        private void FullZoom()
        {
            int width = this.Width;
            int heigth = this.Height;

            float horZoom = (float)this.m_zoomPictBox.Width / (float)((FPGA.FPGA.Instance.MaxX + 1) * this.TileSize);
            float verZoom = (float)this.m_zoomPictBox.Height / (float)((FPGA.FPGA.Instance.MaxY + 1) * this.TileSize);

            this.m_zoomPictBox.Zoom = Math.Min(horZoom, verZoom);
            this.Invalidate();
        }

        public int TileSize
        {
            //get { return 6; }
            get { return 16; } 
        } 

        #region contextMenu
        private void m_contextMenuStoreAsPartialAreas_Click(object sender, EventArgs e)
        {
            StoreCurrentSelectionAs cmd = new StoreCurrentSelectionAs();
            cmd.UserSelectionType = "PartialArea";
            CommandExecuter.Instance.Execute(cmd);
            this.Invalidate();
        }

        private void m_contextMenuStoreAsUserDefinedName_Click(object sender, EventArgs e)
        {
            UserSelectionForm frm = new UserSelectionForm(false, true);
            frm.ShowDialog();
        }

        private void m_contextMenuClear_Click(object sender, EventArgs e)
        {
            ClearSelection clearCmd = new ClearSelection();
            CommandExecuter.Instance.Execute(clearCmd);
        }

        private void m_contextMenuTunnel_Click_1(object sender, EventArgs e)
        {
            PortSelectionForm dlg = new PortSelectionForm();
            dlg.Show();
        }
        private void m_contextMenuCopyIdentifier_Click(object sender, EventArgs e)
        {
            if(FPGA.FPGA.Instance.Contains(this.m_rightClickedKey))
            {
                String location = FPGA.FPGA.Instance.GetTile(this.m_rightClickedKey).Location;
                Clipboard.SetText(location);
            }
        }
        private void m_contextMenuFullZoom_Click(object sender, EventArgs e)
        {
            this.FullZoom();
        }
        #endregion

        public void ZoomOut()
        {
            this.m_zoomPictBox.Zoom *= 0.9F;
            this.Invalidate();
        }

        public void ZoomIn()
        {
            this.m_zoomPictBox.Zoom *= 1.1F;
            this.Invalidate();
        }

        #region Toolbar
        private void m_toolStripBtnZoomOut_Click(object sender, EventArgs e)
        {
            this.ZoomOut();
        }
        private void m_toolStripBtnZoomIn_Click(object sender, EventArgs e)
        {
            this.ZoomIn();

        }

        /// <summary>
        /// Redraw upon enter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_toolStripTxtBoxTileFilter_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                String pattern = this.GetTileFilter();
                m_tileFilter = new Regex(pattern, RegexOptions.Compiled);
                this.TilePaintStrategy.DrawTiles(true, true);
                this.Invalidate();
            }
        }

        private void m_toolStripDrpDownMenuPainting_Click(object sender, EventArgs e)
        {
            this.UpdateDrawingOptionsMenu();
        }

        public void UpdateStatusStrip(TileKey selectedKey)
        {
            String statusString = "";
            if (FPGA.FPGA.Instance.Contains(selectedKey))
            {
                Tile selectedTile = FPGA.FPGA.Instance.GetTile(selectedKey);
                statusString = selectedTile.Location + " " + selectedKey.ToString();

                if (selectedTile.Slices.Count > 0)
                {
                    statusString += " (";

                    // only display up to to slice
                    int displayedSlices = Math.Min(2, selectedTile.Slices.Count);
                    for (int i = 0; i < displayedSlices; i++)
                    {
                        statusString += selectedTile.Slices[i].ToString();
                        if (i != selectedTile.Slices.Count - 1)
                        {
                            statusString += ",";
                        }
                    }

                    // add dots for tiles with more than two slices
                    if (selectedTile.Slices.Count > 2)
                    {
                        statusString += "...";
                    }

                    statusString += ") ";                
                }
                // 2 element buffer
                this.m_lastClickedTile = this.m_currentlyClickedTile;
                this.m_currentlyClickedTile = selectedTile;
            }          
          
            // print selection info
            if (GoAhead.Settings.StoredPreferences.Instance.PrintSelectionResourceInfo)
            {
                statusString += "Selection contains ";
                statusString += FPGA.TileSelectionManager.Instance.NumberOfSelectedTiles + " tiles, ";
                int clbs = 0;
                int brams = 0;
                int dsps = 0;
                FPGA.TileSelectionManager.Instance.GetRessourcesInSelection(FPGA.TileSelectionManager.Instance.GetSelectedTiles(), out clbs, out brams, out dsps);
                int others = FPGA.TileSelectionManager.Instance.NumberOfSelectedTiles - (clbs + brams + dsps);
                statusString += clbs + " CLBs, ";
                statusString += brams + " BRAMs, and ";
                statusString += dsps + " DPS tiles, and ";
                statusString += others + " other tiles";
            }

            this.m_statusStripLabelSelectedTile.Text = statusString;
        }

        private void m_toolStripBtnFind_Click(object sender, EventArgs e)
        {
            FindTileGUI dlg = new FindTileGUI(this);
            dlg.Show();
        }

        #endregion Toolbar
       
        #region ToolbarDropDownMenu
        private void m_toolStripDrpDownMenuPaintingRAM_Click(object sender, EventArgs e)
        {
            Settings.StoredPreferences.Instance.HighLightRAMS = this.m_toolStripDrpDownMenuPaintingRAM.Checked;
            if (!Settings.StoredPreferences.Instance.HighLightRAMS)
            {
                // turn off -> repaint all
                this.TilePaintStrategy.DrawTiles(true, true);
            }
                        
            this.Invalidate();    
        }


        private void m_toolStripDrpDownMenuPaintingSelection_Click(object sender, EventArgs e)
        {
            Settings.StoredPreferences.Instance.HighLightSelection = this.m_toolStripDrpDownMenuPaintingSelection.Checked;
            if (!Settings.StoredPreferences.Instance.HighLightSelection)
            {
                // turn off -> repaint all
                this.TilePaintStrategy.DrawTiles(true, true);
            }

            this.Invalidate();    
        }

        private void m_toolStripDrpDownMenuPaintingClockRegion_Click(object sender, EventArgs e)
        {
            Settings.StoredPreferences.Instance.HighLightClockRegions = this.m_toolStripDrpDownMenuPaintingClockRegion.Checked;
            if (!Settings.StoredPreferences.Instance.HighLightClockRegions)
            {
                // turn off -> repaint all
                this.TilePaintStrategy.DrawTiles(true, true);
            }

            this.Invalidate();
        }

        private void m_toolStripDrpDownMenuPaintingMacros_Click(object sender, EventArgs e)
        {
            Settings.StoredPreferences.Instance.HighLightPlacedMacros = this.m_toolStripDrpDownMenuPaintingMacros.Checked;
            if (!Settings.StoredPreferences.Instance.HighLightPlacedMacros)
            {
                // turn off -> repaint all
                this.TilePaintStrategy.DrawTiles(true, true);
            }

            this.Invalidate();
        }

        private void m_toolStripDrpDownMenuPaintingPossibleMacroPlacements_Click(object sender, EventArgs e)
        {
            Settings.StoredPreferences.Instance.HighLightPossibleMacroPlacements = this.m_toolStripDrpDownMenuPaintingPossibleMacroPlacements.Checked;
            if (!Settings.StoredPreferences.Instance.HighLightPossibleMacroPlacements)
            {
                // turn off -> repaint all
                this.TilePaintStrategy.DrawTiles(true, true);
            }

            this.Invalidate();
        }

        private void m_toolStripDrpDownMenuPaintingToolTips_Click_1(object sender, EventArgs e)
        {
            Settings.StoredPreferences.Instance.ShowToolTips = this.m_toolStripDrpDownMenuPaintingToolTips.Checked;
            this.Invalidate();
        }
        
        private void m_toolStripDrpDownMacro_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.m_toolStripDrpDownMacro.SelectedItem == null)
            {
                return;
            }

            if (!(this.m_toolStripDrpDownMacro.SelectedItem is Objects.LibraryElement))
            {
                // should never happen
                return;
            }
            if (this.TilePaintStrategy == null)
            {
                return;
            }

            foreach (HighLighter highLighter in this.TilePaintStrategy.HighLighter.Where(hl => hl is PossibleMacroPlacementHighLighter))
            {
                LibraryElement libElement = (LibraryElement)this.m_toolStripDrpDownMacro.SelectedItem;
                ((PossibleMacroPlacementHighLighter)highLighter).LibrayElementName = libElement.Name;
            }
        }

        private void m_toolStripTopDrpDownSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            // is null after deleting a user selection
            if (this.m_toolStripTopDrpDownSelect.SelectedItem == null)
            {
                return;
            }
            String userSel = this.m_toolStripTopDrpDownSelect.SelectedItem.ToString();

            if (FPGA.TileSelectionManager.Instance.GetUserSelectionTileCount(userSel) > 0)
            {
                SelectUserSelection selCmd = new SelectUserSelection();
                selCmd.UserSelectionType = userSel;
                CommandExecuter.Instance.Execute(selCmd);

                Tile upperLeft = FPGA.TileSelectionManager.Instance.GetUserSelectedTile("", selCmd.UserSelectionType, FPGATypes.Placement.UpperLeft);
                this.UpdateStatusStrip(upperLeft.TileKey);
            }
        }

        private void m_toolStripTopBtnClear_Click(object sender, EventArgs e)
        {
            // is null after deleting a user selection
            if (this.m_toolStripTopDrpDownSelect.SelectedItem == null)
            {
                return;
            }
            ClearUserSelection clearCmd = new ClearUserSelection();
            clearCmd.UserSelectionType = this.m_toolStripTopDrpDownSelect.SelectedItem.ToString();
            Commands.CommandExecuter.Instance.Execute(clearCmd);
        }

        private void m_toolStripDrpDownMenuPaintingToolTips_Click_2(object sender, EventArgs e)
        {
            StoredPreferences.Instance.ShowToolTips = this.m_toolStripDrpDownMenuPaintingToolTips.Checked;
        }


        private void m_toolStripDrpDownMenuMuteOutput_Click(object sender, EventArgs e)
        {
            if (this.m_toolStripDrpDownMenuMuteOutput.Checked)
            {
                MuteOutput cmd = new MuteOutput();
                CommandExecuter.Instance.Execute(cmd);
            }
            else
            {
                UnmuteOutput cmd = new UnmuteOutput();
                CommandExecuter.Instance.Execute(cmd); 
            }
        }

        private void m_toolStripDrpDownMenuPainting_MouseDown(object sender, MouseEventArgs e)
        {
            this.m_toolStripDrpDownMenuPaintingToolTips.Checked = StoredPreferences.Instance.ShowToolTips;
        }

        #endregion

        public void Reset()
        {
            if (this.TilePaintStrategy != null)
            {
                this.TilePaintStrategy.Reset();
                this.TilePaintStrategy.DrawTiles(true, true);
                this.m_zoomPictBox.Image = this.TilePaintStrategy.TileBitmap;
            }

            this.FullZoom();                       
            this.m_zoomPictBox.Invalidate();
            this.m_panelSelection.Invalidate();
        }
        
        private void m_zoomPictBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button.Equals(MouseButtons.Left))
            {
                this.SelectedFromMouseDownToCurrent();
                
                this.UpdateStatusStrip(this.GetClickedKey(e.X, e.Y));
                this.RectangleSelect = false;
                this.ZoomSelectOngoing = false;
                this.Invalidate();
                this.m_panelSelection.Invalidate();
            }
            else if (e.Button == MouseButtons.Right)
            {
                this.m_rightClickedKey = this.GetClickedKey(e.X, e.Y);
                this.m_contextMenu.Show(this, new Point(e.X, e.Y));
            }
            else if(e.Button == MouseButtons.Middle)
            {
                if (this.ZoomSelectOngoing)
                {
                    this.ZoomFromMouseDownToCurrent();

                    this.ZoomSelectOngoing = false;
                    this.UpdateStatusStrip(this.GetClickedKey(e.X, e.Y));
                    this.RectangleSelect = false;
                    this.Invalidate();
                    this.m_panelSelection.Invalidate();
                }
            }
        }

        private void ZoomFromMouseDownToCurrent()
        {
            if (this.m_currentMousePositionWithRectangleSelect.X < this.m_mouseDownPosition.X && this.m_currentMousePositionWithRectangleSelect.Y < this.m_mouseDownPosition.Y)
            {
                this.m_zoomPictBox.Zoom *= 0.5f;
            }
            else
            {
                this.m_zoomPictBox.Zoom *= 1.5f;
            }

            //this.m_zoomPictBox.HorizontalScroll.Value = upperLeftTile.X;
            //this.m_zoomPictBox.VerticalScroll.Value = upperLeftTile.Y;
        }

        private void SelectedFromMouseDownToCurrent()
        {
            bool shiftDown = Control.ModifierKeys == Keys.Shift;
            bool ctrlDown = Control.ModifierKeys == Keys.Control;
            bool altDown = Control.ModifierKeys == Keys.Alt;
            bool altAndCtrlDown = Control.ModifierKeys == (Keys.Control | Keys.Alt);

            if (!ctrlDown && !altAndCtrlDown)
            {
                CommandExecuter.Instance.Execute(new Commands.Selection.ClearSelection());
            }
            TileKey upperLeftTile = null;
            TileKey lowerRightTile = null;

            if(shiftDown)
            {
                TileKey clickedKey = this.GetClickedKey(this.m_mouseDownPosition.X, this.m_mouseDownPosition.Y);
                if (this.m_lastClickedTile == null)
                {
                    return;
                }
                if (!FPGA.FPGA.Instance.Contains(clickedKey))
                {
                    return;
                }

                upperLeftTile = this.m_lastClickedTile.TileKey;
                lowerRightTile = clickedKey;
            }
            else
            {
                int upperLeftX = Math.Min(this.m_mouseDownPosition.X, this.m_currentMousePositionWithRectangleSelect.X);
                int upperLeftY = Math.Min(this.m_mouseDownPosition.Y, this.m_currentMousePositionWithRectangleSelect.Y);
                int lowerRightX = Math.Max(this.m_mouseDownPosition.X, this.m_currentMousePositionWithRectangleSelect.X);
                int lowerRightY = Math.Max(this.m_mouseDownPosition.Y, this.m_currentMousePositionWithRectangleSelect.Y);

                upperLeftTile = this.GetClickedKey(upperLeftX, upperLeftY);
                lowerRightTile = this.GetClickedKey(lowerRightX, lowerRightY);      
            }

            if (!String.IsNullOrEmpty(this.GetTileFilter()))
            {
                AddToSelectionXY addCmd = new AddToSelectionXY();
                addCmd.Filter = this.GetTileFilter();
                addCmd.LowerRightX = 0;
                addCmd.LowerRightY = 0;
                addCmd.UpperLeftX = 0;
                addCmd.UpperLeftY = 0;
                CommandExecuter.Instance.Execute(addCmd);
            }
            else if (altDown || altAndCtrlDown)
            {
                //Tile ult = FPGA.FPGA.Instance.GetTile(upperLeftTile);
                //Tile lrt = FPGA.FPGA.Instance.GetTile(lowerRightTile);
                try
                {
                    AddBlockToSelection addcmd = new AddBlockToSelection(upperLeftTile.X, upperLeftTile.Y, lowerRightTile.X, lowerRightTile.Y);
                    CommandExecuter.Instance.Execute(addcmd);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                CommandExecuter.Instance.Execute(new AddToSelectionXY(upperLeftTile.X, upperLeftTile.Y, lowerRightTile.X, lowerRightTile.Y));
            }

            if (StoredPreferences.Instance.ExecuteExpandSelection)
            {
                CommandExecuter.Instance.Execute(new Commands.Selection.ExpandSelection());
            }
        }

        private void m_zoomPictBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.RectangleSelect)
            {
                this.m_currentMousePositionWithRectangleSelect = e.Location;
                this.m_panelSelection.Invalidate();
            }
            if (this.ZoomSelectStart || this.ZoomSelectOngoing)
            {
                this.ZoomSelectStart = false;
                this.ZoomSelectOngoing = true;
                this.m_currentMousePositionWithRectangleSelect = e.Location;
                this.m_panelSelection.Invalidate();

            }
            if (this.m_mouseEntered && StoredPreferences.Instance.ShowToolTips)
            {
                // start timer for toll tip
                this.m_timer.Stop();
                this.m_timer.Start();
                // store position
                this.m_timer.Tag = e.Location;
            }
         }

        private void ShowToolTipAfterTimerFired(object sender, EventArgs e)
        {
            if (StoredPreferences.Instance.ShowToolTips)
            {
                Point p = (Point)this.m_timer.Tag;

                // only show a new tool tip, if the position changes
                if (this.m_lastToolTipLocation.Equals(p))
                {
                    return;
                }

                this.m_lastToolTipLocation = p;
                TileKey key = this.GetClickedKey(p.X, p.Y);
                if (FPGA.FPGA.Instance.Contains(key))
                {
                    Tile where = FPGA.FPGA.Instance.GetTile(key);
                    String toolTip = key.ToString() + " " + where.Location;
                    Objects.LibElemInst inst = Objects.LibraryElementInstanceManager.Instance.GetInstantiation(where);
                    if (inst != null)
                    {
                        toolTip += Environment.NewLine + inst.ToString();

                        foreach (Tuple<String, String, PortMapper.MappingKind> tuple in inst.PortMapper.GetMappings().OrderBy(t => t.Item1))
                        {
                            toolTip += Environment.NewLine + tuple.Item1 + " => " + tuple.Item2 + " (" + tuple.Item3 + ")";
                        }
                    }
                    if (Objects.Blackboard.Instance.HasToolTipInfo(where))
                    {
                        toolTip += Environment.NewLine + Objects.Blackboard.Instance.GetToolTipInfo(where);
                    }                    
                    this.m_toolTip.Show(toolTip, this, p.X, p.Y + 20, 10000);
                }
            }
        }

        private void m_zoomPictBox_MouseDown(object sender, MouseEventArgs e)
        {
            this.PointToSelection = false;

            if (e.Button.Equals(MouseButtons.Left) || e.Button.Equals(MouseButtons.Middle))
            {
                // capture old value for range selection
                this.m_mouseDownPosition = e.Location;
                this.m_currentMousePositionWithRectangleSelect = e.Location;
                this.UpdateStatusStrip(this.GetClickedKey(e.X, e.Y));
            }

            if (e.Button.Equals(MouseButtons.Left))
            {
                this.RectangleSelect = true;
            }
            if (e.Button.Equals(MouseButtons.Middle))
            {
                this.ZoomSelectStart = true;
            }
        }

        private void m_zoomPictBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            TileKey clickedKey = this.GetClickedKey(e.X, e.Y);
            if (FPGA.FPGA.Instance.Contains(clickedKey))
            {
                TileViewForm tileView = new TileViewForm(FPGA.FPGA.Instance.GetTile(clickedKey));
                tileView.Show();
            }
        }

        public ZoomPicBox ZoomPictureBox
        {
            get { return this.m_zoomPictBox; }
        }
             
        private void m_contextMenuSelectAll_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(this.GetTileFilter()))
            {
                SelectAllWithFilter selCmd = new SelectAllWithFilter();
                selCmd.Filter = this.GetTileFilter();
                CommandExecuter.Instance.Execute(selCmd);
            }
            else
            {
                SelectAll selCmd = new SelectAll();
                CommandExecuter.Instance.Execute(selCmd);
            }
        }

        private void m_contextMenuInvertSelection_Click(object sender, EventArgs e)
        {
            InvertSelection invCmd = new InvertSelection();
            CommandExecuter.Instance.Execute(invCmd);
        }

        public float RectanglePenWidth
        {
            get { return this.m_rectanglePen.Width; }
            set { this.m_rectanglePen.Width = value; }
        }

        #region Member
        /// <summary>
        /// The strategy to get the tile be mouse down coordinates
        /// </summary>
        public ClickToTileKeyConverter XYConverter = null;
        private Regex m_tileFilter = new Regex("", RegexOptions.Compiled);
        private Point m_mouseDownPosition = new Point(0, 0);
        private Point m_currentMousePositionWithRectangleSelect = new Point(0, 0);
        public bool ZoomSelectStart { get; set; }
        public bool ZoomSelectOngoing { get; set; }
        public bool RectangleSelect { get; set; }
        public bool PointToSelection { get; set; }

        private bool m_mouseEntered = false;

        /// <summary>
        /// Strange: The mouse move event occurs after the tooltip disappears
        /// Hoever, the location property does not change with the event
        /// In order not to show the tooltip again, we store the last position and only reshow the tooltip if the position changes
        /// </summary>
        private Point m_lastToolTipLocation = new Point(0, 0);

        /// <summary>
        ///  painting strategy
        /// </summary>
        public Painter TilePaintStrategy = null;

        private ToolTip m_toolTip = new ToolTip();
        private Timer m_timer = new Timer();
        private Tile m_currentlyClickedTile = null;
        private Tile m_lastClickedTile = null;

        private void m_zoomPictBox_MouseEnter(object sender, EventArgs e)
        {
            this.m_mouseEntered = true;
        }

        private void m_zoomPictBox_MouseLeave(object sender, EventArgs e)
        {
            this.m_mouseEntered = false;
        }

        private TileKey m_rightClickedKey = null;

        private Pen m_rectanglePen = new Pen(Color.Black, 1);

        #endregion Member


  

    }
}

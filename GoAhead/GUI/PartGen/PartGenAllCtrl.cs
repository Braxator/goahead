﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GoAhead.Commands;
using GoAhead.Commands.Data;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace GoAhead.GUI.PartGen
{
    public partial class PartGenAllCtrl : UserControl
    {
        public PartGenAllCtrl()
        {
            InitializeComponent();

            this.m_numDrpMaxGregreeofParallelism.Minimum = 1;
            this.m_numDrpMaxGregreeofParallelism.Maximum = Environment.ProcessorCount;

            FillDeviceList(".*");
        }

        private void FillDeviceList(String filter)
        {
            this.m_lstBoxDevices.Items.Clear();

            foreach (String package in PartGenAll.GetAllPackages().Where(p => Regex.IsMatch(p, filter)))
            {
                this.m_lstBoxDevices.Items.Add(package);
            }
        }

        private void m_btnBrowse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            dlg.ShowNewFolderButton = true;

            // cancel
            if (dlg.ShowDialog() != DialogResult.OK)
                return;

            if (String.IsNullOrEmpty(dlg.SelectedPath))
                return;

            this.m_txtPath.Text = dlg.SelectedPath;
        }

        private String GetFPGAFilter()
        {
            List<String> filters = new List<String>();
            if (this.m_chkBoxKintex7.Checked)
            {
                filters.Add("xc7");
            }
            if (this.m_chkBoxSpartan3.Checked)
            {
                filters.Add("xc3s");
            } 
            if (this.m_chkBoxSpartan6.Checked)
            {
                filters.Add("xc6s");
            } 
            if (this.m_chkBoxVirtex4.Checked)
            {
                filters.Add("xc4v");
            } 
            if (this.m_chkBoxVirtex5.Checked)
            {
                filters.Add("xc5v");
            } 
            if (this.m_chkBoxVirtex6.Checked)
            {
                filters.Add("xc6v");
            }

            foreach (Object obj in this.m_lstBoxDevices.SelectedItems)
            {
                filters.Add(obj.ToString() + "$");
            }

            // build up astring for a regexp
            String regexp = "";
            foreach (String f in filters)
            {
                String fInBrackets = "(^" + f + ")";
                if (String.IsNullOrEmpty(regexp))
                {
                    regexp += fInBrackets;
                }
                else
                {
                    regexp += "|" + fInBrackets;
                }
            }
            return regexp;
        }

        private void m_btnOk_Click(object sender, EventArgs e)
        {
            String filter = this.GetFPGAFilter();

            if (!this.m_txtFilter.Text.Equals(filter))
            {
                filter = this.m_txtFilter.Text;
            }

            if (String.IsNullOrEmpty(filter))
            {
                MessageBox.Show("No FPGA families selected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!System.IO.Directory.Exists(this.m_txtPath.Text))
            {
                MessageBox.Show("Store path " + this.m_txtPath.Text + " does not exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            PartGenAll partGenCmd = new PartGenAll();
            partGenCmd.FPGAFilter = filter;
            partGenCmd.KeepExisting_binFPGAS = this.m_chkBoxKeepBinFPGAs.Checked;
            partGenCmd.KeepXDLFiles = this.m_chkBoxKeepXDL.Checked;
            partGenCmd.StorePath = this.m_txtPath.Text;
            partGenCmd.AllCons = this.m_chkBoxAllConns.Checked;
            //partGenCmd.MaxDegreeOfParallelism = (int) this.m_numDrpMaxGregreeofParallelism.Value;
            partGenCmd.ExcludePipsToBidirectionalWiresFromBlocking = this.m_chkBoxExcludeBirectionalWires.Checked;

            CommandExecuter.Instance.Execute(partGenCmd);
        }

        #region Filter
                
        private void m_txtDeviceFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bool test = Regex.IsMatch("test", this.m_txtDeviceFilter.Text);
                this.FillDeviceList(this.m_txtDeviceFilter.Text);
                this.m_lblDeviceFilter.Text = "Filter";
            }
            catch (Exception)
            {
                this.m_lblDeviceFilter.Text = "Filter (invalid)";
            }
        }
        
        private void UpdateFilter()
        {
            this.m_txtFilter.Text = this.GetFPGAFilter();
        }
        private void m_chkBoxKintex7_CheckedChanged(object sender, EventArgs e)
        {
            this.UpdateFilter();
        }

        private void m_chkBoxVirtex6_CheckedChanged(object sender, EventArgs e)
        {
            this.UpdateFilter();
        }

        private void m_chkBoxVirtex5_CheckedChanged(object sender, EventArgs e)
        {
            this.UpdateFilter();
        }

        private void m_chkBoxVirtex4_CheckedChanged(object sender, EventArgs e)
        {
            this.UpdateFilter();
        }

        private void m_chkBoxSpartan6_CheckedChanged(object sender, EventArgs e)
        {
            this.UpdateFilter();
        }

        private void m_chkBoxSpartan3_CheckedChanged(object sender, EventArgs e)
        {
            this.UpdateFilter();
        }

        private void m_lstBoxDevices_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.UpdateFilter();
        }

        #endregion Filter


    }
}

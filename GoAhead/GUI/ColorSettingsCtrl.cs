﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GoAhead.Settings;

namespace GoAhead.GUI.Help
{
    public partial class ColorSettingsCtrl : UserControl
    {
        public ColorSettingsCtrl()
        {
            InitializeComponent();

            this.m_lxtBoxRegexps.Items.Clear();

            foreach (String str in Settings.ColorSettings.Instance.GetTileRegexps())
            {
                this.m_lxtBoxRegexps.Items.Add(str);
            }

            this.UpdateLabelColors();
        }

        private void m_btnSelectColor_Click(object sender, EventArgs e)
        {
            Object selectedItem = this.m_lxtBoxRegexps.SelectedItem;

            if (selectedItem != null)
            {
                String tileRegexp = selectedItem.ToString();
                ColorDialog cDlg = new ColorDialog();
                cDlg.ShowDialog();


                Commands.Identifier.SetColorSetting setCmd = new Commands.Identifier.SetColorSetting();
                setCmd.Color = cDlg.Color.Name;
                setCmd.FamilyRegexp = FPGA.FPGA.Instance.Family.ToString();
                setCmd.IdentifierRegexp = tileRegexp;
                Commands.CommandExecuter.Instance.Execute(setCmd);

                //Preferences.Instance.ColorSettings.SetColor(tileRegexp, cDlg.Color);
            }
        }

        private void m_btnSelectionIncr_Click(object sender, EventArgs e)
        {
            ColorDialog cDlg = new ColorDialog();
            cDlg.Color = Settings.ColorSettings.Instance.SelectionIncrement;
            cDlg.ShowDialog();

            Settings.ColorSettings.Instance.SelectionIncrement = cDlg.Color;
            this.UpdateLabelColors();
        }

        private void m_btnUserSelectionIncr_Click(object sender, EventArgs e)
        {
            ColorDialog cDlg = new ColorDialog();
            cDlg.Color = Settings.ColorSettings.Instance.UserSelectionIncrement;
            cDlg.ShowDialog();

            Settings.ColorSettings.Instance.UserSelectionIncrement = cDlg.Color;
            this.UpdateLabelColors();
        }

        private void m_btnBlockedPortsIncr_Click(object sender, EventArgs e)
        {
            ColorDialog cDlg = new ColorDialog();
            cDlg.Color = Settings.ColorSettings.Instance.BlockedPortsColor;
            cDlg.ShowDialog();

            Settings.ColorSettings.Instance.BlockedPortsColor = cDlg.Color;
            this.UpdateLabelColors();
        }

        private void UpdateLabelColors()
        {
            this.m_lblBlockedPorts.ForeColor = Settings.ColorSettings.Instance.BlockedPortsColor;
            this.m_lblBlockedPorts.BackColor = Settings.ColorSettings.Instance.BlockedPortsColor;
            this.m_lblSelection.ForeColor = Settings.ColorSettings.Instance.SelectionIncrement;
            this.m_lblSelection.BackColor = Settings.ColorSettings.Instance.SelectionIncrement;
            this.m_lblUserSelection.ForeColor = Settings.ColorSettings.Instance.UserSelectionIncrement;
            this.m_lblUserSelection.BackColor = Settings.ColorSettings.Instance.UserSelectionIncrement;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GoAhead.GUI
{
    public partial class ObjectViewCtrl : UserControl
    {
        public ObjectViewCtrl()
        {
            InitializeComponent();
        }

        private void UpdateView()
        {
            if (this.m_object != null)
            {
                this.m_txtToString.AppendText(this.m_object.ToString());
            }
        }

        public Object Object
        {
            set { this.m_object = value; this.UpdateView(); }
            get { return this.m_object; }
        }

        private Object m_object = null;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using GoAhead.Commands.Variables;
using GoAhead.FPGA;
using GoAhead.Objects;

namespace GoAhead.GUI.SetVariables
{
    public partial class SetVarCtrl : UserControl, Interfaces.IObserver
    {
        public SetVarCtrl()
        {
            InitializeComponent();
            FPGA.TileSelectionManager.Instance.Add(this);
        }

        public void Notify(Object obj)
        {
            if (this.Input == null)
            {
                return;
            }
            if (this.Input.Domain != Commands.Variables.Input.DomainType.TileSelection)
            {
                return;
            }
            else if (FPGA.TileSelectionManager.Instance.NumberOfSelectedTiles > 0)
            {
                Tile ul = FPGA.TileSelectionManager.Instance.GetFirstSelectedTile(FPGATypes.Placement.UpperLeft, IdentifierManager.RegexTypes.InterconnectRegex);
                Tile lr = FPGA.TileSelectionManager.Instance.GetFirstSelectedTile(FPGATypes.Placement.LowerRight, IdentifierManager.RegexTypes.CLBRegex);

                String varNameWithoutBraces = this.m_input.VariableName;
                varNameWithoutBraces = varNameWithoutBraces.Replace("(", "");
                varNameWithoutBraces = varNameWithoutBraces.Replace(")", "");

                String[] varNames = varNameWithoutBraces.Split(')', '(', ' ');
                if (varNames.Length == 4)
                {
                    String text = "";
                    text += varNames[0] + " = " + ul.TileKey.X + ";";
                    text += varNames[1] + " = " + ul.TileKey.Y + ";";
                    text += varNames[2] + " = " + lr.TileKey.X + ";";
                    text += varNames[3] + " = " + lr.TileKey.Y;

                    this.m_txtValue.Text = text;
                }
                else
                {
                    Console.WriteLine("Warning: Ranges of type TileSelection required four variables, e.g. (X1, Y1, X2, Y2). Found " + this.m_input.VariableName);
                }

                //this.m_txtTileIdentifier.Text = tile.Location;
            }
        }

        public Set[] SetCommands
        {
            get { return m_setCommands.ToArray(); }
        }

        public Input Input
        {
            get { return m_input; }
            set
            {
                this.m_input = value;
                this.m_setCommands.Clear();
                this.m_setCommands.Add(new Set());

                this.m_setCommands[0].Variable = this.m_input.VariableName;

                this.m_lblVar.Text = this.m_input.VariableName;

                if (this.m_input.Domain == Commands.Variables.Input.DomainType.Range)
                {
                    this.m_txtValue.Visible = false;
                    this.m_cmbBox.Location = this.m_location;
                    this.m_cmbBox.Visible = true;
                    this.m_cmbBox.Items.Clear();
                    this.m_cmbBox.Items.AddRange(this.m_input.ExplicitRange.ToArray());
                    this.m_cmbBox.SelectedItem = this.m_input.ExplicitRange.Count > 0 ? this.m_input.ExplicitRange[0] : "";
                }
                else if (this.m_input.Domain == Commands.Variables.Input.DomainType.TileSelection)
                {
                    this.m_txtValue.Visible = true;
                    this.m_txtValue.Location = this.m_location;
                    this.m_cmbBox.Visible = false;

                    String varNames = this.m_input.VariableName;
                    varNames = varNames.Replace("(", "");
                    varNames = varNames.Replace(")", "");
                    this.m_lblVar.Text = varNames;
                }
                else
                {
                    this.m_txtValue.Visible = true;
                    this.m_txtValue.Location = this.m_location;
                    this.m_cmbBox.Visible = false;

                    if (Objects.VariableManager.Instance.IsSet(this.m_input.VariableName))
                    {
                        this.m_txtValue.Text = Objects.VariableManager.Instance.GetValue(this.m_input.VariableName);
                    }
                }
            }
        }


        private void m_txtValue_TextChanged_1(object sender, EventArgs e)
        {
            this.SetValue(this.m_txtValue.Text);
        }


        private void m_cmbBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Object selection = this.m_cmbBox.SelectedItem;
            this.SetValue(selection.ToString());
        }

        private void SetValue(String value)
        {
            this.m_setCommands.Clear();
            if (this.Input.Domain == Commands.Variables.Input.DomainType.TileSelection)
            {
                String[] assignements = value.Split(';');

                if (assignements.Length != 4)
                {
                    Console.WriteLine("Warning: Select a range on the TileView to update this text box");
                }

                foreach (String assignement in assignements)
                {
                    String[] tupel = assignement.Split('=');
                    if (tupel.Length != 2)
                    {
                        Console.WriteLine("Warning: Select a range on the TileView to update this text box");
                    }

                    Set setCmd = new Set();

                    setCmd.Variable = tupel[0].Trim();
                    setCmd.Value = tupel[1].Trim();
                    this.m_setCommands.Add(setCmd);
                }

            }
            else
            {               
                Set setCmd = new Set();
                setCmd.Variable = this.Input.VariableName;
                setCmd.Value = value;
                setCmd.Value = setCmd.Value.Trim();

                if (Regex.IsMatch(setCmd.Value, @"\s"))
                {
                    setCmd.Value = "\"" + setCmd.Value + "\"";
                }

                this.m_setCommands.Add(setCmd);
            }
        }

        private List<Set> m_setCommands = new List<Set>();
        private Input m_input = null;
        private Point m_location = new Point(126, 22);


    }
}

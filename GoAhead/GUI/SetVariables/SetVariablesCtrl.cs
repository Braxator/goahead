﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GoAhead.Commands.Variables;
using GoAhead.FPGA;
using GoAhead.Commands.Selection;
using GoAhead.Objects;

namespace GoAhead.GUI.SetVariables
{
    public partial class SetVariablesCtrl : UserControl, Interfaces.IObserver
    {
        public SetVariablesCtrl()
        {
            InitializeComponent();
            FPGA.TileSelectionManager.Instance.Add(this);
        }

        public void Notify(Object obj)
        {
            if (FPGA.TileSelectionManager.Instance.NumberOfSelectedTiles == 2)
            {
                Tile clb = FPGA.TileSelectionManager.Instance.GetFirstSelectedTile(FPGATypes.Placement.UpperLeft, IdentifierManager.RegexTypes.CLBRegex);
                Tile interconnect = FPGA.TileSelectionManager.Instance.GetFirstSelectedTile(FPGATypes.Placement.UpperLeft, IdentifierManager.RegexTypes.InterconnectRegex);

                this.m_txtTileIdentifier.Text = clb.Location + " " + interconnect.Location;
                this.m_txtTileIdentifier.Width = 400;
            }
        }

        public List<Input> Inputs
        {
            get { return m_inputs; }
            set { m_inputs = value; this.AddControls(); }
        }

        private void AddControls()
        {
            if(this.m_inputs == null)
            {
                return;
            }
            // shrink to group box
            this.Parent.Width = Math.Max(this.m_groupBoxCtrl.Width, this.Parent.Width);
            this.Parent.Height = this.m_groupBoxCtrl.Height;
            // grow by controls
            int y = this.m_groupBoxCtrl.Height;
            foreach (Input input in this.m_inputs)
            {
                SetVarCtrl setCtrl = new SetVarCtrl();
                setCtrl.Input = input;
                setCtrl.Location = new Point(0, y);
                this.Parent.Width = Math.Max(setCtrl.Width, this.Parent.Width);
                //this.Parent.Width = Math.Max(setCtrl.Width, this.Parent.Width);
                this.Parent.Height = this.Parent.Height + setCtrl.Height;
                this.m_groupBoxCtrl.Width = this.Parent.Width;

                y += setCtrl.Height;

                FPGA.TileSelectionManager.Instance.Add(setCtrl);
                this.m_setVarControls.Add(setCtrl);
                this.Controls.Add(setCtrl);
            }
        }

        public List<Set> GetSetCommands()
        {
            List<Set> result = new List<Set>();            
            this.m_setVarControls.ForEach(ctrl => result.AddRange(ctrl.SetCommands));
            return result;
        }        

        private void m_btnOk_Click(object sender, EventArgs e)
        {
            this.ParentForm.Close();
        }

        private List<SetVarCtrl> m_setVarControls = new List<SetVarCtrl>();
        private List<Input> m_inputs = null;

       
    }
}

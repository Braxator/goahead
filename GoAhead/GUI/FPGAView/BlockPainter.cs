﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using GoAhead.FPGA;
using GoAhead.Commands;
using GoAhead.Commands.Selection;
using GoAhead.Settings;
using GoAhead.Objects;

namespace GoAhead.GUI
{
    class BlockPainter : Painter, Interfaces.IObserver, Interfaces.IResetable
    {
        public BlockPainter(FPGAViewCtrl view)
            :base(view)
        {
            TileSelectionManager.Instance.Add(this);
            Commands.Reset.ObjectsToReset.Add(this);
        }        

        public void Notify(Object obj)
        {
            if (obj == null)
            {
                return;
            }

            if (this.TileBitmap == null)
            {
                return;
            }

            if (obj is SelectionUpdate)
            {
                this.m_graphicsObj = Graphics.FromImage(this.TileBitmap);
                this.SelectionUpdate((SelectionUpdate)obj, this.m_graphicsObj);
            } 
        }

        private void SelectionUpdate(SelectionUpdate selUpdate, Graphics graphicsObj)
        {
            // ram drawing 
            if (!FPGA.RAMSelectionManager.Instance.HasMappings)
            {
                FPGA.RAMSelectionManager.Instance.UpdateMapping();
            }
        
            switch (selUpdate.Action)
            {
                case (SelectionUpdateAction.AddCurrentSelectionToUserSelection):
                    {
                        foreach (Tile t in FPGA.TileSelectionManager.Instance.GetUserSelection(selUpdate.UserSelectionType, this.m_view.TileRegex))
                        {
                            //if (Regex.IsMatch(t.Location, Objects.IdentifierManager.Instance.GetRegex(IdentifierManager.RegexTypes.CLBRegex)))
                            if (IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.CLBRegex))
                            {
                                Tile intTile = FPGA.FPGATypes.GetInterconnectTile(t);
                                this.DrawTile(intTile, graphicsObj, true, true);
                            }
                            this.DrawTile(t, graphicsObj, true, true);
                        }
                        break;
                    }
                case (SelectionUpdateAction.AddToSelection) :
                case (SelectionUpdateAction.AddToUserSelection):
                    {
                        Tile t = FPGA.FPGA.Instance.GetTile(selUpdate.AffecetedTileKey);
                        this.DrawTile(t, graphicsObj, true, true);
                        break;
                    }
                case (SelectionUpdateAction.ClearAllUserSelections):
                    {
                        foreach (Tile t in FPGA.TileSelectionManager.Instance.GetAllUserSelectedTiles())
                        {
                            if (IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.CLBRegex))
                            {
                                Tile intTile = FPGA.FPGATypes.GetInterconnectTile(t);
                                this.DrawTile(intTile, graphicsObj, true, false);
                            }
                            this.DrawTile(t, graphicsObj, true, false);
                        }
                        break;
                    }
                case (SelectionUpdateAction.ClearSelection):
                    {
                        foreach(Tile tile in FPGA.TileSelectionManager.Instance.GetSelectedTiles().Where(t => this.m_view.TileRegex.IsMatch(t.Location)))
                        {
                            if (IdentifierManager.Instance.IsMatch(tile.Location, IdentifierManager.RegexTypes.CLBRegex))
                            {
                                Tile intTile = FPGA.FPGATypes.GetInterconnectTile(tile);
                                this.DrawTile(intTile, graphicsObj, false, true);
                            }
                            this.DrawTile(tile, graphicsObj, false, true);
                        }
                        break;
                    }
                case (SelectionUpdateAction.ClearUserSelection):
                    {
                        foreach (Tile t in FPGA.TileSelectionManager.Instance.GetAllUserSelectedTiles(selUpdate.UserSelectionType))
                        {
                            if (IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.CLBRegex))
                            {
                                Tile intTile = FPGA.FPGATypes.GetInterconnectTile(t);
                                this.DrawTile(intTile, graphicsObj,true, false);
                            }
                            this.DrawTile(t, graphicsObj, true, false);
                        }
                        break;
                    }
                case (SelectionUpdateAction.RemoveFromSelection):
                    {
                        Tile t = FPGA.FPGA.Instance.GetTile(selUpdate.AffecetedTileKey);
                        if (IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.CLBRegex))
                        {
                            Tile intTile = FPGA.FPGATypes.GetInterconnectTile(t);
                            this.DrawTile(intTile, graphicsObj, false, true);
                        }
                        this.DrawTile(t, graphicsObj, false, true);
                        break;
                    }
                case (SelectionUpdateAction.InversionComplete):
                    {
                        foreach (Tile t in FPGA.FPGA.Instance.GetAllTiles())
                        {
                            this.DrawTile(t, graphicsObj, true, true);
                        }
                        break;
                    }
                default:
                    {
                        throw new ArgumentException("Unexpected UpdateAction: " + selUpdate.Action);
                    }
        
            }
        }

        public override void DrawTiles(bool addIncrementForSelectedTiles, bool addIncrementForUserSelectedTiles)
        {
            int x = FPGA.FPGA.Instance.MaxX;
            int y = FPGA.FPGA.Instance.MaxY;

            // add 1 due to zero based indeces
            this.TileBitmap = new Bitmap((x + 1) * this.m_view.TileSize, (y + 1) * this.m_view.TileSize, this.m_pixelFormat);

            Graphics graphicsObj = Graphics.FromImage(this.TileBitmap);

            graphicsObj.Clear(Color.Gray);

            foreach (Tile tile in this.GetAllTiles())
            {
                this.DrawTile(tile, graphicsObj, addIncrementForSelectedTiles, addIncrementForUserSelectedTiles);
            }

            // get ram block data            
            if (!this.m_ramDataValid)
            {
                this.m_ramDataValid = FPGA.FPGATypes.GetRamBlockSize(this.m_view.TileRegex, out this.m_ramBlockWidth, out this.m_ramBlockHeight, out this.m_ramTiles);
            }

            foreach (Tile ramTile in this.m_ramTiles)
            {
                if(!this.m_view.TileRegex.IsMatch(ramTile.Location))
                {
                    continue;
                }

                this.DrawRAMTile(ramTile, graphicsObj, addIncrementForSelectedTiles, addIncrementForUserSelectedTiles);
            }

            graphicsObj.Dispose();
        }

        public override void DrawTile(Tile tile, Graphics graphicsObj, bool addIncrementForSelectedTiles, bool addIncrementForUserSelectedTiles)
        {
            if(!this.m_view.TileRegex.IsMatch(tile.Location))
            {
                return;
            }
            bool ramTile = false;
            bool hasMapping = false;

            if (this.m_ramTiles.Contains(tile))
            {
                ramTile = true;
            }
            else if (!FPGA.RAMSelectionManager.Instance.HasMapping(tile))
            {
                hasMapping = true;
            }

            if (ramTile)
            {
                this.DrawRAMTile(tile, graphicsObj, addIncrementForSelectedTiles, addIncrementForUserSelectedTiles);
            }
            else if (hasMapping)
            {
                this.DrawNonRAMTile(tile, graphicsObj, addIncrementForSelectedTiles, addIncrementForUserSelectedTiles);
            }
        }

        private void DrawRAMTile(Tile ramTile, Graphics graphicsObj, bool addIncrementForSelectedTiles, bool addIncrementForUserSelectedTiles)
        {
            int x = -1;
            int y = -1;
            switch (FPGA.FPGA.Instance.Family)
            {
                case FPGA.FPGATypes.FPGAFamily.Artix7:
                case FPGA.FPGATypes.FPGAFamily.Kintex7:
                case FPGA.FPGATypes.FPGAFamily.Virtex7:
                case FPGA.FPGATypes.FPGAFamily.Zynq:
                    {
                        if (FPGATypes.BRAMLeft.IsMatch(ramTile.Location))
                        {
                            // ram tile are in the bottom middle
                            x = ramTile.TileKey.X;
                            y = ramTile.TileKey.Y - (m_ramBlockHeight - 1);
                        }
                        else //if (Regex.IsMatch(ramTile.Location, "_R_"))
                        {
                            // ram tile are in the bottom middle
                            x = ramTile.TileKey.X - (m_ramBlockWidth - 1);
                            y = ramTile.TileKey.Y - (m_ramBlockHeight - 1);
                        }
                        break;
                    }
                default:
                    {
                        // ram tile are in the bottom right
                        x = ramTile.TileKey.X - (m_ramBlockWidth - 1);
                        y = ramTile.TileKey.Y - (m_ramBlockHeight - 1);
                        break;
                    }
            }

            if (FPGA.FPGA.Instance.Contains(x, y))
            {
                Tile upperLeft = FPGA.FPGA.Instance.GetTile(x, y);

                int upperLeftX = upperLeft.TileKey.X * this.m_view.TileSize;
                int upperLeftY = upperLeft.TileKey.Y * this.m_view.TileSize;

                Rectangle rect = new Rectangle();
                rect.X = upperLeftX-1;
                rect.Y = upperLeftY-1;
                rect.Width = (m_ramBlockWidth * (this.m_view.TileSize - 1) + m_ramBlockWidth - 1) - 2;
                rect.Height = (m_ramBlockHeight * (this.m_view.TileSize - 1) + m_ramBlockHeight) - 2;

                this.m_sb.Color = this.m_view.GetColor(ramTile, addIncrementForSelectedTiles, addIncrementForUserSelectedTiles);
                graphicsObj.FillRectangle(this.m_sb, rect);
            }
        }

        private void DrawNonRAMTile(Tile tile, Graphics graphicsObj, bool addIncrementForSelectedTiles, bool addIncrementForUserSelectedTiles)
        {
            int upperLeftX = 0;
            int upperLeftY = 0;
            int widthScale = 1;
            int heightScale = 1;

            if (IdentifierManager.Instance.IsMatch(tile.Location, IdentifierManager.RegexTypes.CLBRegex))
            {
                Tile intTile = FPGA.FPGATypes.GetInterconnectTile(tile);

                switch (FPGA.FPGA.Instance.Family)                
                {
                
                    case FPGA.FPGATypes.FPGAFamily.Artix7:
                    case FPGA.FPGATypes.FPGAFamily.Kintex7:
                    case FPGA.FPGATypes.FPGAFamily.Virtex7:
                    case FPGA.FPGATypes.FPGAFamily.Zynq:
                        {
                            if (FPGATypes.ClbLeft.IsMatch(tile.Location))
                            {
                                // CLBLL_L_X INT_L_X
                                upperLeftX = tile.TileKey.X * this.m_view.TileSize;
                                upperLeftY = tile.TileKey.Y * this.m_view.TileSize;
                                widthScale = 2;
                            }
                            else
                            {
                                upperLeftX = intTile.TileKey.X * this.m_view.TileSize;
                                upperLeftY = intTile.TileKey.Y * this.m_view.TileSize;
                                // double size of the rectangle
                                widthScale = 2;
                            }
                            break;
                        }
                    case FPGA.FPGATypes.FPGAFamily.UltraScale:
                        {
                            widthScale = FPGA.FPGATypes.GetCLTile(intTile).Count() + 1;
                            if (FPGATypes.ClbLeft.IsMatch(tile.Location))
                            {
                                // CLBLL_L_X INT_L_X
                                upperLeftX = tile.TileKey.X * this.m_view.TileSize;
                                upperLeftY = tile.TileKey.Y * this.m_view.TileSize;
                            }
                            else
                            {
                                upperLeftX = (intTile.TileKey.X - (widthScale == 2 ? 0 : 1)) * this.m_view.TileSize;
                                upperLeftY = intTile.TileKey.Y * this.m_view.TileSize;
                                // double size of the rectangle
                            }
                            break;
                        }
                default:
                    {
                        upperLeftX = intTile.TileKey.X * this.m_view.TileSize;
                        upperLeftY = intTile.TileKey.Y * this.m_view.TileSize;
                        // double size of the rectangle
                        widthScale = 2;
                        break;
                    }
                }
            }
            else if (Objects.IdentifierManager.Instance.IsMatch(tile.Location, IdentifierManager.RegexTypes.InterconnectRegex))
            {
                // interconnect tiles for CLB have no tiles
                return;
            }
            else
            {
                upperLeftX = tile.TileKey.X * this.m_view.TileSize;
                upperLeftY = tile.TileKey.Y * this.m_view.TileSize;
            }


            this.m_rect.X = upperLeftX - 1;
            this.m_rect.Y = upperLeftY - 1;
            this.m_rect.Width = (widthScale * (this.m_view.TileSize - 1) + widthScale) - 2;
            this.m_rect.Height = (heightScale * (this.m_view.TileSize - 1) + heightScale) - 2;

            // default color maybde overwritten
            this.m_sb.Color = this.m_view.GetColor(tile, addIncrementForSelectedTiles, addIncrementForUserSelectedTiles);

            
            graphicsObj.FillRectangle(this.m_sb, this.m_rect);
        
        }

        public override void Reset()
        {
            this.m_ramDataValid = false;
        }

        private Graphics m_graphicsObj = null;
        private bool m_ramDataValid = false;
        private TileSet m_ramTiles = new TileSet();
        private int m_ramBlockWidth = 0;
        private int m_ramBlockHeight = 0;       
    }
}

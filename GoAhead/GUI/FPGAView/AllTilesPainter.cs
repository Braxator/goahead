﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using GoAhead.FPGA;
using GoAhead.Commands;
using GoAhead.Commands.Selection;
using GoAhead.Settings;
using GoAhead.Objects;

namespace GoAhead.GUI
{
    class AllTilesPainter : Painter, Interfaces.IObserver, Interfaces.IResetable
    {
        public AllTilesPainter(FPGAViewCtrl view)
            : base(view)
        {
            TileSelectionManager.Instance.Add(this);
        }

        public void Notify(Object obj)
        {
            if (obj == null)
            {
                return;
            }

            if (this.TileBitmap == null)
            {
                return;
            }

            if (obj is SelectionUpdate)
            {
                this.SelectionUpdate((SelectionUpdate)obj);
            }
            //this.Invalidate();
        }
        
        private void SelectionUpdate(SelectionUpdate selUpdate)
        {
            // ram drawing 
            if (!FPGA.RAMSelectionManager.Instance.HasMappings)
            {
                FPGA.RAMSelectionManager.Instance.UpdateMapping();
            }
            
            Graphics graphicsObj = Graphics.FromImage(this.TileBitmap);

            switch (selUpdate.Action)
            {
                case (SelectionUpdateAction.AddCurrentSelectionToUserSelection):
                    {
                        foreach (Tile t in FPGA.TileSelectionManager.Instance.GetUserSelection(selUpdate.UserSelectionType, this.m_view.TileRegex))
                        {
                            if(IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.CLBRegex))
                            {
                                Tile intTile = FPGA.FPGATypes.GetInterconnectTile(t);
                                this.DrawTile(intTile, graphicsObj, true, true);
                            }
                            this.DrawTile(t, graphicsObj, true, true);
                        }
                        break;
                    }
                case (SelectionUpdateAction.AddToSelection):
                case (SelectionUpdateAction.AddToUserSelection):
                    {
                        Tile t = FPGA.FPGA.Instance.GetTile(selUpdate.AffecetedTileKey);
                        this.DrawTile(t, graphicsObj, true, true);
                        break;
                    }
                case (SelectionUpdateAction.ClearAllUserSelections):
                    {
                        foreach (Tile t in FPGA.TileSelectionManager.Instance.GetAllUserSelectedTiles())
                        {
                            if (IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.CLBRegex))
                            {
                                Tile intTile = FPGA.FPGATypes.GetInterconnectTile(t);
                                this.DrawTile(intTile, graphicsObj, true, false);
                            }
                            this.DrawTile(t, graphicsObj, true, false);
                        }
                        break;
                    }
                case (SelectionUpdateAction.ClearSelection):
                    {
                        foreach (Tile tile in FPGA.TileSelectionManager.Instance.GetSelectedTiles().Where(t => this.m_view.TileRegex.IsMatch(t.Location)))
                        {
                            if (IdentifierManager.Instance.IsMatch(tile.Location, IdentifierManager.RegexTypes.CLBRegex))
                            {
                                Tile intTile = FPGA.FPGATypes.GetInterconnectTile(tile);
                                this.DrawTile(intTile, graphicsObj, false, true);
                            }
                            this.DrawTile(tile, graphicsObj, false, true);
                        }
                        break;
                    }
                case (SelectionUpdateAction.ClearUserSelection):
                    {
                        foreach (Tile t in FPGA.TileSelectionManager.Instance.GetAllUserSelectedTiles(selUpdate.UserSelectionType))
                        {
                            if (IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.CLBRegex))
                            {
                                Tile intTile = FPGA.FPGATypes.GetInterconnectTile(t);
                                this.DrawTile(intTile, graphicsObj, true, false);
                            }
                            this.DrawTile(t, graphicsObj, true, false);
                        }
                        break;
                    }
                case (SelectionUpdateAction.RemoveFromSelection):
                    {
                        Tile t = FPGA.FPGA.Instance.GetTile(selUpdate.AffecetedTileKey);
                        if (IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.CLBRegex))
                        {
                            Tile intTile = FPGA.FPGATypes.GetInterconnectTile(t);
                            this.DrawTile(intTile, graphicsObj, false, true);
                        }
                        this.DrawTile(t, graphicsObj, false, true);
                        break;
                    }
                case (SelectionUpdateAction.InversionComplete):
                    {
                        foreach (Tile t in FPGA.FPGA.Instance.GetAllTiles())
                        {
                            this.DrawTile(t, graphicsObj, true, true);
                        }
                        break;
                    }

                default:
                    {
                        throw new ArgumentException("Unexpected UpdateAction: " + selUpdate.Action);
                    }
            }
        }

        public override void DrawTiles(bool addIncrementForSelectedTiles, bool addIncrementForUserSelectedTiles)
        {
            int x = FPGA.FPGA.Instance.MaxX;
            int y = FPGA.FPGA.Instance.MaxY;

            // add 1 due to zero based indeces
            this.TileBitmap = new Bitmap(
                (int)((x + 1) * this.m_view.TileSize), 
                (int)((y + 1) * this.m_view.TileSize),
                this.m_pixelFormat);

            Graphics graphicsObj = Graphics.FromImage(this.TileBitmap);

            graphicsObj.Clear(Color.Gray);

            foreach (Tile tile in this.GetAllTiles())
            {
                this.DrawTile(tile, graphicsObj, addIncrementForSelectedTiles, addIncrementForUserSelectedTiles);
            }

            // get ram block data            
            if (!this.m_ramDataValid)
            {
                this.m_ramDataValid = FPGA.FPGATypes.GetRamBlockSize(this.m_view.TileRegex, out this.m_ramBlockWidth, out this.m_ramBlockHeight, out this.m_ramTiles);
            }

            foreach (Tile ramTile in m_ramTiles)
            {
                if (this.m_view.TileRegex.IsMatch(ramTile.Location))
                {
                    continue;
                }

                this.DrawTile(ramTile, graphicsObj, addIncrementForSelectedTiles, addIncrementForUserSelectedTiles);
            }

            graphicsObj.Dispose();
        }

        public override void DrawTile(Tile tile, Graphics graphicsObj, bool addIncrementForSelectedTiles, bool addIncrementForUserSelectedTiles)
        {
            if (!this.m_view.TileRegex.IsMatch(tile.Location))
            {
                return;
            }

            int upperLeftX = tile.TileKey.X * this.m_view.TileSize;
            int upperLeftY = tile.TileKey.Y * this.m_view.TileSize;

            m_rect.X = upperLeftX-1;
            m_rect.Y = upperLeftY-1;
            m_rect.Width = this.m_view.TileSize - 2;
            m_rect.Height = this.m_view.TileSize - 2;

            this.m_sb.Color = this.m_view.GetColor(tile, addIncrementForSelectedTiles, addIncrementForUserSelectedTiles);
            graphicsObj.FillRectangle(this.m_sb, m_rect);
        }

        public override void Reset()
        {
            this.m_ramDataValid = false;
        }
        
        private bool m_ramDataValid = false;
        private TileSet m_ramTiles = new TileSet();
        private int m_ramBlockWidth = 0;
        private int m_ramBlockHeight = 0;
    }
}

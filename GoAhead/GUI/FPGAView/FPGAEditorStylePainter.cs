﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using GoAhead.FPGA;
using GoAhead.Objects;

namespace GoAhead.GUI.FPGAView
{
    class FPGAEditorStylePainter : Painter, Interfaces.IObserver, Interfaces.IResetable
    {
        public FPGAEditorStylePainter(FPGAViewCtrl view)
            :base(view)
        {
            TileSelectionManager.Instance.Add(this);
            Commands.Reset.ObjectsToReset.Add(this);
        }

        public void Notify(Object obj)
        {

        }

        private void SelectionUpdate(SelectionUpdate selUpdate)
        {
            // ram drawing 
            if (!FPGA.RAMSelectionManager.Instance.HasMappings)
            {
                FPGA.RAMSelectionManager.Instance.UpdateMapping();
            }

            Graphics graphicsObj = Graphics.FromImage(this.TileBitmap);

            switch (selUpdate.Action)
            {
                case (SelectionUpdateAction.AddCurrentSelectionToUserSelection):
                    {
                        foreach (Tile t in FPGA.TileSelectionManager.Instance.GetUserSelection(selUpdate.UserSelectionType, this.m_view.TileRegex))
                        {
                            if (IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.CLBRegex))
                            {
                                Tile intTile = FPGA.FPGATypes.GetInterconnectTile(t);
                                this.DrawTile(intTile, graphicsObj, true, true);
                            }
                            this.DrawTile(t, graphicsObj, true, true);
                        }
                        break;
                    }
                case (SelectionUpdateAction.AddToSelection):
                case (SelectionUpdateAction.AddToUserSelection):
                    {
                        Tile t = FPGA.FPGA.Instance.GetTile(selUpdate.AffecetedTileKey);
                        this.DrawTile(t, graphicsObj, true, true);
                        break;
                    }
                case (SelectionUpdateAction.ClearAllUserSelections):
                    {
                        foreach (Tile t in FPGA.TileSelectionManager.Instance.GetAllUserSelectedTiles())
                        {
                            if (IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.CLBRegex))
                            {
                                Tile intTile = FPGA.FPGATypes.GetInterconnectTile(t);
                                this.DrawTile(intTile, graphicsObj, true, false);
                            }
                            this.DrawTile(t, graphicsObj, true, false);
                        }
                        break;
                    }
                case (SelectionUpdateAction.ClearSelection):
                    {
                        foreach (Tile tile in FPGA.TileSelectionManager.Instance.GetSelectedTiles().Where(t => this.m_view.TileRegex.IsMatch(t.Location)))
                        {
                            if (IdentifierManager.Instance.IsMatch(tile.Location, IdentifierManager.RegexTypes.CLBRegex))
                            {
                                Tile intTile = FPGA.FPGATypes.GetInterconnectTile(tile);
                                this.DrawTile(intTile, graphicsObj, false, true);
                            }
                            this.DrawTile(tile, graphicsObj, false, true);
                        }
                        break;
                    }
                case (SelectionUpdateAction.ClearUserSelection):
                    {
                        foreach (Tile t in FPGA.TileSelectionManager.Instance.GetAllUserSelectedTiles(selUpdate.UserSelectionType))
                        {
                            if (IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.CLBRegex))
                            {
                                Tile intTile = FPGA.FPGATypes.GetInterconnectTile(t);
                                this.DrawTile(intTile, graphicsObj, true, false);
                            }
                            this.DrawTile(t, graphicsObj, true, false);
                        }
                        break;
                    }
                case (SelectionUpdateAction.RemoveFromSelection):
                    {
                        Tile t = FPGA.FPGA.Instance.GetTile(selUpdate.AffecetedTileKey);
                        if (IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.CLBRegex))
                        {
                            Tile intTile = FPGA.FPGATypes.GetInterconnectTile(t);
                            this.DrawTile(intTile, graphicsObj, false, true);
                        }
                        this.DrawTile(t, graphicsObj, false, true);
                        break;
                    }
                case (SelectionUpdateAction.InversionComplete):
                    {
                        foreach (Tile t in FPGA.FPGA.Instance.GetAllTiles())
                        {
                            this.DrawTile(t, graphicsObj, true, true);
                        }
                        break;
                    }

                default:
                    {
                        throw new ArgumentException("Unexpected UpdateAction: " + selUpdate.Action);
                    }
            }
        }

        public override void DrawTiles(bool addIncrementForSelectedTiles, bool addIncrementForUserSelectedTiles)
        {
            int x = FPGA.FPGA.Instance.MaxX;
            int y = FPGA.FPGA.Instance.MaxY;

            // add 1 due to zero based indeces
            this.TileBitmap = new Bitmap(
                (int)((x + 1) * this.m_view.TileSize),
                (int)((y + 1) * this.m_view.TileSize),
                this.m_pixelFormat);

            Graphics graphicsObj = Graphics.FromImage(this.TileBitmap);

            graphicsObj.Clear(Color.Black);

            foreach (Tile tile in this.GetAllTiles().Where(t => this.m_view.TileRegex.IsMatch(t.Location)))
            {
                this.DrawTile(tile, graphicsObj, addIncrementForSelectedTiles, addIncrementForUserSelectedTiles);
            }

            graphicsObj.Dispose();
        }

        public override void DrawTile(FPGA.Tile tile, System.Drawing.Graphics graphicsObj, bool addIncrementForSelectedTiles, bool addIncrementForUserSelectedTiles)
        {
            int upperLeftX = tile.TileKey.X * this.m_view.TileSize;
            int upperLeftY = tile.TileKey.Y * this.m_view.TileSize;

            m_rect.X = upperLeftX - 3;
            m_rect.Y = upperLeftY - 3;
            m_rect.Width = this.m_view.TileSize - 6;
            m_rect.Height = this.m_view.TileSize - 6;

            this.m_sb.Color = this.m_view.GetColor(tile, addIncrementForSelectedTiles, addIncrementForUserSelectedTiles);

            if (addIncrementForSelectedTiles && FPGA.TileSelectionManager.Instance.IsSelected(tile.TileKey))
            {
                graphicsObj.FillRectangle(this.m_sb, m_rect);
            }
            else if (addIncrementForUserSelectedTiles && FPGA.TileSelectionManager.Instance.IsUserSelected(tile.TileKey))
            {
                graphicsObj.FillRectangle(this.m_sb, m_rect);
            }
            else
            {
                if (
                    IdentifierManager.Instance.IsMatch(tile.Location, IdentifierManager.RegexTypes.CLBRegex) ||
                    IdentifierManager.Instance.IsMatch(tile.Location, IdentifierManager.RegexTypes.InterconnectRegex) ||
                    IdentifierManager.Instance.IsMatch(tile.Location, IdentifierManager.RegexTypes.BRAMRegex) ||
                    IdentifierManager.Instance.IsMatch(tile.Location, IdentifierManager.RegexTypes.DSPRegex)
                    )
                {
                    graphicsObj.FillRectangle(Brushes.DarkGray, m_rect);
                }
                else
                {
                    graphicsObj.FillRectangle(Brushes.Black, m_rect);
                }
                graphicsObj.DrawRectangle(new Pen(this.m_sb.Color), m_rect);
            }
            
        }

        void Interfaces.IObserver.Notify(object obj)
        {
            if (obj == null)
            {
                return;
            }

            if (this.TileBitmap == null)
            {
                return;
            }

            if (obj is SelectionUpdate)
            {
                this.SelectionUpdate((SelectionUpdate)obj);
            }
            //this.Invalidate();
        }

        void Interfaces.IResetable.Reset()
        {
        }
    }
}

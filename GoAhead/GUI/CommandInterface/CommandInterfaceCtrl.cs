﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using GoAhead.Commands;

namespace GoAhead.GUI
{
    public partial class CommandInterfaceCtrl : UserControl
    {
        public CommandInterfaceCtrl()
        {
            InitializeComponent();
        }

        private void CommandInterfaceCtrl_Load(object sender, EventArgs e)
        {
            this.m_cmdBoxCommands.Items.Clear();
            foreach (Type t in CommandStringParser.GetAllCommandTypes())
            {
                bool publish = true;
                foreach (Attribute attr in Attribute.GetCustomAttributes(t))
                {
                    if (attr is CommandDescription)
                    {
                        CommandDescription descr = (CommandDescription)attr;
                        publish = descr.Publish;
                    }
                }
                if (!publish)
                {
                    continue;
                }
                this.m_cmdBoxCommands.Items.Add(t.Name);

            }
        }

        private void m_cmdBoxCommands_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.m_cmdBoxCommands.SelectedItem != null)
            {
                String cmdTag = this.m_cmdBoxCommands.SelectedItem.ToString();
                this.UpdateParameters(cmdTag);
            }
        }

        private void m_cmdBoxCommands_TextChanged(object sender, EventArgs e)
        {
            String cmdTag = this.m_cmdBoxCommands.Text;
            this.UpdateParameters(cmdTag);
        }

        private void UpdateParameters(String cmdTag)
        {
            // clear previous data
            this.m_dataGrdArguments.Columns.Clear();
            this.m_dataGrdArguments.Columns.Add("Name", "Name");
            this.m_dataGrdArguments.Columns.Add("Value", "Value");
            this.m_dataGrdArguments.Columns.Add("Type", "Type");

            this.m_dataGrdArguments.Columns[0].ReadOnly = true;
            // the user will edit this column
            this.m_dataGrdArguments.Columns[1].ReadOnly = false;
            this.m_dataGrdArguments.Columns[2].ReadOnly = true;

            this.m_dataGrdArguments.Rows.Clear();

            // clear action text, otherwise the previos text would remain in case the selected command provides no action 
            this.m_txtAction.Text = "";

            Type type = CommandStringParser.GetAllCommandTypes().FirstOrDefault(t => t.Name.ToString().Equals(cmdTag));
            if (type == null)
            {
                return;
            }

            // create instance to get default value
            Command cmd = (Command)Activator.CreateInstance(type);

            // do not show unpublished commands
            if (!cmd.PublishCommand)
            {
                return;
            }

            // print command action to text box
            this.m_txtAction.Text = cmd.GetCommandDescription();

            // arguments
            foreach (FieldInfo fi in type.GetFields())
            {
                foreach (object obj in fi.GetCustomAttributes(true).Where(o => o is Parameter))
                {
                    Parameter pf = (Parameter)obj;
                    // skip e.g. Profile
                    if (pf.PrintParameter)
                    {
                        // argument name, default value, type
                        this.m_dataGrdArguments.Rows.Add(fi.Name, fi.GetValue(cmd), fi.FieldType.Name);
                        int index = this.m_dataGrdArguments.Rows.Count - 2;
                        if (index < this.m_dataGrdArguments.Rows.Count)
                        {
                            this.m_dataGrdArguments.Rows[index].Cells[0].ToolTipText = pf.Comment;
                            this.m_dataGrdArguments.Rows[index].Cells[1].ToolTipText = pf.Comment;
                            this.m_dataGrdArguments.Rows[index].Cells[2].ToolTipText = pf.Comment;
                        }
                    }
                }
            }            
        }

        private void m_btnExecute_Click(object sender, EventArgs e)
        {
            if (this.m_cmdBoxCommands.SelectedItem == null)
            {
                return;
            }

            String cmd = this.m_cmdBoxCommands.SelectedItem.ToString();
            if (String.IsNullOrEmpty(cmd))
            {
                return;
            }
            foreach (DataGridViewRow entry in this.m_dataGrdArguments.Rows)
            {
                if (entry.Cells.Count < 2)
                {
                    continue;
                }

                if (entry.Cells[0].Value == null || entry.Cells[1].Value == null)
                {
                    continue;
                }

                String name = entry.Cells[0].Value.ToString();
                String value = entry.Cells[1].Value.ToString();
                // remove leading and traling white spaces
                value = Regex.Replace(value, @"^\s*", "");
                value = Regex.Replace(value, @"\s*$", "");

                // quote value, might be "undefined file name"
                cmd += " " + name + "=\"" + value + "\"";
            }
            cmd += ";";
            Commands.CommandExecuter.Instance.Execute(cmd);
        }

        private void CommandInterfaceCtrl_Resize(object sender, EventArgs e)
        {
            int gap = 40;
            this.m_cmdBoxCommands.Width = this.Width - gap;
            this.m_txtAction.Width = this.Width - gap;
            this.m_dataGrdArguments.Width = this.Width - gap;
            this.m_btnExecute.Left = (this.Width - gap) - this.m_btnExecute.Width;


            this.m_dataGrdArguments.Height = this.Height - 200;
            this.m_btnExecute.Top = this.Height - 50;
        }
    }
}

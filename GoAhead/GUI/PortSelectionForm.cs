﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using GoAhead.FPGA;
using GoAhead.Commands.BlockingShared;

namespace GoAhead.GUI
{
    public partial class PortSelectionForm : Form
    {
        public PortSelectionForm()
        {
            InitializeComponent();

            Settings.StoredPreferences.Instance.GUISettings.Open(this);

            this.Init();
        }

        void Init()
        {
            // collect tile type by prefix to reduce tile count in second loop
            Dictionary<String, Tile> prefices = new Dictionary<String, Tile>();
            foreach (Tile tile in FPGA.TileSelectionManager.Instance.GetSelectedTiles())
            {
                String[] atoms = Regex.Split(tile.Location, "_X"); ;
                String prefix = atoms[0];

                if (!prefices.ContainsKey(prefix))
                {
                    prefices.Add(prefix, tile);
                }
            }


            SortedDictionary<String, bool> ports = new SortedDictionary<string, bool>();

            foreach (Tile tile in prefices.Values)
            {
                foreach (Port p in tile.SwitchMatrix.Ports)
                {
                    if ( (this.m_chkkInvert.Checked &&  (!Regex.IsMatch(p.ToString(), this.m_txtFilter.Text))) ||
                         (!this.m_chkkInvert.Checked &&  Regex.IsMatch(p.ToString(), this.m_txtFilter.Text)))
                    {
                        if(!ports.ContainsKey(p.ToString()))
                        {
                            ports.Add(p.ToString(), false);
                        }
                    }
                }
            }

            // clear and fill left box
            this.m_lstAvailablePorts.Items.Clear();
            foreach (String s in ports.Keys)
            {
                this.m_lstAvailablePorts.Items.Add(s);
            }

            // clear right box
            //this.m_lstSelectedPorts.Items.Clear();
        }

        private void m_btnReset_Click(object sender, EventArgs e)
        {
            this.m_txtFilter.Text = "";
            this.Init();
        }

        private void m_btnAdd_Click(object sender, EventArgs e)
        {
            foreach (object o in this.m_lstAvailablePorts.SelectedItems)
            {
                if (!this.m_lstSelectedPorts.Items.Contains(o.ToString()))
                {
                    this.m_lstSelectedPorts.Items.Add(o.ToString());
                }
            }   
        }

        private void m_btnRemove_Click(object sender, EventArgs e)
        {
            foreach(object o in this.m_lstSelectedPorts.SelectedItems)
            {
                this.m_lstSelectedPorts.Items.Remove(o.ToString());
            }
        }

        private void m_btnBlock_Click(object sender, EventArgs e)
        {
            foreach (object o in this.m_lstSelectedPorts.Items)
            {
                ExcludePortsFromBlockingInSelection cmd = new ExcludePortsFromBlockingInSelection();
                cmd.PortName = o.ToString();
                cmd.IncludeAllPorts = this.m_chkIncludeAllPorts.Checked;
                cmd.CheckForExistence = false;
                Commands.CommandExecuter.Instance.Execute(cmd);
            }

            this.Close();
        }

        private void m_btnRemovePortsFromNets_Click(object sender, EventArgs e)
        {
            foreach (object o in this.m_lstSelectedPorts.Items)
            {
                Commands.CommandExecuter.Instance.Execute(new Commands.Sets.RemoveArcs(o.ToString()));
            }
            this.Close();
        }

  

        private void m_txtFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bool test = Regex.IsMatch("test", this.m_txtFilter.Text);
                this.m_lblRegexpError.Text = "";
                this.Init();
            }
            catch (Exception error)
            {
                String errorMessage = "No valid regular expression given: " + error.Message;
                this.m_lblRegexpError.Text = errorMessage;
                //MessageBox.Show(errorMessage);
            }
        }



        private void PortSelectionForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Settings.StoredPreferences.Instance.GUISettings.Close(this);
        }
    }
}

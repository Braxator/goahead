﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GoAhead.Settings;

namespace GoAhead.GUI
{
    public partial class FileSelectionCtrl : UserControl
    {
        public FileSelectionCtrl()
        {
            InitializeComponent();
            this.m_lblFile.Text = this.Label;

        }

        public void RestorePreviousSelection()
        {
            if (StoredPreferences.Instance.TextBoxSettings.HasSetting(this.CallerString))
            {
                this.m_txtFile.Text = StoredPreferences.Instance.TextBoxSettings.GetSetting(this.CallerString);
            }
            this.m_lblFile.Text = this.Label;
        }

        private String CallerString
        {
            get { return "FileSelectionCtrl" + this.Label; }
        }

        String _label = "Label";
        [
            Category("Appearance"),
            Description("The String to display on the label")
        ]
        public String Label
        {
            get { return this._label; }
            set { this._label = value; }
        }

        String _filter = "All files|*.*";
        [
            Category("Appearance"),
            Description("The file filter in the browse dialog")
        ]
        public String Filter
        {
            get { return this._filter; }
            set { this._filter = value; }
        }

        /// <summary>
        /// The value for FileName for CommandWithFileOutput
        /// </summary>
        public String FileName
        {
            get { return this.m_txtFile.Text; }
            set { this.m_txtFile.Text = value; }
        }

        /// <summary>
        /// The value for Append for CommandWithFileOutput
        /// </summary>
        public bool Append
        {
            get { return this.m_chkBoxAppend.Checked; }
            set { this.m_chkBoxAppend.Checked = value; }
        }

        public void DisableAppendCheckBox()
        {
            this.m_chkBoxAppend.Visible = false;
        }

        private void m_btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Select a File";
            openFileDialog.Multiselect = false;
            openFileDialog.CheckFileExists = false;
            openFileDialog.Filter = this.Filter;

            if (StoredPreferences.Instance.FileDialogSettings.HasSetting(this.CallerString))
            {
                openFileDialog.InitialDirectory = StoredPreferences.Instance.FileDialogSettings.GetSetting(this.CallerString);
            }

            // cancel
            if (openFileDialog.ShowDialog() != DialogResult.OK)
                return;

            if (String.IsNullOrEmpty(openFileDialog.FileName))
                return;

            // store last user path
            StoredPreferences.Instance.FileDialogSettings.AddOrUpdateSetting(this.CallerString, System.IO.Path.GetDirectoryName(openFileDialog.FileName));

            this.m_txtFile.Text = openFileDialog.FileName;

            StoredPreferences.Instance.TextBoxSettings.AddOrUpdateSetting(this.CallerString, this.m_txtFile.Text);
           
        }

        private void m_txtFile_TextChanged(object sender, EventArgs e)
        {
            StoredPreferences.Instance.TextBoxSettings.AddOrUpdateSetting(this.CallerString, this.m_txtFile.Text);
        }
    }
}

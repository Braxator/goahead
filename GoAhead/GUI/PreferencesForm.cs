﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GoAhead.Commands;
using GoAhead.Settings;
using GoAhead.Commands.CommandExecutionSettings;

namespace GoAhead.GUI
{
    public partial class PreferencesForm : Form
    {
        public PreferencesForm(Form parentForm)
        {
            InitializeComponent();

            this.m_parentForm = parentForm;

            this.m_chkExpandSelection.Checked = StoredPreferences.Instance.ExecuteExpandSelection;
            this.m_chkPrintWrappedCommands.Checked = CommandExecuter.Instance.PrintWrappedCommands;
            this.m_chkShowToolTips.Checked = StoredPreferences.Instance.ShowToolTips;
            this.m_chkPrintSelectionResourceInfo.Checked = StoredPreferences.Instance.PrintSelectionResourceInfo;
            this.m_numDropDownConsoleGUIShare.Value = (decimal)StoredPreferences.Instance.ConsoleGUIShare;
            this.m_numDropDownRectangleWidth.Value = (decimal)StoredPreferences.Instance.RectangleWidth;
           
            // get window position
            Settings.StoredPreferences.Instance.GUISettings.Open(this);
        }

        private void m_btnOK_Click(object sender, EventArgs e)
        {
            this.AccecptSettings(true);
            this.Close();
        }

        private void m_btnApply_Click(object sender, EventArgs e)
        {
            this.AccecptSettings(false);
        }

        private void AccecptSettings(bool closeForm)
        {
            StoredPreferences.Instance.ExecuteExpandSelection = this.m_chkExpandSelection.Checked;
            StoredPreferences.Instance.ShowToolTips = this.m_chkShowToolTips.Checked;
            StoredPreferences.Instance.PrintSelectionResourceInfo = this.m_chkPrintSelectionResourceInfo.Checked;
            StoredPreferences.Instance.ConsoleGUIShare = (double)this.m_numDropDownConsoleGUIShare.Value;
            StoredPreferences.Instance.RectangleWidth = (float)this.m_numDropDownRectangleWidth.Value;
            StoredPreferences.SavePrefernces();

            if (this.m_chkPrintWrappedCommands.Checked)
            {
                CommandExecuter.Instance.Execute(new PrintWrappedCommands());
            }
            else
            {
                CommandExecuter.Instance.Execute(new StopToPrintWrappedCommands());
            }

            this.m_parentForm.Invalidate();

            if (closeForm)
            {
                this.Close();
            }
        }

        private void m_btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private readonly Form m_parentForm;

        private void PreferencesForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            // store window position
            Settings.StoredPreferences.Instance.GUISettings.Close(this);
        }

        private void m_numDropDownConsoleGUIShare_ValueChanged(object sender, EventArgs e)
        {
            StoredPreferences.Instance.ConsoleGUIShare = (double)this.m_numDropDownConsoleGUIShare.Value;
            this.m_parentForm.Invalidate();
        }
    }
}

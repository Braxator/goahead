﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using GoAhead.Commands;
using GoAhead.Settings;

namespace GoAhead.GUI.ScriptDebugger
{
    public partial class ScriptDebuggerCtrl : UserControl
    {
        public ScriptDebuggerCtrl()
        {
            InitializeComponent();

            this.m_eventHanlder = new FileSystemEventHanlder(this);
            this.ReadloadScriptDelegateInstance = new ReadloadScriptDelegate(this.ReloadScript);

            this.m_txtCmds.WordWrap = false;
            this.m_txtLineNumber.WordWrap = false;

            this.m_timer.Interval = 1000;
            this.m_timer.Tick += this.ShowToolTipAfterTimerFired;
           
        }

        public void Close()
        {
            this.m_fsw.EnableRaisingEvents = false;
        }

        private void m_btnBrowseForScript_Click_1(object sender, EventArgs e)
        {
            String caller = "m_btnBrowseForScript_Click";

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Select a Script File";
            openFileDialog.Multiselect = false;
            openFileDialog.CheckFileExists = true;
            openFileDialog.Filter = "GoAhead Script File|*.goa";

            if (StoredPreferences.Instance.FileDialogSettings.HasSetting(caller))
            {
                openFileDialog.InitialDirectory = StoredPreferences.Instance.FileDialogSettings.GetSetting(caller);
            }
            
            // cancel
            if (openFileDialog.ShowDialog() != DialogResult.OK || String.IsNullOrEmpty(openFileDialog.FileName))
            {
                return;
            }
            
            this.m_lblScriptFiled.Text = "No Script File loaded";
            // store last user path
            StoredPreferences.Instance.FileDialogSettings.AddOrUpdateSetting(caller, System.IO.Path.GetDirectoryName(openFileDialog.FileName));

            this.LoadScript(openFileDialog.FileName);
        }

        private void LoadScript(String fileName)
        {
            this.m_eventHanlder.FileName = fileName;
            this.m_lblScriptFiled.Text = fileName;

            this.m_fsw = new FileSystemWatcher();
            this.m_fsw.Path = Directory.GetParent(fileName).FullName;
            this.m_fsw.EnableRaisingEvents = true;
            this.m_fsw.Changed += new FileSystemEventHandler(this.m_eventHanlder.ScriptFileChanged);

            this.ReloadScript();
        }

        private void m_btnReload_Click(object sender, EventArgs e)
        {
            this.ReloadScript();
        }

        private void ReloadScript()
        {
            String scriptFile = this.m_lblScriptFiled.Text;
            if (!File.Exists(scriptFile))
            {
                MessageBox.Show("Script file not found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            this.m_txtCmds.Clear();
            this.m_txtLineNumber.Clear();
            this.m_commandIndeces.Clear();
            this.m_commandIndex = 0;
            this.m_form = 0;
            this.m_length = 0;
            this.m_lblCmd.Text = "No command selected";

            // copy file into text box
            StreamReader sr = new StreamReader(scriptFile);
            String line = "";

            int lineCount = 0;
            while ((line = sr.ReadLine()) != null)
            {
                this.m_txtCmds.AppendText(line + Environment.NewLine);
                this.m_txtLineNumber.AppendText((lineCount + 1).ToString() + Environment.NewLine);
                lineCount++;
            }
            sr.Close();

            this.m_context = new CommandExecutionContext();

            CommandStringParser parser = new CommandStringParser(this.m_txtCmds.Text);
            foreach (String cmdStr in parser.Parse())
            {
                Command cmd;
                String errorDescr;
                bool valid = parser.ParseCommand(cmdStr, false, out cmd, out errorDescr);
                if (valid)
                {
                    this.m_context.Parsed(cmdStr, cmd);
                }
                else
                {
                    MessageBox.Show(errorDescr + Environment.NewLine + parser.State.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            this.m_context.SetLabels();

            foreach (Tuple<int, int> range in parser.Topology.GetBorders(CommandStringTopology.TopologyType.Comment))
            {
                this.m_txtCmds.Select(range.Item1, (range.Item2 - range.Item1) + 1);
                this.m_txtCmds.SelectionColor = Color.Green;
            }
            foreach (Tuple<int, int> range in parser.Topology.GetBorders(CommandStringTopology.TopologyType.CommandTag))
            {
                this.m_txtCmds.Select(range.Item1, (range.Item2 - range.Item1) + 1);
                this.m_txtCmds.SelectionColor = Color.Red;               
            }
            foreach (Tuple<int, int> range in parser.Topology.GetBorders(CommandStringTopology.TopologyType.ArgumentNames))
            {
                this.m_txtCmds.Select(range.Item1, (range.Item2 - range.Item1) + 1);
                this.m_txtCmds.SelectionColor = Color.Blue;
            }
            foreach (Tuple<int, int> range in parser.Topology.GetBorders(CommandStringTopology.TopologyType.ArgumentValues))
            {
                this.m_txtCmds.Select(range.Item1, (range.Item2 - range.Item1) + 1);
                this.m_txtCmds.SelectionColor = Color.Black;
            }
            foreach (Tuple<int, int> range in parser.Topology.GetBorders(CommandStringTopology.TopologyType.CompleteCommand))
            {
                this.m_commandIndeces.Add(new Tuple<int, int>(range.Item1, range.Item2));
            }

            this.SetSelectedItemToNextCommandListView();
        }

        private void m_btnClear_Click(object sender, EventArgs e)
        {
            this.m_txtCmds.Clear();
            this.m_lblScriptFiled.Text = "No Script File loaded";

            this.m_fsw.EnableRaisingEvents = false;

            this.m_lblCmd.Text = "";
        }
        
        private void SetSelectedItemToNextCommandListView()
        {
            this.m_txtCmds.Select(this.m_form, this.m_length);
            //String debug = this.m_txtCmds.Text.Substring(this.m_form, this.m_length);
            if (this.m_breakPoints.FirstOrDefault(t => t.Item1 == this.m_form) != null)
            {
                this.m_txtCmds.SelectionBackColor = this.m_breakpointColor;
            }
            else
            {
                this.m_txtCmds.SelectionBackColor = this.m_defaultColor;
            }
            this.m_txtCmds.DeselectAll();

            // wrap around
            if (this.m_commandIndex == this.m_commandIndeces.Count)
            {
                this.m_commandIndex = 0;
            }

            if (this.m_commandIndex < this.m_commandIndeces.Count)
            {               
                this.m_length = this.m_commandIndeces[this.m_commandIndex].Item2 - this.m_commandIndeces[this.m_commandIndex].Item1;
                this.m_form = this.m_commandIndeces[this.m_commandIndex].Item1;

                this.m_txtCmds.Select(this.m_form, this.m_length);
                this.m_selectedText = this.m_txtCmds.SelectedText;
                this.m_txtCmds.SelectionBackColor = this.m_nextCommandColor;
                this.m_lblCmd.Text = "Next command: " + this.m_txtCmds.SelectedText;
                this.m_txtCmds.DeselectAll();
            }
        }

        /// <summary>
        /// Execute the next command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_btnExecuteNextCmd_Click(object sender, EventArgs e)
        {
            this.ExecuteNextCmd();
        }

        /// <summary>
        /// Execute commands until breakpoint is reached
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_btnRun_Click(object sender, EventArgs e)
        {
            while (true)
            {
                bool furtherCommandFound = this.ExecuteNextCmd();

                // all commands done
                if (!furtherCommandFound)
                {
                    break;
                }
                
                // breakpoint reached
                int from = this.m_commandIndeces[this.m_commandIndex].Item1;
                if (this.m_breakPoints.FirstOrDefault(t => t.Item1 == from) != null)
                {
                    break;
                }
            }
        }

        private bool ExecuteNextCmd()
        {
            // empty list
            if (this.m_txtCmds.TextLength == 0)
            {
                MessageBox.Show("No command script loaded", "Error", MessageBoxButtons.OK);
                return false;
            }

            if (String.IsNullOrEmpty(this.m_selectedText))
            {
                MessageBox.Show("No command selected", "Error", MessageBoxButtons.OK);
                return false;
            }

            int nextCommandIndex = this.m_context.Execute(this.m_commandIndex);

            //Commands.CommandExecuter.Instance.Execute(this.m_selectedText);

            //bool furherCommandFound = this.m_commandIndex + 1 < this.m_commandIndeces.Count;
            bool furherCommandFound = nextCommandIndex < this.m_commandIndeces.Count;
            this.m_commandIndex = nextCommandIndex;

            this.SetSelectedItemToNextCommandListView();

            // update views
            if (this.m_InvalidateMeAfterEachCommand != null)
            {
                this.m_InvalidateMeAfterEachCommand.Invalidate();
            }

            return furherCommandFound;
        }

        private void m_btnStop_Click(object sender, EventArgs e)
        {
            this.m_commandIndex = 0;
            this.SetSelectedItemToNextCommandListView();
        }

        private void ScriptDebugger_Load(object sender, EventArgs e)
        {
            if (File.Exists(this.m_scriptToLoadAtStartup))
            {
                this.LoadScript(this.m_scriptToLoadAtStartup);
            }

            // Create the ToolTip and associate with the Form container.
            ToolTip toolTips = new ToolTip();

            // Set up the delays for the ToolTip.
            toolTips.AutoPopDelay = 5000;
            toolTips.InitialDelay = 1000;
            toolTips.ReshowDelay = 500;
            // Force the ToolTip text to be displayed whether or not the form is active.
            toolTips.ShowAlways = true;

            // Set up the ToolTip text for the Button and Checkbox.
            toolTips.SetToolTip(this.m_btnExecuteNextCmd, "Execute the currently selected line");
            toolTips.SetToolTip(this.m_btnRun, "Execute all commands until the next breakpoint is reached");
            toolTips.SetToolTip(this.m_btnStop, "Jump back to begin of script file");
            toolTips.SetToolTip(this.m_btnBrowseForScript, "Load a script file");
            toolTips.SetToolTip(this.m_btnClear, "Clear all commands in text box");
        }

        #region ContextMenu
       
        private void m_setBreakpointToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int from = this.m_commandIndeces[this.m_rightClickedCommandIndex].Item1;
            int length = this.m_commandIndeces[this.m_rightClickedCommandIndex].Item2 - this.m_commandIndeces[this.m_rightClickedCommandIndex].Item1;

            if (this.m_breakPoints.FirstOrDefault(t => t.Item1 == from) == null)
            {
                this.m_breakPoints.Add(new Tuple<int,int>(from, length));
                this.m_txtCmds.Select(from, length);
                this.m_txtCmds.SelectionBackColor = this.m_breakpointColor;
                this.m_txtCmds.DeselectAll();
            }
        }

        private void m_deleteBreakpointToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int from = this.m_commandIndeces[this.m_rightClickedCommandIndex].Item1;
            int length = this.m_commandIndeces[this.m_rightClickedCommandIndex].Item2 - this.m_commandIndeces[this.m_rightClickedCommandIndex].Item1;

            Tuple<int, int> breakPointInfo = this.m_breakPoints.FirstOrDefault(t => t.Item1 == from);
            if (breakPointInfo != null)
            {
                this.m_txtCmds.Select(from, length);
                this.m_txtCmds.SelectionBackColor = this.m_defaultColor;
                this.m_txtCmds.DeselectAll();

                this.m_breakPoints.Remove(breakPointInfo);
            }
        }

        private void m_removeAllBreakpointsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Tuple<int, int> pair in this.m_breakPoints)
            {
                this.m_txtCmds.Select(pair.Item1, pair.Item2);
                this.m_txtCmds.SelectionBackColor = this.m_defaultColor;
                this.m_txtCmds.DeselectAll();
            }
            this.m_breakPoints.Clear();
            this.SetSelectedItemToNextCommandListView();
        }

        #endregion ContextMenu

        private void ScriptDebuggerCtrl_Resize(object sender, EventArgs e)
        {
            if (this.Width < 200)
            {
                this.Width = 200;
            }
            if (this.Height < 400)
            {
                this.Height = 400;
            }

            //Console.WriteLine("W" + this.Width);
            //Console.WriteLine("H" + this.Height);

            int left = 5;
            int top = 5;
            double listViewShare = 0.8;
            int height = (int)((double)this.Height * listViewShare) - 4 * top;

            this.m_txtLineNumber.Left = left;
            this.m_txtLineNumber.Top = top;
            this.m_txtLineNumber.Height = height;

            this.m_txtCmds.Left = left + this.m_txtLineNumber.Width - 18;
            this.m_txtCmds.Top = top;
            this.m_txtCmds.Height = height;
            this.m_txtCmds.Width = (this.Width - 4 * left) - this.m_txtLineNumber.Width + 18;

            this.m_grpBoxControls.Left = left;
            this.m_grpBoxControls.Top = top + this.m_txtCmds.Height + top;
            this.m_grpBoxControls.Height = (int)((double)this.Height * (1-listViewShare)) - 4*top;
            this.m_grpBoxControls.Width = this.Width-4*left;           
        }

        private void m_txtCmds_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.m_lstViewContextMenu.Show(this, e.Location);
                int cmdIndex = this.GetCommandIndex(e.Location);
                if (cmdIndex != -1)
                {
                    this.m_rightClickedCommandIndex = cmdIndex;
                }
            }
        }

        private void m_txtCmds_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                int cmdIndex = this.GetCommandIndex(e.Location);
                if (cmdIndex != -1)
                {
                    this.m_commandIndex = cmdIndex;
                    this.SetSelectedItemToNextCommandListView();
                }
            }
        }

        private int GetCommandIndex(Point p)
        {
            int positionToSearch = this.m_txtCmds.GetCharIndexFromPosition(p);
            int cmdIndex = this.m_commandIndeces.FindIndex(t => t.Item1 <= positionToSearch && positionToSearch <= t.Item2);
            return cmdIndex;
        }
 
        public String ScriptToLoadAtStartup
        {
            get { return this.m_scriptToLoadAtStartup; }
            set { this.m_scriptToLoadAtStartup = value; }
        }

        public Form InvalidateMeAfterEachCommand
        {
            get 
            { 
                return m_InvalidateMeAfterEachCommand; 
            }
            set 
            { 
                m_InvalidateMeAfterEachCommand = value; 
            }
        }

        #region Tooltips
        private void ShowToolTipAfterTimerFired(object sender, EventArgs e)
        {
            if (StoredPreferences.Instance.ShowToolTips)
            {
                Point p = (Point)this.m_timer.Tag;

                // only show a new tool tip, if the position changes
                if (this.m_lastToolTipLocation.Equals(p))
                {
                    return;
                }

                this.m_lastToolTipLocation = p;

                int cmdIndex = this.GetCommandIndex(p);
                if (cmdIndex == -1)
                {
                    return;
                }

                int from = this.m_commandIndeces[cmdIndex].Item1;
                int length = this.m_commandIndeces[cmdIndex].Item2 - this.m_commandIndeces[cmdIndex].Item1;

                String commandString = this.m_txtCmds.Text.Substring(from, length);

                Command cmd = null;
                String errorDescription = "";
                CommandStringParser parser = new CommandStringParser(commandString);
                bool valid = parser.ParseCommand(commandString, false, out cmd, out errorDescription);
                if (valid)
                {
                    // print command action to text box
                    foreach (Attribute attr in Attribute.GetCustomAttributes(cmd.GetType()).Where(a => a is CommandDescription))
                    {
                        CommandDescription descr = (CommandDescription)attr;
                        String toolTip = descr.Description;

                        this.m_toolTip.Show(toolTip, this, p.X, p.Y + 20, 10000);
                        break;
                    }
                }
            }
        }

        private void m_txtCmds_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.m_mouseEntered && StoredPreferences.Instance.ShowToolTips)
            {
                // start timer for toll tip
                this.m_timer.Stop();
                this.m_timer.Start();
                // store position
                this.m_timer.Tag = e.Location;
            }
        }

        private void m_txtCmds_MouseEnter(object sender, EventArgs e)
        {
            this.m_mouseEntered = true;
        }

        private void m_txtCmds_MouseLeave(object sender, EventArgs e)
        {
            this.m_mouseEntered = false;
        }
        #endregion

        private String m_scriptToLoadAtStartup = "";
        private CommandExecutionContext m_context = new CommandExecutionContext();
               
        private bool m_mouseEntered = false;
        private ToolTip m_toolTip = new ToolTip();
        private Timer m_timer = new Timer();
        /// <summary>
        /// Strange: The mouse move event occurs after the tooltip disappears
        /// Hoever, the location property does not change with the event
        /// In order not to show the tooltip again, we store the last position and only reshow the tooltip if the position changes
        /// </summary>
        private Point m_lastToolTipLocation = new Point(0, 0);

        private readonly Color m_breakpointColor = Color.Orange;
        private readonly Color m_nextCommandColor = Color.DarkGray;
        private readonly Color m_defaultColor = Color.FromName("Control");
        private String m_selectedText = "";
        private int m_form = 0;
        private int m_length = 0;
        private int m_commandIndex = 0;
        private int m_rightClickedCommandIndex = 0;
        private Form m_InvalidateMeAfterEachCommand = null;


        private List<Tuple<int, int>> m_commandIndeces = new List<Tuple<int, int>>();
        private List<Tuple<int, int>> m_breakPoints = new List<Tuple<int, int>>();

        private FileSystemWatcher m_fsw = new FileSystemWatcher();

        public delegate void ReadloadScriptDelegate();
        public ReadloadScriptDelegate ReadloadScriptDelegateInstance;
        private FileSystemEventHanlder m_eventHanlder;
    }

    public class FileSystemEventHanlder
    {
        public FileSystemEventHanlder(ScriptDebuggerCtrl debuggerCtrl)
        {
            this.m_debuggerCtrl = debuggerCtrl;
        }

        public void ScriptFileChanged(object sender, FileSystemEventArgs e)
        {
            if (!File.Exists(this.FileName))
            {
                return;

            }
            DateTime lastChange = File.GetLastWriteTime(this.FileName);
            if (!e.FullPath.Equals(this.FileName) || e.ChangeType != WatcherChangeTypes.Changed || this.m_lastChange == lastChange)
            {
                return;
            }

            this.m_lastChange = lastChange;

            DialogResult result = MessageBox.Show("GOA Script File Changed. Reload?", "File Changed", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.m_debuggerCtrl.Invoke(m_debuggerCtrl.ReadloadScriptDelegateInstance);
            }
        }

        private ScriptDebuggerCtrl m_debuggerCtrl;
        /// <summary>
        /// stores the last occurences on OnChange event
        /// </summary>
        private DateTime m_lastChange = DateTime.MinValue;

        public String FileName = "";
    }
}

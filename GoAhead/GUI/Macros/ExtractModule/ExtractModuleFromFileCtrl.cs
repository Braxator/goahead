﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GoAhead.Commands.XDLManipulation;

namespace GoAhead.GUI.ExtractModules
{
    public partial class ExtractModuleCtrl : UserControl
    {
        public ExtractModuleCtrl()
        {
            InitializeComponent();
           
            this.m_fileSelXDLInFile.DisableAppendCheckBox();
            this.m_fileSelXDLOutFile.DisableAppendCheckBox();
            this.m_fileSelBinMacro.DisableAppendCheckBox();
            
            this.m_fileSelXDLInFile.RestorePreviousSelection();
            this.m_fileSelXDLOutFile.RestorePreviousSelection();
            this.m_fileSelBinMacro.RestorePreviousSelection();
        }

        private void m_btnOk_Click(object sender, EventArgs e)
        {
            switch (this.ModuleSource)
            {
                case ExtractModuleForm.ModuleSourceType.FromNetlist:
                    {
                        Commands.XDLManipulation.ExtractModule cmd = new Commands.XDLManipulation.ExtractModule();
                        cmd.BinaryNetlist = this.m_fileSelBinMacro.FileName;
                        cmd.XDLInFile = this.m_fileSelXDLInFile.FileName;
                        cmd.XDLOutFile = this.m_fileSelXDLOutFile.FileName;
                        Commands.CommandExecuter.Instance.Execute(cmd);
                        break;
                    }
                case ExtractModuleForm.ModuleSourceType.FromSelection:
                    {
                        Commands.NetlistContainerGeneration.SaveSelectionAsModule cmd = new Commands.NetlistContainerGeneration.SaveSelectionAsModule();
                        cmd.BinaryNetlist = this.m_fileSelBinMacro.FileName;
                        cmd.OutFile = this.m_fileSelXDLOutFile.FileName;
                        Commands.CommandExecuter.Instance.Execute(cmd);
                        break;
                    }
                default:
                    {
                        throw new ArgumentException("Unknown source for module: " + this.ModuleSource);
                    };
            }          
        }

        private void UpdateGUI()
        {
            switch (this.ModuleSource)
            {
                case ExtractModuleForm.ModuleSourceType.FromNetlist:                                
                    // all three
                    this.m_fileSelXDLInFile.Visible = true;
                    this.m_fileSelXDLInFile.Top = 0;
                    this.m_fileSelXDLOutFile.Top = this.m_fileSelXDLInFile.Height;
                    this.m_fileSelBinMacro.Top = this.m_fileSelXDLOutFile.Top + this.m_fileSelXDLInFile.Height;
                    break;
                case ExtractModuleForm.ModuleSourceType.FromSelection:                    
                    // no in file
                    this.m_fileSelXDLInFile.Visible = true;
                    this.m_fileSelXDLOutFile.Top = 0;
                    this.m_fileSelBinMacro.Top = this.m_fileSelXDLOutFile.Height;

                    break;
                default:
                    break;
            }
        }

        public ExtractModuleForm.ModuleSourceType ModuleSource
        {
            get { return this.m_moduleSource;}
            set { this.m_moduleSource = value; this.UpdateGUI(); }

        }

        private ExtractModuleForm.ModuleSourceType m_moduleSource = ExtractModuleForm.ModuleSourceType.FromNetlist;
    }
}

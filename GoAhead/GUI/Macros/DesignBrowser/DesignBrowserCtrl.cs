﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GoAhead.Commands;
using GoAhead.Objects;
using GoAhead.Code.XDL;

namespace GoAhead.GUI.Macros.DesignBrowser
{
    public partial class DesignBrowserCtrl : UserControl
    {
        public DesignBrowserCtrl()
        {
            InitializeComponent();
        }

        private void DesignBrowserCtrl_Load(object sender, EventArgs e)
        {
            PrintResourceConsumptionPerModule printResCmd = new PrintResourceConsumptionPerModule();
            printResCmd.NetlistContainerName = this.NetlistContainerName;
            printResCmd.TreeView = this.m_treeView;
            printResCmd.Do();
        }
                
        public String NetlistContainerName { get; set; }

        private void m_treeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if(!Objects.NetlistContainerManager.Instance.Contains(this.NetlistContainerName))
            {
                return;
            }

            NetlistContainer nlc = Objects.NetlistContainerManager.Instance.Get(this.NetlistContainerName);
            
            int slices = 0;
            int dsp = 0;
            int bram = 0;

            // primitive
            if (e.Node.Tag != null)
            {
                this.m_txtInstanceCode.Clear();

                if (!(e.Node.Tag is TreeNodeTag))
                { 
                }

                TreeNodeTag tag = (TreeNodeTag)e.Node.Tag;
                XDLInstance inst = tag.Instance;

                if (tag.Instance == null)
                {
                    this.m_txtInstanceCode.AppendText("no primitive code available for hierarchy node");

                    foreach (TreeNode leave in e.Node.GetChildNodes())
                    {
                        TreeNodeTag leaveTag = (TreeNodeTag)leave.Tag;
                        XDLInstance leaveInstance = leaveTag.Instance;
                        if (leaveInstance != null)
                        {
                            if (Objects.IdentifierManager.Instance.IsMatch(leaveInstance.Location, IdentifierManager.RegexTypes.CLBRegex)) { slices += 1; }
                            else if (Objects.IdentifierManager.Instance.IsMatch(leaveInstance.Location, IdentifierManager.RegexTypes.BRAMRegex)) { dsp += 1; }
                            else if (Objects.IdentifierManager.Instance.IsMatch(leaveInstance.Location, IdentifierManager.RegexTypes.DSPRegex)) { bram += 1; }
                        }
                    }

                }
                else
                {
                    this.m_txtInstanceCode.AppendText(inst.ToString());

                    if (Objects.IdentifierManager.Instance.IsMatch(inst.Location, IdentifierManager.RegexTypes.CLBRegex)) { slices = 1; }
                    else if (Objects.IdentifierManager.Instance.IsMatch(inst.Location, IdentifierManager.RegexTypes.BRAMRegex)) { dsp = 1; }
                    else if (Objects.IdentifierManager.Instance.IsMatch(inst.Location, IdentifierManager.RegexTypes.DSPRegex)) { bram = 1; }
                }
            }
            this.m_lblSlices.Text = slices + " Slices";
            this.m_lblBRAM.Text = bram + " BRAMs";
            this.m_lblDSP.Text = dsp + " DSPs";
        }
    }
}

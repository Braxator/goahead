﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GoAhead.Commands.Data;
using GoAhead.Objects;

namespace GoAhead.GUI.Macros.XDLGeneration
{
    public partial class XDLGenerationCtrl : UserControl
    {
        public XDLGenerationCtrl()
        {
            InitializeComponent();

            BindingSource bsrc = new BindingSource();
            bsrc.DataSource = Objects.NetlistContainerManager.Instance.NetlistContainerBindingList;

            this.m_checkedListBoxNetlistContainer.DisplayMember = "Name";
            this.m_checkedListBoxNetlistContainer.ValueMember = "Name";
            this.m_checkedListBoxNetlistContainer.DataSource = bsrc;

            this.m_fileSel.Label = "Output XDL File";
            this.m_fileSel.Append = false;
            this.m_fileSel.RestorePreviousSelection();            
        }

        private void m_btnGenerate_Click(object sender, EventArgs e)
        {
            List<String> names = new List<String>();
            foreach (Object o in this.m_checkedListBoxNetlistContainer.SelectedItems)
            {
                NetlistContainer nlc = (NetlistContainer)o;
                names.Add(nlc.Name);
            }

            GenerateXDL genCmd = new GenerateXDL();
            genCmd.DesignName = this.m_txtDesignName.Text;
            genCmd.FileName = this.m_fileSel.FileName;
            genCmd.IncludeDesignStatement = this.m_chkIncludeDesignStatement.Checked;
            genCmd.IncludeDummyNets = this.m_chkIncludeDummyNets.Checked;
            genCmd.IncludeModuleFooter = this.m_chkIncludeFooter.Checked;
            genCmd.IncludeModuleHeader = this.m_chkIncludeHeader.Checked;
            genCmd.IncludePorts = this.m_chkIncludePorts.Checked;
            genCmd.NetlistContainerNames = names;
            genCmd.SortInstancesBySliceName = this.m_chkSort.Checked;

            Commands.CommandExecuter.Instance.Execute(genCmd);

            if (this.ParentForm != null)
            {
                this.ParentForm.Close();
            }
        }
    }
}

﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using GoAhead.FPGA;
using GoAhead.Objects;
using GoAhead.Commands.LibraryElementInstantiation;
using GoAhead.GUI.LibraryElementInstantiation;
using GoAhead.Commands.NetlistContainerGeneration;
using GoAhead.Commands.LibraryElementInstantiationManager;

namespace GoAhead.GUI.MacroForm
{
    public partial class LibraryElementPlacerCtrl : UserControl, Interfaces.IObserver
    {
        public LibraryElementPlacerCtrl()
        {
            InitializeComponent();

            FPGA.TileSelectionManager.Instance.Add(this);

            this.m_netlistContainerSelector.Label = "Which netlist container to update";
            this.m_libElementSelector.Label = "Which element to place";
        }

        private void m_btnPlaceMacro_Click(object sender, EventArgs e)
        {
            if (!this.IsPlacementValidForSingleInstantiation(false))
            {
                return;
            }

            String libraryElementName = null;
            Tile anchor = null;
            String errorMessage = null;
            String instanceName = null;
            String netlistContainerName = null;

            bool paramsValid = this.FindParametersForSingleInstantiation(out libraryElementName, out anchor, out instanceName, out netlistContainerName, out errorMessage);

            if (!paramsValid)
            {
                MessageBox.Show(errorMessage, "Error", MessageBoxButtons.OK);
                return;
            }

            AddSingleInstantiationByTile addInst = new AddSingleInstantiationByTile();
            addInst.AutoClearModuleSlot = this.m_chkBoxAutoclearSingle.Checked;
            addInst.AnchorLocation = anchor.Location;
            addInst.InstanceName = instanceName;
            addInst.LibraryElementName = libraryElementName;
            addInst.NetlistContainerName = netlistContainerName;
            Commands.CommandExecuter.Instance.Execute(addInst);
        }

        private void m_btnPlaceBySlice_Click(object sender, EventArgs e)
        {
            String libraryElementName = null;
            Slice slice = null;
            String instanceName = null;
            String errorMessage = null;
            String netlistContainerName = null;

            bool paramsValid = this.FindParametersForSingleInstantiation(out libraryElementName, out slice, out instanceName, out netlistContainerName, out errorMessage);

            if (!paramsValid)
            {
                MessageBox.Show(errorMessage, "Error", MessageBoxButtons.OK);
                return;
            }

            AddSingleInstantiationBySlice addInst = new AddSingleInstantiationBySlice();
            addInst.AutoClearModuleSlot = this.m_chkBoxAutoclearSingle.Checked;
            addInst.InstanceName = instanceName;
            addInst.LibraryElementName = libraryElementName;
            addInst.NetlistContainerName = netlistContainerName;
            addInst.SliceName = slice.SliceName;

            Commands.CommandExecuter.Instance.Execute(addInst);
        }

        private void m_btnCheck_Click(object sender, EventArgs e)
        {
            String libraryElementName = this.m_libElementSelector.SelectedLibraryElementName;
            String instanceName = this.m_txtSingleInstanceName.Text;
            if (instanceName.Length == 0 && libraryElementName != null)
            {
                this.m_txtSingleInstanceName.Text = LibraryElementInstanceManager.Instance.ProposeInstanceName(libraryElementName);
            }
            else if (LibraryElementInstanceManager.Instance.HasInstanceName(instanceName) && libraryElementName != null)
            {
                this.m_txtSingleInstanceName.Text = LibraryElementInstanceManager.Instance.ProposeInstanceName(libraryElementName);
            }
            this.IsPlacementValidForSingleInstantiation(true);
        }

        private void m_btnCheckPlacementForSelection_Click(object sender, EventArgs e)
        {
            String libraryElementName = this.m_libElementSelector.SelectedLibraryElementName;
            String instanceName = this.m_txtMultiInstanceName.Text;
            if (instanceName.Length == 0 && libraryElementName != null)
            {
                this.m_txtMultiInstanceName.Text = libraryElementName;
            }
            else if (LibraryElementInstanceManager.Instance.HasInstanceName(instanceName) && libraryElementName != null)
            {
                this.m_txtMultiInstanceName.Text = libraryElementName;
            }
            this.IsPlacementValidForMultiInstantiation();
        }

        private void m_btnPlaceInSelection_Click(object sender, EventArgs e)
        {
            String macroName = null;
            String errorMessage = null;
            String instanceName = null;
            String netlistContainerName = null;

            bool paramsValid = this.FindParametersForMultiInstantiation(out macroName, out instanceName, out netlistContainerName, out errorMessage);

            if (!paramsValid)
            {
                MessageBox.Show(errorMessage, "Error", MessageBoxButtons.OK);
                return;
            }

            LibraryElement el = Library.Instance.GetElement(macroName);

            AddInstantiationInSelectedTiles cmd = new AddInstantiationInSelectedTiles();
            cmd.AutoClearModuleSlot = this.m_chkBoxAutoclearMulti.Checked;
            cmd.Horizontal = this.m_drpDownHorizontalOrder.Text;
            cmd.InstanceName = instanceName;
            cmd.LibraryElementName = macroName;
            cmd.Mode = this.m_drpDownMode.Text;
            cmd.NetlistContainerName = netlistContainerName;
            cmd.SliceNumber = el.ResourceShape.Anchor.AnchorSliceNumber;
            cmd.Vertical = this.m_drpDownVerticallOrder.Text;
            
            Commands.CommandExecuter.Instance.Execute(cmd);
        }

        private bool FindParametersForSingleInstantiation(out String libraryElementName, out Slice slice, out String instanceName, out String netlistContainerName, out String errorMessage)
        {
            libraryElementName = this.m_libElementSelector.SelectedLibraryElementName;
            slice = FPGA.FPGA.Instance.GetSlice(this.m_txtAnchorSlice.Text);
            instanceName = this.m_txtSingleInstanceName.Text;
            netlistContainerName = "";
            errorMessage = "";

            if(String.IsNullOrEmpty(this.m_netlistContainerSelector.SelectedNetlistContainerName))
            {
                errorMessage = "No netlist container selected";
                return false;
            }
            netlistContainerName = this.m_netlistContainerSelector.SelectedNetlistContainerName;

            if (libraryElementName == null)
            {
                errorMessage = "No library element selected";
                return false;
            }

            if (!Library.Instance.Contains(libraryElementName))
            {
                errorMessage = "Library element " + libraryElementName + " not found";
                return false;
            }

            if (slice == null)
            {
                errorMessage = "Slice " + this.m_txtAnchorSlice.Text + " not found";
                return false;
            }

            if (LibraryElementInstanceManager.Instance.HasInstanceName(instanceName))
            {
                errorMessage = "Instance name " + instanceName + " already in use";
                return false;
            }

            if (instanceName.Length == 0)
            {
                errorMessage = "No instance name given";
                return false;
            }

            return true;
        }

        private bool FindParametersForSingleInstantiation(out String libraryElementName, out Tile anchor, out String instanceName, out String netlistContainerName, out String errorMessage)
        {
            libraryElementName = this.m_libElementSelector.SelectedLibraryElementName;
            anchor = null;
            errorMessage = "";
            instanceName = this.m_txtSingleInstanceName.Text;
            netlistContainerName = null;

            if (String.IsNullOrEmpty(this.m_netlistContainerSelector.SelectedNetlistContainerName))
            {
                errorMessage = "No netlist container selected";
                return false;
            }
            netlistContainerName = this.m_netlistContainerSelector.SelectedNetlistContainerName;

            if (libraryElementName == null)
            {
                errorMessage = "No library element selected";
                return false;
            }

            if (!Library.Instance.Contains(libraryElementName))
            {
                errorMessage = "Library element " + libraryElementName + " not found";
                return false;
            }

            // no multiple selection
            if (!FPGA.FPGA.Instance.Contains(this.m_txtAnchorLocation.Text))
            {
                errorMessage = "Anchor " + this.m_txtAnchorLocation.Text + " not found";
                return false;
            }

            // no multiple selection
            if (!IdentifierManager.Instance.IsMatch(this.m_txtAnchorLocation.Text, IdentifierManager.RegexTypes.CLBRegex))
            {
                errorMessage = "Select a CLB for placement";
                return false;
            }

            if (instanceName.Length == 0)
            {
                errorMessage = "No instance name given";
                return false;
            }

            if (LibraryElementInstanceManager.Instance.HasInstanceName(instanceName))
            {
                errorMessage = "Instance name " + instanceName + " already in use";
                return false;
            }

            Tile clickedAnchor = FPGA.FPGA.Instance.GetTile(this.m_txtAnchorLocation.Text);
            //clickedAnchor = FPGA.FPGA.Instance.GetTile("BRAMSITE2_X3Y28");
            LibraryElement libElement = Library.Instance.GetElement(libraryElementName);
            anchor = clickedAnchor;

            return true;
        }

        private bool FindParametersForMultiInstantiation(out String libraryElementName, out String instanceName, out String netlistContainerName, out String errorMessage)
        {
            libraryElementName = this.m_libElementSelector.SelectedLibraryElementName;
            errorMessage = "";
            instanceName = this.m_txtMultiInstanceName.Text;
            netlistContainerName = null;

            if (String.IsNullOrEmpty(this.m_netlistContainerSelector.SelectedNetlistContainerName))
            {
                errorMessage = "No netlist container selected";
                return false;
            }
            netlistContainerName = this.m_netlistContainerSelector.SelectedNetlistContainerName;
            if (libraryElementName == null)
            {
                errorMessage = "No Macro selected";
                return false;
            }

            if (!Library.Instance.Contains(libraryElementName))
            {
                errorMessage = "Macro " + libraryElementName + " not found";
                return false;
            }

            if (instanceName.Length == 0)
            {
                errorMessage = "No instance name given";
                return false;
            }

            if (LibraryElementInstanceManager.Instance.HasInstanceName(instanceName))
            {
                errorMessage = "Instance name " + instanceName + " already in use";
                return false;
            }

            return true;
        }

        private bool IsPlacementValidForMultiInstantiation()
        {
            String libElementName = null;
            String errorMessage = null;
            String instanceName = null;
            String netlistContainerName = null;

            bool paramsValid = this.FindParametersForMultiInstantiation(out libElementName, out instanceName, out netlistContainerName, out errorMessage);

            if (!paramsValid)
            {
                MessageBox.Show(errorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            foreach (Tile clb in FPGA.TileSelectionManager.Instance.GetSelectedTiles().Where(t => IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.CLBRegex)))
            {
                StringBuilder errorList = null;
                LibraryElement libElement = Library.Instance.GetElement(libElementName);
                bool placementOk = DesignRuleChecker.CheckLibraryElementPlacement(clb, libElement, out errorList);
                if (!placementOk)
                {
                    MessageBox.Show("Library element " + libElementName + " can not be placed at " + clb.Location + ": " + errorList.ToString(), "Placement failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
           
            // placement is ok
            int clbsInSelection = FPGA.TileSelectionManager.Instance.GetSelectedTiles().Count(t => IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.CLBRegex));
            MessageBox.Show("Library element " + libElementName + " can be placed at the selected " + clbsInSelection + " position(s)", "Placement OK", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);          
            return true;
        }

        private bool IsPlacementValidForSingleInstantiation(bool showMessageBoxForValidPlacement)
        {
            String libraryElementName = null;
            Tile anchor = null;
            String errorMessage = null;
            String instanceName = null;
            String netlistContainerName = null;
            bool paramsValid = this.FindParametersForSingleInstantiation(out libraryElementName, out anchor, out instanceName, out netlistContainerName, out errorMessage);

            if (!paramsValid)
            {
                MessageBox.Show(errorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            StringBuilder errorList = null;
            bool placementOk = DesignRuleChecker.CheckLibraryElementPlacement(anchor, Library.Instance.GetElement(libraryElementName), out errorList);

            if (placementOk)
            {
                if (showMessageBoxForValidPlacement)
                {
                    MessageBox.Show("Library element " + libraryElementName + " can be placed at " + anchor.Location, "Placement OK", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            else
            {
                MessageBox.Show("Library element " + libraryElementName + " can not be placed at " + anchor.Location + ": " + errorList.ToString(), "Placement failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return placementOk;
        }

        public void Notify(Object obj)
        {
            this.UpdateAnchorTextBoxes();
        }

        private void UpdateAnchorTextBoxes()
        {
            if (FPGA.TileSelectionManager.Instance.NumberOfSelectedTiles != 2)
            {
                // "Select exactly one tile (one pair of CLB and Interconnect Tile)", "Error");
                return;
            }

            Tile clb =
                FPGA.TileSelectionManager.Instance.GetSelectedTiles().FirstOrDefault(t => IdentifierManager.Instance.IsMatch(t.Location, IdentifierManager.RegexTypes.CLBRegex));

            if (clb == null)
            {
                // no clb selected
                return;
            }

            this.m_txtAnchorLocation.Text = clb.Location;

            String libraryElementName = this.m_libElementSelector.SelectedLibraryElementName;
            if (String.IsNullOrEmpty(libraryElementName))
            {
                // no macro selected
                this.m_txtAnchorSlice.Text = clb.Slices[0].SliceName;
                return;
            }

            LibraryElement el = Objects.Library.Instance.GetElement(libraryElementName);
            /*
            AnchorInfo clbAnchor = el.GetAnchorInfo(IdentifierManager.RegexTypes.CLBRegex);
            // slice out of range
            int sliceNumber = clb.Slices.Count < clbAnchor.AnchorSliceNumber ? 0 : clbAnchor.AnchorSliceNumber;
            */
            this.m_txtAnchorSlice.Text = clb.Slices[0].SliceName;
        }

        private void ParentFormClosed(object sender, FormClosedEventArgs e)
        {
            FPGA.TileSelectionManager.Instance.Remove(this);
        }

        private void MacroPlacerCtrl_Load(object sender, EventArgs e)
        {
            if (this.ParentForm != null)
            {
                this.ParentForm.FormClosed += new FormClosedEventHandler(this.ParentFormClosed);
                this.UpdateAnchorTextBoxes();
            }
        }

        private void m_btnPrintPossiblePlacements_Click(object sender, EventArgs e)
        {
            String libraryElementName = this.m_libElementSelector.SelectedLibraryElementName;
            if (String.IsNullOrEmpty(libraryElementName))
            {
                MessageBox.Show("No library element selected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (!Objects.Library.Instance.Contains(libraryElementName))
            {
                MessageBox.Show("Library element " + libraryElementName + " not found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (FPGA.TileSelectionManager.Instance.NumberOfSelectedTiles == 0)
            {
                MessageBox.Show("No tiles selected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            PrintPossibleLibraryElementPlacementsInSelection printCmd = new PrintPossibleLibraryElementPlacementsInSelection();
            printCmd.LibraryElementName = libraryElementName;
            Commands.CommandExecuter.Instance.Execute(printCmd);
        }
    }
}

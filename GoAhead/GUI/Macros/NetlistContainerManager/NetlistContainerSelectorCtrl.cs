﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GoAhead.Objects;

namespace GoAhead.GUI.Macros.NetlistContainerManager
{
    public partial class NetlistContainerSelectorCtrl : UserControl
    {
        public NetlistContainerSelectorCtrl()
        {
            InitializeComponent();

            BindingSource bsrc = new BindingSource();
            bsrc.DataSource = Objects.NetlistContainerManager.Instance.NetlistContainerBindingList;

            this.m_cmbBoxNetlistContainerNames.DisplayMember = "Name";
            this.m_cmbBoxNetlistContainerNames.ValueMember = "Name";
            this.m_cmbBoxNetlistContainerNames.DataSource = bsrc;

            this.m_cmbBoxNetlistContainerNames.SelectedItem = Objects.NetlistContainerManager.Instance.NetlistContainerBindingList.Count == 0 ? null : Objects.NetlistContainerManager.Instance.NetlistContainerBindingList[0];
        }

        public String SelectedNetlistContainerName
        {
            get
            {
                return this.m_cmbBoxNetlistContainerNames.SelectedItem == null ? "" : ((NetlistContainer)this.m_cmbBoxNetlistContainerNames.SelectedItem).Name;
            }
        }

        /// <summary>
        /// Gets or sets the label text
        /// </summary>
        public String Label
        {
            get { return this.m_lblNetlistContainer.Text; }
            set { this.m_lblNetlistContainer.Text = value;  }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using GoAhead.Objects;
using GoAhead.Commands;
using GoAhead.Settings;
using GoAhead.Commands.Data;

namespace GoAhead.GUI
{
    public abstract partial class NetlistContainerForm : Form
    {
        protected NetlistContainerForm()
        {
            InitializeComponent();

            // add all macros and check the current 
            foreach (Objects.NetlistContainer next in NetlistContainerManager.Instance.NetlistContainer)
            {
                this.m_chkListMacros.Items.Add(next.Name, false);
            }

            // pre select if there is only one item
            if (this.m_chkListMacros.Items.Count == 1)
            {
                this.m_chkListMacros.SetItemChecked(0, true);
            }

            this.m_chkXDLIncludePorts.Checked = Settings.StoredPreferences.Instance.XDL_IncludePortStatements;
            this.m_chkRunFEScript.Checked = Settings.StoredPreferences.Instance.XDL_RunFEScript;

            Settings.StoredPreferences.Instance.GUISettings.Open(this);

            this.m_fileSelectionCtrl.RestorePreviousSelection();
        }

        private void m_btnGenerate_Click(object sender, EventArgs e)
        {
            List<String> netlistContainerNames = new List<string>();
            foreach (Object obj in this.m_chkListMacros.CheckedItems)
            {
                netlistContainerNames.Add(obj.ToString());
            }

            if (this.m_checkFileExistence && !System.IO.File.Exists(this.m_fileSelectionCtrl.FileName))
            {
                MessageBox.Show("File " + this.m_fileSelectionCtrl.FileName + " does not exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (this.DoGeneration(this.m_fileSelectionCtrl.FileName, netlistContainerNames))
            {
                // save settings
                Settings.StoredPreferences.Instance.XDL_IncludePortStatements = this.m_chkXDLIncludePorts.Checked;
                Settings.StoredPreferences.Instance.XDL_RunFEScript = this.m_chkRunFEScript.Checked ;

                this.Close();
            }
        }
        
        public FileSelectionCtrl FileSelectionCtrl
        {
            get { return this.m_fileSelectionCtrl; }
            set { this.m_fileSelectionCtrl = value; }
        }

        protected abstract bool DoGeneration(String fileName, List<String> selectedMacros);

        protected String m_title;
        protected String m_filter;
        protected bool m_checkFileExistence = true;

        private void MacroSelectionForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Settings.StoredPreferences.Instance.GUISettings.Close(this);
        }
    }

    public class LaunchFEGUI : NetlistContainerForm
    {
        public LaunchFEGUI()
            :base()
        {
            this.Text = "Launch Macro in FPGA-Editor";
            this.m_btnGenerate.Text = "&Launch";
            this.m_checkFileExistence = false;
            this.FileSelectionCtrl.Visible = false;
        }

        protected override bool DoGeneration(string fileName, List<string> selectedMacros)
        {
            CommandExecuter.Instance.Execute(
                new LanchNetlistInFE(selectedMacros, "", this.m_chkXDLIncludePorts.Checked, this.m_chkXDLIncludeDummyNets.Checked, this.m_chkRunFEScript.Checked, true));

            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GoAhead.Commands.VHDL;
using GoAhead.Commands.UCF;

namespace GoAhead.GUI.Macros.VHDL
{
    public partial class PrintVHDLCtrl : UserControl
    {
        public PrintVHDLCtrl()
        {
            InitializeComponent();

            this.m_fileSelVHDLWrapper.RestorePreviousSelection();
            this.m_fileSelVHDLWrapperInstantiation.RestorePreviousSelection();
            this.m_fileSelUCF.RestorePreviousSelection();
        }

        private void m_btnPrintWrapper_Click(object sender, EventArgs e)
        {
            PrintVHDLWrapper printCmd = new PrintVHDLWrapper();
            printCmd.Append = this.m_fileSelVHDLWrapper.Append;
            printCmd.CreateBackupFile = true;
            printCmd.EntityName = this.m_txtEntityName.Text;
            printCmd.FileName = this.m_fileSelVHDLWrapper.FileName;
            printCmd.InstantiationFilter = this.m_libElInstSelector.InstanceFilter;

            Commands.CommandExecuter.Instance.Execute(printCmd);
        }

        private void m_btnPrintWrapperInstantiation_Click(object sender, EventArgs e)
        {
            PrintVHDLWrapperInstantiation printCmd = new PrintVHDLWrapperInstantiation();
            printCmd.Append = this.m_fileSelVHDLWrapperInstantiation.Append;
            printCmd.CreateBackupFile = true;
            printCmd.EntityName = this.m_txtEntityName.Text;
            printCmd.FileName = this.m_fileSelVHDLWrapperInstantiation.FileName;
            printCmd.InstantiationFilter = this.m_libElInstSelector.InstanceFilter;

            Commands.CommandExecuter.Instance.Execute(printCmd);
        }

        private void m_btnPrintUCF_Click(object sender, EventArgs e)
        {
            PrintLocationConstraints printCmd = new PrintLocationConstraints();
            printCmd.InstantiationFilter = this.m_libElInstSelector.InstanceFilter;
            printCmd.HierarchyPrefix = this.m_txtHierarchyPrefix.Text;
            printCmd.FileName = this.m_fileSelUCF.FileName;
            printCmd.Append = this.m_fileSelUCF.Append;
            printCmd.CreateBackupFile = true;

            Commands.CommandExecuter.Instance.Execute(printCmd);
        }
    }
}

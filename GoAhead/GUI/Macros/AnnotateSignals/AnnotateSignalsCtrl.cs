﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using GoAhead.Commands.UCF;
using GoAhead.Commands.VHDL;
using GoAhead.Code.XDL;
using GoAhead.Commands.LibraryElementInstantiationManager;
using GoAhead.Objects;

namespace GoAhead.GUI.LibraryElementInstantiation
{
    public partial class LibraryElementInstantiationManagerCtrl : UserControl
    {
        public LibraryElementInstantiationManagerCtrl()
        {
            InitializeComponent();

            this.m_libElInstSelector.SelectionChanged += this.m_libElInstSelector_SelectionChanged;
        }
        
        private void m_libElInstSelector_SelectionChanged(object sender, EventArgs e)
        {
            List<LibElemInst> instances = this.m_libElInstSelector.SelectedInstances;

            int count = (from i in instances select i.LibraryElementName).Distinct().Count();
            if (count > 1)
            {
                return;
                //MessageBox.Show("Can only handle one library element kind per selection.", "Multiple library elements used", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            // collect all ports
            List<String> ports = new List<String>();
            instances.ForEach(i => ports.AddRange(from p in i.GetLibraryElement().Containter.Ports select Regex.Replace(p.ExternalName, @"\d", "")));

            List<String> prefices = new List<String>();
            prefices.AddRange(ports.Distinct());

            this.m_grdViewMapping.Rows.Clear();
            foreach (String p in prefices)
            {
                string[] row = new string[] { p, "vhdl_signal", "external" };
                this.m_grdViewMapping.Rows.Add(row);
            }
        }

        private void m_btnAnnotateSignals_Click(object sender, EventArgs e)
        {
            ClearSignalAnnotations clearCmd = new ClearSignalAnnotations();
            clearCmd.InstantiationFilter = this.m_libElInstSelector.InstanceFilter;
            Commands.CommandExecuter.Instance.Execute(clearCmd);

            List<String> portMapping = new List<String>();
            foreach (DataGridViewRow row in this.m_grdViewMapping.Rows)
            {
                String port = row.Cells[0].FormattedValue.ToString();
                String signal = row.Cells[1].FormattedValue.ToString();
                String mapping = row.Cells[2].FormattedValue.ToString();
                if (port.Length > 0 && signal.Length > 0 && mapping.Length > 0)
                {
                    portMapping.Add(port + ":" + signal + ":" + mapping);
                }
            }
            
            AnnotateSignalNames annoteCmd = new AnnotateSignalNames();
            annoteCmd.InstantiationFilter = this.m_libElInstSelector.InstanceFilter;
            annoteCmd.PortMapping = portMapping;

            Commands.CommandExecuter.Instance.Execute(annoteCmd);
        }

        private void LibraryElementInstantiationManagerCtrl_Resize(object sender, EventArgs e)
        {
            this.m_grpAnoteSignals.Width = this.Width - 10;
            this.m_btnAnnotateSignals.Left = (this.m_grpAnoteSignals.Width - this.m_btnAnnotateSignals.Width) - 10;
        }
    }
}

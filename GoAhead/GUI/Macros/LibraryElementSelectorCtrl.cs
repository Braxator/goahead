﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GoAhead.Objects;

namespace GoAhead.GUI.Macros.LibraryManager
{
    public partial class LibraryElementSelectorCtrl : UserControl
    {
        public LibraryElementSelectorCtrl()
        {
            InitializeComponent();

            BindingSource bsrc = new BindingSource();
            bsrc.DataSource = Objects.Library.Instance.LibraryElements;
            this.m_cmbBoxLibraryElementNames.DisplayMember = "Name";
            this.m_cmbBoxLibraryElementNames.ValueMember = "Name";
            this.m_cmbBoxLibraryElementNames.DataSource = bsrc;

            this.m_cmbBoxLibraryElementNames.SelectedItem = Objects.Library.Instance.LibraryElements.Count == 0 ? null : Objects.Library.Instance.LibraryElements[0].Name;
        }

        public String SelectedLibraryElementName
        {
            get
            {
                return this.m_cmbBoxLibraryElementNames.SelectedItem == null ? "" : ((LibraryElement)this.m_cmbBoxLibraryElementNames.SelectedItem).Name;
            }
        }

        /// <summary>
        /// Gets or sets the label text
        /// </summary>
        public String Label
        {
            get { return this.m_lblElements.Text; }
            set { this.m_lblElements.Text = value;  }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GoAhead.Commands.BlockingShared;
using GoAhead.Commands.NetlistContainerGeneration;

namespace GoAhead.GUI.Blocker
{
    public partial class BlockerCtrl : UserControl
    {
        public BlockerCtrl()
        {
            InitializeComponent();
        }

        private void m_btnBlock_Click(object sender, EventArgs e)
        {
            BlockSelection blockCmd = new BlockSelection();
            blockCmd.BlockWithEndPips = this.m_chkUseEndPips.Checked;
            blockCmd.NetlistContainerName = this.m_netlistContainerSelector.SelectedNetlistContainerName;
            blockCmd.Prefix = this.m_txtPrefix.Text;
            blockCmd.PrintUnblockedPorts = this.m_chkPrintUnblockedPorts.Checked;
            blockCmd.SliceNumber = (int) this.m_numDrpSliceNumber.Value;
            blockCmd.PrintProgress = true;
            Commands.CommandExecuter.Instance.Execute(blockCmd);

            if (this.CloseMeAfterBlocking != null)
            {
                this.CloseMeAfterBlocking.Close();
            }
        }

        public Form CloseMeAfterBlocking = null;
        
    }
}

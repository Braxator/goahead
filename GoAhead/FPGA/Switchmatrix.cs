using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoAhead.FPGA
{
    [Serializable]
    public class RawSwitchMatrix
    {
        public RawSwitchMatrix(SwitchMatrix switchMatrix)
        {
            foreach (Port driver in switchMatrix.GetAllDrivers())
            {
                this.InPorts.Add(driver.Name);
                List<String> drivenPorts = new List<String>();
                foreach (Port drivenPort in switchMatrix.GetDrivenPorts(driver))
                {
                    drivenPorts.Add(drivenPort.Name);
                }
                this.OutPorts.Add(drivenPorts);
            }

            this.HashCode = switchMatrix.HashCode;
        }

        /// <summary>
        /// The hashcode of the switch matrix. This valud should not be recalculated as this may result in different values on differnt machines
        /// </summary>
        public int HashCode = 0;

        public List<String> InPorts = new List<String>();
        public List<List<String>> OutPorts = new List<List<String>>();
    }

    [Serializable]
    public class SwitchMatrix : FPGAElement
    {
        public SwitchMatrix()
        {
        }

        public SwitchMatrix(RawSwitchMatrix rawSwitchMatrix)
        {
            for (int i = 0; i < rawSwitchMatrix.InPorts.Count; i++)
            {
                Port from = new Port(rawSwitchMatrix.InPorts[i]);
                foreach (String to in rawSwitchMatrix.OutPorts[i])
                {
                    this.Add(from, new Port(to));
                }
            }

            this.HashCode = rawSwitchMatrix.HashCode;
        }

        public void Add(Port from, Port to)
        {
            if (!this.m_in2out.ContainsKey(from))
            {
                this.m_in2out.Add(from, new Dictionary<Port, bool>());
                this.m_in2out[from].Add(to, false);
            }
            else if (!this.m_in2out[from].ContainsKey(to))
            {
                this.m_in2out[from].Add(to, false);
            }

            if (!this.m_allPortsFrom.ContainsKey(from.NameKey))
            {
                this.m_allPortsFrom.Add(from.NameKey, from);
            }
            if (!this.m_allPortsTo.ContainsKey(to.NameKey))
            {
                this.m_allPortsTo.Add(to.NameKey, to);
            }
        }

        public IEnumerable<Port> GetDrivenPorts()
        {
            Dictionary<uint, Port> returnedPorts = new Dictionary<uint, Port>();
            foreach (Port driver in this.m_in2out.Keys)
            {
                foreach (KeyValuePair<Port, bool> drivenPort in this.m_in2out[driver])
                {
                    if (!returnedPorts.ContainsKey(drivenPort.Key.NameKey))
                    {
                        returnedPorts.Add(drivenPort.Key.NameKey, drivenPort.Key);
                        yield return drivenPort.Key;
                    }
                }
            }
        }

        public IEnumerable<Port> GetDrivenPorts(Port driver)
        {
            if (driver.Name.Equals("LOGIC_OUTS_W1"))
            {
            }

            if (this.m_in2out.ContainsKey(driver)) // example driver; LH12
            {
                // return all the possible directions from the driver port LH12
                foreach (KeyValuePair<Port, bool> drivenPort in this.m_in2out[driver])
                {
                    yield return drivenPort.Key;
                }
            }
        }

        public Port GetFirstDrivenPort(Port driver)
        {
            if (this.m_in2out.ContainsKey(driver))
            {
                foreach (KeyValuePair<Port, bool> drivenPort in this.m_in2out[driver])
                {
                    return drivenPort.Key;
                }
            }
            throw new ArgumentException(driver.Name + " drives no port");
        }

        /// <summary>
        /// Get all ports that drive other pins
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Port> GetAllDrivers()
        {
            foreach (KeyValuePair<Port, Dictionary<Port, bool>> incoming in m_in2out)
            {
                yield return incoming.Key;
            }
        }

        /// <summary>
        /// Get all ports that drive other pins and match the filter with the lowest connectivity first
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Port> GetAllDriversSortedAscByConnectivity(Predicate<Port> filter)
        {
            // m_sortedPorts might be null after deserialization
            if (this.m_sortedPorts == null)
            {
                this.m_sortedPorts = new Dictionary<Port, int>();
            }

            if (this.m_sortedPorts.Count == 0)
            {
                // m_in2out contains all signals coming in to the switch matrix and all the possible routing options to out
                foreach (KeyValuePair<Port, Dictionary<Port, bool>> incoming in m_in2out)
                {
                    // predicate is applied later as the port may be blocked meanwhile
                    this.m_sortedPorts.Add(incoming.Key, incoming.Value.Count);
                }
            }

            // at this point, m_sortedPorts (Dictionary<Port, int>) has all incoming ports and the number of possible routing options to out 

            // here, sort the number of possible routing options to out from low to high and return the corresponding input port 
            foreach (KeyValuePair<Port, int> tupel in this.m_sortedPorts.OrderBy(key => key.Value))
            {
                if (filter(tupel.Key))
                {
                    yield return tupel.Key;
                }
            }
        }

        public void ClearBlockingPortList()
        {
            this.m_sortedPorts = null;
        }

        public int ArcCount
        {
            get { return this.m_in2out.Sum(d => d.Value.Count); }
        }

        public IEnumerable<Tuple<Port, Port>> GetAllArcs()
        {
            foreach (KeyValuePair<Port, Dictionary<Port, bool>> incoming in this.m_in2out)
            {
                foreach (Port p in incoming.Value.Keys)
                {
                    Tuple<Port, Port> result = new Tuple<Port, Port>(incoming.Key, p);
                    yield return result;
                }
            }
        }

        public Tuple<Port, Port> GetFirstArcOrDefault(Predicate<Port> outerFilter, Predicate<Tuple<Port, Port>> innerFilter)
        {
            foreach (KeyValuePair<Port, Dictionary<Port, bool>> incoming in m_in2out)
            {
                if (outerFilter(incoming.Key))
                {
                    foreach (Port p in incoming.Value.Keys)
                    {
                        Tuple<Port, Port> result = new Tuple<Port, Port>(incoming.Key, p);
                        if (innerFilter(result))
                        {
                            return result;
                        }
                    }
                }
            }
            return new Tuple<Port, Port>(null, null);
        }

        public IEnumerable<Port> Ports
        {
            get
            {
                Dictionary<Port, bool> returnedPorts = new Dictionary<Port, bool>();
                foreach (KeyValuePair<Port, Dictionary<Port, bool>> incoming in this.m_in2out)
                {
                    if (!returnedPorts.ContainsKey(incoming.Key))
                    {
                        returnedPorts.Add(incoming.Key, false);
                        yield return incoming.Key;
                    }

                    foreach (KeyValuePair<Port, bool> outgoing in incoming.Value)
                    {
                        if (!returnedPorts.ContainsKey(outgoing.Key))
                        {
                            returnedPorts.Add(outgoing.Key, false);
                            yield return outgoing.Key;
                        }
                    }
                }
            }
        }

        public bool Contains(Port from, Port to)
        {
            if (!this.m_in2out.ContainsKey(from))
            {
                return false;
            }

            if (this.m_in2out[from].ContainsKey(to))
            {
                return true;
            }
            else
            {
                // ROUTETHROUGH
                foreach (Port key in this.m_in2out[from].Keys)
                {
                    if (key.Name.Contains("ROUTETHROUGH") && key.Name.StartsWith(to.Name))
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public bool Contains(String from, String to)
        {
            return this.Contains(new Port(from), new Port(to));
        }

        public bool Contains(String portName)
        {
            return this.Contains(new Port(portName));
        }

        public bool Contains(Port port)
        {
            return
                this.m_allPortsFrom.ContainsKey(port.NameKey) ||
                this.m_allPortsTo.ContainsKey(port.NameKey);
        }

        public bool ContainsLeft(Port port)
        {
            return this.m_allPortsFrom.ContainsKey(port.NameKey);
        }

        public bool ContainsRight(Port port)
        {
            return this.m_allPortsTo.ContainsKey(port.NameKey);
        }

        /// <summary>
        /// Sorted string representation to allow compares
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            StringBuilder buffer = new StringBuilder();

            foreach (KeyValuePair<Port, Dictionary<Port, bool>> incoming in m_in2out.OrderBy(t => t.Key.Name))
            {
                foreach (KeyValuePair<Port, bool> outgoing in incoming.Value.OrderBy(p => p.Key.Name))
                {
                    buffer.AppendLine(incoming.Key + "\t" + outgoing.Key);
                }
            }

            return buffer.ToString();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is SwitchMatrix))
            {
                return false;
            }

            SwitchMatrix other = (SwitchMatrix)obj;

            // compare all ports
            if (other.m_allPortsFrom.Count != this.m_allPortsFrom.Count)
            {
                return false;
            }
            if (other.m_allPortsTo.Count != this.m_allPortsTo.Count)
            {
                return false;
            }

            foreach (Port otherPort in other.Ports)
            {
                if (!this.Contains(otherPort))
                {
                    return false;
                }
            }
            return true;

            //return this.ToString().Equals(obj.ToString());
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        public void RemoveAllDrivers(String drivenPortName)
        {
            Port drivenPort = new Port(drivenPortName);
            List<Port> driversToDelete = new List<Port>();
            foreach (KeyValuePair<Port, Dictionary<Port, bool>> tupel in this.m_in2out)
            {
                if (tupel.Value.ContainsKey(drivenPort))
                {
                    tupel.Value.Remove(drivenPort);
                }
                if (tupel.Value.Count == 0)
                {
                    driversToDelete.Add(tupel.Key);
                }
            }
            foreach (Port p in driversToDelete)
            {
                this.m_in2out.Remove(p);
            }
        }

        public int Outputs
        {
            get { return this.m_allPortsTo.Count; }
        }

        public int Inputs
        {
            get { return this.m_allPortsFrom.Count; }
        }

        /// <summary>
        /// The hashcode must be calculated after reading in the XDl description of this SwitchMatrix. After this, it may not be changed again as the HasCode is used as a key
        /// and on different machines the calculated hash codes may differ!
        /// </summary>
        public int HashCode { get; set; }

        private Dictionary<Port, Dictionary<Port, bool>> m_in2out = new Dictionary<Port, Dictionary<Port, bool>>();
        private Dictionary<uint, Port> m_allPortsFrom = new Dictionary<uint, Port>();
        private Dictionary<uint, Port> m_allPortsTo = new Dictionary<uint, Port>();

        private Dictionary<Port, int> m_sortedPorts;
    }
}
using System;

namespace GoAhead.FPGA
{
    [Serializable]
    public class TileKey
    {
        public TileKey(int x, int y)
        {
            this.m_x = x;
            this.m_y = y;
        }

        public int X
        {
            get { return m_x; }
        }

        public int Y
        {
            get { return m_y; }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is TileKey))
            {
                return false;
            }

            TileKey other = (TileKey)obj;
            return this.X == other.X && this.Y == other.Y;
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        public override String ToString()
        {
            return "X" + this.m_x + "Y" + this.m_y;
        }

        private readonly int m_x;
        private readonly int m_y;
    }
}
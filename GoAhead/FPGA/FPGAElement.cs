using System;

namespace GoAhead.FPGA
{
    /// <summary>
    /// The base class for all Elements on FPGAs
    /// </summary>
    [Serializable()]
    public abstract class FPGAElement : IComparable
    {
        /// <summary>
        /// All FPGAElements can be distiguished by their String representation
        /// </summary>
        public virtual int CompareTo(object obj)
        {
            return String.CompareOrdinal(this.ToString(), obj.ToString());
        }

        public override bool Equals(object obj)
        {
            return this.CompareTo(obj) == 0;
        }

        public override int GetHashCode()
        {
            if (!this.m_hashCodeValid)
            {
                this.m_hashCode = this.ToString().GetHashCode();
                this.m_hashCodeValid = true;
            }
            return m_hashCode;
        }

        public abstract override String ToString();

        private int m_hashCode = 0;
        private bool m_hashCodeValid = false;
    }
}
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace GoAhead.FPGA
{
    [Serializable]
    public partial class FPGA
    {
        private FPGA()
        {
        }

        public void Add(Tile tile)
        {
            this.m_tiles.Add(tile);

            if (tile.TileKey.X > this.MaxX)
            {
                this.MaxX = tile.TileKey.X;
            }
            if (tile.TileKey.Y > this.MaxY)
            {
                this.MaxY = tile.TileKey.Y;
            }

            if (!this.m_location2TileKey.ContainsKey(tile.Location))
            {
                this.m_location2TileKey.Add(tile.Location, tile.TileKey);
            }
        }

        public void Add(int hashCode, WireList wires)
        {
            this.m_wires.Add(hashCode, wires);
        }

        public void Add(int hashcode, SwitchMatrix matrix)
        {
            this.m_matrices.Add(hashcode, matrix);
        }

        public void Add(InPortOutPortMapping portMapping)
        {
            this.m_inOutPortMappings.Add(portMapping.GetHashCode(), portMapping);
        }

        public bool Contains(String tileIdentifier)
        {
            return this.m_location2TileKey.ContainsKey(tileIdentifier);
        }

        public bool Contains(TileKey key)
        {
            return this.m_tiles.Contains(key);
        }

        public bool Contains(int x, int y)
        {
            return this.m_tiles.Contains(x, y);
        }

        public bool ContainsSlice(String sliceName)
        {
            return this.GetSlice(sliceName) != null;
        }

        public bool Contains(SwitchMatrix matrix)
        {
            return this.m_matrices.ContainsKey(matrix.HashCode);
        }

        public bool ContainsWireList(int hashCode)
        {
            return this.m_wires.ContainsKey(hashCode);
        }

        public bool ContainsSwitchMatrix(int hashCode)
        {
            return this.m_matrices.ContainsKey(hashCode);
        }

        public bool Contains(WireList wires)
        {
            return this.m_wires.ContainsKey(wires.GetHashCode());
        }

        public bool Contains(InPortOutPortMapping portMapping)
        {
            return this.m_inOutPortMappings.ContainsKey(portMapping.GetHashCode());
        }

        public Tile GetTile(TileKey key)
        {
            if (this.Contains(key))
            {
                return this.m_tiles.GetTile(key);
            }
            else
            {
                throw new ArgumentException("Tile " + key.ToString() + " not found");
            }
        }

        public Tile GetTile(int x, int y)
        {
            if (this.m_tiles.Contains(x, y))
            {
                return this.m_tiles.GetTile(x, y);
            }
            else
            {
                throw new ArgumentException("Tile " + x.ToString() + y.ToString() + " not found");
            }
        }

        public Tile GetTile(String location)
        {
            if (this.m_location2TileKey.ContainsKey(location))
            {
                TileKey key = this.m_location2TileKey[location];
                return this.m_tiles.GetTile(key);//[key.X][key.Y];
            }
            else
            {
                return null;
                throw new ArgumentException("Tile " + location + " not found");
            }
        }

        public int GetMaxTileKeyX()
        {
            if (this.m_maxTileKeyX > 0)
            {
                return m_maxTileKeyX;
            }
            else
            {
                foreach (Tile t in this.GetAllTiles())
                {
                    if (t.TileKey.X > this.m_maxTileKeyX)
                    {
                        this.m_maxTileKeyX = t.TileKey.X;
                    }
                }

                return this.m_maxTileKeyX;
            }
        }

        public int GetMaxTileKeyY()
        {
            if (this.m_maxTileKeyY > 0)
            {
                return m_maxTileKeyY;
            }
            else
            {
                foreach (Tile t in this.GetAllTiles())
                {
                    if (t.TileKey.Y > this.m_maxTileKeyY)
                    {
                        this.m_maxTileKeyY = t.TileKey.Y;
                    }
                }

                return this.m_maxTileKeyY;
            }
        }

        public SwitchMatrix GetSwitchMatrix(int hashCode)
        {
            return this.m_matrices[hashCode];
        }

        public WireList GetWireList(int hashCode)
        {
            if (!this.m_wires.ContainsKey(hashCode))
            {
                return null;
            }

            return this.m_wires[hashCode];
        }

        public Slice GetSlice(String sliceName)
        {
            if (this.m_sliceName2TileKey == null)
            {
                this.m_sliceName2TileKey = new Dictionary<string, TileKey>();
            }
            if (this.m_sliceName2TileKey.Count == 0)
            {
                foreach (Tile t in this.GetAllTiles())
                {
                    foreach (Slice s in t.Slices)
                    {
                        this.m_sliceName2TileKey.Add(s.SliceName, t.TileKey);
                    }
                }
            }

            if (this.m_sliceName2TileKey.ContainsKey(sliceName))
            {
                Tile t = this.GetTile(this.m_sliceName2TileKey[sliceName]);
                return t.GetSliceByName(sliceName);
            }
            else
            {
                foreach (Tile t in this.GetAllTiles())
                {
                    foreach (Slice s in t.Slices)
                    {
                        if (s.SliceName.Equals(sliceName))
                        {
                            return s;
                        }
                    }
                }
            }
            return null;
            throw new ArgumentException("Slice " + sliceName + " not found");
        }

        public IEnumerable<Tile> GetAllTiles()
        {
            return this.m_tiles;
        }

        public IEnumerable<SwitchMatrix> GetAllSwitchMatrices()
        {
            foreach (SwitchMatrix sm in this.m_matrices.Values)
            {
                yield return sm;
            }
        }

        public IEnumerable<WireList> GetAllWireLists()
        {
            return this.m_wires.Values;
        }

        public void Reset()
        {
            FPGA.Instance = new FPGA();
        }

        public void ClearWireList()
        {
            this.m_wires.Clear();
        }

        public InPortOutPortMapping GetInPortOutPortMapping(int hashCode)
        {
            return this.m_inOutPortMappings[hashCode];
        }

        public int TileCount
        {
            get { return this.m_tiles.Count; }
        }

        public int SwitchMatrixCount
        {
            get { return this.m_matrices.Count; }
        }

        public int WireListCount
        {
            get { return this.m_wires.Count; }
        }

        public int InPortOutPortMappingCount
        {
            get { return this.m_inOutPortMappings.Count; }
        }

        public override String ToString()
        {
            StringBuilder buffer = new StringBuilder();

            buffer.AppendLine("Family: " + this.Family);
            buffer.AppendLine("Device: " + this.DeviceName);
            buffer.AppendLine("Tiles: " + this.m_tiles.Count);
            buffer.AppendLine("Switchmatrices: " + this.m_matrices.Count);
            buffer.AppendLine("InOutPort-Mappings: " + this.m_inOutPortMappings.Count);
            buffer.AppendLine("Wirelists: " + this.m_wires.Count);
            buffer.AppendLine("Identifier: " + this.IdentifierListLookup.Count);
            buffer.AppendLine("");

            /*
            int index = 0;
            foreach(WireList wl in this.GetAllWireLists())
            {
                buffer.AppendLine("Wirelist " + index++ + " has " + wl.Count + " wires");
            }*/

            return buffer.ToString();
        }

        public void DoPreSerializationTasks()
        {
            // no clear as this field might be NULL after deserialization
            this.m_rawTiles = new Queue<RawTile>();
            foreach (Tile t in this.GetAllTiles())
            {
                RawTile rawTile = new RawTile(t);
                m_rawTiles.Enqueue(rawTile);
            }
            //this.m_wires.Clear();
            this.m_tiles.Clear();
            this.m_location2TileKey.Clear();

            // no clear as this field might be NULL after deserialization
            this.m_rawMatrices = new Queue<RawSwitchMatrix>();
            foreach (SwitchMatrix sm in this.m_matrices.Values)
            {
                RawSwitchMatrix rawSm = new RawSwitchMatrix(sm);
                this.m_rawMatrices.Enqueue(rawSm);
            }
            this.m_matrices.Clear();
        }

        /// <summary>
        /// Restore m_location2TileKey, m_tiles and m_matrices
        /// </summary>
        public void DoPostSerializationTasks()
        {
            this.m_location2TileKey.Clear();
            this.m_tiles.Clear();
            this.m_maxTileKeyX = 0;
            this.MaxX = 0;
            this.MaxY = 0;

            while (this.m_rawTiles.Count > 0)
            {
                RawTile rawTile = this.m_rawTiles.Dequeue();
                Tile tile = new Tile(rawTile);

                foreach (RawSlice rawSlice in rawTile.RawSlices)
                {
                    Slice slice = Slice.GetSliceFromRawSlice(tile, rawSlice);
                    tile.Slices.Add(slice);
                }

                this.Add(tile);
            }

            this.m_matrices.Clear();
            while (this.m_rawMatrices.Count > 0)
            {
                RawSwitchMatrix rawSM = this.m_rawMatrices.Dequeue();
                SwitchMatrix sm = new SwitchMatrix(rawSM);
                this.Add(sm.HashCode, sm);
            }

            this.m_rawTiles.Clear();
            this.m_rawMatrices.Clear();

            Commands.Debug.PrintGoAheadInternals.ObjectsToPrint.Add(this);
        }

        public String DeviceName
        {
            get { return this.m_deviceName; }
            set { this.m_deviceName = value; }
        }

        private String m_deviceName = "undefined_device_name";

        public FPGATypes.FPGAFamily Family
        {
            get { return this.m_familiy; }
            set { this.m_familiy = value; }
        }

        private FPGATypes.FPGAFamily m_familiy = FPGATypes.FPGAFamily.Undefined;

        public IdentifierLookup IdentifierListLookup
        {
            get { return this.m_identifierLookUp; }
            set { this.m_identifierLookUp = value; }
        }

        private IdentifierLookup m_identifierLookUp = new IdentifierLookup();

        public Tile Current = null;

        /// <summary>
        /// The class FPGA is a Singelton
        /// </summary>
        public static FPGA Instance = new FPGA();

        /// <summary>
        /// set in device shape parser
        /// </summary>
        [DefaultValue(0)]
        public int NumberOfExpectedTiles { get; set; }

        /// <summary>
        ///
        /// </summary>
        [DefaultValue(Int32.MinValue)]
        public int MaxX { get; set; }

        /// <summary>
        ///
        /// </summary>
        [DefaultValue(FPGATypes.BackendType.ISE)]
        public FPGATypes.BackendType BackendType { get; set; }

        /// <summary>
        ///
        /// </summary>
        [DefaultValue(Int32.MinValue)]
        public int MaxY { get; set; }

        private int m_maxTileKeyX = Int32.MinValue;
        private int m_maxTileKeyY = Int32.MinValue;

        public Dictionary<string, TileKey> m_location2TileKey = new Dictionary<string, TileKey>();
        public Dictionary<string, TileKey> m_sliceName2TileKey = new Dictionary<string, TileKey>();
        public TileSet m_tiles = new TileSet();

        public Dictionary<int, SwitchMatrix> m_matrices = new Dictionary<int, SwitchMatrix>();
        public Dictionary<int, WireList> m_wires = new Dictionary<int, WireList>();
        public Dictionary<int, InPortOutPortMapping> m_inOutPortMappings = new Dictionary<int, InPortOutPortMapping>();

        private Queue<RawTile> m_rawTiles = new Queue<RawTile>();
        private Queue<RawSwitchMatrix> m_rawMatrices = new Queue<RawSwitchMatrix>();
    }
}
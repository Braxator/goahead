﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace GoAhead.FPGA
{
    [Serializable]
    public class WireList : ISerializable, IEnumerable<Wire>
    {
        public WireList()
        {
        }

        public void Add(Wire wire, bool updateIndex = true)
        {
            this.m_wires.Add(wire);

            if (updateIndex && this.m_wireKeys != null)
            {
                if (!this.m_wireKeys.ContainsKey(wire.LocalPipKey))
                {
                    this.m_wireKeys.Add(wire.LocalPipKey, new Dictionary<uint, Dictionary<int, Dictionary<int, bool>>>());
                }
                if (!this.m_wireKeys[wire.LocalPipKey].ContainsKey(wire.PipOnOtherTileKey))
                {
                    this.m_wireKeys[wire.LocalPipKey].Add(wire.PipOnOtherTileKey, new Dictionary<int, Dictionary<int, bool>>());
                }
                if (!this.m_wireKeys[wire.LocalPipKey][wire.PipOnOtherTileKey].ContainsKey(wire.XIncr))
                {
                    this.m_wireKeys[wire.LocalPipKey][wire.PipOnOtherTileKey].Add(wire.XIncr, new Dictionary<int, bool>());
                }
                if (!this.m_wireKeys[wire.LocalPipKey][wire.PipOnOtherTileKey][wire.XIncr].ContainsKey(wire.YIncr))
                {
                    this.m_wireKeys[wire.LocalPipKey][wire.PipOnOtherTileKey][wire.XIncr].Add(wire.YIncr, wire.LocalPipIsDriver);
                }
            }
        }

        public void Clear()
        {
            this.ClearCache();
            this.m_wireKeys.Clear();
            this.m_wires.Clear();
        }

        public void ClearCache()
        {
            if (this.m_chache != null)
            {
                this.m_chache.Clear();
                this.m_chache = null;
            }
        }

        public void ClearWireKeys()
        {
            this.m_wireKeys.Clear();
        }

        public bool HasWire(Wire wire)
        {
            if (this.m_wireKeys == null)
            {
                return this.m_wires.Contains(wire);
            }
            else
            {
                if (!this.m_wireKeys.ContainsKey(wire.LocalPipKey))
                {
                    return false;
                }
                else if (!this.m_wireKeys[wire.LocalPipKey].ContainsKey(wire.PipOnOtherTileKey))
                {
                    return false;
                }
                else if (!this.m_wireKeys[wire.LocalPipKey][wire.PipOnOtherTileKey].ContainsKey(wire.XIncr))
                {
                    return false;
                }
                else if (!this.m_wireKeys[wire.LocalPipKey][wire.PipOnOtherTileKey][wire.XIncr].ContainsKey(wire.YIncr))
                {
                    return false;
                }
                else
                {
                    return this.m_wireKeys[wire.LocalPipKey][wire.PipOnOtherTileKey][wire.XIncr][wire.YIncr] == wire.LocalPipIsDriver;
                }
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is WireList))
            {
                return false;
            }

            WireList other = (WireList)obj;

            if (other.m_wires.Count != this.m_wires.Count)
            {
                return false;
            }

            foreach (Wire w in other)
            {
                if (!this.HasWire(w))
                {
                    return false;
                }
            }
            return true;
        }

        public IEnumerable<Wire> GetAllWires(Port port)
        {
            return this.GetAllWires(port.NameKey);
        }

        public IEnumerable<Wire> GetAllWires(String portName)
        {
            return this.GetAllWires(FPGA.Instance.IdentifierListLookup.GetKey(portName));
        }

        public IEnumerable<Wire> GetAllWires(uint key)
        {
            if (this.m_chache == null)
            {
                this.m_chache = new Dictionary<uint, List<Wire>>();
                //Console.WriteLine("Creating cache for wire " + port.Name + " in wire list " + this.Key);
            }
            if (!this.m_chache.ContainsKey(key))
            {
                this.m_chache.Add(key, new List<Wire>());
                this.m_chache[key].AddRange(this.m_wires.Where(w => w.LocalPipKey == key));
            }

            return this.m_chache[key];
        }

        private WireList(SerializationInfo info, StreamingContext context)
        {
            String[] atoms = info.GetString("wirelist").Split(';');
            int i = 0;
            while (i + 4 < atoms.Length)
            {
                bool localPipIsDriver = atoms[i + 2].Equals("1");
                //Wire w = new Wire(atoms[i + 0], atoms[i + 1], localPipIsDriver, (short)Int32.Parse(atoms[i + 3]), (short)Int32.Parse(atoms[i + 4]));
                Wire w = new Wire((uint)Int32.Parse(atoms[i + 0]), (uint)Int32.Parse(atoms[i + 1]), localPipIsDriver, (short)Int32.Parse(atoms[i + 3]), (short)Int32.Parse(atoms[i + 4]));
                this.m_wires.Add(w);
                i += 5;
            }
            this.Key = Int32.Parse(info.GetString("hash"));
            // release for GC
            this.m_wireKeys = null;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            StringBuilder buffer = new StringBuilder();
            foreach (Wire w in this)
            {
                buffer.Append(w.GetStringForSerialization());
            }

            info.AddValue("wirelist", buffer.ToString());
            info.AddValue("hash", this.Key);
        }

        public override string ToString()
        {
            StringBuilder buffer = new StringBuilder();
            foreach (Wire w in this.m_wires.OrderBy(w => w.LocalPip))
            {
                buffer.AppendLine(w.GetStringForSerialization());
            }

            return buffer.ToString();
        }

        public override int GetHashCode()
        {
            return this.Key;
        }

        /// <summary>
        /// the number of wires in this wire list
        /// </summary>
        public int Count
        {
            get { return this.m_wires.Count; }
        }

        private List<Wire> m_wires = new List<Wire>();
        private Dictionary<uint, List<Wire>> m_chache = null;

        public Dictionary<uint, Dictionary<uint, Dictionary<int, Dictionary<int, bool>>>> m_wireKeys = new Dictionary<uint, Dictionary<uint, Dictionary<int, Dictionary<int, bool>>>>();

        /// <summary>
        /// The hashcode must be calculated after reading in the XDL description of this SwitchMatrix. Afterone, it may not be changed again as the HasCode is used as a key
        /// and on different machines the calculated hash codes may differ!
        /// </summary>
        public int Key { get; set; }

        public IEnumerator<Wire> GetEnumerator()
        {
            foreach (Wire w in this.m_wires)
            {
                yield return w;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    [Serializable]
    public class Wire : IComparable
    {
        public Wire(uint localPipKey, uint pipOnOtherTileKey, bool localPipIsDriver, int xIncr, int yIncr)
        {
            this.m_localPipKey = localPipKey;
            this.m_pipOnOtherTileKey = pipOnOtherTileKey;
            this.m_localPipIsDriver = localPipIsDriver;
            this.m_xIncr = xIncr;
            this.m_yIncr = yIncr;
        }

        public uint LocalPipKey
        {
            get { return this.m_localPipKey; }
        }

        public String LocalPip
        {
            get { return FPGA.Instance.IdentifierListLookup.GetIdentifier(this.m_localPipKey); }
        }

        public uint PipOnOtherTileKey
        {
            get { return this.m_pipOnOtherTileKey; }
        }

        public String PipOnOtherTile
        {
            get { return FPGA.Instance.IdentifierListLookup.GetIdentifier(this.PipOnOtherTileKey); }
        }

        public bool LocalPipIsDriver
        {
            get { return this.m_localPipIsDriver; }
        }

        public int XIncr
        {
            get { return this.m_xIncr; }
        }

        public int YIncr
        {
            get { return this.m_yIncr; }
        }

        public override int GetHashCode()
        {
            return this.GetStringForSerialization().GetHashCode();
        }

        public String GetStringForSerialization()
        {
            return this.m_localPipKey + ";" + this.m_pipOnOtherTileKey + ";" + (this.LocalPipIsDriver ? "1" : "0") + ";" + this.XIncr + ";" + this.YIncr + ";";
            //return this.LocalPip + ";" + this.PipOnOtherTile + ";" + (this.LocalPipIsDriver ? "1" : "0") + ";" + this.XIncr + ";" + this.YIncr + ";";
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Wire))
            {
                return false;
            }

            Wire other = (Wire)obj;

            return
                this.LocalPipIsDriver == other.LocalPipIsDriver &&
                this.m_localPipKey == other.m_localPipKey &&
                this.m_pipOnOtherTileKey == other.m_pipOnOtherTileKey &&
                this.m_xIncr == other.m_xIncr &&
                this.m_yIncr == other.m_yIncr;

            //bool equal = this.GetStringForSerialization().Equals(other.GetStringForSerialization());
            //return equal;
        }

        public override string ToString()
        {
            return this.GetStringForSerialization();
        }

        public int CompareTo(object obj)
        {
            return this.ToString().CompareTo(obj.ToString());
        }

        private readonly uint m_localPipKey;
        private readonly uint m_pipOnOtherTileKey;
        private readonly int m_xIncr;
        private readonly int m_yIncr;
        private readonly bool m_localPipIsDriver;
    }
}
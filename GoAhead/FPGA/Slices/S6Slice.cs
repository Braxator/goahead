﻿using System;
using System.Text.RegularExpressions;

namespace GoAhead.FPGA.Slices
{
    [Serializable]
    public class S6Slice : Slice
    {
        public S6Slice(Tile containingTile, String name, String type)
            : base(containingTile, name, type)
        {
        }

        protected override String ModifySetting(Tuple<String, String> nextTupel)
        {
            if (Regex.IsMatch(nextTupel.Item1, "^[A-D][5-6]LUT$|^[A-D]FF$") && !nextTupel.Item2.Equals(":#OFF"))
            {
                // A6LUT:replace_with_instance_name.A6LUT:#LUT:O6=A6 OR
                // AFF:replace_with_instance_name.AFF:#FF
                return nextTupel.Item1 + ":" + this.SliceName + "." + nextTupel.Item1 + ":" + nextTupel.Item2;
            }
            else
            {
                return nextTupel.Item1 + ":" + nextTupel.Item2;
            }
        }

        public override void InitAttributes()
        {
            if (Regex.IsMatch(this.ContainingTile.Location, "BRAMSITE"))
            {
                /*
                this.m_attributes.Add("CLKAINV", ":OFF");
                this.m_attributes.Add("CLKBINV", ":OFF");
                this.m_attributes.Add("DATA_WIDTH_A", ":OFF");
                this.m_attributes.Add("DATA_WIDTH_B", ":OFF");
                this.m_attributes.Add("DOA_REG", ":OFF");
                this.m_attributes.Add("DOB_REG", ":OFF");
                this.m_attributes.Add("ENAINV", ":OFF");
                this.m_attributes.Add("ENBINV", ":OFF");
                this.m_attributes.Add("EN_RSTRAM_A", ":OFF");
                this.m_attributes.Add("EN_RSTRAM_B", ":OFF");
                this.m_attributes.Add("RAM_MODE", ":OFF");
                this.m_attributes.Add("REGCEAINV", ":OFF");
                this.m_attributes.Add("REGCEBINV", ":OFF");
                this.m_attributes.Add("RSTAINV", ":OFF");
                this.m_attributes.Add("RSTBINV", ":OFF");
                this.m_attributes.Add("RSTTYPE", ":OFF");
                this.m_attributes.Add("RST_PRIORITY_A", ":OFF");
                this.m_attributes.Add("RST_PRIORITY_B", ":OFF");
                this.m_attributes.Add("WEA0INV", ":OFF");
                this.m_attributes.Add("WEA1INV", ":OFF");
                this.m_attributes.Add("WEA2INV", ":OFF");
                this.m_attributes.Add("WEA3INV", ":OFF");
                this.m_attributes.Add("WEB0INV", ":OFF");
                this.m_attributes.Add("WEB1INV", ":OFF");
                this.m_attributes.Add("WEB2INV", ":OFF");
                this.m_attributes.Add("WEB3INV", ":OFF");
                this.m_attributes.Add("WRITE_MODE_A", ":OFF");
                this.m_attributes.Add("WRITE_MODE_B", ":OFF");
                this.m_attributes.Add("RAMB16BWER:$Blocker_BRAM_" + this.SliceName + ".RAMB16BWER:", ":OFF");
                 */
            }
            else if (Regex.IsMatch(this.ContainingTile.Location, "CLEX"))
            {
                this.m_attributes.Add("A5FFSRINIT", ":#OFF");
                this.m_attributes.Add("A5LUT", ":#OFF");
                this.m_attributes.Add("A6LUT", ":#OFF");
                this.m_attributes.Add("AFF", ":#OFF");
                this.m_attributes.Add("AFFMUX", ":#OFF");
                this.m_attributes.Add("AFFSRINIT", ":#OFF");
                this.m_attributes.Add("AOUTMUX", ":#OFF");
                this.m_attributes.Add("AUSED", ":#OFF");
                this.m_attributes.Add("B5FFSRINIT", ":#OFF");
                this.m_attributes.Add("B5LUT", ":#OFF");
                this.m_attributes.Add("B6LUT", ":#OFF");
                this.m_attributes.Add("BFF", ":#OFF");
                this.m_attributes.Add("BFFMUX", ":#OFF");
                this.m_attributes.Add("BFFSRINIT", ":#OFF");
                this.m_attributes.Add("BOUTMUX", ":#OFF");
                this.m_attributes.Add("BUSED", ":#OFF");
                this.m_attributes.Add("C5FFSRINIT", ":#OFF");
                this.m_attributes.Add("C5LUT", ":#OFF");
                this.m_attributes.Add("C6LUT", ":#OFF");
                this.m_attributes.Add("CEUSED", ":#OFF");
                this.m_attributes.Add("CFF", ":#OFF");
                this.m_attributes.Add("CFFMUX", ":#OFF");
                this.m_attributes.Add("CFFSRINIT", ":#OFF");
                this.m_attributes.Add("CLKINV", ":#OFF");
                this.m_attributes.Add("COUTMUX", ":#OFF");
                this.m_attributes.Add("CUSED", ":#OFF");
                this.m_attributes.Add("D5FFSRINIT", ":#OFF");
                this.m_attributes.Add("D5LUT", ":#OFF");
                this.m_attributes.Add("D6LUT", ":#OFF");
                this.m_attributes.Add("DFF", ":#OFF");
                this.m_attributes.Add("DFFMUX", ":#OFF");
                this.m_attributes.Add("DFFSRINIT", ":#OFF");
                this.m_attributes.Add("DOUTMUX", ":#OFF");
                this.m_attributes.Add("DUSED", ":#OFF");
                this.m_attributes.Add("SRUSED", ":#OFF");
                this.m_attributes.Add("SYNC_ATTR", ":#OFF");
            }
            else
            {
                // do not add attributes
            }
        }
    }
}
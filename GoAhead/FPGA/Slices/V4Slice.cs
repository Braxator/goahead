﻿using System;
using System.Text.RegularExpressions;

namespace GoAhead.FPGA.Slices
{
    [Serializable]
    public class V4Slice : Slice
    {
        public V4Slice(Tile containingTile, String name, String type)
            : base(containingTile, name, type)
        {
            /* Slice order in FE
             3
             1
            2
            0
             */
        }

        protected override String ModifySetting(Tuple<String, String> nextTupel)
        {
            //if (Regex.IsMatch(nextTupel.Key, "^F|F$|^[A-D]FF$") && !nextTupel.Value.Equals(":#OFF"))
            if (Regex.IsMatch(nextTupel.Item1, "(^(F|G)$)|(^FF(X|Y))|(^CYMUX(F|G))") && !nextTupel.Item2.Equals(":OFF"))
            {
                // A6LUT:replace_with_instance_name.A6LUT:#LUT:O6=A6 OR
                // AFF:replace_with_instance_name.AFF:#FF
                return nextTupel.Item1 + ":" + this.SliceName + "." + nextTupel.Item1 + ":" + nextTupel.Item2;
            }
            else
            {
                return nextTupel.Item1 + ":" + nextTupel.Item2;
            }
        }

        public override void InitAttributes()
        {
            // for non CLB slice types, do nothing
            if (!Regex.IsMatch(this.m_type, "SLICE"))
                return;

            // in V4 there are 2 slice types in CLBs
            bool memory = this.m_type.Equals("SLICEM");
            bool logic = this.m_type.Equals("SLICEL");

            if (memory)
            {
                this.m_attributes.Add("BXINV", ":OFF");
                this.m_attributes.Add("BYINV", ":OFF");
                this.m_attributes.Add("BYINVOUTUSED", ":OFF");
                this.m_attributes.Add("BYOUTUSED", ":OFF");
                this.m_attributes.Add("CEINV", ":OFF");
                this.m_attributes.Add("CLKINV", ":OFF");
                this.m_attributes.Add("COUTUSED", ":OFF");
                this.m_attributes.Add("CY0F", ":OFF");
                this.m_attributes.Add("CY0G", ":OFF");
                this.m_attributes.Add("CYINIT", ":OFF");
                this.m_attributes.Add("CYMUXF", ":OFF"); // manually added
                this.m_attributes.Add("CYMUXG", ":OFF"); // manually added
                this.m_attributes.Add("DIF_MUX", ":OFF");
                this.m_attributes.Add("DIGUSED", ":OFF");
                this.m_attributes.Add("DIG_MUX", ":OFF");
                this.m_attributes.Add("DXMUX", ":OFF");
                this.m_attributes.Add("DYMUX", ":OFF");
                this.m_attributes.Add("F", ":OFF");
                this.m_attributes.Add("F5USED", ":OFF");
                this.m_attributes.Add("FFX", ":OFF");
                this.m_attributes.Add("FFX_INIT_ATTR", ":OFF");
                this.m_attributes.Add("FFX_SR_ATTR", ":OFF");
                this.m_attributes.Add("FFY", ":OFF");
                this.m_attributes.Add("FFY_INIT_ATTR", ":OFF");
                this.m_attributes.Add("FFY_SR_ATTR", ":OFF");
                this.m_attributes.Add("FXMUX", ":OFF");
                this.m_attributes.Add("FXUSED", ":OFF");
                this.m_attributes.Add("F_ATTR", ":OFF");
                this.m_attributes.Add("G", ":OFF");
                this.m_attributes.Add("GYMUX", ":OFF");
                this.m_attributes.Add("G_ATTR", ":OFF");
                this.m_attributes.Add("REVUSED", ":OFF");
                this.m_attributes.Add("SHIFTOUTUSED", ":OFF");
                this.m_attributes.Add("SLICEWE0USED", ":OFF");
                this.m_attributes.Add("SLICEWE1USED", ":OFF");
                this.m_attributes.Add("SRFFMUX", ":OFF");
                this.m_attributes.Add("SRINV", ":OFF");
                this.m_attributes.Add("SYNC_ATTR", ":OFF");
                this.m_attributes.Add("WF1USED", ":OFF");
                this.m_attributes.Add("WF2USED", ":OFF");
                this.m_attributes.Add("WF3USED", ":OFF");
                this.m_attributes.Add("WF4USED", ":OFF");
                this.m_attributes.Add("WG1USED", ":OFF");
                this.m_attributes.Add("WG2USED", ":OFF");
                this.m_attributes.Add("WG3USED", ":OFF");
                this.m_attributes.Add("WG4USED", ":OFF");
                this.m_attributes.Add("XBMUX", ":OFF");
                this.m_attributes.Add("XBUSED", ":OFF");
                this.m_attributes.Add("XMUXUSED", ":OFF");
                this.m_attributes.Add("XUSED", ":OFF");
                this.m_attributes.Add("YBMUX", ":OFF");
                this.m_attributes.Add("YBUSED", ":OFF");
                this.m_attributes.Add("YMUXUSED", ":OFF");
                this.m_attributes.Add("YUSED", ":OFF");
            }

            if (logic)
            {
                this.m_attributes.Add("BXINV", ":OFF");
                this.m_attributes.Add("BYINV", ":OFF");
                this.m_attributes.Add("CEINV", ":OFF");
                this.m_attributes.Add("CLKINV", ":OFF");
                this.m_attributes.Add("COUTUSED", ":OFF");
                this.m_attributes.Add("CYMUXF", ":OFF"); // manually added
                this.m_attributes.Add("CYMUXG", ":OFF"); // manually added
                this.m_attributes.Add("CY0F", ":OFF");
                this.m_attributes.Add("CY0G", ":OFF");
                this.m_attributes.Add("CYINIT", ":OFF");
                this.m_attributes.Add("DXMUX", ":OFF");
                this.m_attributes.Add("DYMUX", ":OFF");
                this.m_attributes.Add("F", ":OFF");
                this.m_attributes.Add("F5USED", ":OFF");
                this.m_attributes.Add("FFX", ":OFF");
                this.m_attributes.Add("FFX_INIT_ATTR", ":OFF");
                this.m_attributes.Add("FFX_SR_ATTR", ":OFF");
                this.m_attributes.Add("FFY", ":OFF");
                this.m_attributes.Add("FFY_INIT_ATTR", ":OFF");
                this.m_attributes.Add("FFY_SR_ATTR", ":OFF");
                this.m_attributes.Add("FXMUX", ":OFF");
                this.m_attributes.Add("FXUSED", ":OFF");
                this.m_attributes.Add("G", ":OFF");
                this.m_attributes.Add("GYMUX", ":OFF");
                this.m_attributes.Add("REVUSED", ":OFF");
                this.m_attributes.Add("SRINV", ":OFF");
                this.m_attributes.Add("SYNC_ATTR", ":OFF");
                this.m_attributes.Add("XBUSED", ":OFF");
                this.m_attributes.Add("XMUXUSED", ":OFF");
                this.m_attributes.Add("XUSED", ":OFF");
                this.m_attributes.Add("YBUSED", ":OFF");
                this.m_attributes.Add("YMUXUSED", ":OFF");
                this.m_attributes.Add("YUSED", ":OFF");
            }
        }
    }
}
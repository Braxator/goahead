﻿using System;
using System.Text.RegularExpressions;

namespace GoAhead.FPGA.Slices
{
    [Serializable]
    public class V5Slice : Slice
    {
        public V5Slice(Tile containingTile, String name, String type)
            : base(containingTile, name, type)
        {
        }

        protected override String ModifySetting(Tuple<String, String> nextTupel)
        {
            if (Regex.IsMatch(nextTupel.Item1, "^[A-D][5-6]LUT$|^[A-D]FF$") && !nextTupel.Item2.Equals(":#OFF"))
            {
                // A6LUT:replace_with_instance_name.A6LUT:#LUT:O6=A6 OR
                // AFF:replace_with_instance_name.AFF:#FF
                return nextTupel.Item1 + ":" + this.SliceName + "." + nextTupel.Item1 + ":" + nextTupel.Item2;
            }
            else
            {
                return nextTupel.Item1 + ":" + nextTupel.Item2;
            }
        }

        public override void InitAttributes()
        {
            if (!Regex.IsMatch(this.m_type, "SLICE"))
                return;
            bool memory = this.m_type.Equals("SLICEM");

            this.m_attributes.Add("A5LUT", ":#OFF");
            if (memory)
                this.m_attributes.Add("A5RAMMODE", ":#OFF");
            this.m_attributes.Add("A6LUT", ":#OFF");
            if (memory)
                this.m_attributes.Add("A6RAMMODE", ":#OFF");
            this.m_attributes.Add("ACY0", ":#OFF");
            if (memory)
                this.m_attributes.Add("ADI1MUX", ":#OFF");
            this.m_attributes.Add("AFF", ":#OFF");
            this.m_attributes.Add("AFFINIT", ":#OFF");
            this.m_attributes.Add("AFFMUX", ":#OFF");
            this.m_attributes.Add("AFFSR", ":#OFF");
            this.m_attributes.Add("AOUTMUX", ":#OFF");
            this.m_attributes.Add("AUSED", ":#OFF");
            this.m_attributes.Add("B5LUT", ":#OFF");
            if (memory)
                this.m_attributes.Add("B5RAMMODE", ":#OFF");
            this.m_attributes.Add("B6LUT", ":#OFF");
            if (memory)
                this.m_attributes.Add("B6RAMMODE", ":#OFF");
            this.m_attributes.Add("BCY0", ":#OFF");
            if (memory)
                this.m_attributes.Add("BDI1MUX", ":#OFF");
            this.m_attributes.Add("BFF", ":#OFF");
            this.m_attributes.Add("BFFINIT", ":#OFF");
            this.m_attributes.Add("BFFMUX", ":#OFF");
            this.m_attributes.Add("BFFSR", ":#OFF");
            this.m_attributes.Add("BOUTMUX", ":#OFF");
            this.m_attributes.Add("BUSED", ":#OFF");
            this.m_attributes.Add("C5LUT", ":#OFF");
            if (memory)
                this.m_attributes.Add("C5RAMMODE", ":#OFF");
            this.m_attributes.Add("C6LUT", ":#OFF");
            if (memory)
                this.m_attributes.Add("C6RAMMODE", ":#OFF");
            this.m_attributes.Add("CCY0", ":#OFF");
            if (memory)
                this.m_attributes.Add("CDI1MUX", ":#OFF");

            this.m_attributes.Add("CFF", ":#OFF");
            this.m_attributes.Add("CFFINIT", ":#OFF");
            this.m_attributes.Add("CFFMUX", ":#OFF");
            this.m_attributes.Add("CFFSR", ":#OFF");
            this.m_attributes.Add("COUTMUX", ":#OFF");

            this.m_attributes.Add("CUSED", ":#OFF");
            this.m_attributes.Add("D5LUT", ":#OFF");
            if (memory)
                this.m_attributes.Add("D5RAMMODE", ":#OFF");
            this.m_attributes.Add("D6LUT", ":#OFF");
            if (memory)
                this.m_attributes.Add("D6RAMMODE", ":#OFF");
            this.m_attributes.Add("DCY0", ":#OFF");
            this.m_attributes.Add("DFF", ":#OFF");
            this.m_attributes.Add("DFFINIT", ":#OFF");
            this.m_attributes.Add("DFFMUX", ":#OFF");
            this.m_attributes.Add("DFFSR", ":#OFF");
            this.m_attributes.Add("DOUTMUX", ":#OFF");
            this.m_attributes.Add("DUSED", ":#OFF");
            this.m_attributes.Add("PRECYINIT", ":#OFF");
            this.m_attributes.Add("REVUSED", ":#OFF");
            this.m_attributes.Add("SRUSED", ":#OFF");
            this.m_attributes.Add("SYNC_ATTR", ":#OFF");
            if (memory)
            {
                this.m_attributes.Add("WA7USED", ":#OFF");
                this.m_attributes.Add("WA8USED", ":#OFF");
                this.m_attributes.Add("WEMUX", ":#OFF");
            }

            // for readability do not include this settings into c part
            this.m_attributes.Add("CEUSED", ":#OFF");
            this.m_attributes.Add("CLKINV", ":#OFF");

            this.m_attributes.Add("COUTUSED", ":#OFF");
        }
    }
}
﻿using System;
using System.Text.RegularExpressions;

namespace GoAhead.FPGA.Slices
{
    [Serializable]
    public class USSlice : Slice
    {
        public USSlice(Tile containingTile, String name, String type)
            : base(containingTile, name, type)
        {
        }

        public override void InitAttributes()
        {
        }
    }
}
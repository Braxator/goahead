﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace GoAhead.FPGA
{
    [Serializable]
    public class TileSet : IEnumerable<Tile>
    {
        public void Add(TileKey tileKey)
        {
            Tile t = FPGA.Instance.GetTile(tileKey);
            this.Add(t);
        }

        public void Add(IEnumerable<Tile> tiles)
        {
            foreach (Tile t in tiles)
            {
                this.Add(t);
            }
        }

        public void Add(Tile tile)
        {
            if (!this.m_tiles.ContainsKey(tile.TileKey.X))
            {
                this.m_tiles.Add(tile.TileKey.X, new Dictionary<int, Tile>());
            }
            this.m_tiles[tile.TileKey.X].Add(tile.TileKey.Y, tile);
        }

        public void Clear()
        {
            this.m_tiles.Clear();
        }

        public bool Contains(Tile t)
        {
            return this.Contains(t.TileKey);
        }

        public bool Contains(TileKey key)
        {
            return this.Contains(key.X, key.Y);
        }

        public bool Contains(int x, int y)
            {
            if (!this.m_tiles.ContainsKey(x))
            {
                return false;
            }
            else if (!this.m_tiles[x].ContainsKey(y))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public int Count
        {
            get
            {
                int count = 0;
                foreach (int x in this.m_tiles.Keys)
                {
                    count += this.m_tiles[x].Count;
                }
                return count;
            }
        }

        public IEnumerator<Tile> GetEnumerator()
        {
            foreach (int x in this.m_tiles.Keys)
            {
                foreach (int y in this.m_tiles[x].Keys)
                {
                    yield return this.m_tiles[x][y];
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public Tile GetTile(TileKey key)
        {
            return this.GetTile(key.X, key.Y);
        }

        public Tile GetTile(int x, int y)
        {
            if (!this.m_tiles.ContainsKey(x))
            {
                return null;
            }
            else if (!this.m_tiles[x].ContainsKey(y))
            {
                return null;
            }
            else
            {
                return this.m_tiles[x][y];
            }
        }

        public void Remove(TileKey key)
        {
            this.Remove(key.X, key.Y);
        }

        public void Remove(int x, int y)
        {
            if (this.m_tiles.ContainsKey(x))
            {
                if (this.m_tiles[x].ContainsKey(y))
                {
                    this.m_tiles[x].Remove(y);
                }
                if (this.m_tiles[x].Count == 0)
                {
                    this.m_tiles.Remove(x);
                }
            }
        }

        private Dictionary<int, Dictionary<int, Tile>> m_tiles = new Dictionary<int, Dictionary<int, Tile>>();
    }
}
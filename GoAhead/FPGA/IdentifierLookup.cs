﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace GoAhead.FPGA
{
    [Serializable]
    public class IdentifierLookup : ISerializable
    {
        public IdentifierLookup()
        {
        }

        private void AddIdentifier(String identifier)
        {
            if (!this.m_identifier.ContainsKey(identifier))
            {
                uint key = (uint )this.m_identifier.Count;
                String internedIdentifier = String.Intern(identifier);
                this.m_identifier.Add(internedIdentifier, key);
                this.m_keys.Add(key, internedIdentifier);
            }

            // test only
            uint idKey = this.GetKey(identifier);
            if (!this.GetIdentifier(idKey).Equals(identifier))
            {
                throw new ArgumentException("Hash collision for " + identifier);
            }
        }

        public String GetIdentifier(uint key)
        {
            if (!this.m_keys.ContainsKey(key))
            {
                return "";
            }

            return this.m_keys[key];
        }

        public uint GetKey(String identifier)
        {
            if (!this.m_identifier.ContainsKey(identifier))
            {
                this.AddIdentifier(identifier);
            }

            uint key = this.m_identifier[identifier];
            if (key > 0xFFFFF)
            {
                Console.WriteLine("key overflow with " + identifier);
            }
            return key;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            StringBuilder buffer = new StringBuilder();
            foreach (KeyValuePair<uint, String> keyIdentifierTupel in this.m_keys.OrderBy(t => t.Key))
            {
                buffer.Append(keyIdentifierTupel.Value + ";");
            }

            info.AddValue("identifierlist", buffer.ToString());
        }

        private IdentifierLookup(SerializationInfo info, StreamingContext context)
        {
            this.m_identifier.Clear();
            this.m_keys.Clear();

            String[] atoms = info.GetString("identifierlist").Split(';');
            for (uint i = 0; i < atoms.Length; i++)
            {
                if (!this.m_identifier.ContainsKey(atoms[i]))
                {
                    this.m_identifier.Add(atoms[i], i);
                    this.m_keys.Add(i, atoms[i]);
                }
                else
                {
                    // nothing to do
                }
            }
        }

        /// <summary>
        /// Number of known identifiers
        /// </summary>
        public int Count
        {
            get { return this.m_identifier.Count; }
        }

        private Dictionary<String, uint> m_identifier = new Dictionary<String, uint>();
        private Dictionary<uint, String> m_keys = new Dictionary<uint, String>();
    }
}
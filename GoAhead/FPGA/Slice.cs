using System;
using System.Collections.Generic;
using System.Linq;
using GoAhead.FPGA.Slices;

namespace GoAhead.FPGA
{
    [Serializable]
    public class RawSlice
    {
        public RawSlice(Slice slice)
        {
            this.X = slice.X;
            this.Y = slice.Y;
            this.Name = slice.SliceName;
            this.Type = slice.SliceType;
            this.InPortOutPortMappingHasCode = slice.InPortOutPortMappingHashCode;
            this.Bels = new List<string>();
            foreach (string s in slice.Bels)
            {
                this.Bels.Add(s);
            }
        }

        public readonly int X;
        public readonly int Y;
        public readonly String Name;
        public readonly String Type;
        public readonly int InPortOutPortMappingHasCode;
        public readonly List<string> Bels;
    }

    [Serializable]
    public abstract class Slice
    {
        public static Slice GetSliceFromRawSlice(Tile containingTile, RawSlice rawSlice)
        {
            Slice s = Slice.GetSlice(containingTile, rawSlice.Name, rawSlice.Type);

            s.m_X = rawSlice.X;
            s.m_Y = rawSlice.Y;
            s.Usage = FPGATypes.SliceUsage.Free;
            s.InPortOutPortMappingHashCode = rawSlice.InPortOutPortMappingHasCode;
            s.m_attributes.Clear();
            // do not call init here to save memory
            // if a slices attribute is get or set, the InitAttributes will be called
            //s.InitAttributes();

            s.ExtractXYCoordindates();
            // vivado onyl
            if (rawSlice.Bels != null)
            {
                foreach (string b in rawSlice.Bels)
                {
                    s.AddBel(b);
                }
            }

            return s;
        }

        public static Slice GetSlice(Tile containingTile, String sliceName, String sliceType)
        {
            Slice slice = null;
            if (FPGA.Instance.Family.Equals(FPGATypes.FPGAFamily.Virtex2))
            {
                slice = new V2Slice(containingTile, sliceName, sliceType);
            }
            else if (FPGA.Instance.Family.Equals(FPGATypes.FPGAFamily.Virtex4))
            {
                slice = new V4Slice(containingTile, sliceName, sliceType);
            }
            else if (FPGA.Instance.Family.Equals(FPGATypes.FPGAFamily.Virtex5))
            {
                slice = new V5Slice(containingTile, sliceName, sliceType);
            }
            else if (FPGA.Instance.Family.Equals(FPGATypes.FPGAFamily.Virtex6))
            {
                slice = new V6Slice(containingTile, sliceName, sliceType);
            }
            else if (FPGA.Instance.Family.Equals(FPGATypes.FPGAFamily.Kintex7))
            {
                slice = new K7Slice(containingTile, sliceName, sliceType);
            }
            else if (FPGA.Instance.Family.Equals(FPGATypes.FPGAFamily.Artix7))
            {
                slice = new ASlice(containingTile, sliceName, sliceType);
            }
            else if (FPGA.Instance.Family.Equals(FPGATypes.FPGAFamily.Spartan3))
            {
                slice = new S3Slice(containingTile, sliceName, sliceType);
            }
            else if (FPGA.Instance.Family.Equals(FPGATypes.FPGAFamily.Spartan6))
            {
                slice = new S6Slice(containingTile, sliceName, sliceType);
            }
            else if (FPGA.Instance.Family.Equals(FPGATypes.FPGAFamily.Zynq))
            {
                slice = new ZSlice(containingTile, sliceName, sliceType);
            }
            else if (FPGA.Instance.Family.Equals(FPGATypes.FPGAFamily.UltraScale))
            {
                slice = new USSlice(containingTile, sliceName, sliceType);
            }
            else if (FPGA.Instance.Family.Equals(FPGATypes.FPGAFamily.Virtex7))
            {
                slice = new V7Slice(containingTile, sliceName, sliceType);
            }
            else
            {
                slice = new S6Slice(containingTile, sliceName, sliceType);
                Console.WriteLine("Warning: SliceParser.Parse: Unsupported family: " + FPGA.Instance.Family + " (continue with S6)");
            }
            return slice;
        }

        public Slice(Tile containingTile, String name, String type)
        {
            this.m_containingTile = containingTile;
            this.m_name = name;
            this.m_type = type;
            this.Usage = FPGATypes.SliceUsage.Free;
            this.m_attributes.Clear();
            //this.InitAttributes();

            this.ExtractXYCoordindates();
        }

        private void ExtractXYCoordindates()
        {
            int x, y;

            // location in of form LEFTPARTX\d+Y\d+
            if (FPGATypes.GetXYFromIdentifier(this.m_name, out x, out y))
            {
                this.m_X = x;
                this.m_Y = y;
            }
        }

        public abstract void InitAttributes();

        protected virtual String ModifySetting(Tuple<String, String> nextTupel)
        {
            return nextTupel.Item1 + ":" + nextTupel.Item2;
        }

        public override String ToString()
        {
            return this.m_name;
        }

        public Tile ContainingTile
        {
            get { return m_containingTile; }
        }

        public String SliceName
        {
            get { return m_name; }
        }

        public String SliceType
        {
            get { return this.m_type; }
        }

        public int X
        {
            get { return this.m_X; }
        }

        public int Y
        {
            get { return this.m_Y; }
        }

        public IEnumerable<String> GetAllAttributeValues()
        {
            // called on empty slice?
            if (this.m_attributes.Count == 0)
            {
                this.InitAttributes();
            }

            foreach (KeyValuePair<String, String> nextTupel in this.m_attributes)
            {
                yield return this.ModifySetting(new Tuple<String, String>(nextTupel.Key, nextTupel.Value));
            }
        }

        public IEnumerable<String> GetAllAttributes()
        {
            // called on empty slice?
            if (this.m_attributes.Count == 0)
            {
                this.InitAttributes();
            }

            foreach (KeyValuePair<String, String> nextTupel in this.m_attributes)
            {
                yield return nextTupel.Key;
            }
        }

        public void SetAttributeValue(String attribute, String value)
        {
            // called on empty slice?
            if (this.m_attributes.Count == 0)
            {
                this.InitAttributes();
            }

            if (!this.m_attributes.ContainsKey(attribute))
                throw new ArgumentException("Attribute " + attribute + " not known on slice " + this.SliceName);

            this.m_attributes[attribute] = value;
        }

        public String GetAttributeValue(String attribute)
        {
            // called on empty slice?
            if (this.m_attributes.Count == 0)
            {
                this.InitAttributes();
            }

            if (!this.m_attributes.ContainsKey(attribute))
                throw new ArgumentException("Attribute " + attribute + " not known on slice " + this.SliceName);

            return m_attributes[attribute];
        }

        public InPortOutPortMapping PortMapping
        {
            get { return FPGA.Instance.GetInPortOutPortMapping(this.InPortOutPortMappingHashCode); }
        }

        public void ClearAttributes()
        {
            this.m_attributes.Clear();
        }

        public void Reset()
        {
            this.m_attributes.Clear();
            this.Usage = FPGATypes.SliceUsage.Free;
            //this.InitAttributes();
        }

        public void AddBel(string bel)
        {
            if (!this.m_bels.ContainsKey(bel))
            {
                this.m_bels.Add(bel, FPGATypes.SliceUsage.Free);
            }
        }

        public IEnumerable<string> Bels
        {
            get { return this.m_bels.Keys.AsEnumerable(); }
        }

        public void SetBelUsage(string bel, FPGATypes.SliceUsage usage)
        {
            if (bel != null)
            {
                this.m_bels[bel] = usage;
            }
        }

        public FPGATypes.SliceUsage GetBelUsage(string bel)
        {
            return this.m_bels[bel];
        }


        private Dictionary<string, FPGATypes.SliceUsage> m_bels = new Dictionary<string, FPGATypes.SliceUsage>();

        /// <summary>
        /// A slice can have different usages (e.g Free, Macro)
        /// </summary>
        public FPGATypes.SliceUsage Usage { get; set; }

        private int m_X = 0;
        private int m_Y = 0;

        /// <summary>
        /// The port mappings are shared and therefore stored in FPGA
        /// </summary>
        public int InPortOutPortMappingHashCode { get; set; }

        protected readonly String m_name;
        protected readonly String m_type;
        protected readonly Tile m_containingTile;
        protected Dictionary<String, String> m_attributes = new Dictionary<String, String>();

        //protected Dictionary<String, List<String>> m_possibleSettings = null; //new Dictionary<String, List<String>>();
    }
}
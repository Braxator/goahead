using System;
using System.Runtime.Serialization;

namespace GoAhead.FPGA
{
    [Serializable, DataContract]
    public class Port
    {
        public Port(String name)
        {
            this.m_nameKey = FPGA.Instance.IdentifierListLookup.GetKey(name);
        }

        public override String ToString()
        {
            return this.Name;
        }

        public String Name
        {
            get { return FPGA.Instance.IdentifierListLookup.GetIdentifier(this.m_nameKey); }
        }

        public uint NameKey
        {
            get { return this.m_nameKey; }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Port))
            {
                return false;
            }

            Port other = (Port)obj;

            return this.m_nameKey == other.m_nameKey;
        }

        public override int GetHashCode()
        {
            return (int) this.m_nameKey;
        }

        [DataMember]
        private readonly uint m_nameKey;

        //private readonly String m_name;
    }
}
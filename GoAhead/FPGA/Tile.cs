using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace GoAhead.FPGA
{
    [Serializable]
    public class RawTile
    {
        public RawTile(Tile tile)
        {
            this.TileKeyX = tile.TileKey.X;
            this.TileKeyY = tile.TileKey.Y;
            this.TileLocationString = tile.Location;
            this.SwitchMatrixHashCode = tile.SwitchMatrixHashCode;
            this.WireListHashCode = tile.WireListHashCode;
            this.ClockRegion = tile.ClockRegion;
            foreach (Slice slice in tile.Slices)
            {
                RawSlice rawSlice = new RawSlice(slice);
                this.RawSlices.Add(rawSlice);
            }

            foreach (String excludedPort in tile.GetAllBlockedPorts(Tile.BlockReason.ExcludedFromBlocking))
            {
                this.PortsToExcludeFromBlocking.Add(excludedPort);
            }
        }

        public readonly int TileKeyX;
        public readonly int TileKeyY;

        public readonly String TileLocationString;
        public readonly int SwitchMatrixHashCode;
        public readonly int WireListHashCode;

        public readonly String ClockRegion;

        public readonly List<RawSlice> RawSlices = new List<RawSlice>();
        public readonly List<String> PortsToExcludeFromBlocking = new List<String>();
    }

    [Serializable]
    public class Tile
    {
        public enum BlockReason {
            Blocked = 0,
            ExcludedFromBlocking = 1,
            OccupiedByMacro = 2,
            UsedByNorthernTunnel = 3,
            UsedByEasternTunnel = 4,
            UsedBySouthernTunnel = 5,
            UsedByWesternTunnel = 6
        };

        public Tile(RawTile rawTile)
        {
            this.m_key = new TileKey(rawTile.TileKeyX, rawTile.TileKeyY);
            this.m_location = rawTile.TileLocationString;
            this.SwitchMatrixHashCode = rawTile.SwitchMatrixHashCode;
            this.WireListHashCode = rawTile.WireListHashCode;
            // vivado
            this.m_clockRegion = rawTile.ClockRegion != null ? rawTile.ClockRegion : "unknown";

            // location in of form LEFTPART_X\d+Y\d+
            // and left part may contain X. extract x and y digits from tight part
            if (Tile.m_tileMatch.IsMatch(this.m_location))
            {
                String[] atoms = this.m_location.Split('X', 'Y');
                this.m_locationX = Int32.Parse(atoms[atoms.Length - 2]);
                this.m_locationY = Int32.Parse(atoms[atoms.Length - 1]);
            }
            else if (Tile.m_tileMatchS3.IsMatch(this.m_location)) // Spartan 3
            {
                String[] atoms = this.m_location.Split('R', 'C');
                this.m_locationX = Int32.Parse(atoms[atoms.Length - 1]); // reverse!
                this.m_locationY = Int32.Parse(atoms[atoms.Length - 2]);
            }

            foreach (String portName in rawTile.PortsToExcludeFromBlocking)
            {
                // restore s6 bugfix
                this.BlockPort(portName, Tile.BlockReason.ExcludedFromBlocking);
            }
        }

        public Tile(TileKey key, String location)
        {
            this.m_key = key;
            this.m_location = location;

            // location in of form LEFTPART_X\d+Y\d+
            // and left part may contain X. extract x and y digits from tight part
            if (Tile.m_tileMatch.IsMatch(this.m_location))
            {
                String[] atoms = this.m_location.Split('X', 'Y');
                this.m_locationX = (short)Int32.Parse(atoms[atoms.Length - 2]);
                this.m_locationY = (short)Int32.Parse(atoms[atoms.Length - 1]);
            }
            else if (Tile.m_tileMatchS3.IsMatch(this.m_location)) // Spartan 3
            {
                String[] atoms = this.m_location.Split('R', 'C');
                this.m_locationX = (short)Int32.Parse(atoms[atoms.Length - 1]);  // reverse!
                this.m_locationY = (short)Int32.Parse(atoms[atoms.Length - 2]);
            }
        }

        public override String ToString()
        {
            return this.m_key.ToString() + ":" + this.Location;
        }

        public TileKey TileKey
        {
            get { return m_key; }
        }

        public List<Slice> Slices
        {
            get
            {
                if (this.m_slices == null)
                {
                    this.m_slices = new List<Slice>();
                }
                return m_slices;
            }
        }

        public List<Tile> Subtiles
        {
            get
            {
                if (this.m_subTiles == null)
                {
                    this.m_subTiles = new List<Tile>();
                }
                return m_subTiles;
            }
        }

        public String Location
        {
            get { return m_location; }
        }

        public int LocationX
        {
            get { return m_locationX; }
        }

        public int LocationY
        {
            get { return m_locationY; }
        }

        public void Add(Tile subtile)
        {
            this.Subtiles.Add(subtile);
        }

        public void BlockPort(String portName, BlockReason reason)
        {
            this.BlockPort(new Port(portName), reason);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="p"></param>
        /// <param name="blockedByMacro">True: this port was blocked because an instantiated macro now occupies this port. False anything else, blocked by a net, excluded from future blocking, blocked by a macro</param>
        public void BlockPort(Port p, BlockReason reason)
        {
            if (this.m_blockedPorts == null)
            {
                this.m_blockedPorts = new Dictionary<uint, BlockReason>();
            }

            if (this.m_blockedPortsWithName == null)
            {
                this.m_blockedPortsWithName = new Dictionary<String, BlockReason>();
            }

            if (!this.m_blockedPorts.ContainsKey(p.NameKey))
            {
                this.m_blockedPorts.Add(p.NameKey, reason);
            }

            if (!this.m_blockedPortsWithName.ContainsKey(p.Name))
            {
                this.m_blockedPortsWithName.Add(p.Name, reason);
            }
            // notify FPGA about blocked port
            //FPGAEventManager.Instance.PortBlockingChanged(this, p);
        }

        public void UnblockPort(Port p)
        {
            if (this.m_blockedPorts != null)
            {
                this.m_blockedPorts.Remove(p.NameKey);
            }
            // notify FPGA about blocked port
            //FPGAEventManager.Instance.PortBlockingChanged(this, p);
        }

        public void UnblockPort(String portName)
        {
            if (this.m_blockedPorts != null)
            {
                // intern key
                Port p = new Port(portName);
                this.m_blockedPorts.Remove(p.NameKey);
            }
        }

        public bool IsPortBlocked(Port port)
        {
            if (this.m_blockedPorts == null)
            {
                return false;
            }

            return this.m_blockedPorts.ContainsKey(port.NameKey);
        }

        public bool IsPortBlocked(String portName)
        {
            if (this.m_blockedPorts == null)
            {
                return false;
            }

            return this.IsPortBlocked(new Port(portName));
        }

        public bool IsPortBlocked(Port port, BlockReason reason)
        {
            if (this.m_blockedPorts == null)
            {
                return false;
            }

            if (!this.m_blockedPorts.ContainsKey(port.NameKey))
            {
                return false;
            }

            return this.m_blockedPorts[port.NameKey].Equals(reason);
        }

        public bool IsPortBlocked(String portName, BlockReason reason)
        {
            if (this.m_blockedPorts == null)
            {
                return false;
            }
            // intern key
            Port p = new Port(portName);
            return this.IsPortBlocked(p, reason);
            //return this.m_blockedPorts.ContainsKey(p.NameKey);
        }

        public void UnblockAllPorts()
        {
            if (this.m_blockedPorts != null)
            {
                this.m_blockedPorts.Clear();
            }
        }

        public bool HasUsedSlice
        {
            get
            {
                return this.Slices.Count(s => !s.Usage.Equals(FPGATypes.SliceUsage.Free)) > 0;
            }
        }

        public bool HasBlockedPorts
        {
            get { return (this.m_blockedPorts == null ? false : this.m_blockedPorts.Count != 0); }
        }

        public int AllBlockedPortsHash
        {
            get
            {
                StringBuilder buffer = new StringBuilder();
                if (this.m_blockedPorts != null)
                {
                    foreach (KeyValuePair<uint, BlockReason> tupel in this.m_blockedPorts.OrderBy(t => t.Key))
                    {
                        String portName = FPGA.Instance.IdentifierListLookup.GetIdentifier(tupel.Key);
                        buffer.AppendLine(portName);
                    }
                }
                return buffer.ToString().GetHashCode();
            }
        }

        public IEnumerable<String> GetAllBlockedPorts()
        {
            if (this.m_blockedPorts != null)
            {
                foreach (KeyValuePair<uint, BlockReason> tupel in this.m_blockedPorts.OrderBy(t => t.Key))
                {
                    String portName = FPGA.Instance.IdentifierListLookup.GetIdentifier(tupel.Key);
                    yield return portName;
                }
            }
        }

        public IEnumerable<String> GetAllBlockedPorts(BlockReason reason)
        {
            if (this.m_blockedPorts != null)
            {
                foreach (KeyValuePair<uint, BlockReason> tupel in this.m_blockedPorts.Where(t => t.Value.Equals(reason)))
                {
                    String portName = FPGA.Instance.IdentifierListLookup.GetIdentifier(tupel.Key);
                    yield return portName;
                }
            }
        }

        public IEnumerable<Tuple<String, BlockReason>> GetAllBlockedPortsWithBlockReason()
        {
            if (this.m_blockedPorts != null)
            {
                foreach (KeyValuePair<uint, BlockReason> blockedPort in this.m_blockedPorts)
                {
                    String portName = FPGA.Instance.IdentifierListLookup.GetIdentifier(blockedPort.Key);
                    BlockReason blockReason = blockedPort.Value;

                    yield return new Tuple<String, BlockReason>(portName, blockReason);
                }
            }
        }

        public void Reset()
        {
            if (this.m_slices != null)
            {
                foreach (Slice slice in this.m_slices)
                {
                    slice.Reset();
                }
            }
            if (this.m_blockedPorts != null)
            {
                foreach (KeyValuePair<uint, BlockReason> item in this.m_blockedPorts.Where(i => i.Value != BlockReason.ExcludedFromBlocking).ToList())
                {
                    this.m_blockedPorts.Remove(item.Key);
                }
                //this.m_blockedPorts.Clear();
            }
        }

        public bool HasSlice(String sliceName)
        {
            for (int i = 0; i < this.Slices.Count; i++)
            {
                if (this.m_slices[i].SliceName.Equals(sliceName))
                {
                    return true;
                }
            }
            return false;
        }

        public Slice GetSliceByName(String sliceName)
        {
            return this.Slices[(int)this.GetSliceNumberByName(sliceName)];
        }

        public int GetSliceNumberByName(String sliceName)
        {
            for (int i = 0; i < this.Slices.Count; i++)
            {
                if (this.Slices[(int)i].SliceName.Equals(sliceName))
                {
                    return i;
                }
            }
            throw new ArgumentException("No slice named " + sliceName + " found on tile " + this.Location);
        }

        public int GetSliceNumberByPortName(Port p)
        {
            for (int i = 0; i < this.Slices.Count; i++)
            {
                if (this.Slices[i].PortMapping.Contains(p))
                {
                    return i;
                }
            }
            return -1;
        }

        public bool IsSlicePort(String portName)
        {
            return this.IsSliceInPort(portName) || this.IsSliceOutPort(portName);
        }

        /// <summary>
        /// Returns wether on of the slice provides the given port as an input
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public bool IsSliceInPort(Port p)
        {
            foreach (Slice s in this.Slices)
            {
                if (FPGA.Instance.GetInPortOutPortMapping(s.InPortOutPortMappingHashCode).IsSliceInPort(p))
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsSliceInPort(String portName)
        {
            return this.IsSliceInPort(new Port(portName));
        }

        public bool IsSliceOutPort(Port p)
        {
            foreach (Slice s in this.Slices)
            {
                if (FPGA.Instance.GetInPortOutPortMapping(s.InPortOutPortMappingHashCode).IsSliceOutPort(p))
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsSliceOutPort(String portName)
        {
            return this.IsSliceOutPort(new Port(portName));
        }

        public SwitchMatrix SwitchMatrix
        {
            get { return FPGA.Instance.GetSwitchMatrix(this.SwitchMatrixHashCode); }
        }

        public WireList WireList
        {
            get { return FPGA.Instance.GetWireList(this.WireListHashCode); }
        }

        public String ClockRegion
        {
            get { return this.m_clockRegion; }
            set { this.m_clockRegion = value; }
        }

        public int SwitchMatrixHashCode { get; set; }

        public int WireListHashCode
        {
            get { return this.m_wireListHashCode; }
            set { this.m_wireListHashCode = value; }
        }

        private List<Tile> m_subTiles = null;

        private List<Slice> m_slices = null;

        /// <summary>
        /// Contains all blocked ports
        /// True: this port was blocked becuase a macro has been instantiated
        /// False: this port was blocked because of anything else (net, blocker, exclude from further blocking, ...)
        /// </summary>
        //private Dictionary<String, bool> m_blockedPorts = null;
        private Dictionary<uint, BlockReason> m_blockedPorts = null;


        private Dictionary<String, BlockReason> m_blockedPortsWithName = null;

        private int m_wireListHashCode = -1;
        private readonly TileKey m_key;
        private readonly String m_location;
        private readonly int m_locationX;
        private readonly int m_locationY;
        private String m_clockRegion;

        private static Regex m_tileMatch = new Regex(@"X\d+Y\d+", RegexOptions.Compiled);
        private static Regex m_tileMatchS3 = new Regex(@"^R\d+C\d+", RegexOptions.Compiled);
    }
}
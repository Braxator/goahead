﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using GoAhead.FPGA;
using GoAhead.Interfaces;

namespace GoAhead.Settings
{
    public class ColorSettings : IResetable
    {
        private ColorSettings()
        {
            this.SelectionColor = Color.Green;
            this.MacroColor = Color.Yellow;
            this.BlockedPortsColor = Color.Red;

            Color userSelIncr = System.Drawing.Color.FromArgb(230, 0, 0);
            this.UserSelectionIncrement = userSelIncr;

            Color selIncr = System.Drawing.Color.FromArgb(150, 150, 150);
            this.SelectionIncrement = selIncr;

            Color blockedPorts = System.Drawing.Color.FromArgb(200, 200, 200);
            this.BlockedPortsColor = blockedPorts;

            Commands.Reset.ObjectsToReset.Add(this);
        }

        public void Reset()
        {
            this.m_bufferedColors.Clear();
        }

        public static ColorSettings Instance = new ColorSettings();

        public Color GetColor(Tile tile)
        {
            if (tile.HasBlockedPorts)
            {
                return this.BlockedPortsColor;
            }

            if (!this.ColorBufferContains(tile))
            {
                bool colorFound = false;
                foreach (String str in this.m_colorSettings.Keys)
                {
                    if (Regex.IsMatch(tile.Location, str))
                    {
                        this.Add(tile, this.m_colorSettings[str]);
                        colorFound = true;
                        break;
                    }
                }

                // no match, set default color
                if (!colorFound)
                {
                    this.Add(tile, Color.DarkGreen);
                }
            }
            else
            {
            }

            return this.m_bufferedColors[tile.TileKey.X][tile.TileKey.Y];
        }

        private bool ColorBufferContains(Tile t)
        {
            if (!this.m_bufferedColors.ContainsKey(t.TileKey.X))
            {
                return false;
            }

            return this.m_bufferedColors[t.TileKey.X].ContainsKey(t.TileKey.Y);
        }

        private void Add(Tile t, Color c)
        {
            if (!this.m_bufferedColors.ContainsKey(t.TileKey.X))
            {
                this.m_bufferedColors.Add(t.TileKey.X, new Dictionary<int, Color>());
            }
            this.m_bufferedColors[t.TileKey.X].Add(t.TileKey.Y, c);
        }

        public IEnumerable<String> GetTileRegexps()
        {
            return this.m_colorSettings.Keys;
        }

        public void SetColor(String tileRegexp, String colorName)
        {
            this.m_colorSettings[tileRegexp] = Color.FromName(colorName);
            this.m_bufferedColors.Clear();
        }

        public Color SelectionColor { get; set; }
        public Color MacroColor { get; set; }
        public Color BlockedPortsColor { get; set; }
        public Color SelectionIncrement { get; set; }
        public Color UserSelectionIncrement { get; set; }

        private Dictionary<String, Color> m_colorSettings = new Dictionary<String, Color>();

        private Dictionary<int, Dictionary<int, Color>> m_bufferedColors = new Dictionary<int, Dictionary<int, Color>>();
    }
}
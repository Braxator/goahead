﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace GoAhead.Settings
{
    [Serializable]
    public class GUISettings
    {
        public void Open(Form form)
        {
            if (this.m_sizes.ContainsKey(form.Name))
            {
                form.StartPosition = FormStartPosition.Manual;
                form.Size = this.m_sizes[form.Name];
                if (this.m_location[form.Name].X > 0 && this.m_location[form.Name].Y > 0)
                {
                    form.Location = this.m_location[form.Name];
                }
            }
        }

        public void Close(Form form)
        {
            if (!this.m_sizes.ContainsKey(form.Name))
            {
                this.m_sizes.Add(form.Name, form.Size);
                this.m_location.Add(form.Name, form.Location);
            }
            else
            {
                this.m_sizes[form.Name] = form.Size;
                this.m_location[form.Name] = form.Location;
            }
        }

        private Dictionary<String, Size> m_sizes = new Dictionary<string, Size>();
        private Dictionary<String, Point> m_location = new Dictionary<String, Point>();
    }
}
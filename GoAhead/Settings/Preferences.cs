﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GoAhead.Settings
{
    [Serializable]
    public class StoredPreferences
    {
        private StoredPreferences()
        {
            this.ExecuteExpandSelection = true;
            this.PrintSelectionResourceInfo = true;
        }

        /// <summary>
        /// Singelton
        /// </summary>
        public static StoredPreferences Instance = new StoredPreferences();

        public IOBarSettings IOBarSettings
        {
            get
            {
                if (this.m_IOBarSettings == null)
                {
                    this.m_IOBarSettings = new IOBarSettings();
                }
                return this.m_IOBarSettings;
            }
        }

        public DialogSettings TextBoxSettings
        {
            get
            {
                if (this.m_textBoxSetting == null)
                {
                    this.m_textBoxSetting = new DialogSettings();
                }
                return this.m_textBoxSetting;
            }
        }

        public DialogSettings FileDialogSettings
        {
            get
            {
                if (this.m_fileDialogSetting == null)
                {
                    this.m_fileDialogSetting = new DialogSettings();
                }
                return this.m_fileDialogSetting;
            }
        }

        public GUISettings GUISettings
        {
            get
            {
                if (this.m_guiSetting == null)
                {
                    this.m_guiSetting = new GUISettings();
                }
                return this.m_guiSetting;
            }
        }

        private IOBarSettings m_IOBarSettings = new IOBarSettings();
        private DialogSettings m_fileDialogSetting = new DialogSettings();
        private DialogSettings m_textBoxSetting = new DialogSettings();
        private GUISettings m_guiSetting = new GUISettings();

        // TODO move to subclass
        public bool XDL_RunFEScript { get; set; }

        public bool XDL_IncludePortStatements { get; set; }

        // TODO move to subclass
        public bool HighLightClockRegions { get; set; }

        public bool HighLightPlacedMacros { get; set; }
        public bool HighLightPossibleMacroPlacements { get; set; }
        public bool HighLightRAMS { get; set; }
        public bool HighLightSelection { get; set; }

        public bool ExecuteExpandSelection { get; set; }
        public bool ShowToolTips { get; set; }
        public bool PrintSelectionResourceInfo { get; set; }

        /// <summary>
        /// Ranges between 0 and 100
        /// </summary>
        public double ConsoleGUIShare
        {
            get { return this.m_consoleGUIShare; }
            set { this.m_consoleGUIShare = value; }
        }

        private double m_consoleGUIShare = 20;

        /// <summary>
        /// Ranges between 0 and 100
        /// </summary>
        public float RectangleWidth
        {
            get { return this.m_rectangleWidth; }
            set { this.m_rectangleWidth = value; }
        }

        private float m_rectangleWidth = 1;

        /// <summary>
        /// Return the path to application data
        /// </summary>
        /// <returns></returns>
        public static String GetApplicationDataPath()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + Path.DirectorySeparatorChar + "GoAhead" + Path.DirectorySeparatorChar;
        }

        /// <summary>
        /// Return the full path including the filename to the preferences.bin files in which we store our user settings
        /// </summary>
        /// <returns></returns>
        public static String GetPreferenceFileName()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + Path.DirectorySeparatorChar + "GoAhead" + Path.DirectorySeparatorChar + "preferences.bin";
        }

        /// <summary>
        /// Return the full path including to the directory in which GoAhead searches for detailed Command documentation
        /// </summary>
        /// <returns></returns>
        public static String GetCommandDocDirectory()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + Path.DirectorySeparatorChar + "GoAhead" + Path.DirectorySeparatorChar + "CmdDoc" + Path.DirectorySeparatorChar;
        }

        public static void LoadPrefernces()
        {
            String prefFile = StoredPreferences.GetPreferenceFileName();

            if (!File.Exists(prefFile))
            {
                //load default values
                StoredPreferences.Instance = new StoredPreferences();
            }
            else
            {
                //load setting from file
                Stream stream = File.OpenRead(prefFile);

                BinaryFormatter formatter = new BinaryFormatter();

                try
                {
                    StoredPreferences.Instance = (StoredPreferences)formatter.Deserialize(stream);
                }
                catch (Exception error)
                {
                    Console.WriteLine("Could not open " + prefFile + ": " + error);
                    StoredPreferences.Instance = new StoredPreferences();
                    stream.Close();
                    File.Delete(prefFile);
                    return;
                }
                stream.Close();
            }
        }

        public static void SavePrefernces()
        {
            // create directory if it does not exist
            String appDataPath = StoredPreferences.GetApplicationDataPath();
            if (!Directory.Exists(appDataPath))
            {
                Directory.CreateDirectory(appDataPath);
            }

            String prefFile = StoredPreferences.GetPreferenceFileName();

            //save config
            Stream stream = File.Open(prefFile, FileMode.OpenOrCreate);

            BinaryFormatter formatter = new BinaryFormatter();

            try
            {
                formatter.Serialize(stream, StoredPreferences.Instance);
            }
            catch (Exception error)
            {
                Console.WriteLine("Could not write to " + prefFile + ": " + error.Message);
            }
            stream.Close();
        }
    }
}
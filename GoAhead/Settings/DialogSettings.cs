﻿using System;
using System.Collections.Generic;

namespace GoAhead.Settings
{
    [Serializable]
    public class DialogSettings
    {
        public bool HasSetting(String caller)
        {
            return this.m_settings.ContainsKey(caller);
        }

        public String GetSetting(String caller)
        {
            return this.m_settings[caller];
        }

        public void AddOrUpdateSetting(String caller, String path)
        {
            this.m_settings[caller] = path;
        }

        private Dictionary<String, String> m_settings = new Dictionary<String, String>();
    }
}